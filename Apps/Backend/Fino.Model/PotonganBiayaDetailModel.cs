﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Fino.Lib.Core;

namespace Fino.Model
{
    public class PotonganBiayaDetailModel: BaseModel
    {
        public static readonly string NilaiPropertyName = "Nilai";
        public static readonly string IsPersenPropertyName = "IsPersen";
        public static readonly string MulaiTanggalPropertyName = "Mulai_Tanggal";
        public static readonly string HinggaTanggalPropertyName = "Hingga_Tanggal";

        public int Potongan_Id { get; set; }
        public int Biaya_Id { get; set; }
        public int Siswa_Id { get; set; }

        double _Nilai;
        [Display(Name = "Nilai")]
        [Required]
        public double Nilai
        {
            get
            {
                return _Nilai;
            }
            set
            {
                _Nilai = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NilaiPropertyName));
                ValidateProperty(value, NilaiPropertyName);
            }
        }

        bool _IsPersen;
        [Display(Name = "Persen")]
        [Required]
        public bool IsPersen
        {
            get
            {
                return _IsPersen;
            }
            set
            {
                _IsPersen = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(IsPersenPropertyName));
                ValidateProperty(value, IsPersenPropertyName);
            }
        }

        DateTime _Mulai_Tanggal;
        [Display(Name = "Mulai")]
        [Required]
        public DateTime Mulai_Tanggal
        {
            get
            {
                return _Mulai_Tanggal;
            }
            set
            {
                _Mulai_Tanggal = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(MulaiTanggalPropertyName));
                ValidateProperty(value, MulaiTanggalPropertyName);
            }
        }

        DateTime _Hingga_Tanggal;
        [Display(Name = "Hingga")]
        [Required]
        public DateTime Hingga_Tanggal
        {
            get
            {
                return _Hingga_Tanggal;
            }
            set
            {
                _Hingga_Tanggal = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(HinggaTanggalPropertyName));
                ValidateProperty(value, HinggaTanggalPropertyName);
            }
        }
    }
}
