﻿using Fino.Datalib.Entity;
using Fino.Model;

namespace Fino.Repository
{
    public class PosBiayaPaidCancelRepository : BaseRepository, IPosBiayaPaidCancelRepository
    {
        public void AddPosBiayaPaidCancel(PosBiayaPaidCancelModel entity)
        {
            if (entity != null)
            {
                Context.PosBiayaPaidCancels.Add(new PosBiayaPaidCancel
                    {
                        PosBiaya_Id = entity.PosBiaya_Id,
                        datepaid = entity.datepaid,
                        paynilai = entity.paynilai,
                        status = entity.status,
                        nota_pembayaran_id = entity.nota_pembayaran_id,
                        DateCancel = entity.DateCancel
                    });

                Context.SaveChanges();
            }
        }
    }
}
