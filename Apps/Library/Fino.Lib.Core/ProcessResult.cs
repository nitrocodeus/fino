﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core
{
    public sealed class ProcessResult
    {
        public bool IsSucess { get; set; }
        public Exception ProcessException { get; set; }
        public object DataResult { get; set; }        
    }
}
