﻿using Fino.Cient.Startup;
using Fino.Lib.Core.Settings;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class PenerimaanUmumView : BaseForm, IPenerimaanUmumView
    {
        #region Events

        public event Action SaveButtonClicked;
        public event Action SaveNextButtonClicked;
        public event Action NewDataButtonClicked;
        public event Action CloseButtonClicked;
        public event Action AddNewSPUButtonClicked;
        public event Action ReloadDataEvent;

        #endregion Events

        #region Constructors

        public PenerimaanUmumView()
        {
            InitializeComponent();

            this.btnSave.Click += btnSave_Click;
            this.btnSaveNew.Click += btnSaveNew_Click;
            this.btnNew.Click += btnNew_Click;
            this.btnClose.Click += btnClose_Click;
            this.btnVendorBaru.Click += btnVendorBaru_Click;
        }

        #endregion Constructors

        public PenerimaanUmumModel ViewModel { get; set; }
        public object RefSPUDataSource { get; set; }
        public object RefBiayaDataSource { get; set; }

        #region Public Properties

        public string RefSPU
        {
            get
            {
                return cboSumberPenerimaan.Text;
            }
            set
            {
                cboSumberPenerimaan.SelectedValue = value;
            }
        }

        public string RefBiaya
        {
            get
            {
                return cboJenisPenerimaan.Text;
            }
            set
            {
                cboJenisPenerimaan.SelectedValue = value;
            }
        }

        public string Deskripsi
        {
            get
            {
                return txtDeskripsi.Text;
            }
            set
            {
                txtDeskripsi.Text = value;
            }
        }

        public double Nilai
        {
            get
            {
                return (double)nmNilai.Value;
            }
            set
            {
                nmNilai.Value = (decimal)value;
            }
        }

        #endregion Public Properties

        #region Application Events

        private void btnVendorBaru_Click(object sender, EventArgs e)
        {
            if (AddNewSPUButtonClicked != null)
            {
                AddNewSPUButtonClicked();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (NewDataButtonClicked != null)
            {
                NewDataButtonClicked();
            }
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveNextButtonClicked != null)
            {
                SaveNextButtonClicked();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
            {
                SaveButtonClicked();
            }
        }

        #endregion Application Events

        #region IPayablePaidView

        public void SetBinding()
        {
            try
            {
                txtDeskripsi.DataBindings.Clear();
                txtDeskripsi.DataBindings.Add("Text", ViewModel, PenerimaanUmumModel.DeskripsiPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                cboSumberPenerimaan.DataSource = RefSPUDataSource;
                cboSumberPenerimaan.DisplayMember = "Value";
                cboSumberPenerimaan.ValueMember = "Id";
                cboSumberPenerimaan.DataBindings.Clear();
                cboSumberPenerimaan.DataBindings.Add("SelectedValue", ViewModel, PenerimaanUmumModel.SumberPenerimaanIdPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                //cboVendor.DataBindings.Add("Text", ViewModel, PenerimaanUmumModel.SumberPenerimaanNamaPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                nmNilai.DataBindings.Clear();
                nmNilai.DataBindings.Add("Value", ViewModel, PenerimaanUmumModel.NilaiBiayaPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                
                cboJenisPenerimaan.DataSource = RefBiayaDataSource;
                cboJenisPenerimaan.DisplayMember = "Value";
                cboJenisPenerimaan.ValueMember = "Id";
                cboJenisPenerimaan.DataBindings.Clear();
                cboJenisPenerimaan.DataBindings.Add("SelectedValue", ViewModel, PenerimaanUmumModel.RefBiayaIdPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message, "Error Binding");
            }
        }

        public void ClearView()
        {
            SetBinding();
        }

        public void ChangeEditable(bool enabled)
        {
            foreach (var c in groupBox2.Controls)
            {
                var ctype = c.GetType();
                if (ctype.Equals(typeof(TextBox)))
                {
                    ((TextBox)c).ReadOnly = !enabled;
                }
                if (ctype.Equals(typeof(ComboBox)))
                {
                    ((ComboBox)c).Enabled = enabled;
                }
                if (ctype.Equals(typeof(DateTimePicker)))
                {
                    ((DateTimePicker)c).Enabled = enabled;
                }
                if (ctype.Equals(typeof(NumericUpDown)))
                {
                    ((NumericUpDown)c).Enabled = enabled;
                }
            }

            btnSaveNew.Enabled = enabled;
            btnSave.Enabled = enabled;
        }

        public void ShowErrorMessage(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowInformation(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void ShowCreateNewRefSPUView()
        {
            ViewLocator.Instance.EditRefSPUView.PenerimaanUmumView = this;
            ViewLocator.Instance.EditRefSPUView.Show();
        }

        public void ReloadData()
        {
            if (ReloadDataEvent != null)
            {
                ReloadDataEvent();
            }
        }

        public void CetakNota(CetakPenerimaanUmumModel cetakNotaModel)
        {
            LocalReport report = new LocalReport();
            report.ReportEmbeddedResource = @"Fino.Cient.View.Report.NotaPenerimaanUmum.rdlc";

            ReportParameter[] parameters = new ReportParameter[2];
            parameters[0] = new ReportParameter("Petugas", AppVariable.Instance.GetLoggedOnUserFullName());
            parameters[1] = new ReportParameter("TahunAjaran", AppVariable.Instance.GetTahunAjaranAktif());

            BindingSource bsNotaPenerimaan = new BindingSource();

            bsNotaPenerimaan.DataSource = cetakNotaModel;

            report.SetParameters(parameters);
            report.DataSources.Add(
               new ReportDataSource("NotaPenerimaanUmum", bsNotaPenerimaan));

            ReportPrintingHelper.Instance.Export(report);
            ReportPrintingHelper.Instance.Print();
        }
        #endregion IPayablePaidView
    }
}
