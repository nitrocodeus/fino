﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class RekapitulasiPiutangDanPenerimaanModel
    {
        public string NamaBiaya { get; set; }
        public double Piutang { get; set; }
        public double Penerimaan { get; set; }
    }
}
