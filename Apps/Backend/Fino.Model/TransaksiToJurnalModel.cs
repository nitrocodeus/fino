﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class TransaksiToJurnalModel
    {
        public bool IsSelected { get; set; }
        public int AccountClassificationId { get; set; }
        public int SourceId { get; set; }
        public string TransactionName { get; set; }
        public DateTime TransactionDate { get; set; }
        public Double TransactionValue { get; set; }
        public int AccountingPeriodId { get; set; }
        public int ParamId { get; set; }
    }
}
