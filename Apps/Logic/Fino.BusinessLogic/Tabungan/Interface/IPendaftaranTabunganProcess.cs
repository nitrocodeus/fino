﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public interface IPendaftaranTabunganProcess
    {
        IPendaftaranTabunganRepository PendaftaranTabunganRepo { get; set; }
        IRefSiswaRepository RefSiswaRepo { get; set; }
        ProcessResult BukaRekening(PendaftaranTabunganModel p_Model);
        ProcessResult GetSiswaByNoInduk(string pNoInduk);
    }
}
