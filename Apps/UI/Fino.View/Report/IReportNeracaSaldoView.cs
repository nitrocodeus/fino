﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IReportNeracaSaldoView: IView
    {
        event Action BtnOkClicked;
        event Action BtnTutupClicked;
        object PeriodDataSource { get; set; }
        int PeriodId { get; }
        void ShowRPP(List<NeracaSaldoModel> p_Model);
    }
}
