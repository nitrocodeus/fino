﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Migration.Constants
{
    public static class DataTypeConstants
    {
        public const string BOOL = "bool";
        public const string INTEGER = "int";
        public const string LONG = "long";
        public const string FLOAT = "float";
        public const string DOUBLE = "double";
        public const string DECIMAL = "decimal";
        public const string DATETIME = "datetime";
    }
}
