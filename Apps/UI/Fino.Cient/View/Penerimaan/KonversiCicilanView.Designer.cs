﻿namespace Fino.Cient
{
    partial class KonversiCicilanView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KonversiCicilanView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbSiswa = new System.Windows.Forms.GroupBox();
            this.txtCari = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.rbPerempuan = new System.Windows.Forms.RadioButton();
            this.rbLaki = new System.Windows.Forms.RadioButton();
            this.btnSearchSiswa = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamaSiswa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoInduk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbKonversi = new System.Windows.Forms.GroupBox();
            this.btnTutup = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tabCicilan = new System.Windows.Forms.TabControl();
            this.tbpCicilan = new System.Windows.Forms.TabPage();
            this.gbCicilanHeader = new System.Windows.Forms.GroupBox();
            this.rbNilai = new System.Windows.Forms.RadioButton();
            this.rbTenor = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.nmCicilan = new System.Windows.Forms.NumericUpDown();
            this.nmLama = new System.Windows.Forms.NumericUpDown();
            this.nmNilai = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tbpView = new System.Windows.Forms.TabPage();
            this.dgView = new System.Windows.Forms.DataGridView();
            this.NamaBiaya = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JTempo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NilaiBiaya = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lstBiaya = new System.Windows.Forms.ListBox();
            this.pencarianSiswaControlView1 = new Fino.Cient.Common.PencarianSiswaControlView();
            this.gbSiswa.SuspendLayout();
            this.gbKonversi.SuspendLayout();
            this.tabCicilan.SuspendLayout();
            this.tbpCicilan.SuspendLayout();
            this.gbCicilanHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmCicilan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmLama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmNilai)).BeginInit();
            this.tbpView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).BeginInit();
            this.SuspendLayout();
            // 
            // gbSiswa
            // 
            this.gbSiswa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSiswa.Controls.Add(this.txtCari);
            this.gbSiswa.Controls.Add(this.label8);
            this.gbSiswa.Controls.Add(this.rbPerempuan);
            this.gbSiswa.Controls.Add(this.rbLaki);
            this.gbSiswa.Controls.Add(this.btnSearchSiswa);
            this.gbSiswa.Controls.Add(this.label3);
            this.gbSiswa.Controls.Add(this.txtNamaSiswa);
            this.gbSiswa.Controls.Add(this.label2);
            this.gbSiswa.Controls.Add(this.txtNoInduk);
            this.gbSiswa.Controls.Add(this.label1);
            this.gbSiswa.Location = new System.Drawing.Point(28, 18);
            this.gbSiswa.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.gbSiswa.Name = "gbSiswa";
            this.gbSiswa.Padding = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.gbSiswa.Size = new System.Drawing.Size(865, 178);
            this.gbSiswa.TabIndex = 0;
            this.gbSiswa.TabStop = false;
            this.gbSiswa.Text = "Data Siswa";
            // 
            // txtCari
            // 
            this.txtCari.Location = new System.Drawing.Point(164, 30);
            this.txtCari.Margin = new System.Windows.Forms.Padding(4);
            this.txtCari.MaxLength = 25;
            this.txtCari.Name = "txtCari";
            this.txtCari.Size = new System.Drawing.Size(252, 28);
            this.txtCari.TabIndex = 1;
            this.txtCari.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCari_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 33);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "Cari Siswa:";
            // 
            // rbPerempuan
            // 
            this.rbPerempuan.AutoSize = true;
            this.rbPerempuan.Enabled = false;
            this.rbPerempuan.Location = new System.Drawing.Point(278, 139);
            this.rbPerempuan.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.rbPerempuan.Name = "rbPerempuan";
            this.rbPerempuan.Size = new System.Drawing.Size(127, 24);
            this.rbPerempuan.TabIndex = 7;
            this.rbPerempuan.TabStop = true;
            this.rbPerempuan.Text = "Perempuan";
            this.rbPerempuan.UseVisualStyleBackColor = true;
            // 
            // rbLaki
            // 
            this.rbLaki.AutoSize = true;
            this.rbLaki.Enabled = false;
            this.rbLaki.Location = new System.Drawing.Point(164, 139);
            this.rbLaki.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.rbLaki.Name = "rbLaki";
            this.rbLaki.Size = new System.Drawing.Size(102, 24);
            this.rbLaki.TabIndex = 6;
            this.rbLaki.TabStop = true;
            this.rbLaki.Text = "Laki-laki";
            this.rbLaki.UseVisualStyleBackColor = true;
            // 
            // btnSearchSiswa
            // 
            this.btnSearchSiswa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchSiswa.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchSiswa.Image")));
            this.btnSearchSiswa.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSearchSiswa.Location = new System.Drawing.Point(426, 28);
            this.btnSearchSiswa.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnSearchSiswa.Name = "btnSearchSiswa";
            this.btnSearchSiswa.Size = new System.Drawing.Size(71, 65);
            this.btnSearchSiswa.TabIndex = 2;
            this.btnSearchSiswa.Text = "Cari";
            this.btnSearchSiswa.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSearchSiswa.UseVisualStyleBackColor = true;
            this.btnSearchSiswa.Click += new System.EventHandler(this.btnSearchSiswa_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(68, 139);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Kelamin:";
            // 
            // txtNamaSiswa
            // 
            this.txtNamaSiswa.Location = new System.Drawing.Point(164, 104);
            this.txtNamaSiswa.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtNamaSiswa.MaxLength = 25;
            this.txtNamaSiswa.Name = "txtNamaSiswa";
            this.txtNamaSiswa.ReadOnly = true;
            this.txtNamaSiswa.Size = new System.Drawing.Size(378, 28);
            this.txtNamaSiswa.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 107);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nama Siswa:";
            // 
            // txtNoInduk
            // 
            this.txtNoInduk.Location = new System.Drawing.Point(164, 68);
            this.txtNoInduk.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.txtNoInduk.MaxLength = 10;
            this.txtNoInduk.Name = "txtNoInduk";
            this.txtNoInduk.ReadOnly = true;
            this.txtNoInduk.Size = new System.Drawing.Size(198, 28);
            this.txtNoInduk.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 71);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "No Induk:";
            // 
            // gbKonversi
            // 
            this.gbKonversi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbKonversi.Controls.Add(this.btnTutup);
            this.gbKonversi.Controls.Add(this.btnSave);
            this.gbKonversi.Controls.Add(this.tabCicilan);
            this.gbKonversi.Controls.Add(this.lstBiaya);
            this.gbKonversi.Location = new System.Drawing.Point(26, 205);
            this.gbKonversi.Margin = new System.Windows.Forms.Padding(4);
            this.gbKonversi.Name = "gbKonversi";
            this.gbKonversi.Padding = new System.Windows.Forms.Padding(4);
            this.gbKonversi.Size = new System.Drawing.Size(867, 368);
            this.gbKonversi.TabIndex = 1;
            this.gbKonversi.TabStop = false;
            this.gbKonversi.Text = "Konversi Cicilan";
            // 
            // btnTutup
            // 
            this.btnTutup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(708, 311);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(138, 35);
            this.btnTutup.TabIndex = 3;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            this.btnTutup.Click += new System.EventHandler(this.btnTutup_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(558, 311);
            this.btnSave.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(138, 35);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Simpan";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tabCicilan
            // 
            this.tabCicilan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabCicilan.Controls.Add(this.tbpCicilan);
            this.tabCicilan.Controls.Add(this.tbpView);
            this.tabCicilan.Location = new System.Drawing.Point(295, 29);
            this.tabCicilan.Name = "tabCicilan";
            this.tabCicilan.SelectedIndex = 0;
            this.tabCicilan.Size = new System.Drawing.Size(552, 251);
            this.tabCicilan.TabIndex = 1;
            this.tabCicilan.SelectedIndexChanged += new System.EventHandler(this.tabCicilan_SelectedIndexChanged);
            // 
            // tbpCicilan
            // 
            this.tbpCicilan.Controls.Add(this.gbCicilanHeader);
            this.tbpCicilan.Controls.Add(this.label7);
            this.tbpCicilan.Controls.Add(this.btnAdd);
            this.tbpCicilan.Location = new System.Drawing.Point(4, 29);
            this.tbpCicilan.Name = "tbpCicilan";
            this.tbpCicilan.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCicilan.Size = new System.Drawing.Size(544, 218);
            this.tbpCicilan.TabIndex = 0;
            this.tbpCicilan.Text = "Cicilan";
            this.tbpCicilan.UseVisualStyleBackColor = true;
            // 
            // gbCicilanHeader
            // 
            this.gbCicilanHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCicilanHeader.Controls.Add(this.rbNilai);
            this.gbCicilanHeader.Controls.Add(this.rbTenor);
            this.gbCicilanHeader.Controls.Add(this.label6);
            this.gbCicilanHeader.Controls.Add(this.label4);
            this.gbCicilanHeader.Controls.Add(this.dtpStart);
            this.gbCicilanHeader.Controls.Add(this.label5);
            this.gbCicilanHeader.Controls.Add(this.nmCicilan);
            this.gbCicilanHeader.Controls.Add(this.nmLama);
            this.gbCicilanHeader.Controls.Add(this.nmNilai);
            this.gbCicilanHeader.Location = new System.Drawing.Point(12, 7);
            this.gbCicilanHeader.Margin = new System.Windows.Forms.Padding(4);
            this.gbCicilanHeader.Name = "gbCicilanHeader";
            this.gbCicilanHeader.Padding = new System.Windows.Forms.Padding(4);
            this.gbCicilanHeader.Size = new System.Drawing.Size(525, 200);
            this.gbCicilanHeader.TabIndex = 1;
            this.gbCicilanHeader.TabStop = false;
            // 
            // rbNilai
            // 
            this.rbNilai.AutoSize = true;
            this.rbNilai.Enabled = false;
            this.rbNilai.Location = new System.Drawing.Point(103, 144);
            this.rbNilai.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.rbNilai.Name = "rbNilai";
            this.rbNilai.Size = new System.Drawing.Size(201, 24);
            this.rbNilai.TabIndex = 0;
            this.rbNilai.TabStop = true;
            this.rbNilai.Text = "Nilai per bulan (Rp)";
            this.rbNilai.UseVisualStyleBackColor = true;
            this.rbNilai.CheckedChanged += new System.EventHandler(this.rbNilai_CheckedChanged);
            // 
            // rbTenor
            // 
            this.rbTenor.AutoSize = true;
            this.rbTenor.Enabled = false;
            this.rbTenor.Location = new System.Drawing.Point(103, 110);
            this.rbTenor.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.rbTenor.Name = "rbTenor";
            this.rbTenor.Size = new System.Drawing.Size(143, 24);
            this.rbTenor.TabIndex = 4;
            this.rbTenor.TabStop = true;
            this.rbTenor.Text = "Lama (tenor)";
            this.rbTenor.UseVisualStyleBackColor = true;
            this.rbTenor.CheckedChanged += new System.EventHandler(this.rbTenor_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(396, 114);
            this.label6.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "bulan";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Total Nilai:";
            // 
            // dtpStart
            // 
            this.dtpStart.CustomFormat = "dd-MMM-yyyy";
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(140, 65);
            this.dtpStart.Margin = new System.Windows.Forms.Padding(4);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(209, 28);
            this.dtpStart.TabIndex = 3;
            this.dtpStart.Validated += new System.EventHandler(this.dtpStart_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(66, 71);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "Mulai:";
            // 
            // nmCicilan
            // 
            this.nmCicilan.Location = new System.Drawing.Point(312, 145);
            this.nmCicilan.Margin = new System.Windows.Forms.Padding(4);
            this.nmCicilan.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nmCicilan.Name = "nmCicilan";
            this.nmCicilan.Size = new System.Drawing.Size(196, 28);
            this.nmCicilan.TabIndex = 8;
            this.nmCicilan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmCicilan.ThousandsSeparator = true;
            this.nmCicilan.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.nmCicilan.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nmCicilan.Validated += new System.EventHandler(this.nmCicilan_Validated);
            // 
            // nmLama
            // 
            this.nmLama.Location = new System.Drawing.Point(312, 110);
            this.nmLama.Margin = new System.Windows.Forms.Padding(4);
            this.nmLama.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nmLama.Name = "nmLama";
            this.nmLama.Size = new System.Drawing.Size(72, 28);
            this.nmLama.TabIndex = 5;
            this.nmLama.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmLama.ThousandsSeparator = true;
            this.nmLama.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.nmLama.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nmLama.Validated += new System.EventHandler(this.nmLama_Validated);
            // 
            // nmNilai
            // 
            this.nmNilai.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nmNilai.Location = new System.Drawing.Point(140, 29);
            this.nmNilai.Margin = new System.Windows.Forms.Padding(4);
            this.nmNilai.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nmNilai.Name = "nmNilai";
            this.nmNilai.ReadOnly = true;
            this.nmNilai.Size = new System.Drawing.Size(257, 28);
            this.nmNilai.TabIndex = 1;
            this.nmNilai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmNilai.ThousandsSeparator = true;
            this.nmNilai.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(168, 82);
            this.label7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 20);
            this.label7.TabIndex = 2;
            this.label7.Text = "Tambah";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(86, 60);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(66, 65);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tbpView
            // 
            this.tbpView.Controls.Add(this.dgView);
            this.tbpView.Location = new System.Drawing.Point(4, 29);
            this.tbpView.Name = "tbpView";
            this.tbpView.Padding = new System.Windows.Forms.Padding(3);
            this.tbpView.Size = new System.Drawing.Size(544, 218);
            this.tbpView.TabIndex = 1;
            this.tbpView.Text = "Rincian";
            this.tbpView.UseVisualStyleBackColor = true;
            // 
            // dgView
            // 
            this.dgView.AllowUserToAddRows = false;
            this.dgView.AllowUserToDeleteRows = false;
            this.dgView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgView.BackgroundColor = System.Drawing.Color.White;
            this.dgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NamaBiaya,
            this.JTempo,
            this.NilaiBiaya});
            this.dgView.Location = new System.Drawing.Point(6, 6);
            this.dgView.MultiSelect = false;
            this.dgView.Name = "dgView";
            this.dgView.ReadOnly = true;
            this.dgView.RowTemplate.Height = 24;
            this.dgView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgView.Size = new System.Drawing.Size(532, 206);
            this.dgView.TabIndex = 0;
            // 
            // NamaBiaya
            // 
            this.NamaBiaya.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NamaBiaya.DataPropertyName = "NamaBiaya";
            dataGridViewCellStyle1.NullValue = null;
            this.NamaBiaya.DefaultCellStyle = dataGridViewCellStyle1;
            this.NamaBiaya.HeaderText = "Nama Kewajiban";
            this.NamaBiaya.Name = "NamaBiaya";
            this.NamaBiaya.ReadOnly = true;
            // 
            // JTempo
            // 
            this.JTempo.DataPropertyName = "JTempo";
            dataGridViewCellStyle2.Format = "dd-MMM-yy";
            dataGridViewCellStyle2.NullValue = null;
            this.JTempo.DefaultCellStyle = dataGridViewCellStyle2;
            this.JTempo.HeaderText = "Jatuh Tempo";
            this.JTempo.Name = "JTempo";
            this.JTempo.ReadOnly = true;
            this.JTempo.Width = 150;
            // 
            // NilaiBiaya
            // 
            this.NilaiBiaya.DataPropertyName = "NilaiBiaya";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.NilaiBiaya.DefaultCellStyle = dataGridViewCellStyle3;
            this.NilaiBiaya.HeaderText = "Nilai Per Bulan";
            this.NilaiBiaya.Name = "NilaiBiaya";
            this.NilaiBiaya.ReadOnly = true;
            this.NilaiBiaya.Width = 150;
            // 
            // lstBiaya
            // 
            this.lstBiaya.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lstBiaya.FormattingEnabled = true;
            this.lstBiaya.ItemHeight = 20;
            this.lstBiaya.Location = new System.Drawing.Point(20, 29);
            this.lstBiaya.Margin = new System.Windows.Forms.Padding(4);
            this.lstBiaya.Name = "lstBiaya";
            this.lstBiaya.Size = new System.Drawing.Size(268, 244);
            this.lstBiaya.TabIndex = 0;
            this.lstBiaya.SelectedIndexChanged += new System.EventHandler(this.lstBiaya_SelectedIndexChanged);
            // 
            // pencarianSiswaControlView1
            // 
            this.pencarianSiswaControlView1.Font = new System.Drawing.Font("Verdana", 10F);
            this.pencarianSiswaControlView1.Location = new System.Drawing.Point(192, 77);
            this.pencarianSiswaControlView1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.pencarianSiswaControlView1.Name = "pencarianSiswaControlView1";
            this.pencarianSiswaControlView1.Size = new System.Drawing.Size(452, 412);
            this.pencarianSiswaControlView1.StringSiswa = null;
            this.pencarianSiswaControlView1.TabIndex = 0;
            this.pencarianSiswaControlView1.Visible = false;
            // 
            // KonversiCicilanView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 591);
            this.Controls.Add(this.pencarianSiswaControlView1);
            this.Controls.Add(this.gbKonversi);
            this.Controls.Add(this.gbSiswa);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "KonversiCicilanView";
            this.Text = "Konversi Cicilan";
            this.gbSiswa.ResumeLayout(false);
            this.gbSiswa.PerformLayout();
            this.gbKonversi.ResumeLayout(false);
            this.tabCicilan.ResumeLayout(false);
            this.tbpCicilan.ResumeLayout(false);
            this.tbpCicilan.PerformLayout();
            this.gbCicilanHeader.ResumeLayout(false);
            this.gbCicilanHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmCicilan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmLama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmNilai)).EndInit();
            this.tbpView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSiswa;
        private System.Windows.Forms.RadioButton rbPerempuan;
        private System.Windows.Forms.RadioButton rbLaki;
        private System.Windows.Forms.Button btnSearchSiswa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNamaSiswa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNoInduk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbKonversi;
        private System.Windows.Forms.GroupBox gbCicilanHeader;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nmNilai;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox lstBiaya;
        private System.Windows.Forms.TabControl tabCicilan;
        private System.Windows.Forms.TabPage tbpCicilan;
        private System.Windows.Forms.RadioButton rbNilai;
        private System.Windows.Forms.RadioButton rbTenor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nmCicilan;
        private System.Windows.Forms.NumericUpDown nmLama;
        private System.Windows.Forms.TabPage tbpView;
        private System.Windows.Forms.DataGridView dgView;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn NamaBiaya;
        private System.Windows.Forms.DataGridViewTextBoxColumn JTempo;
        private System.Windows.Forms.DataGridViewTextBoxColumn NilaiBiaya;
        private System.Windows.Forms.TextBox txtCari;
        private System.Windows.Forms.Label label8;
        private Common.PencarianSiswaControlView pencarianSiswaControlView1;
    }
}