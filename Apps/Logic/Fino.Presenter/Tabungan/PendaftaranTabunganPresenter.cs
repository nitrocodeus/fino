﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class PendaftaranTabunganPresenter : BasePresenter
    {
        private readonly IPendaftaranTabunganProcess _pendaftaranTabunganProcess;
        private readonly IPendaftaranTabunganView _pendaftaranTabunganView;

        public PendaftaranTabunganPresenter(IPendaftaranTabunganProcess p_Process, IPendaftaranTabunganView p_View) : base(p_View)
        {
            try
            {
                _pendaftaranTabunganProcess = p_Process;
                _pendaftaranTabunganView = p_View;

                _pendaftaranTabunganView.CloseButtonClicked += _pendaftaranTabunganView_CloseButtonClicked;
                _pendaftaranTabunganView.txtSiswaKeyPressed += _view_txtSiswaKeyPressed;
                
                _pendaftaranTabunganView.NewDataButtonClicked += _pendaftaranTabunganView_NewDataButtonClicked;
                _pendaftaranTabunganView.SaveButtonClicked += _pendaftaranTabunganView_SaveButtonClicked;
                _pendaftaranTabunganView.SaveNextButtonClicked +=_pendaftaranTabunganView_SaveNextButtonClicked;
                _pendaftaranTabunganView.SelectingSiswaDone += _view_SelectingSiswaDone;

                AssignNewViewModel();
                _pendaftaranTabunganView.SetBinding();

                _pendaftaranTabunganView.SetSearchMode();

            }
            catch (Exception ex)
            {
                _pendaftaranTabunganView.ShowErrorMessage(ex.Message, AppResource.CAP_PROCESS_ERROR);
            }
        }

        void _pendaftaranTabunganView_CloseButtonClicked()
        {
            _pendaftaranTabunganView.Close();
        }

        void _view_txtSiswaKeyPressed()
        {
            _pendaftaranTabunganView.PencarianSiswaView.Visible = true;
            _pendaftaranTabunganView.PencarianSiswaView.StringSiswa = _pendaftaranTabunganView.TextSearchSiswa;

            _pendaftaranTabunganView.PencarianSiswaView.Cari();
        }

        void _view_SelectingSiswaDone()
        {
            _pendaftaranTabunganView.ViewModel.jkelamin = _pendaftaranTabunganView.PencarianSiswaView.SelectedSiswa.JenisKelamin;
            _pendaftaranTabunganView.ViewModel.siswa_code = _pendaftaranTabunganView.PencarianSiswaView.SelectedSiswa.Code;
            _pendaftaranTabunganView.ViewModel.siswa_nama = _pendaftaranTabunganView.PencarianSiswaView.SelectedSiswa.Nama;
            _pendaftaranTabunganView.ViewModel.siswa_id = _pendaftaranTabunganView.PencarianSiswaView.SelectedSiswa.Id;

            _pendaftaranTabunganView.SetBinding();
            _pendaftaranTabunganView.SetEntryMode();
        }

        private void _pendaftaranTabunganView_NewDataButtonClicked()
        {
            AssignNewViewModel();
            _pendaftaranTabunganView.ClearView();
            _pendaftaranTabunganView.SetSearchMode();
        }

        void p_View_Load(object sender, EventArgs e)
        {
            AssignNewViewModel();
            _pendaftaranTabunganView.ClearView();
            _pendaftaranTabunganView.SetBinding();
            _pendaftaranTabunganView.SetSearchMode();
        }

        private void AssignNewViewModel()
        {
            _pendaftaranTabunganView.ViewModel = new PendaftaranTabunganModel();
        }

        void _pendaftaranTabunganView_SaveNextButtonClicked()
        {
            _pendaftaranTabunganView_SaveButtonClicked();

            AssignNewViewModel();
            _pendaftaranTabunganView.ClearView();
            _pendaftaranTabunganView.SetSearchMode();
        }


        void SaveData()
        {
            var model = _pendaftaranTabunganView.ViewModel;
            ProcessResult procResult = _pendaftaranTabunganProcess.BukaRekening(model);

            if (procResult.ProcessException != null)
            {
                _pendaftaranTabunganView.ShowErrorMessage(procResult.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
            else
            {
                _pendaftaranTabunganView.ViewModel = (PendaftaranTabunganModel)procResult.DataResult;
                _pendaftaranTabunganView.ShowInformation(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.CAP_PROCESS_INFORMATION);
            }
        }

        void _pendaftaranTabunganView_SaveButtonClicked()
        {
            SaveData();
            _pendaftaranTabunganView.SetViewMode();
        }

    }
}
