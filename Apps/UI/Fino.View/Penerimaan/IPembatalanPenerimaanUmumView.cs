﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IPembatalanPenerimaanUmumView : IView
    {
        event Action BtnCariClicked;
        event Action BtnTutupClicked;
        event Action BtnBatalClicked;
        event Action BtnAllClicked;
        event Action BtnResetClicked;

        DateTime DateFrom { get; }
        DateTime DateTo { get; }

        void SetData(List<PembatalanPenerimaanUmumModel> p_ListPenerimaanUmum);
        void RefreshDataGrid();
        void SelectAllItem();
        void UnselectAllItem();
        List<PembatalanPenerimaanUmumModel> GetSelectedPenerimaanUmum();
        List<PembatalanPenerimaanUmumModel> GetGVPenerimaanUmumDataSource();
    }
}
