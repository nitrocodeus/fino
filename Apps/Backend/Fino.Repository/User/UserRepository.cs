﻿using Fino.Datalib.Entity;
using Fino.Model;
using System.Linq;

namespace Fino.Repository
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public SysUser UserLogon(string p_UserName, string p_Password)
        {
            SysUser user = (from a in Context.Users
                            where a.name.ToLower().Equals(p_UserName.ToLower()) &&
                            a.password.ToLower().Equals(p_Password.ToLower())
                            select a).FirstOrDefault();

            if (user != null)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        public bool UpdatePassword(ChangePasswordModel entity)
        {
            SysUser user = UserLogon(entity.Username, entity.OldPassword);

            if (user != null)
            {
                user.password = entity.NewPassword;
                this.Context.SaveChanges();

                return true;
            }

            return false;
        }

        public bool SetNewPassword(RestorePasswordModel entity)
        {
            SysUser existingUser = Context.Users.Where(x => x.name == entity.Username).FirstOrDefault();

            if (existingUser != null)
            {
                existingUser.password = entity.NewPassword;
                Context.SaveChanges();

                return true;
            }

            return false;
        }
    }
}
