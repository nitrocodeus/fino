﻿
namespace Fino.Lib.Core
{
    public enum AccountClassificationEnum
    {
        KAS = 1,
        PIUTANG = 2,
        PENDAPATAN = 3,
        HUTANG = 4,
        CANCELED_PIUTANG = 5,
        BEBAN = 6,
        PENDAPATAN_NONUSAHA = 7,
        CANCELED_PENDAPATAN = 8,
        CANCELED_BEBAN = 9,
        CANCELED_PENDAPATAN_NONUSAHA = 10,
    }
}
