﻿using Fino.Lib.Win32;
using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IPosBiayaView : IView
    {
        event Action CariRaised;
        event Action gvPosBiayaClicked;
        event Action HideControl;
        event Action btnTambahkanClicked;
        event EventHandler<CustomEventArgs<PosBiayaModel>> CellSelectedClicked;

        bool Visible { get; set; }
        int SiswaId { get; set; }
        bool ShowAllBiaya { get; set; }
        bool ByPassGridClick { get; set; }

        List<PosBiayaModel> gvPosBiayaSource { get; set; }
        List<PosBiayaModel> SelectedPosBiaya { get; }        

        void Cari();
        void RaiseHideControl();
        void ShowSelectedColumn(bool p_ShowSelectedColumn);
        void ShowToolbar(bool p_ShowToolbar);
        void ClearSelected();
    }
}
