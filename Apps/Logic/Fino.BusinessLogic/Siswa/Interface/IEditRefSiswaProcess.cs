﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IEditRefSiswaProcess
    {
        IRefSiswaRepository RefSiswaRepo { get; set; }

        ProcessResult UpdateSiswa(EditRefSiswaModel model);
        ProcessResult GetSiswaByCode(string siswa_code);
    }
}
