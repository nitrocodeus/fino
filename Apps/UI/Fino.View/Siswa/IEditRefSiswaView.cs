﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IEditRefSiswaView : IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;

        SiswaExplorerModel SelectedSiswaExplorer { get; }
        ISiswaExplorerView SiswaExplorerView { get; set; }
        EditRefSiswaModel SelectedSiswa { get; }

        void SetBinding();
        void ClearData();
    }
}
