﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class UbahKataSandiPresenter : BasePresenter
    {
        IUbahKataSandiProcess _ubahKataSandiProcess;
        IUbahKataSandiView _ubahKataSandiView;

        public UbahKataSandiPresenter(IUbahKataSandiProcess p_Process, IUbahKataSandiView p_View) : base(p_View)
        {
            this._ubahKataSandiProcess = p_Process;
            this._ubahKataSandiView = p_View;

            this._ubahKataSandiView.Load += _ubahKataSandiView_Load;
            this._ubahKataSandiView.BtnSimpanClicked += _ubahKataSandiView_BtnSimpanClicked;
            this._ubahKataSandiView.BtnTutupClicked += _ubahKataSandiView_BtnTutupClicked;
            this._ubahKataSandiView.BtnBrowseClicked += _ubahKataSandiView_BtnBrowseClicked;
        }

        private void _ubahKataSandiView_Load(object sender, EventArgs e)
        {
            _ubahKataSandiView.CurrentModel = new Model.ChangePasswordModel();
            _ubahKataSandiView.RestorePasswordModel = new Model.RestorePasswordModel();

            if (_ubahKataSandiView.IsRestorePassword)
            {
                _ubahKataSandiView.SetRestorePasswordBinding();
            }
            else
            {
                _ubahKataSandiView.SetBinding();
            }
            _ubahKataSandiView.LoadData();
        }

        private void _ubahKataSandiView_BtnTutupClicked()
        {
            this._ubahKataSandiView.Close();
        }

        private void _ubahKataSandiView_BtnSimpanClicked()
        {
            if (_ubahKataSandiView.IsRestorePassword)
            {
                RestorePassword();
            }
            else
            {
                ChangePassword();
            }
            
        }

        private void _ubahKataSandiView_BtnBrowseClicked()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Buka file kunci";
            dialog.Filter = "All Files(*.*)|*.*";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this._ubahKataSandiView.FileKunci = dialog.FileName;
            }
        }

        private void Save()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                            () => _ubahKataSandiProcess.UpdatePassword(_ubahKataSandiView.CurrentModel)),
                            AppResource.MSG_UPDATE_PASSWORD);

            if (result.IsSucess)
            {
                bool updateSuccessful = (bool)result.DataResult;

                if (updateSuccessful)
                {
                    MessageBox.Show(AppResource.MSG_UPDATE_PASSWORD_SUCCESS, "Ubah kata sandi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(AppResource.MSG_UPDATE_INCORRECT_OLDPASSWORD, "Ubah kata sandi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(result.ProcessException.Message, "Ubah kata sandi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SetNewPassword()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                            () => _ubahKataSandiProcess.SetNewPassword(_ubahKataSandiView.RestorePasswordModel)),
                            AppResource.MSG_UPDATE_PASSWORD);

            if (result.IsSucess)
            {
                bool updateSuccessful = (bool)result.DataResult;

                if (updateSuccessful)
                {
                    MessageBox.Show(AppResource.MSG_UPDATE_PASSWORD_SUCCESS, "Ubah kata sandi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(AppResource.MSG_UPDATE_INCORRECT_OLDPASSWORD, "Ubah kata sandi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(result.ProcessException.Message, "Ubah kata sandi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ChangePassword()
        {
            var context = new ValidationContext(_ubahKataSandiView.CurrentModel, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(_ubahKataSandiView.CurrentModel, context, results, true);

            if (isValid)
            {
                Save();
                _ubahKataSandiView.CurrentModel.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                _ubahKataSandiView.CurrentModel.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        private void RestorePassword()
        {
            var context = new ValidationContext(_ubahKataSandiView.RestorePasswordModel, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(_ubahKataSandiView.RestorePasswordModel, context, results, true);

            if (isValid)
            {
                SetNewPassword();
                _ubahKataSandiView.RestorePasswordModel.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                _ubahKataSandiView.RestorePasswordModel.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }
    }
}
