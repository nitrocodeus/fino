﻿using System;

namespace Fino.Datalib.Entity
{
    public class PosBiayaPaid
    {
        public int PosBiaya_Id { get; set; }
        public DateTime datepaid { get; set; }
        public double paynilai { get; set; }
        public int status { get; set; }
        public int? nota_pembayaran_id { get; set; }

        public virtual PosBiaya PosOfBiaya { get; set; }
        public virtual NotaPembayaran NotaPembayaran { get; set; }
    }
}
