﻿using Fino.Datalib.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class PosBiayaPaidCancelConfiguration
    {
        internal static void ConfigurePosBiayaPaidCancel(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PosBiayaPaidCancel>().HasKey(e => e.PosBiayaPaidCancel_Id);
            modelBuilder.Entity<PosBiayaPaidCancel>().Property(e => e.PosBiayaPaidCancel_Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<PosBiayaPaidCancel>()
                .HasRequired(e => e.PosOfBiaya);
        }
    }
}
