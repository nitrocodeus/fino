﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IUbahKataSandiProcess
    {
        IUserRepository UserRepo { get; set; }
        ProcessResult UpdatePassword(ChangePasswordModel entity);
        ProcessResult SetNewPassword(RestorePasswordModel entity);
    }
}
