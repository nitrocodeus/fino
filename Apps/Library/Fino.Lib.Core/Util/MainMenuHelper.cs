﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Lib.Core.Util
{
    public class MainMenuHelper
    {
        #region Singleton

        private static readonly Lazy<MainMenuHelper> lazy = new Lazy<MainMenuHelper>(() => new MainMenuHelper());

        public static MainMenuHelper Instance { get { return lazy.Value; } }

        private MainMenuHelper() { }

        #endregion Singleton

        #region Public Methods

        public void InitializeMainMenu(ListView listViewMainMenu, string role)
        {
            foreach (var header in Constants.Instance.MenuGroupHeader)
            {
                listViewMainMenu.Groups.Add(header, header);
            }

            ImageList imgList = new ImageList();
            foreach (MainMenuEnum menuEnum in Enum.GetValues(typeof(MainMenuEnum)))
            {
                string imageFilepath = Path.Combine(Constants.MENU_IMAGE_DIRECTORY, menuEnum.ToString() + ".png");

                if (File.Exists(imageFilepath))
                {
                    Image img = Image.FromFile(imageFilepath);
                    imgList.Images.Add(menuEnum.ToString(), img);
                }
            }

            listViewMainMenu.LargeImageList = imgList;
            listViewMainMenu.SmallImageList = imgList;

            InitializeSiswaMenu(listViewMainMenu, role);
            InitializePenerimaanMenu(listViewMainMenu, role);
            InitializePengeluaranMenu(listViewMainMenu, role);
            InitializeAkuntansiMainMenu(listViewMainMenu, role);
            InitializeTabunganMainMenu(listViewMainMenu, role);
        }

        #endregion Public Methods

        #region Private Methods

        private void InitializeSiswaMenu(ListView listViewMainMenu, string role)
        {
            ListViewItem siswaExplorerItem = new ListViewItem("Pencarian Siswa", MainMenuEnum.SiswaExplorer.ToString());
            siswaExplorerItem.Tag = MainMenuEnum.SiswaExplorer;
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(siswaExplorerItem);
            listViewMainMenu.Items.Add(siswaExplorerItem);

            ListViewItem pendaftaranSiswaBaruItem = new ListViewItem("Pendaftaran Siswa Baru", MainMenuEnum.PendaftaranSiswaBaru.ToString());
            pendaftaranSiswaBaruItem.Tag = MainMenuEnum.PendaftaranSiswaBaru;
            listViewMainMenu.Items.Add(pendaftaranSiswaBaruItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(pendaftaranSiswaBaruItem);

            ListViewItem daftarUlangItem = new ListViewItem("Daftar Ulang", MainMenuEnum.DaftarUlang.ToString());
            daftarUlangItem.Tag = MainMenuEnum.DaftarUlang;
            listViewMainMenu.Items.Add(daftarUlangItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(daftarUlangItem);

            ListViewItem potonganBiayaItem = new ListViewItem("Potongan Biaya", MainMenuEnum.PotonganBiaya.ToString());
            potonganBiayaItem.Tag = MainMenuEnum.PotonganBiaya;
            listViewMainMenu.Items.Add(potonganBiayaItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(potonganBiayaItem);

            ListViewItem layananItem = new ListViewItem("Daftar Layanan", MainMenuEnum.DaftarLayanan.ToString());
            layananItem.Tag = MainMenuEnum.DaftarLayanan;
            listViewMainMenu.Items.Add(layananItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(layananItem);


            ListViewItem cicilanBiaya = new ListViewItem("Cicilan Biaya", MainMenuEnum.CicilanBiaya.ToString());
            cicilanBiaya.Tag = MainMenuEnum.CicilanBiaya;
            listViewMainMenu.Items.Add(cicilanBiaya);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(cicilanBiaya);

            if (role.Equals(Constants.AUTHORIZATION_ROLE_ADMIN))
            {
                ListViewItem pengelolaanKelasItem = new ListViewItem("Pengelolaan Kelas", MainMenuEnum.PengelolaanKelas.ToString());
                pengelolaanKelasItem.Tag = MainMenuEnum.PengelolaanKelas;
                listViewMainMenu.Items.Add(pengelolaanKelasItem);
                listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(pengelolaanKelasItem);
            }
        }

        private void InitializePenerimaanMenu(ListView listViewMainMenu, string role)
        {
            ListViewItem jenisPenerimaanItem = null;
            ListViewItem grupPenerimaanItem = null;
            ListViewItem settingNilaiBiayaItem = null;

            if (role.Equals(Constants.AUTHORIZATION_ROLE_ADMIN))
            {
                jenisPenerimaanItem = new ListViewItem("Jenis Penerimaan", MainMenuEnum.JenisPenerimaan.ToString());
                jenisPenerimaanItem.Tag = MainMenuEnum.JenisPenerimaan;
                listViewMainMenu.Items.Add(jenisPenerimaanItem);
                listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(jenisPenerimaanItem);

                grupPenerimaanItem = new ListViewItem("Grup Penerimaan", MainMenuEnum.GrupPenerimaan.ToString());
                grupPenerimaanItem.Tag = MainMenuEnum.GrupPenerimaan;
                listViewMainMenu.Items.Add(grupPenerimaanItem);
                listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(grupPenerimaanItem);

                settingNilaiBiayaItem = new ListViewItem("Nilai Biaya", MainMenuEnum.SettingNilai.ToString());
                settingNilaiBiayaItem.Tag = MainMenuEnum.SettingNilai;
                listViewMainMenu.Items.Add(settingNilaiBiayaItem);
                listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(settingNilaiBiayaItem);
            }
            ListViewItem penerimaanPelunasanItem = new ListViewItem("Penerimaan Pelunasan", MainMenuEnum.PenerimaanPelunasan.ToString());
            penerimaanPelunasanItem.Tag = MainMenuEnum.PenerimaanPelunasan;
            listViewMainMenu.Items.Add(penerimaanPelunasanItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(penerimaanPelunasanItem);

            ListViewItem pembatalanPenerimaanPelunasanItem = new ListViewItem("Pembatalan Penerimaan Pelunasan", MainMenuEnum.PembatalanPenerimaanPelunasan.ToString());
            pembatalanPenerimaanPelunasanItem.Tag = MainMenuEnum.PembatalanPenerimaanPelunasan;
            listViewMainMenu.Items.Add(pembatalanPenerimaanPelunasanItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(pembatalanPenerimaanPelunasanItem);

            ListViewItem ReportRPPItem = new ListViewItem("Laporan Penerimaan dan Piutang", MainMenuEnum.ReportRPP.ToString());
            ReportRPPItem.Tag = MainMenuEnum.ReportRPP;
            listViewMainMenu.Items.Add(ReportRPPItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(ReportRPPItem);

            ListViewItem LaporanPiutangItem = new ListViewItem("Laporan Piutang", MainMenuEnum.LaporanPiutang.ToString());
            LaporanPiutangItem.Tag = MainMenuEnum.LaporanPiutang;
            listViewMainMenu.Items.Add(LaporanPiutangItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(LaporanPiutangItem);

            ListViewItem RiwayatPenerimaanItem = new ListViewItem("Riwayat Penerimaan", MainMenuEnum.RiwayatPenerimaan.ToString());
            RiwayatPenerimaanItem.Tag = MainMenuEnum.RiwayatPenerimaan;
            listViewMainMenu.Items.Add(RiwayatPenerimaanItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(RiwayatPenerimaanItem);

            ListViewItem PenerimaanUmumItem = new ListViewItem("Penerimaan Umum", MainMenuEnum.PenerimaanUmum.ToString());
            PenerimaanUmumItem.Tag = MainMenuEnum.PenerimaanUmum;
            listViewMainMenu.Items.Add(PenerimaanUmumItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(PenerimaanUmumItem);

            ListViewItem pembatalanPenerimaanUmumItem = new ListViewItem("Pembatalan Penerimaan Umum", MainMenuEnum.PembatalanPenerimaanUmum.ToString());
            pembatalanPenerimaanUmumItem.Tag = MainMenuEnum.PembatalanPenerimaanUmum;
            listViewMainMenu.Items.Add(pembatalanPenerimaanUmumItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(pembatalanPenerimaanUmumItem);
        }

        private void InitializePengeluaranMenu(ListView listViewMainMenu, string role)
        {
            if (role.Equals(Constants.AUTHORIZATION_ROLE_ADMIN))
            {
                ListViewItem jenisPengeluaranItem = new ListViewItem("Jenis Pengeluaran", MainMenuEnum.JenisPengeluaran.ToString());
                jenisPengeluaranItem.Tag = MainMenuEnum.JenisPengeluaran;
                listViewMainMenu.Items.Add(jenisPengeluaranItem);
                listViewMainMenu.Groups[Constants.MAIN_MENU_PENGELUARAN_GROUP_NAME].Items.Add(jenisPengeluaranItem);
            }

            ListViewItem pembayaranItem = new ListViewItem("Pembayaran", MainMenuEnum.Pembayaran.ToString());
            pembayaranItem.Tag = MainMenuEnum.Pembayaran;
            listViewMainMenu.Items.Add(pembayaranItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENGELUARAN_GROUP_NAME].Items.Add(pembayaranItem);


            ListViewItem pembatalanPembayaranItem = new ListViewItem("Pembatalan Pembayaran", MainMenuEnum.PembatalanPembayaran.ToString());
            pembatalanPembayaranItem.Tag = MainMenuEnum.PembatalanPembayaran;
            listViewMainMenu.Items.Add(pembatalanPembayaranItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENGELUARAN_GROUP_NAME].Items.Add(pembatalanPembayaranItem);

            ListViewItem rekapPengeluaranItem = new ListViewItem("Rekap Pengeluaran", MainMenuEnum.RekapPengeluaran.ToString());
            rekapPengeluaranItem.Tag = MainMenuEnum.RekapPengeluaran;
            listViewMainMenu.Items.Add(rekapPengeluaranItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENGELUARAN_GROUP_NAME].Items.Add(rekapPengeluaranItem);


            ListViewItem laporanPengeluaranItem = new ListViewItem("Laporan Pengeluaran", MainMenuEnum.LaporanPengeluaran.ToString());
            laporanPengeluaranItem.Tag = MainMenuEnum.LaporanPengeluaran;
            listViewMainMenu.Items.Add(laporanPengeluaranItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENGELUARAN_GROUP_NAME].Items.Add(laporanPengeluaranItem);


        }

        private void InitializeAkuntansiMainMenu(ListView listViewMainMenu, string role)
        {
            if (role.Equals(Constants.AUTHORIZATION_ROLE_ADMIN))
            {
                ListViewItem kodeAkunItem = new ListViewItem("Kode Akun", MainMenuEnum.KodeAkun.ToString());
                kodeAkunItem.Tag = MainMenuEnum.KodeAkun;
                listViewMainMenu.Items.Add(kodeAkunItem);
                listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(kodeAkunItem);

                ListViewItem periodeAkunItem = new ListViewItem("Periode Akun", MainMenuEnum.PeriodeAkun.ToString());
                periodeAkunItem.Tag = MainMenuEnum.PeriodeAkun;
                listViewMainMenu.Items.Add(periodeAkunItem);
                listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(periodeAkunItem);

                ListViewItem settingAkunJurnalItem = new ListViewItem("Setting Akun Jurnal", MainMenuEnum.SettingAkunJurnal.ToString());
                settingAkunJurnalItem.Tag = MainMenuEnum.SettingAkunJurnal;
                listViewMainMenu.Items.Add(settingAkunJurnalItem);
                listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(settingAkunJurnalItem);

                ListViewItem postingJurnalItem = new ListViewItem("Posting Transaksi", MainMenuEnum.PostingJurnal.ToString());
                postingJurnalItem.Tag = MainMenuEnum.PostingJurnal;
                listViewMainMenu.Items.Add(postingJurnalItem);
                listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(postingJurnalItem);
            }

            ListViewItem jurnalHarianItem = new ListViewItem("Jurnal Harian", MainMenuEnum.JurnalHarian.ToString());
            jurnalHarianItem.Tag = MainMenuEnum.JurnalHarian;
            listViewMainMenu.Items.Add(jurnalHarianItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(jurnalHarianItem);
            
            ListViewItem laporanAkuntansiItem = new ListViewItem("Neraca Saldo", MainMenuEnum.LaporanAkuntansi.ToString());
            laporanAkuntansiItem.Tag = MainMenuEnum.LaporanAkuntansi;
            listViewMainMenu.Items.Add(laporanAkuntansiItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(laporanAkuntansiItem);
        }

        private void InitializeTabunganMainMenu(ListView listViewMainMenu, string role)
        {
            ListViewItem rekeningItem = new ListViewItem("Buka Rekening", MainMenuEnum.BukaRekening.ToString());
            rekeningItem.Tag = MainMenuEnum.BukaRekening;
            listViewMainMenu.Items.Add(rekeningItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_TABUNGAN_GROUP_NAME].Items.Add(rekeningItem);
            
            ListViewItem transaksiItem = new ListViewItem("Transaksi", MainMenuEnum.Transaksi.ToString());
            transaksiItem.Tag = MainMenuEnum.Transaksi;
            listViewMainMenu.Items.Add(transaksiItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_TABUNGAN_GROUP_NAME].Items.Add(transaksiItem);
            
            ListViewItem laporanTabunganItem = new ListViewItem("Laporan Saldo ", MainMenuEnum.LaporanTabungan.ToString());
            laporanTabunganItem.Tag = MainMenuEnum.LaporanTabungan;
            listViewMainMenu.Items.Add(laporanTabunganItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_TABUNGAN_GROUP_NAME].Items.Add(laporanTabunganItem);
            
            ListViewItem laporanMutasiItem = new ListViewItem("Laporan Mutasi ", MainMenuEnum.LaporanMutasi.ToString());
            laporanMutasiItem.Tag = MainMenuEnum.LaporanMutasi;
            listViewMainMenu.Items.Add(laporanMutasiItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_TABUNGAN_GROUP_NAME].Items.Add(laporanMutasiItem);
        }

        #endregion Private Methods
    }
}
