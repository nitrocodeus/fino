﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class PembatalanPenerimaanUmumModel : BaseModel
    {
        public bool Selected { get; set; }
        public int PenerimaanUmum_Id { get; set; }
        public int RefSumberPenerimaan_Id { get; set; }
        public string RefSumberPenerimaan_Name { get; set; }
        public string Deskripsi { get; set; }
        public double Nilai { get; set; }
        public int NotaPenerimaanUmumId { get; set; }
        public DateTime? DatePaid { get; set; }
    }
}
