﻿using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class RefAccountRepository : BaseRepository, IRefAccountRepository
    {
        private RefAccount GetExistingRefAccount(int account_id)
        {         
            return Context.RefAccounts.Where(x => x.account_id == account_id).FirstOrDefault();
        }

        public void UpdateRefAccount(RefAccountModel entity)
        {
            var existingRefAccount = GetExistingRefAccount(entity.Account_id);

            if (existingRefAccount == null)
            {
                Context.RefAccounts.Add(new RefAccount
                    {
                        account_code = entity.Account_code,
                        account_name = entity.Account_name
                    });
            }
            else
            {
                existingRefAccount.account_code = entity.Account_code;
                existingRefAccount.account_name = entity.Account_name;
            }

            Context.SaveChanges();
        }

        public RefAccountModel GetRefAccount(int account_id)
        {
            RefAccountModel result = null;

            return result;
        }

        public List<RefAccountModel> GetAllRefAccount()
        {
            List<RefAccountModel> result = new List<RefAccountModel>();

            var refAccounts = Context.RefAccounts.ToList();

            foreach(var refAccount in refAccounts)
            {
                result.Add(new RefAccountModel
                    {
                        Account_id = refAccount.account_id,
                        Account_code = refAccount.account_code,
                        Account_name = refAccount.account_name
                    });
            }

            return result;
        }

        public void DeleteRefAccount(RefAccountModel entity)
        {
            var existingRefAccount = GetExistingRefAccount(entity.Account_id);

            if (existingRefAccount != null)
            {
                Context.RefAccounts.Remove(existingRefAccount);
            }

            Context.SaveChanges();
        }
    }
}
