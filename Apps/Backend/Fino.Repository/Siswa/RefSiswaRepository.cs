﻿using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class RefSiswaRepository : BaseRepository, IRefSiswaRepository
    {
        public List<SiswaModel> GetSiswa(string p_stringSiswa)
        {
            var sqlCmd = new StringBuilder("select a.siswa_id as Id, a.siswa_code as Code, a.nama as Nama, a.jkelamin as JenisKelamin, ")
                .Append("b.kelas_id as IdKelas, e.nama as Kelas, b.tahun_ajaran_id as IdTahunAjaran, ")
                .Append("d.nama as TahunAjaran, ")
                .Append("rk.rekening_code as NoRekening ")
                .Append("from refsiswas a ")
                .Append("left join siswakelas b on b.siswa_id = a.siswa_id ")
                .Append("inner join ")
                .Append("( select siswa_id, ")
                .Append("max(mulai_tanggal) mdtm ")
                .Append("from SiswaKelas ")
                .Append("group by siswa_id) c on a.siswa_id = c.siswa_id and b.mulai_tanggal = c.mdtm ")
                .Append("left join reftahunajarans d on d.tahunajaran_id = b.tahun_ajaran_id ")
                .Append("left join refkelas e on e.kelas_id = b.kelas_id ")
                .Append("left join siswarekenings sr on a.siswa_id = sr.siswa_id ")
                .Append("left join refrekenings rk on sr.rekening_id = rk.rekening_id ")
                .Append("where a.siswa_code like @p0 or a.nama like @p1 ");

            //List<RefSiswa> returnData = null;

            //returnData = (from a in context.RefSiswas
            //              where a.siswa_code.Contains(p_stringSiswa) || a.nama.Contains(p_stringSiswa)
            //              select a).ToList();

            List<SiswaModel> resultSql = Context.Database
                    .SqlQuery<SiswaModel>(
                        sqlCmd.ToString(),
                        string.Format("%{0}%", p_stringSiswa),
                        string.Format("%{0}%", p_stringSiswa)).ToList();

            return resultSql;            
        }

        public RefSiswa GetSiswaByCode(string pCode)
        {
            RefSiswa returnModel = null;

            RefSiswa siswaEntity = Context.RefSiswas.Where(e => e.siswa_code.Equals(pCode)).FirstOrDefault();
            if (siswaEntity != null)
            {
                returnModel = siswaEntity;
            }

            return returnModel;
        }

        public List<SiswaExplorerModel> GetAllRefSiswaForSiswaExplorer()
        {
            List<SiswaExplorerModel> result = null;

            var refSiswa = Context.RefSiswas;
            var unpaidBiaya = Context.PosBiayas.Where(p=> p.IsActive && !p.IsPaid).Select(p=> new
                              {
                                  siswaId = p.Siswa_Id,
                                  jTempo = p.JTempo
                              });
            var startDate = new DateTime(DateTime.Today.Year-2, 1, 1);
            var siswaKelasStartedTwoYearsAgo = Context.SiswaKelases.Where(e => e.mulai_tanggal >= startDate)
                                    .Join(Context.RefKelases, k=>k.kelas_id,
                                    e=>e.kelas_id,
                                    (sk,k)=> 
                                    new {
                                        siswaId =sk.siswa_id,
                                        kelasId = sk.kelas_id,
                                        kelasNama = k.nama,
                                        mulaiTanggal = sk.mulai_tanggal
                                    });
            if (refSiswa.Any())
            {
                result = new List<SiswaExplorerModel>();
                foreach (var siswa in refSiswa)
                {
                    // Initialize result
                    var model = new SiswaExplorerModel
                    {
                        SiswaId = siswa.siswa_id,
                        SiswaCode = siswa.siswa_code,
                        NamaSiswa = siswa.nama,
                        TglMasuk = DateTime.MinValue, 
                        Kelas = string.Empty, 
                        IsPindahan = siswa.ispindahan,
                        RekeningTabungan = string.Empty,
                        Tunggakan = 0
                    };
                    // Tunggakan
                    if (unpaidBiaya.Any(x => x.siswaId.Equals(siswa.siswa_id)))
                    {
                        var jTempo = unpaidBiaya.Where(x => x.siswaId.Equals(siswa.siswa_id)).OrderBy(x => x.jTempo).FirstOrDefault().jTempo;
                        model.Tunggakan = (DateTime.Today - jTempo).TotalDays;
                    }
                    //
                    //Tabungan
                    if (Context.SiswaRekenings.Any(x => x.siswa_id.Equals(siswa.siswa_id)))
                    {
                        model.RekeningTabungan = Context.SiswaRekenings.Where(x => x.siswa_id.Equals(siswa.siswa_id)).Join(Context.Rekenings,
                            c => c.rekening_id,
                            cm => cm.rekening_id,
                            (c, cm) => new
                            {
                                cm.rekening_code
                            }).FirstOrDefault().rekening_code.ToString();
                    }
                    // Layanan
                    var listLayanan = siswa.AnggotaLayanans.Where(x => x.siswa_id == siswa.siswa_id && x.hingga_tanggal >= DateTime.Today).ToList();
                    List<string> layananSiswa = new List<string>();
                    //
                    layananSiswa = new List<string>();
                    foreach (var layanan in listLayanan)
                    {
                        layananSiswa.Add(layanan.Layanan.code);
                    }
                    model.KodeLayanan = layananSiswa;
                    //
                    var siswaKelas = siswaKelasStartedTwoYearsAgo.Where(e=>e.siswaId.Equals(siswa.siswa_id)).OrderByDescending(e => e.mulaiTanggal).FirstOrDefault();
                    if (siswaKelas != null)
                    {
                        model.Kelas = siswaKelas.kelasNama;
                        model.TglMasuk = siswaKelas.mulaiTanggal;
                    }
                    result.Add(model);
                }
            }

            return result;
        }

        public void UpdateSiswa(RefSiswa entity)
        {
            var existingSiswa = this.Context.RefSiswas.Where(x => x.siswa_id == entity.siswa_id).FirstOrDefault();

            if (existingSiswa != null)
            {
                existingSiswa.siswa_code = entity.siswa_code;
                existingSiswa.nama = entity.nama;
                existingSiswa.jkelamin = entity.jkelamin;

                this.Context.SaveChanges();
            }
        }
    }
}
