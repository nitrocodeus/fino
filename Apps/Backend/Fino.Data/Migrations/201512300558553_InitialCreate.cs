namespace Fino.Datalib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountClassSettings",
                c => new
                    {
                        account_id = c.Int(nullable: false),
                        acc_class_id = c.Int(nullable: false),
                        param_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.account_id)
                .ForeignKey("dbo.RefAccounts", t => t.account_id)
                .Index(t => t.account_id);
            
            CreateTable(
                "dbo.RefAccounts",
                c => new
                    {
                        account_id = c.Int(nullable: false, identity: true),
                        account_code = c.String(maxLength: 20),
                        account_name = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.account_id);
            
            CreateTable(
                "dbo.GLAccounts",
                c => new
                    {
                        glaccount_id = c.Int(nullable: false, identity: true),
                        gl_id = c.Int(nullable: false),
                        accountclassification_id = c.Int(nullable: false),
                        account_id = c.Int(nullable: false),
                        factor = c.Int(nullable: false),
                        amount = c.Double(nullable: false),
                        RefAccGroup_accgroup_id = c.Int(),
                    })
                .PrimaryKey(t => t.glaccount_id)
                .ForeignKey("dbo.RefAccounts", t => t.account_id)
                .ForeignKey("dbo.RefGLs", t => t.gl_id)
                .ForeignKey("dbo.RefAccGroups", t => t.RefAccGroup_accgroup_id)
                .Index(t => t.gl_id)
                .Index(t => t.account_id)
                .Index(t => t.RefAccGroup_accgroup_id);
            
            CreateTable(
                "dbo.RefGLs",
                c => new
                    {
                        gl_id = c.Int(nullable: false, identity: true),
                        gl_name = c.String(maxLength: 100),
                        period_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.gl_id)
                .ForeignKey("dbo.RefAccPeriods", t => t.period_id)
                .Index(t => t.period_id);
            
            CreateTable(
                "dbo.RefAccPeriods",
                c => new
                    {
                        period_id = c.Int(nullable: false, identity: true),
                        period_name = c.String(maxLength: 20),
                        period_start = c.DateTime(nullable: false),
                        period_end = c.DateTime(nullable: false),
                        status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.period_id);
            
            CreateTable(
                "dbo.JournalHeaders",
                c => new
                    {
                        journal_id = c.Int(nullable: false, identity: true),
                        period_id = c.Int(nullable: false),
                        journal_date = c.DateTime(nullable: false),
                        journal_name = c.String(maxLength: 500),
                        status = c.Int(nullable: false),
                        is_generated = c.Boolean(nullable: false),
                        AccountClassificationId = c.Int(nullable: false),
                        sourceid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.journal_id)
                .ForeignKey("dbo.RefAccPeriods", t => t.period_id)
                .Index(t => t.period_id);
            
            CreateTable(
                "dbo.JournalDetails",
                c => new
                    {
                        journaldetail_id = c.Int(nullable: false, identity: true),
                        journal_id = c.Int(nullable: false),
                        account_id = c.Int(nullable: false),
                        factor = c.Int(nullable: false),
                        amount = c.Double(nullable: false),
                        detail_name = c.String(maxLength: 200),
                        status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.journaldetail_id)
                .ForeignKey("dbo.RefAccounts", t => t.account_id)
                .ForeignKey("dbo.JournalHeaders", t => t.journal_id)
                .Index(t => t.journal_id)
                .Index(t => t.account_id);
            
            CreateTable(
                "dbo.JournalGLAccounts",
                c => new
                    {
                        glaccount_id = c.Int(nullable: false),
                        journaldetail_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.glaccount_id)
                .ForeignKey("dbo.GLAccounts", t => t.glaccount_id)
                .ForeignKey("dbo.JournalDetails", t => t.glaccount_id)
                .Index(t => t.glaccount_id);
            
            CreateTable(
                "dbo.JournalPosBiayas",
                c => new
                    {
                        journal_id = c.Int(nullable: false),
                        PosBiaya_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.journal_id)
                .ForeignKey("dbo.JournalHeaders", t => t.journal_id)
                .ForeignKey("dbo.PosBiayas", t => t.PosBiaya_Id)
                .Index(t => t.journal_id)
                .Index(t => t.PosBiaya_Id);
            
            CreateTable(
                "dbo.PosBiayas",
                c => new
                    {
                        PosBiaya_Id = c.Int(nullable: false, identity: true),
                        Siswa_Id = c.Int(nullable: false),
                        Biaya_Id = c.Int(nullable: false),
                        Deskripsi = c.String(maxLength: 70),
                        JTempo = c.DateTime(nullable: false),
                        Nilai = c.Double(nullable: false),
                        Potongan = c.Double(nullable: false),
                        IsPersen = c.Boolean(nullable: false),
                        NilaiPotongan = c.Double(nullable: false),
                        IsPaid = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        DatePaid = c.DateTime(),
                        Status_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PosBiaya_Id)
                .ForeignKey("dbo.RefBiayas", t => t.Biaya_Id)
                .ForeignKey("dbo.RefSiswas", t => t.Siswa_Id)
                .Index(t => t.Siswa_Id)
                .Index(t => t.Biaya_Id);
            
            CreateTable(
                "dbo.RefBiayas",
                c => new
                    {
                        biaya_id = c.Int(nullable: false, identity: true),
                        nama = c.String(maxLength: 30),
                        code = c.String(maxLength: 10),
                        repetisi = c.Int(nullable: false),
                        jt_tanggal = c.Int(nullable: false),
                        jt_bulan = c.Int(nullable: false),
                        aktif = c.Boolean(nullable: false),
                        mulai_tanggal = c.DateTime(nullable: false),
                        noposbiaya = c.Boolean(nullable: false),
                        bisacicil = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.biaya_id);
            
            CreateTable(
                "dbo.BiayaNilaiOptions",
                c => new
                    {
                        biayanilaioption_id = c.Int(nullable: false, identity: true),
                        biaya_id = c.Int(nullable: false),
                        option = c.Int(nullable: false),
                        nilai = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.biayanilaioption_id)
                .ForeignKey("dbo.RefBiayas", t => t.biaya_id)
                .Index(t => t.biaya_id);
            
            CreateTable(
                "dbo.BiayaLayanans",
                c => new
                    {
                        layanan_id = c.Int(nullable: false),
                        biaya_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.layanan_id)
                .ForeignKey("dbo.RefBiayas", t => t.layanan_id)
                .ForeignKey("dbo.RefLayanans", t => t.layanan_id)
                .Index(t => t.layanan_id);
            
            CreateTable(
                "dbo.RefLayanans",
                c => new
                    {
                        layanan_id = c.Int(nullable: false, identity: true),
                        nama = c.String(maxLength: 25),
                        code = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => t.layanan_id);
            
            CreateTable(
                "dbo.AgtLayanans",
                c => new
                    {
                        agtlayanan_id = c.Int(nullable: false, identity: true),
                        siswa_id = c.Int(nullable: false),
                        layanan_id = c.Int(nullable: false),
                        mulai_tanggal = c.DateTime(nullable: false),
                        hingga_tanggal = c.DateTime(nullable: false),
                        tgl_daftar = c.DateTime(nullable: false),
                        nilai = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.agtlayanan_id)
                .ForeignKey("dbo.RefLayanans", t => t.layanan_id)
                .ForeignKey("dbo.RefSiswas", t => t.siswa_id)
                .Index(t => t.siswa_id)
                .Index(t => t.layanan_id);
            
            CreateTable(
                "dbo.RefSiswas",
                c => new
                    {
                        siswa_id = c.Int(nullable: false, identity: true),
                        siswa_code = c.String(maxLength: 10),
                        nama = c.String(maxLength: 50),
                        jkelamin = c.Boolean(nullable: false),
                        ispindahan = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.siswa_id);
            
            CreateTable(
                "dbo.SiswaAlamats",
                c => new
                    {
                        siswa_id = c.Int(nullable: false),
                        alamat = c.String(maxLength: 50),
                        kecamatan = c.String(maxLength: 50),
                        kota = c.String(maxLength: 50),
                        propinsi = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.siswa_id)
                .ForeignKey("dbo.RefSiswas", t => t.siswa_id)
                .Index(t => t.siswa_id);
            
            CreateTable(
                "dbo.NotaPembayarans",
                c => new
                    {
                        nota_pembayaran_Id = c.Int(nullable: false, identity: true),
                        tanggal_cetak = c.DateTime(nullable: false),
                        status = c.Int(nullable: false),
                        siswa_id = c.Int(nullable: false),
                        user_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.nota_pembayaran_Id)
                .ForeignKey("dbo.RefSiswas", t => t.siswa_id)
                .ForeignKey("dbo.SysUsers", t => t.user_id)
                .Index(t => t.siswa_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.PosBiayaPaids",
                c => new
                    {
                        PosBiaya_Id = c.Int(nullable: false),
                        datepaid = c.DateTime(nullable: false),
                        paynilai = c.Double(nullable: false),
                        status = c.Int(nullable: false),
                        nota_pembayaran_id = c.Int(),
                    })
                .PrimaryKey(t => t.PosBiaya_Id)
                .ForeignKey("dbo.NotaPembayarans", t => t.nota_pembayaran_id)
                .ForeignKey("dbo.PosBiayas", t => t.PosBiaya_Id)
                .Index(t => t.PosBiaya_Id)
                .Index(t => t.nota_pembayaran_id);
            
            CreateTable(
                "dbo.SysUsers",
                c => new
                    {
                        user_id = c.Int(nullable: false, identity: true),
                        name = c.String(maxLength: 15),
                        fullname = c.String(maxLength: 50),
                        password = c.String(maxLength: 100),
                        role = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.user_id);
            
            CreateTable(
                "dbo.SysUserContacts",
                c => new
                    {
                        user_id = c.Int(nullable: false),
                        contact = c.String(nullable: false, maxLength: 100),
                        type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.user_id, t.contact })
                .ForeignKey("dbo.SysUsers", t => t.user_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.NotaPenerimaanUmums",
                c => new
                    {
                        nota_penerimaan_Id = c.Int(nullable: false, identity: true),
                        tanggal_cetak = c.DateTime(nullable: false),
                        status = c.Int(nullable: false),
                        SumberPenerimaanId = c.Int(nullable: false),
                        user_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.nota_penerimaan_Id)
                .ForeignKey("dbo.RefSumberPenerimaanUmums", t => t.SumberPenerimaanId)
                .ForeignKey("dbo.SysUsers", t => t.user_id)
                .Index(t => t.SumberPenerimaanId)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.PenerimaanUmums",
                c => new
                    {
                        penerimaanumum_Id = c.Int(nullable: false, identity: true),
                        refsumberpenerimaan_id = c.Int(nullable: false),
                        Biaya_Id = c.Int(nullable: false),
                        Deskripsi = c.String(maxLength: 70),
                        Nilai = c.Double(nullable: false),
                        IsPaid = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        DatePaid = c.DateTime(),
                        Status_Id = c.Int(nullable: false),
                        nota_penerimaan_id = c.Int(),
                    })
                .PrimaryKey(t => t.penerimaanumum_Id)
                .ForeignKey("dbo.RefBiayas", t => t.Biaya_Id)
                .ForeignKey("dbo.NotaPenerimaanUmums", t => t.nota_penerimaan_id)
                .ForeignKey("dbo.RefSumberPenerimaanUmums", t => t.refsumberpenerimaan_id)
                .Index(t => t.refsumberpenerimaan_id)
                .Index(t => t.Biaya_Id)
                .Index(t => t.nota_penerimaan_id);
            
            CreateTable(
                "dbo.RefSumberPenerimaanUmums",
                c => new
                    {
                        refsumberpenerimaan_id = c.Int(nullable: false, identity: true),
                        refsumberpenerimaan_name = c.String(maxLength: 50),
                        refsumberpenerimaan_alamat = c.String(maxLength: 200),
                        refsumberpenerimaan_kelasid = c.Int(),
                    })
                .PrimaryKey(t => t.refsumberpenerimaan_id)
                .ForeignKey("dbo.RefKelas", t => t.refsumberpenerimaan_kelasid)
                .Index(t => t.refsumberpenerimaan_kelasid);
            
            CreateTable(
                "dbo.RefKelas",
                c => new
                    {
                        kelas_id = c.Int(nullable: false, identity: true),
                        kelas_code = c.String(maxLength: 10),
                        nama = c.String(maxLength: 15),
                        tingkat = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.kelas_id);
            
            CreateTable(
                "dbo.SiswaKelas",
                c => new
                    {
                        siswa_kelas_id = c.Int(nullable: false, identity: true),
                        siswa_id = c.Int(nullable: false),
                        tahun_ajaran_id = c.Int(nullable: false),
                        is_siswa_baru = c.Boolean(nullable: false),
                        mulai_tanggal = c.DateTime(nullable: false),
                        tgl_daftar = c.DateTime(nullable: false),
                        kelas_id = c.Int(nullable: false),
                        option_tingkat = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.siswa_kelas_id)
                .ForeignKey("dbo.RefKelas", t => t.kelas_id)
                .ForeignKey("dbo.RefSiswas", t => t.siswa_id)
                .ForeignKey("dbo.RefTahunAjarans", t => t.tahun_ajaran_id)
                .Index(t => t.siswa_id)
                .Index(t => t.tahun_ajaran_id)
                .Index(t => t.kelas_id);
            
            CreateTable(
                "dbo.RefTahunAjarans",
                c => new
                    {
                        tahunajaran_id = c.Int(nullable: false, identity: true),
                        nama = c.String(maxLength: 4000),
                        mulai_bulan = c.Int(nullable: false),
                        mulai_tahun = c.Int(nullable: false),
                        hingga_bulan = c.Int(nullable: false),
                        hingga_tahun = c.Int(nullable: false),
                        semcawu = c.Int(nullable: false),
                        status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.tahunajaran_id);
            
            CreateTable(
                "dbo.Potongans",
                c => new
                    {
                        potongan_id = c.Int(nullable: false, identity: true),
                        siswa_id = c.Int(nullable: false),
                        biaya_id = c.Int(nullable: false),
                        nilai = c.Double(nullable: false),
                        persen = c.Boolean(nullable: false),
                        mulai_tanggal = c.DateTime(nullable: false),
                        hingga_tanggal = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.potongan_id)
                .ForeignKey("dbo.RefBiayas", t => t.biaya_id)
                .ForeignKey("dbo.RefSiswas", t => t.siswa_id)
                .Index(t => t.siswa_id)
                .Index(t => t.biaya_id);
            
            CreateTable(
                "dbo.SiswaRekenings",
                c => new
                    {
                        rekening_id = c.Int(nullable: false),
                        siswa_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.rekening_id)
                .ForeignKey("dbo.RefRekenings", t => t.rekening_id)
                .ForeignKey("dbo.RefSiswas", t => t.rekening_id)
                .Index(t => t.rekening_id);
            
            CreateTable(
                "dbo.RefRekenings",
                c => new
                    {
                        rekening_id = c.Int(nullable: false, identity: true),
                        rekening_code = c.String(maxLength: 20),
                        tgl_buka = c.DateTime(nullable: false),
                        status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.rekening_id);
            
            CreateTable(
                "dbo.MutasiRekenings",
                c => new
                    {
                        mutasi_id = c.Int(nullable: false, identity: true),
                        rekening_id = c.Int(nullable: false),
                        jenis_mutasi = c.Int(nullable: false),
                        tgl_mutasi = c.DateTime(nullable: false),
                        factor = c.Int(nullable: false),
                        mutasi_nilai = c.Double(nullable: false),
                        is_awal = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.mutasi_id)
                .ForeignKey("dbo.RefRekenings", t => t.rekening_id)
                .Index(t => t.rekening_id);
            
            CreateTable(
                "dbo.GrupBiayas",
                c => new
                    {
                        Grup_Id = c.Int(nullable: false, identity: true),
                        GrupBiaya_Id = c.Int(nullable: false),
                        Biaya_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Grup_Id)
                .ForeignKey("dbo.RefBiayas", t => t.Biaya_Id)
                .ForeignKey("dbo.RefGrupBiayas", t => t.GrupBiaya_Id)
                .Index(t => t.GrupBiaya_Id)
                .Index(t => t.Biaya_Id);
            
            CreateTable(
                "dbo.RefGrupBiayas",
                c => new
                    {
                        Grupbiaya_id = c.Int(nullable: false, identity: true),
                        nama = c.String(maxLength: 70),
                        code = c.String(maxLength: 10),
                        active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Grupbiaya_id);
            
            CreateTable(
                "dbo.KonversiCicilans",
                c => new
                    {
                        PosBiaya_Id = c.Int(nullable: false),
                        Tenor = c.Int(nullable: false),
                        MulaiTanggal = c.DateTime(nullable: false),
                        NilaiCicilan = c.Double(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Status_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PosBiaya_Id)
                .ForeignKey("dbo.PosBiayas", t => t.PosBiaya_Id)
                .Index(t => t.PosBiaya_Id);
            
            CreateTable(
                "dbo.KonversiCicilanPosBiayas",
                c => new
                    {
                        destination_posbiaya_id = c.Int(nullable: false),
                        source_posbiaya_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.destination_posbiaya_id)
                .ForeignKey("dbo.PosBiayas", t => t.destination_posbiaya_id)
                .ForeignKey("dbo.KonversiCicilans", t => t.source_posbiaya_id)
                .Index(t => t.destination_posbiaya_id)
                .Index(t => t.source_posbiaya_id);
            
            CreateTable(
                "dbo.GroupAccounts",
                c => new
                    {
                        groupaccount_id = c.Int(nullable: false, identity: true),
                        accgroup_id = c.Int(nullable: false),
                        account_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.groupaccount_id)
                .ForeignKey("dbo.RefAccounts", t => t.account_id)
                .ForeignKey("dbo.RefAccGroups", t => t.accgroup_id)
                .Index(t => t.accgroup_id)
                .Index(t => t.account_id);
            
            CreateTable(
                "dbo.RefAccGroups",
                c => new
                    {
                        accgroup_id = c.Int(nullable: false, identity: true),
                        accgroup_name = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.accgroup_id);
            
            CreateTable(
                "dbo.PayablePaids",
                c => new
                    {
                        payable_id = c.Int(nullable: false),
                        datepaid = c.DateTime(nullable: false),
                        paynilai = c.Double(nullable: false),
                        referenceno = c.String(maxLength: 4000),
                        status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.payable_id)
                .ForeignKey("dbo.Payables", t => t.payable_id)
                .Index(t => t.payable_id);
            
            CreateTable(
                "dbo.Payables",
                c => new
                    {
                        payable_id = c.Int(nullable: false, identity: true),
                        refpayable_id = c.Int(nullable: false),
                        refvendor_id = c.Int(nullable: false),
                        deskripsi = c.String(maxLength: 500),
                        jtempo = c.DateTime(nullable: false),
                        nilai = c.Double(nullable: false),
                        ispaid = c.Boolean(nullable: false),
                        isactive = c.Boolean(nullable: false),
                        datepaid = c.DateTime(),
                        created = c.DateTime(nullable: false),
                        status_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.payable_id)
                .ForeignKey("dbo.RefPayables", t => t.refpayable_id)
                .ForeignKey("dbo.RefVendors", t => t.refvendor_id)
                .Index(t => t.refpayable_id)
                .Index(t => t.refvendor_id);
            
            CreateTable(
                "dbo.RefPayables",
                c => new
                    {
                        refpayable_id = c.Int(nullable: false, identity: true),
                        name = c.String(maxLength: 70),
                        code = c.String(maxLength: 10),
                        aktif = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.refpayable_id);
            
            CreateTable(
                "dbo.RefVendors",
                c => new
                    {
                        refvendor_id = c.Int(nullable: false, identity: true),
                        vendorname = c.String(maxLength: 50),
                        vendor_addr = c.String(maxLength: 200),
                        vendor_email = c.String(maxLength: 100),
                        vendor_phone = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.refvendor_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PayablePaids", "payable_id", "dbo.Payables");
            DropForeignKey("dbo.Payables", "refvendor_id", "dbo.RefVendors");
            DropForeignKey("dbo.Payables", "refpayable_id", "dbo.RefPayables");
            DropForeignKey("dbo.AccountClassSettings", "account_id", "dbo.RefAccounts");
            DropForeignKey("dbo.GroupAccounts", "accgroup_id", "dbo.RefAccGroups");
            DropForeignKey("dbo.GLAccounts", "RefAccGroup_accgroup_id", "dbo.RefAccGroups");
            DropForeignKey("dbo.GroupAccounts", "account_id", "dbo.RefAccounts");
            DropForeignKey("dbo.GLAccounts", "gl_id", "dbo.RefGLs");
            DropForeignKey("dbo.RefGLs", "period_id", "dbo.RefAccPeriods");
            DropForeignKey("dbo.JournalPosBiayas", "PosBiaya_Id", "dbo.PosBiayas");
            DropForeignKey("dbo.PosBiayas", "Siswa_Id", "dbo.RefSiswas");
            DropForeignKey("dbo.KonversiCicilanPosBiayas", "source_posbiaya_id", "dbo.KonversiCicilans");
            DropForeignKey("dbo.KonversiCicilanPosBiayas", "destination_posbiaya_id", "dbo.PosBiayas");
            DropForeignKey("dbo.KonversiCicilans", "PosBiaya_Id", "dbo.PosBiayas");
            DropForeignKey("dbo.PosBiayas", "Biaya_Id", "dbo.RefBiayas");
            DropForeignKey("dbo.GrupBiayas", "GrupBiaya_Id", "dbo.RefGrupBiayas");
            DropForeignKey("dbo.GrupBiayas", "Biaya_Id", "dbo.RefBiayas");
            DropForeignKey("dbo.BiayaLayanans", "layanan_id", "dbo.RefLayanans");
            DropForeignKey("dbo.AgtLayanans", "siswa_id", "dbo.RefSiswas");
            DropForeignKey("dbo.SiswaRekenings", "rekening_id", "dbo.RefSiswas");
            DropForeignKey("dbo.SiswaRekenings", "rekening_id", "dbo.RefRekenings");
            DropForeignKey("dbo.MutasiRekenings", "rekening_id", "dbo.RefRekenings");
            DropForeignKey("dbo.Potongans", "siswa_id", "dbo.RefSiswas");
            DropForeignKey("dbo.Potongans", "biaya_id", "dbo.RefBiayas");
            DropForeignKey("dbo.NotaPembayarans", "user_id", "dbo.SysUsers");
            DropForeignKey("dbo.NotaPenerimaanUmums", "user_id", "dbo.SysUsers");
            DropForeignKey("dbo.NotaPenerimaanUmums", "SumberPenerimaanId", "dbo.RefSumberPenerimaanUmums");
            DropForeignKey("dbo.PenerimaanUmums", "refsumberpenerimaan_id", "dbo.RefSumberPenerimaanUmums");
            DropForeignKey("dbo.RefSumberPenerimaanUmums", "refsumberpenerimaan_kelasid", "dbo.RefKelas");
            DropForeignKey("dbo.SiswaKelas", "tahun_ajaran_id", "dbo.RefTahunAjarans");
            DropForeignKey("dbo.SiswaKelas", "siswa_id", "dbo.RefSiswas");
            DropForeignKey("dbo.SiswaKelas", "kelas_id", "dbo.RefKelas");
            DropForeignKey("dbo.PenerimaanUmums", "nota_penerimaan_id", "dbo.NotaPenerimaanUmums");
            DropForeignKey("dbo.PenerimaanUmums", "Biaya_Id", "dbo.RefBiayas");
            DropForeignKey("dbo.SysUserContacts", "user_id", "dbo.SysUsers");
            DropForeignKey("dbo.NotaPembayarans", "siswa_id", "dbo.RefSiswas");
            DropForeignKey("dbo.PosBiayaPaids", "PosBiaya_Id", "dbo.PosBiayas");
            DropForeignKey("dbo.PosBiayaPaids", "nota_pembayaran_id", "dbo.NotaPembayarans");
            DropForeignKey("dbo.SiswaAlamats", "siswa_id", "dbo.RefSiswas");
            DropForeignKey("dbo.AgtLayanans", "layanan_id", "dbo.RefLayanans");
            DropForeignKey("dbo.BiayaLayanans", "layanan_id", "dbo.RefBiayas");
            DropForeignKey("dbo.BiayaNilaiOptions", "biaya_id", "dbo.RefBiayas");
            DropForeignKey("dbo.JournalPosBiayas", "journal_id", "dbo.JournalHeaders");
            DropForeignKey("dbo.JournalGLAccounts", "glaccount_id", "dbo.JournalDetails");
            DropForeignKey("dbo.JournalGLAccounts", "glaccount_id", "dbo.GLAccounts");
            DropForeignKey("dbo.JournalDetails", "journal_id", "dbo.JournalHeaders");
            DropForeignKey("dbo.JournalDetails", "account_id", "dbo.RefAccounts");
            DropForeignKey("dbo.JournalHeaders", "period_id", "dbo.RefAccPeriods");
            DropForeignKey("dbo.GLAccounts", "account_id", "dbo.RefAccounts");
            DropIndex("dbo.Payables", new[] { "refvendor_id" });
            DropIndex("dbo.Payables", new[] { "refpayable_id" });
            DropIndex("dbo.PayablePaids", new[] { "payable_id" });
            DropIndex("dbo.GroupAccounts", new[] { "account_id" });
            DropIndex("dbo.GroupAccounts", new[] { "accgroup_id" });
            DropIndex("dbo.KonversiCicilanPosBiayas", new[] { "source_posbiaya_id" });
            DropIndex("dbo.KonversiCicilanPosBiayas", new[] { "destination_posbiaya_id" });
            DropIndex("dbo.KonversiCicilans", new[] { "PosBiaya_Id" });
            DropIndex("dbo.GrupBiayas", new[] { "Biaya_Id" });
            DropIndex("dbo.GrupBiayas", new[] { "GrupBiaya_Id" });
            DropIndex("dbo.MutasiRekenings", new[] { "rekening_id" });
            DropIndex("dbo.SiswaRekenings", new[] { "rekening_id" });
            DropIndex("dbo.Potongans", new[] { "biaya_id" });
            DropIndex("dbo.Potongans", new[] { "siswa_id" });
            DropIndex("dbo.SiswaKelas", new[] { "kelas_id" });
            DropIndex("dbo.SiswaKelas", new[] { "tahun_ajaran_id" });
            DropIndex("dbo.SiswaKelas", new[] { "siswa_id" });
            DropIndex("dbo.RefSumberPenerimaanUmums", new[] { "refsumberpenerimaan_kelasid" });
            DropIndex("dbo.PenerimaanUmums", new[] { "nota_penerimaan_id" });
            DropIndex("dbo.PenerimaanUmums", new[] { "Biaya_Id" });
            DropIndex("dbo.PenerimaanUmums", new[] { "refsumberpenerimaan_id" });
            DropIndex("dbo.NotaPenerimaanUmums", new[] { "user_id" });
            DropIndex("dbo.NotaPenerimaanUmums", new[] { "SumberPenerimaanId" });
            DropIndex("dbo.SysUserContacts", new[] { "user_id" });
            DropIndex("dbo.PosBiayaPaids", new[] { "nota_pembayaran_id" });
            DropIndex("dbo.PosBiayaPaids", new[] { "PosBiaya_Id" });
            DropIndex("dbo.NotaPembayarans", new[] { "user_id" });
            DropIndex("dbo.NotaPembayarans", new[] { "siswa_id" });
            DropIndex("dbo.SiswaAlamats", new[] { "siswa_id" });
            DropIndex("dbo.AgtLayanans", new[] { "layanan_id" });
            DropIndex("dbo.AgtLayanans", new[] { "siswa_id" });
            DropIndex("dbo.BiayaLayanans", new[] { "layanan_id" });
            DropIndex("dbo.BiayaNilaiOptions", new[] { "biaya_id" });
            DropIndex("dbo.PosBiayas", new[] { "Biaya_Id" });
            DropIndex("dbo.PosBiayas", new[] { "Siswa_Id" });
            DropIndex("dbo.JournalPosBiayas", new[] { "PosBiaya_Id" });
            DropIndex("dbo.JournalPosBiayas", new[] { "journal_id" });
            DropIndex("dbo.JournalGLAccounts", new[] { "glaccount_id" });
            DropIndex("dbo.JournalDetails", new[] { "account_id" });
            DropIndex("dbo.JournalDetails", new[] { "journal_id" });
            DropIndex("dbo.JournalHeaders", new[] { "period_id" });
            DropIndex("dbo.RefGLs", new[] { "period_id" });
            DropIndex("dbo.GLAccounts", new[] { "RefAccGroup_accgroup_id" });
            DropIndex("dbo.GLAccounts", new[] { "account_id" });
            DropIndex("dbo.GLAccounts", new[] { "gl_id" });
            DropIndex("dbo.AccountClassSettings", new[] { "account_id" });
            DropTable("dbo.RefVendors");
            DropTable("dbo.RefPayables");
            DropTable("dbo.Payables");
            DropTable("dbo.PayablePaids");
            DropTable("dbo.RefAccGroups");
            DropTable("dbo.GroupAccounts");
            DropTable("dbo.KonversiCicilanPosBiayas");
            DropTable("dbo.KonversiCicilans");
            DropTable("dbo.RefGrupBiayas");
            DropTable("dbo.GrupBiayas");
            DropTable("dbo.MutasiRekenings");
            DropTable("dbo.RefRekenings");
            DropTable("dbo.SiswaRekenings");
            DropTable("dbo.Potongans");
            DropTable("dbo.RefTahunAjarans");
            DropTable("dbo.SiswaKelas");
            DropTable("dbo.RefKelas");
            DropTable("dbo.RefSumberPenerimaanUmums");
            DropTable("dbo.PenerimaanUmums");
            DropTable("dbo.NotaPenerimaanUmums");
            DropTable("dbo.SysUserContacts");
            DropTable("dbo.SysUsers");
            DropTable("dbo.PosBiayaPaids");
            DropTable("dbo.NotaPembayarans");
            DropTable("dbo.SiswaAlamats");
            DropTable("dbo.RefSiswas");
            DropTable("dbo.AgtLayanans");
            DropTable("dbo.RefLayanans");
            DropTable("dbo.BiayaLayanans");
            DropTable("dbo.BiayaNilaiOptions");
            DropTable("dbo.RefBiayas");
            DropTable("dbo.PosBiayas");
            DropTable("dbo.JournalPosBiayas");
            DropTable("dbo.JournalGLAccounts");
            DropTable("dbo.JournalDetails");
            DropTable("dbo.JournalHeaders");
            DropTable("dbo.RefAccPeriods");
            DropTable("dbo.RefGLs");
            DropTable("dbo.GLAccounts");
            DropTable("dbo.RefAccounts");
            DropTable("dbo.AccountClassSettings");
        }
    }
}
