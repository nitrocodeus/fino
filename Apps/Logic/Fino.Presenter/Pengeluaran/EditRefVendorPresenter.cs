﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class EditRefVendorPresenter : BasePresenter
    {
        IEditRefVendorView _editRefVendorView;
        IPayablePaidProcess _payablePaidProcess;

        public EditRefVendorPresenter(IEditRefVendorView p_View, IPayablePaidProcess p_Process)
            : base (p_View)
        {
            this._editRefVendorView = p_View;
            this._payablePaidProcess = p_Process;

            this._editRefVendorView.BtnSimpanClicked += _editRefVendorView_BtnSimpanClicked;
            this._editRefVendorView.BtnBatalClicked += _editRefVendorView_BtnBatalClicked;
            this._editRefVendorView.Load += _editRefVendorView_Load;
        }

        void _editRefVendorView_Load(object sender, EventArgs e)
        {
            this._editRefVendorView.ViewModel = new Model.RefVendorModel();
            this._editRefVendorView.SetBinding();
        }

        void _editRefVendorView_BtnBatalClicked()
        {
            this._editRefVendorView.Close();
        }

        void _editRefVendorView_BtnSimpanClicked()
        {
            var context = new ValidationContext(this._editRefVendorView.ViewModel, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(this._editRefVendorView.ViewModel, context, results, true);

            if (isValid)
            {
                Save();

                this._editRefVendorView.ViewModel.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                this._editRefVendorView.ViewModel.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        private void Save()
        {
            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => this._payablePaidProcess.CreateNewRefVendor(this._editRefVendorView.ViewModel)),
                        AppResource.MSG_SAVING_REFVENDOR);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                this._editRefVendorView.PayablePaidView.ReloadData();
                this._editRefVendorView.Close();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }
    }
}
