﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditRefVendorView : BaseForm, IEditRefVendorView
    {
        #region Events

        public event Action BtnBatalClicked;
        public event Action BtnSimpanClicked;

        #endregion Events

        #region Constructors

        public EditRefVendorView()
        {
            InitializeComponent();

            this.btnSimpan.Click += btnSimpan_Click;
            this.btnBatal.Click += btnBatal_Click;
        }

        #region Control Events

        void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        #endregion Control Events

        #endregion Constructors

        #region Public Properties

        public RefVendorModel ViewModel { get; set; }
        public IPayablePaidView PayablePaidView { get; set; }

        public string VendorName
        {
            get
            {
                return txtVendorName.Text;
            }
            set
            {
                txtVendorName.Text = value;
            }
        }

        public string VendorAddr
        {
            get
            {
                return txtVendorAddr.Text;
            }
            set
            {
                txtVendorAddr.Text = value;
            }
        }

        public string VendorEmail
        {
            get
            {
                return txtVendorEmail.Text;
            }
            set
            {
                txtVendorEmail.Text = value;
            }
        }

        public string VendorPhone
        {
            get
            {
                return txtVendorPhone.Text;
            }
            set
            {
                txtVendorPhone.Text = value;
            }
        }

        #endregion Public Properties

        #region IEditRefVendorView

        public void SetBinding()
        {
            txtVendorName.DataBindings.Add("Text", ViewModel, RefVendorModel.VendorNamePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtVendorAddr.DataBindings.Add("Text", ViewModel, RefVendorModel.VendorAddrPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtVendorEmail.DataBindings.Add("Text", ViewModel, RefVendorModel.VendorEmailPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtVendorPhone.DataBindings.Add("Text", ViewModel, RefVendorModel.VendorPhonePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        #endregion IEditRefVendorView
    }
}
