﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IPembatalanPenerimaanUmumProcess
    {
        IPenerimaanUmumRepository PenerimaanUmumRepo { get; set; }

        ProcessResult GetPembatalanPenerimaanUmumList(DateTime p_FromDate, DateTime p_ToDate);
        ProcessResult GetPenerimaanUmum(int p_PenerimaanUmumId);
        ProcessResult UpdatePenerimaanUmum(PenerimaanUmumModel entity);
        ProcessResult DeletePenerimaanUmum(PenerimaanUmumModel entity);
        ProcessResult DeleteNotaPenerimaanUmum(int p_NotaPenerimaanUmumId);
    }
}
