﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class EditRefSiswaPresenter : BasePresenter
    {
        IEditRefSiswaView _editRefSiswaView;
        IEditRefSiswaProcess _editRefSiswaProcess;

        public EditRefSiswaPresenter(IEditRefSiswaProcess p_process, IEditRefSiswaView p_View)
            : base(p_View)
        {
            this._editRefSiswaProcess = p_process;
            this._editRefSiswaView = p_View;

            this._editRefSiswaView.Load += _editRefSiswaView_Load;
            this._editRefSiswaView.BtnSimpanClicked += _editRefSiswaView_BtnSimpanClicked;
            this._editRefSiswaView.BtnBatalClicked += _editRefSiswaView_BtnBatalClicked;
        }

        #region Control Events

        void _editRefSiswaView_BtnBatalClicked()
        {
            this._editRefSiswaView.Close();
        }

        void _editRefSiswaView_BtnSimpanClicked()
        {
            var context = new ValidationContext(this._editRefSiswaView.SelectedSiswa, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(this._editRefSiswaView.SelectedSiswa, context, results, true);

            var existingSiswaByCode = this._editRefSiswaProcess.GetSiswaByCode(this._editRefSiswaView.SelectedSiswa.KodeSiswa).DataResult as EditRefSiswaModel;
            bool isSiswaCodeUsed = existingSiswaByCode != null && existingSiswaByCode.SiswaId != this._editRefSiswaView.SelectedSiswa.SiswaId;

            if (isValid && !isSiswaCodeUsed)
            {
                Save();

                this._editRefSiswaView.SelectedSiswa.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                if (isSiswaCodeUsed)
                {
                    sb.Append("Kode siswa sudah terpakai");
                }

                this._editRefSiswaView.SelectedSiswa.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        void _editRefSiswaView_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        #endregion Control Events

        #region Private Methods

        private void LoadData()
        {
            this._editRefSiswaView.SetBinding();
        }

        private void Save()
        {
            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => this._editRefSiswaProcess.UpdateSiswa(this._editRefSiswaView.SelectedSiswa)),
                        AppResource.MSG_SAVING_REF_KELAS);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                this._editRefSiswaView.SiswaExplorerView.ReloadDataEvent();
                this._editRefSiswaView.Close();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        #endregion Private Methods
    }
}
