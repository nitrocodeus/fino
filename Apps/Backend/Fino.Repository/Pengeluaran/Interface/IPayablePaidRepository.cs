﻿using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public interface IPayablePaidRepository
    {
        void UpdatePayablePaid(PayablePaidModel entity);
    }
}
