﻿using Fino.Lib.Core;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface ILaporanPengeluaranProcess
    {
        ILaporanPengeluaranRepository LaporanPengeluaranRepo { get; set; }
        ProcessResult GetLaporanPengeluaranData(DateTime p_FromDate, DateTime p_ToDate);
    }
}
