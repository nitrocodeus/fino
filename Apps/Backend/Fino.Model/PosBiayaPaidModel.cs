﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class PosBiayaPaidModel
    {
        int _posBiayaId;
        DateTime _datePaid;
        double _nilaiBayar;
        int _status;
        int? _notaPembayaranId;

        public int? NotaPembayaranId
        {
            get { return _notaPembayaranId; }
            set { _notaPembayaranId = value; }
        }

        public int PosBiayaId
        {
            get { return _posBiayaId; }
            set { _posBiayaId = value; }
        }

        public DateTime DatePaid
        {
            get { return _datePaid; }
            set { _datePaid = value; }
        }

        public double NilaiBayar
        {
            get { return _nilaiBayar; }
            set { _nilaiBayar = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }
    }
}
