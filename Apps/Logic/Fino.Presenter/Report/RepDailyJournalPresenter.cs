﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class RepDailyJournalPresenter : BasePresenter
    {
        IRepDailyJournalView _view;
        IPostTransactionToJournalProcess _process;

        public RepDailyJournalPresenter(IRepDailyJournalView p_view, IPostTransactionToJournalProcess p_process)
            : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.BtnOkClicked += OkBtnClicked;
            _view.BtnTutupClicked += BtnTutupClicked;
        }

        public void BtnTutupClicked()
        {
            _view.Close();
        }

        public void OkBtnClicked()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _process.GetDailyJournal(_view.FromDate, _view.ToDate)),
                AppResource.MSG_REPORT_DAILY_JOURNAL);

            if (result.IsSucess)
            {
                _view.ShowRPP(result.DataResult as List<DailyJournalModel>);
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            } 
        }
    }
}
