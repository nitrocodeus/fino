﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class PosBiayaPresenter : BasePresenter
    {
        IPosBiayaView _view;
        IPosBiayaProcess _process;

        public PosBiayaPresenter(IPosBiayaView p_view, IPosBiayaProcess p_process) : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.Load += Load;
            _view.gvPosBiayaClicked += GvPosBiayaClicked;
            _view.CariRaised += CariRaised;
        }

        void CariRaised()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _process.GetPosBiaya(_view.SiswaId, _view.ShowAllBiaya)),
                        AppResource.MSG_LOADING_REF_BIAYA);

            if (result.IsSucess)
            {
                List<PosBiayaModel> data = result.DataResult as List<PosBiayaModel>;

                _view.gvPosBiayaSource = data;
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        void GvPosBiayaClicked()
        {
            //_view.Hide();
            //_view.RaiseHideControl();
        }

        void Load(object sender, EventArgs e)
        {

        }
    }
}
