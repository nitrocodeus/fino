﻿
namespace Fino.Datalib.Entity
{
    public class JournalGLAccount
    {
        public int journaldetail_id { get; set; }
        public int glaccount_id { get; set; }

        public virtual GLAccount AccountOfGL { get; set; }
        public virtual JournalDetail DetailofJournal { get; set; }
    }
}
