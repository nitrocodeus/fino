﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;

namespace Fino.Cient
{
    public partial class PostingJournalView : BaseForm, IPostingJournalView
    {
        public PostingJournalView()
        {
            InitializeComponent();
        }

        public event Action PostingButtonClicked;
        public event Action SearchTransactionButtonClicked;
        public event Action CloseButtonClicked;
        public event Action SelectAllClicked;
        public event Action UnselectAllClicked;
        private BindingSource _TrxBindingSource;

        RefAccPeriodModel _AccountPeriod;
        public RefAccPeriodModel AccountPeriod 
        { 
            get
            {
                return _AccountPeriod;
            }
            set
            {
                _AccountPeriod = value;
                label1.Text = String.Format("{0}    Mulai: {1} s/d {2}",
                            _AccountPeriod.Period_name,
                            _AccountPeriod.Period_start.ToString("dd MMM yyyy"),
                            _AccountPeriod.Period_end.ToString("dd MMM yyyy"));
            }
        }

        public DateTime StartDate
        {
            get
            {
                return AccountPeriod.Period_start.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return AccountPeriod.Period_end.Date;
            }
        }

        public TransaksiToJurnalModel SelectedItem
        {
            get
            {
                return (TransaksiToJurnalModel)_TrxBindingSource.Current;
            }
            
        }

        public List<TransaksiToJurnalModel> ViewModel
        {
            get
            {
                if (_TrxBindingSource == null || _TrxBindingSource.DataSource == null)
                {
                    return null;
                }
                else
                {
                    return (List<TransaksiToJurnalModel>)_TrxBindingSource.DataSource;
                }
            }
            set
            {
                gvTransaksi.AutoGenerateColumns = false;
                if (_TrxBindingSource == null)
                {
                    _TrxBindingSource = new BindingSource();
                }
                _TrxBindingSource.DataSource = value;
                
                gvTransaksi.DataSource = _TrxBindingSource;

            }
        }

        public void SelectAllItem()
        {
            foreach(DataGridViewRow  row in gvTransaksi.Rows)
            {
                row.Cells[0].Value = true;
            }
            
        }

        public void UnselectAllItem()
        {
            foreach (DataGridViewRow row in gvTransaksi.Rows)
            {
                row.Cells[0].Value = false;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (SearchTransactionButtonClicked != null)
            {
                SearchTransactionButtonClicked();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            if (SelectAllClicked != null)
            {
                SelectAllClicked();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (UnselectAllClicked != null)
            {
                UnselectAllClicked();
            }
        }

        private void btnPost_Click(object sender, EventArgs e)
        {
            if (PostingButtonClicked != null)
            {
                PostingButtonClicked();
            }
        }
    }
}
