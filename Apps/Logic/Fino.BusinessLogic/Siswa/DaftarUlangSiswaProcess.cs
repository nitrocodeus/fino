﻿using System;
using System.Linq;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public class DaftarUlangSiswaProcess : IDaftarUlangSiswaProcess
    {
        public ITahunAjaranRepository TahunAjaranRepo { get; set; }
        public IDaftarUlangSiswaRepository DaftarUlangSiswaRepo { get; set; }
        public IRefKelasRepository RefKelasRepo { get; set; }
        public ISiswaKelasRepository SiswaKelasRepo { get; set; }

        void ValidateData(DaftarUlangSiswaModel model)
        {
            if (string.IsNullOrEmpty(model.Nama)
            || string.IsNullOrEmpty(model.Code)
            || model.Kelas_Id.Equals(0))
            {
                throw new ApplicationException(AppResource.MSG_INCOMPLETE_DATA);
            }

            // Check whether siswa has been enrolled for this year of study
            var existing = SiswaKelasRepo.GetExistingSiswaKelas(model.Siswa_Id, model.TahunAjaran_Id);
            if (existing != null)
            {
                throw new ApplicationException(AppResource.MSG_SAVING_SISWA_ENROLLED_ALREADY);
            }
        }

        public ProcessResult TambahDaftarUlang(DaftarUlangSiswaModel model)
        {
            ProcessResult result = new ProcessResult();
            var data = model;
            try
            {
                ValidateData(data);
                
                data = DaftarUlangSiswaRepo.TambahDaftarUlang(model);

                result.IsSucess = true;
                result.DataResult = data;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllKelas()
        {
            ProcessResult result = new ProcessResult();
            try
            {
                result.DataResult = RefKelasRepo.SelectAllKelas().Select(e => new ValueList { Id = e.kelas_id, Value = e.nama }).ToList();

                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetTahunAjaran(DateTime currentDate)
        {
            ProcessResult result = new ProcessResult();
            try
            {                
                var refTA = TahunAjaranRepo.GetValidTahunAjaran(currentDate.Year, currentDate.Month);
                if (refTA != null)
                {
                    result.DataResult = new ValueList
                    {
                        Id = refTA.tahunajaran_id,
                        Value = refTA.nama
                    };
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }


        public ProcessResult GetSiswaByNoInduk(string pNoInduk)
        {
            ProcessResult result = new ProcessResult();
            try
            {                
                var model = DaftarUlangSiswaRepo.SelectSiswaByCode(pNoInduk);
                if (model != null)
                {
                    result.DataResult = model;
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
