﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public class MutasiTabunganProcess : IMutasiTabunganProcess
    {
        public IMutasiTabunganRepository MutasiTabunganRepo { get; set; }

        private void ValidateMutasi(MutasiTabunganModel p_Model)
        {
            if (p_Model.MutasiNilai <= 0)
            {
                throw new ApplicationException(AppResource.MSG_SAVING_MUTASI_TABUNGAN_NILAI_INVALID);
            }
            if (p_Model.JenisMutasi.Equals((int)JenisMutasiEnum.PENARIKAN) && p_Model.MutasiNilai > p_Model.Saldo)
            {
                throw new ApplicationException(AppResource.MSG_SAVING_MUTASI_TABUNGAN_INSUFFICIENT);
            }
        }

        public Lib.Core.ProcessResult NewTransaksiRekening(MutasiTabunganModel p_Model)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                ValidateMutasi(p_Model);
                var resultModel = MutasiTabunganRepo.NewMutasiRekening(p_Model);
                result.DataResult = resultModel;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult InitiateMutasiByNoRekening(string pRekening)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var model = MutasiTabunganRepo.GetRekeningByRekeningNo(pRekening);
                if (model != null)
                {
                    result.DataResult = model;
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }


        public ProcessResult GetAllSaldoAsOn(DateTime p_Date)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<LaporanSaldoModel> dataResult = MutasiTabunganRepo.GetAllSaldoAsOn(p_Date.Date);

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllMutasiAsOn(DateTime p_Date)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<LaporanMutasiModel> dataResult = MutasiTabunganRepo.GetAllMutasiAsOn(p_Date.Date);

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
