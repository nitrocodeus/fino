﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class PembatalanPengeluaranPresenter : BasePresenter
    {
        private IPembatalanPengeluaranView _view;
        private IPembatalanPengeluaranProcess _process;

        public PembatalanPengeluaranPresenter(IPembatalanPengeluaranView p_View, IPembatalanPengeluaranProcess p_process)
            : base(p_View)
        {
            _view = p_View;
            _process = p_process;

            _view.BtnCariClicked += _view_BtnCariClicked;
            _view.BtnTutupClicked += _view_BtnTutupClicked;
            _view.BtnBatalClicked += _view_BtnBatalClicked;
            _view.BtnAllClicked += _view_BtnAllClicked;
            _view.BtnResetClicked += _view_BtnResetClicked;
        }

        private void LoadData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _process.GetPembatalanPengeluaranList(_view.DateFrom, _view.DateTo)),
                AppResource.MSG_CARI_PEMBATALAN_PENGELUARAN);

            if (result.IsSucess)
            {
                List<PembatalanPengeluaranModel> data = result.DataResult as List<PembatalanPengeluaranModel>;

                _view.SetData(data);
                _view.RefreshDataGrid();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        void _view_BtnBatalClicked()
        {
            var selectedPengeluaranToCancel = _view.GetSelectedPengeluaran();

            if (selectedPengeluaranToCancel.Count > 0 &&
                MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_CANCEL_PENGELUARAN, selectedPengeluaranToCancel.Count), "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    foreach (var selectedPenerimaanUmum in selectedPengeluaranToCancel)
                    {
                        var payable = _process.GetPayable(selectedPenerimaanUmum.Payable_Id).DataResult as PayableModel;

                        if (payable != null)
                        {
                            if (payable.status_id == (int)TransactionStatusEnum.OPEN)
                            {
                                // Delete Payable and PayablePaid
                                _process.DeletePayable(payable);
                            }
                            else if (payable.status_id == (int)TransactionStatusEnum.POSTED)
                            {
                                payable.status_id = (int)TransactionStatusEnum.CANCELED;
                                payable.status_date = DateTime.Now;

                                _process.UpdatePayable(payable);
                            }

                            LoadData();
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(AppResource.MSG_GAGAL_PEMBATALAN, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        void _view_BtnResetClicked()
        {
            _view.UnselectAllItem();
        }

        void _view_BtnAllClicked()
        {
            _view.SelectAllItem();
        }

        void _view_BtnTutupClicked()
        {
            _view.Close();
        }

        void _view_BtnCariClicked()
        {
            LoadData();
        }
    }
}
