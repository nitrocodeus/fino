﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core
{
    public class CustomEventArgs<T> : EventArgs
    {
        public T Data { get; set; }

        public CustomEventArgs(T p_Data)
        {
            Data = p_Data;
        }
    }
}
