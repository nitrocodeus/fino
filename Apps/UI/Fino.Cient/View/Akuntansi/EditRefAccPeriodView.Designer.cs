﻿namespace Fino.Cient
{
    partial class EditRefAccPeriodView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditRefAccPeriodView));
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNamaBiaya = new System.Windows.Forms.Label();
            this.dtPeriodStart = new System.Windows.Forms.DateTimePicker();
            this.txtPeriodName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtPeriodEnd = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(161, 113);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(100, 28);
            this.btnSimpan.TabIndex = 20;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(270, 113);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 28);
            this.btnBatal.TabIndex = 21;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(89, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Tanggal Mulai:";
            // 
            // lblNamaBiaya
            // 
            this.lblNamaBiaya.AutoSize = true;
            this.lblNamaBiaya.Location = new System.Drawing.Point(86, 15);
            this.lblNamaBiaya.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNamaBiaya.Name = "lblNamaBiaya";
            this.lblNamaBiaya.Size = new System.Drawing.Size(109, 17);
            this.lblNamaBiaya.TabIndex = 16;
            this.lblNamaBiaya.Text = "Nama Periode:";
            // 
            // dtPeriodStart
            // 
            this.dtPeriodStart.CustomFormat = "dd-MMM-yyyy";
            this.dtPeriodStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPeriodStart.Location = new System.Drawing.Point(203, 44);
            this.dtPeriodStart.Margin = new System.Windows.Forms.Padding(4);
            this.dtPeriodStart.Name = "dtPeriodStart";
            this.dtPeriodStart.Size = new System.Drawing.Size(160, 24);
            this.dtPeriodStart.TabIndex = 19;
            // 
            // txtPeriodName
            // 
            this.txtPeriodName.Location = new System.Drawing.Point(203, 12);
            this.txtPeriodName.Margin = new System.Windows.Forms.Padding(4);
            this.txtPeriodName.Name = "txtPeriodName";
            this.txtPeriodName.Size = new System.Drawing.Size(160, 24);
            this.txtPeriodName.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(89, 82);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 22;
            this.label1.Text = "Tanggal Akhir:";
            // 
            // dtPeriodEnd
            // 
            this.dtPeriodEnd.CustomFormat = "dd-MMM-yyyy";
            this.dtPeriodEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPeriodEnd.Location = new System.Drawing.Point(203, 76);
            this.dtPeriodEnd.Margin = new System.Windows.Forms.Padding(4);
            this.dtPeriodEnd.Name = "dtPeriodEnd";
            this.dtPeriodEnd.Size = new System.Drawing.Size(160, 24);
            this.dtPeriodEnd.TabIndex = 23;
            // 
            // EditRefAccPeriodView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 154);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtPeriodEnd);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblNamaBiaya);
            this.Controls.Add(this.dtPeriodStart);
            this.Controls.Add(this.txtPeriodName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditRefAccPeriodView";
            this.Text = "EditRefAccPeriodView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNamaBiaya;
        private System.Windows.Forms.DateTimePicker dtPeriodStart;
        private System.Windows.Forms.TextBox txtPeriodName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtPeriodEnd;
    }
}