﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.Presenter;
using Fino.View;

namespace Fino.Cient
{
    public partial class PotonganBiayaView : BaseForm, IPotonganBiayaView
    {
        public PotonganBiayaView()
        {
            InitializeComponent();

            InitialiseSelectSiswa();

            pencarianSiswaControlView1.HideControl += pencarianSiswaControlView1_HideControl;
        }



        public event Action SaveButtonClicked;

        public event Action NewDataButtonClicked;

        public event Action DeleteButtonClicked;

        public event Action SearchSiswaButtonClicked;

        public event Action CloseButtonClicked;

        public event Action BiayaListSelectionChanged;

        public event Action CariSiswaTextKeypressed;

        public event Action SelectingSiswaDone;

        public string Keyword
        {
            get { return txtCari.Text.Trim(); }
        }

        public int SiswaId { get; set; }

        public string NoInduk
        {
            get
            {
                return txtNoInduk.Text.Trim();
            }
            set
            {
                txtNoInduk.Text = value;
            }
        }

        public string NamaSiswa
        {
            get
            {
                return txtNamaSiswa.Text.Trim();
            }
            set
            {
                txtNamaSiswa.Text = value;
            }
        }

        public bool JenisKelamin
        {
            get
            {
                return rbLaki.Checked;
            }
            set
            {
                rbLaki.Checked = value; ;
            }
        }

        public bool IsSearchMode { get; set; }

        public int BiayaId
        {
            get
            {
                if (lstBiaya.SelectedValue == null)
                {
                    return 0;
                }
                else
                {
                    return (int)lstBiaya.SelectedValue;
                }
            }
            //set
            //{
            //    lstBiaya.SelectedValue = value;
            //}
        }


        public int PotonganId
        {
            get
            {
                if (CurrentDetail != null)
                {
                    return CurrentDetail.Potongan_Id;
                }
                else
                {
                    return 0;
                }
            }
        }

        public double Nilai
        {
            get
            {
                return CurrentDetail.Nilai; // (double)nmNilai.Value;
            }

            //set
            //{
            //    nmNilai.Value = (decimal)value;
            //}
        }

        public bool IsPersen
        {
            get
            {
                return CurrentDetail.IsPersen; // rbPercent.Checked;
            }

            //set
            //{
            //    rbPercent.Checked = value;
            //}
        }

        public DateTime MulaiTanggal
        {
            get
            {
                return CurrentDetail.Mulai_Tanggal; // dtpStart.Value.Date;
            }

            //set
            //{
            //    dtpStart.Value = value;
            //}
        }

        public DateTime HinggaTanggal
        {
            get
            {
                return CurrentDetail.Hingga_Tanggal; // dtpHingga.Value.Date;
            }

            //set
            //{
            //    dtpHingga.Value = value;
            //}
        }

        public PotonganBiayaHeaderModel ViewModel { get; set; }

        object _BiayaDataSource;
        public object BiayaDataSource
        {
            get
            {
                return _BiayaDataSource;
            }
            set
            {
                _BiayaDataSource = value;
                lstBiaya.DataSource = _BiayaDataSource;
                lstBiaya.DisplayMember = "Value";
                lstBiaya.ValueMember = "Id";
            }
        }

        public bool IsDetailExist { get; set; }

        public PotonganBiayaDetailModel CurrentDetail { get; set; }

        public IPencarianSiswaView PencarianSiswaView
        {
            get { return pencarianSiswaControlView1; }
        }


        public void ShowHeader()
        {
            SiswaId = ViewModel.Siswa_Id;
            NoInduk = ViewModel.Code;
            NamaSiswa = ViewModel.Nama;
            JenisKelamin = ViewModel.JKelamin;
        }

        private void pencarianSiswaControlView1_HideControl()
        {
            if (SelectingSiswaDone != null && pencarianSiswaControlView1.SelectedSiswa != null)
            {
                SelectingSiswaDone();
            }
        }

        public void SetBinding()
        {
            if (CurrentDetail != null)
            {
                nmNilai.DataBindings.Clear();
                nmNilai.DataBindings.Add("Value", CurrentDetail, PotonganBiayaDetailModel.NilaiPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                chkPersen.DataBindings.Clear();
                chkPersen.DataBindings.Add("Checked", CurrentDetail, PotonganBiayaDetailModel.IsPersenPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                dtpStart.DataBindings.Clear();
                dtpStart.DataBindings.Add("Value", CurrentDetail, PotonganBiayaDetailModel.MulaiTanggalPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                dtpHingga.DataBindings.Clear();
                dtpHingga.DataBindings.Add("Value", CurrentDetail, PotonganBiayaDetailModel.HinggaTanggalPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

            }
            else
            {
                nmNilai.Value = 0;
                rbLaki.Checked = true;
                dtpStart.Value = DateTime.Now.Date;
                dtpHingga.Value = DateTime.Now.Date;
            }



        }

        public void ClearView()
        {
            SetBinding();
        }


        public void ShowErrorMessage(string message, string caption)
        {
            WinApi.ShowErrorMessage(message, caption);
        }

        public void ShowInformation(string message, string caption)
        {
            WinApi.ShowInformationMessage(message, caption);
        }

        public void ShowSiswaToEdit(int pSiswaId)
        {
            throw new NotImplementedException();
        }

        public void SetSearchMode()
        {
            txtCari.ReadOnly = false;
            txtCari.Focus();
            IsSearchMode = true;

            ControlHelper.ChangeEditable(false, grpPotongan);
            ControlHelper.ChangeEditable(false, groupBox1);
            ShowPotonganDetail(false);
            btnSearchSiswa.Visible = false;

        }

        public void SetEntryMode()
        {
            IsSearchMode = false;

            ControlHelper.ChangeEditable(true, groupBox1);
            ControlHelper.ChangeEditable(false, groupBox2, new string[] { "btnSearchSiswa" });
            ControlHelper.ChangeEditable(true, grpPotongan);
            btnSearchSiswa.Visible = true;
        }

        public void SetViewMode()
        {

        }

        public void ShowPotonganDetail(bool p_Show)
        {
            grpPotongan.Visible = p_Show;
            IsDetailExist = p_Show;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (NewDataButtonClicked != null)
            {
                NewDataButtonClicked();
            }
        }

        private void btnSearchSiswa_Click(object sender, EventArgs e)
        {
            if (SearchSiswaButtonClicked != null)
            {
                SearchSiswaButtonClicked();
            }
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            if (DeleteButtonClicked != null)
            {
                DeleteButtonClicked();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
            {
                SaveButtonClicked();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        private void lstBiaya_SelectedValueChanged(object sender, EventArgs e)
        {
            if (BiayaListSelectionChanged != null)
            {
                BiayaListSelectionChanged();
            }
        }
        private void txtCari_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && CariSiswaTextKeypressed != null)
            {
                CariSiswaTextKeypressed();
            }
        }

        #region Initialise other view
        private void InitialiseSelectSiswa()
        {
            SiswaProcess processSiswa = new SiswaProcess();
            PencarianSiswaPresenter presenterSiswa = new PencarianSiswaPresenter(pencarianSiswaControlView1, processSiswa);
        }

        #endregion Initialise other view



    }
}
