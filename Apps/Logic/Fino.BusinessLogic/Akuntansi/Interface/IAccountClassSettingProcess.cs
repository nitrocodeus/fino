﻿using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IAccountClassSettingProcess
    {
        ProcessResult GetAccountClassSetting(int accountId);
        ProcessResult GetAllAccountClassSettingByAccClassId(int accClassId);
        ProcessResult UpdateAccountClassSetting(AccountClassSettingModel entity);
        ProcessResult DeleteAccountClassSetting(AccountClassSettingModel entity);

        ProcessResult GetAccountData();
        ProcessResult GetParamFromRefBiaya();
        ProcessResult GetParamFromRefPayable();
    }
}
