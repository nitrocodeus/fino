﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;
using Fino.Lib.Core;

namespace Fino.Datalib.Dbmodel
{
    public class FinoDBInitializer : CreateDatabaseIfNotExists<FinoDBContext>
    {
        protected override void Seed(FinoDBContext context)
        {

            SeedUser(context);

            var bsb = SeedGrupBiaya(context, "Biaya Siswa Baru", "BSB");
            var bdu = SeedGrupBiaya(context, "Biaya Daftar Ulang", "BDU");
            var spt = SeedGrupBiaya(context, "Siswa Pindahan Pertengahan", "SPT");

            var PenerimaanFormPendaftaran = SeedBiaya(context, "Penerimaan Form Pendaftaran", "300.001", 0, 10, (int)RepetisiEnum.NONE, false, false);
            var PenerimaanSPP = SeedBiaya(context, "SPP", "300.002", 0, 10, (int)RepetisiEnum.BULANAN, false, false);
            var PenerimaanPOMG = SeedBiaya(context, "POMG", "300.003", 0, 10, (int)RepetisiEnum.BULANAN, false, false);
            var PenerimaanBuku = SeedBiaya(context, "Buku", "300.004", 0, 10, (int)RepetisiEnum.NONE, false, true);
            var PenerimaanSeragam = SeedBiaya(context, "Seragam", "300.005", 0, 10, (int)RepetisiEnum.NONE, false, true);
            var PenerimaanPembangunan = SeedBiaya(context, "Pembangunan", "300.006", 0, 10, (int)RepetisiEnum.NONE, false, true);
            var PenerimaanKegiatan = SeedBiaya(context, "Kegiatan", "300.007", 0, 10, (int)RepetisiEnum.NONE, false, true);
            var PenerimaanAntarJemput = SeedBiaya(context, "Antar Jemput", "300.008", 0, 10, (int)RepetisiEnum.NONE, false, false);
            var PenerimaanKatering = SeedBiaya(context, "Katering", "300.009", 0, 10, (int)RepetisiEnum.NONE, false, false);
            var SubsidiListrikdanAirMinum = SeedBiaya(context, "Listrik dan Air Minum", "300.010", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var PenerimaanZakatMal = SeedBiaya(context, "Zakat Mal", "300.011", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var PenerimaanZakatFitrah = SeedBiaya(context, "Zakat Fitrah", "300.012", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var PengembaliaanPinjamanGuru = SeedBiaya(context, "Pengembaliaan Pinjaman Guru", "300.013", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var SubsididariBOS = SeedBiaya(context, "Subsidi dari BOS", "300.014", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var SubsidiBOSSPP = SeedBiaya(context, "Subsidi BOS SPP", "300.015", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var SubsidiBOSBuku = SeedBiaya(context, "Subsidi BOS Buku", "300.016", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var SubsidiBOSEkskul = SeedBiaya(context, "Subsidi BOS Ekskul", "300.017", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var SubsidiBOSPramuka = SeedBiaya(context, "Subsidi BOS Pramuka", "300.018", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var PinjamanPihakke3PSB = SeedBiaya(context, "Pinjaman Pihak ke 3 (PSB)", "300.019", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var BukuAlQurandanHadits = SeedBiaya(context, "Buku Al Qur'an dan Hadits", "300.020", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var BukuBahasaArab = SeedBiaya(context, "Buku Bahasa Arab", "300.021", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var BTQ = SeedBiaya(context, "BTQ", "300.022", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var Bukupenghubung = SeedBiaya(context, "Buku penghubung", "300.023", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var Bukumenulishalus = SeedBiaya(context, "Buku menulis halus", "300.024", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var Keterlambatanguru = SeedBiaya(context, "Keterlambatan guru", "300.025", 0, 10, (int)RepetisiEnum.NONE, true, false);
            var Jasabank = SeedBiaya(context, "Jasa bank", "300.026", 0, 10, (int)RepetisiEnum.NONE, true, false);

            var ljpt = SeedRefLayanan(context, "Antar Jemput", "JPT");
            var lkat = SeedRefLayanan(context, "Katering", "KAT");
            SeedRefKelas(context);
            context.SaveChanges();

            SeedBiayaToGrup(context, PenerimaanFormPendaftaran, bsb);
            SeedBiayaToGrup(context, PenerimaanSPP, bsb);
            SeedBiayaToGrup(context, PenerimaanBuku, bsb);
            SeedBiayaToGrup(context, PenerimaanPembangunan, bsb);
            SeedBiayaToGrup(context, PenerimaanKegiatan, bsb);
            SeedBiayaToGrup(context, PenerimaanSeragam, bsb);
            SeedBiayaToGrup(context, PenerimaanPOMG, bsb);

            SeedBiayaToGrup(context, PenerimaanSPP, bdu);
            SeedBiayaToGrup(context, PenerimaanBuku, bdu);
            SeedBiayaToGrup(context, PenerimaanKegiatan, bdu);
            SeedBiayaToGrup(context, PenerimaanPOMG, bdu);

            SeedBiayaToGrup(context, PenerimaanSPP, spt);
            SeedBiayaToGrup(context, PenerimaanPembangunan, spt);
            SeedBiayaToGrup(context, PenerimaanPOMG, spt);

            SeedRefTahunAjaran(context);


            SeedBiayaLayanan(context, PenerimaanAntarJemput, ljpt);
            SeedBiayaLayanan(context, PenerimaanKatering, lkat);

            // For new student (Tingkat 1)
            SeedBiayaNilaiOption(context, PenerimaanFormPendaftaran, 1, 325000);
            SeedBiayaNilaiOption(context, PenerimaanSPP, 1, 290000);
            SeedBiayaNilaiOption(context, PenerimaanSeragam, 1, 1500000);
            SeedBiayaNilaiOption(context, PenerimaanKegiatan, 1, 2400000);
            SeedBiayaNilaiOption(context, PenerimaanBuku, 1, 1000000);
            SeedBiayaNilaiOption(context, PenerimaanPembangunan, 1, 3000000);

            // For Tingkat II - VI
            SeedBiayaNilaiOption(context, PenerimaanSPP, 2, 280000);
            SeedBiayaNilaiOption(context, PenerimaanSPP, 3, 270000);
            SeedBiayaNilaiOption(context, PenerimaanSPP, 4, 270000);
            SeedBiayaNilaiOption(context, PenerimaanSPP, 5, 250000);
            SeedBiayaNilaiOption(context, PenerimaanSPP, 6, 240000);

            SeedBiayaNilaiOption(context, PenerimaanKegiatan, 2, 1550000);
            SeedBiayaNilaiOption(context, PenerimaanKegiatan, 3, 1550000);
            SeedBiayaNilaiOption(context, PenerimaanKegiatan, 4, 1550000);
            SeedBiayaNilaiOption(context, PenerimaanKegiatan, 5, 1550000);
            SeedBiayaNilaiOption(context, PenerimaanKegiatan, 6, 1550000);

            SeedBiayaNilaiOption(context, PenerimaanBuku, 2, 561000);
            SeedBiayaNilaiOption(context, PenerimaanBuku, 3, 549000);
            SeedBiayaNilaiOption(context, PenerimaanBuku, 4, 578000);
            SeedBiayaNilaiOption(context, PenerimaanBuku, 5, 557000);
            SeedBiayaNilaiOption(context, PenerimaanBuku, 6, 557000);

            // For all class
            SeedBiayaNilaiOption(context, PenerimaanPOMG, 0, 5000);

            // Set accounting data
            // Accounting data
            SeedAccountingPeriod(context);

            var accKas = SeedRefAccount(context, "100.001", "Kas");
            var accPiutangFormPendaftaran = SeedRefAccount(context, "120.001", "Piutang Form Pendaftaran");
            var accPiutangSPP = SeedRefAccount(context, "120.002", "Piutang SPP");
            var accPiutangPOMG = SeedRefAccount(context, "120.003", "Piutang POMG");
            var accPiutangBuku = SeedRefAccount(context, "120.004", "Piutang Buku");
            var accPiutangSeragam = SeedRefAccount(context, "120.005", "Piutang Seragam");
            var accPiutangPembangunan = SeedRefAccount(context, "120.006", "Piutang Pembangunan");
            var accPiutangKegiatan = SeedRefAccount(context, "120.007", "Piutang Kegiatan");
            var accPiutangAntarJemput = SeedRefAccount(context, "120.008", "Piutang Antar Jemput");
            var accPiutangKatering = SeedRefAccount(context, "120.009", "Piutang Katering");
            var accPiutangZakatMal = SeedRefAccount(context, "120.010", "Piutang Zakat Mal");
            var accPiutangZakatFitrah = SeedRefAccount(context, "120.011", "Piutang Zakat Fitrah");
            var accPenerimaanFormPendaftaran = SeedRefAccount(context, "300.001", "Penerimaan Form Pendaftaran");
            var accPenerimaanSPP = SeedRefAccount(context, "300.002", "Penerimaan SPP");
            var accPenerimaanPOMG = SeedRefAccount(context, "300.003", "Penerimaan POMG");
            var accPenerimaanBuku = SeedRefAccount(context, "300.004", "Penerimaan Buku");
            var accPenerimaanSeragam = SeedRefAccount(context, "300.005", "Penerimaan Seragam");
            var accPenerimaanPembangunan = SeedRefAccount(context, "300.006", "Penerimaan Pembangunan");
            var accPenerimaanKegiatan = SeedRefAccount(context, "300.007", "Penerimaan Kegiatan");
            var accPenerimaanAntarJemput = SeedRefAccount(context, "300.008", "Penerimaan Antar Jemput");
            var accPenerimaanKatering = SeedRefAccount(context, "300.009", "Penerimaan Katering");
            var accSubsidiListrikdanAirMinum = SeedRefAccount(context, "300.010", "Subsidi Listrik dan Air Minum");
            var accPenerimaanZakatMal = SeedRefAccount(context, "300.011", "Penerimaan Zakat Mal");
            var accPenerimaanZakatFitrah = SeedRefAccount(context, "300.012", "Penerimaan Zakat Fitrah");
            var accPengembaliaanPinjamanGuru = SeedRefAccount(context, "300.013", "Pengembaliaan Pinjaman Guru");
            var accSubsididariBOS = SeedRefAccount(context, "300.014", "Subsidi dari BOS");
            var accSubsidiBOSSPP = SeedRefAccount(context, "300.015", "Subsidi BOS SPP");
            var accSubsidiBOSBuku = SeedRefAccount(context, "300.016", "Subsidi BOS Buku");
            var accSubsidiBOSEkskul = SeedRefAccount(context, "300.017", "Subsidi BOS Ekskul");
            var accSubsidiBOSPramuka = SeedRefAccount(context, "300.018", "Subsidi BOS Pramuka");
            var accPinjamanPihakke3PSB = SeedRefAccount(context, "300.019", "Pinjaman Pihak ke 3 (PSB)");
            var accBukuAlQurandanHadits = SeedRefAccount(context, "300.020", "Buku Al Qur'an dan Hadits");
            var accBukuBahasaArab = SeedRefAccount(context, "300.021", "Buku Bahasa Arab");
            var accBTQ = SeedRefAccount(context, "300.022", "BTQ");
            var accBukupenghubung = SeedRefAccount(context, "300.023", "Buku penghubung");
            var accBukumenulishalus = SeedRefAccount(context, "300.024", "Buku menulis halus");
            var accKeterlambatanguru = SeedRefAccount(context, "300.025", "Keterlambatan guru");
            var accJasabank = SeedRefAccount(context, "300.026", "Jasa bank");

            SeedAccountClassSetting(AccountClassificationEnum.KAS, 1, accKas);

            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, PenerimaanFormPendaftaran.biaya_id, accPiutangFormPendaftaran);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, PenerimaanSPP.biaya_id, accPiutangSPP);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, PenerimaanPOMG.biaya_id, accPiutangPOMG);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, PenerimaanBuku.biaya_id, accPiutangBuku);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, PenerimaanSeragam.biaya_id, accPiutangSeragam);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, PenerimaanPembangunan.biaya_id, accPiutangPembangunan);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, PenerimaanKegiatan.biaya_id, accPiutangKegiatan);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, PenerimaanAntarJemput.biaya_id, accPiutangAntarJemput);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, PenerimaanKatering.biaya_id, accPiutangKatering);

            // Sales income
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanFormPendaftaran.biaya_id, accPenerimaanFormPendaftaran);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanSPP.biaya_id, accPenerimaanSPP);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanPOMG.biaya_id, accPenerimaanPOMG);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanBuku.biaya_id, accPenerimaanBuku);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanSeragam.biaya_id, accPenerimaanSeragam);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanPembangunan.biaya_id, accPenerimaanPembangunan);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanKegiatan.biaya_id, accPenerimaanKegiatan);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanAntarJemput.biaya_id, accPenerimaanAntarJemput);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanKatering.biaya_id, accPenerimaanKatering);

            // Non sales income
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, SubsidiListrikdanAirMinum.biaya_id, accSubsidiListrikdanAirMinum);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanZakatMal.biaya_id, accPenerimaanZakatMal);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PenerimaanZakatFitrah.biaya_id, accPenerimaanZakatFitrah);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PengembaliaanPinjamanGuru.biaya_id, accPengembaliaanPinjamanGuru);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, SubsididariBOS.biaya_id, accSubsididariBOS);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, SubsidiBOSSPP.biaya_id, accSubsidiBOSSPP);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, SubsidiBOSBuku.biaya_id, accSubsidiBOSBuku);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, SubsidiBOSEkskul.biaya_id, accSubsidiBOSEkskul);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, SubsidiBOSPramuka.biaya_id, accSubsidiBOSPramuka);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, PinjamanPihakke3PSB.biaya_id, accPinjamanPihakke3PSB);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, BukuAlQurandanHadits.biaya_id, accBukuAlQurandanHadits);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, BukuBahasaArab.biaya_id, accBukuBahasaArab);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, BTQ.biaya_id, accBTQ);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, Bukupenghubung.biaya_id, accBukupenghubung);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, Bukumenulishalus.biaya_id, accBukumenulishalus);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, Keterlambatanguru.biaya_id, accKeterlambatanguru);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, Jasabank.biaya_id, accJasabank);

            context.SaveChanges();

            SeedPayable(context);
            SeedVendor(context);
            SeedRefSPU(context);
        }

        private void SeedRefSPU(FinoDBContext context)
        {

            foreach (RefKelas item in context.RefKelases.ToList())
            {
                RefSumberPenerimaanUmum rspu = new RefSumberPenerimaanUmum();

                rspu.refsumberpenerimaan_name = "Kelas " + item.nama;
                rspu.refsumberpenerimaan_kelasid = item.kelas_id;

                context.SumberPenerimaanUmums.Add(rspu);
            }

            context.SaveChanges();
        }

        private void SeedVendor(FinoDBContext context)
        {
            context.RefVendors.Add(new RefVendor
            {
                vendorname = "Internal",
                vendor_phone = "-",
                vendor_addr = "-",
                vendor_email = "-"
            });
            context.SaveChanges();
        }

        private void SeedPayable(FinoDBContext context)
        {
            var payGajitransportUM = SeedPayableItem(context, "Gaji/transport/UM", "510.001");
            var payRapat = SeedPayableItem(context, "Rapat", "510.002");
            var payTelepon = SeedPayableItem(context, "Telepon", "510.003");
            var payRumahtanggasekolah = SeedPayableItem(context, "Rumahtanggasekolah", "510.004");
            var payAlattuliskantorATK = SeedPayableItem(context, "Alattuliskantor(ATK)", "510.005");
            var payPerlengkapanKantor = SeedPayableItem(context, "PerlengkapanKantor", "510.006");
            var payPerlengkapanOlahraga = SeedPayableItem(context, "PerlengkapanOlahraga", "510.007");
            var payPerlengkapanKelas = SeedPayableItem(context, "PerlengkapanKelas", "510.008");
            var payPerlengkapanLaboratorium = SeedPayableItem(context, "PerlengkapanLaboratorium", "510.009");
            var payPerlengkapanPerpustakaan = SeedPayableItem(context, "PerlengkapanPerpustakaan", "510.010");
            var payPerlengkapanUKS = SeedPayableItem(context, "PerlengkapanUKS", "510.011");
            var payPerlengkapanElektronika = SeedPayableItem(context, "PerlengkapanElektronika", "510.012");
            var payFotoCopy = SeedPayableItem(context, "FotoCopy", "510.013");
            var payPenggandaanSoal = SeedPayableItem(context, "PenggandaanSoal", "510.014");
            var payBiayaSoalDiknas = SeedPayableItem(context, "BiayaSoalDiknas", "510.015");
            var payPerawatansekolah = SeedPayableItem(context, "Perawatansekolah", "510.016");
            var payPemeliharaan = SeedPayableItem(context, "Pemeliharaan", "510.017");
            var payPemeliharanGedung = SeedPayableItem(context, "PemeliharanGedung", "510.018");
            var payPemeliharaanPeralatan = SeedPayableItem(context, "PemeliharaanPeralatan", "510.019");
            var payListrik = SeedPayableItem(context, "Listrik", "510.020");
            var payKatering = SeedPayableItem(context, "Katering", "510.021");
            var payKegiatan = SeedPayableItem(context, "Kegiatan", "510.022");
            var payPHBN = SeedPayableItem(context, "PHBN", "510.023");
            var payPHBI = SeedPayableItem(context, "PHBI", "510.024");
            var payPesan = SeedPayableItem(context, "Pesan", "510.025");
            var payPekanolahragadankesenian = SeedPayableItem(context, "Pekanolahragadankesenian", "510.026");
            var payOutbond = SeedPayableItem(context, "Outbond", "510.027");
            var payMedia = SeedPayableItem(context, "Media", "510.028");
            var payTaawunsiswa = SeedPayableItem(context, "Taawunsiswa", "510.029");
            var payPembinaanguru = SeedPayableItem(context, "Pembinaanguru", "510.030");
            var payPentasKreasiSeniAnakIslam = SeedPayableItem(context, "PentasKreasiSeniAnakIslam", "510.031");
            var payMLM = SeedPayableItem(context, "MLM", "510.032");
            var payOutingclass = SeedPayableItem(context, "Outingclass", "510.033");
            var payEkskul = SeedPayableItem(context, "Ekskul", "510.034");
            var payRihlahakhirtahun = SeedPayableItem(context, "Rihlahakhirtahun", "510.035");
            var payPerpisahankelas6 = SeedPayableItem(context, "Perpisahankelas6", "510.036");
            var payLatihanqurban = SeedPayableItem(context, "Latihanqurban", "510.037");
            var payKegiatanYayasan = SeedPayableItem(context, "KegiatanYayasan", "510.038");
            var payUjianTengahSemesterUTS = SeedPayableItem(context, "UjianTengahSemester(UTS)", "510.039");
            var payUjianAkhirSemesterUAS = SeedPayableItem(context, "UjianAkhirSemester(UAS)", "510.040");
            var paySiswaberprestasi = SeedPayableItem(context, "Siswaberprestasi", "510.041");
            var payKonsumsi = SeedPayableItem(context, "Konsumsi", "510.042");
            var payKonsumsiSiswa = SeedPayableItem(context, "KonsumsiSiswa", "510.043");
            var payKonsumsiGuru = SeedPayableItem(context, "KonsumsiGuru", "510.044");
            var paySnackSiswa = SeedPayableItem(context, "SnackSiswa", "510.045");
            var payAirMinumSiswa = SeedPayableItem(context, "AirMinumSiswa", "510.046");
            var payKonsumsiYayasan = SeedPayableItem(context, "KonsumsiYayasan", "510.047");
            var payKonsumsiTamu = SeedPayableItem(context, "KonsumsiTamu", "510.048");
            var payOperasionalKebutuhanDapur = SeedPayableItem(context, "OperasionalKebutuhanDapur", "510.049");
            var payPDAM = SeedPayableItem(context, "PDAM", "510.050");
            var payPerjalanandinas = SeedPayableItem(context, "Perjalanandinas", "510.051");
            var payIuranYAB = SeedPayableItem(context, "IuranYAB", "510.052");
            var payIuransekolah = SeedPayableItem(context, "Iuransekolah", "510.053");
            var payJaminanHariTuaJHT = SeedPayableItem(context, "JaminanHariTua(JHT)", "510.054");
            var payKomiteSekolah = SeedPayableItem(context, "KomiteSekolah", "510.055");
            var payKomputer = SeedPayableItem(context, "Komputer", "510.056");
            var payBukuguru = SeedPayableItem(context, "Bukuguru", "510.057");
            var payBukusiswa = SeedPayableItem(context, "Bukusiswa", "510.058");
            var paySeragam = SeedPayableItem(context, "Seragam", "510.059");
            var paySeragamguru = SeedPayableItem(context, "Seragamguru", "510.060");
            var paySeragamsiswa = SeedPayableItem(context, "Seragamsiswa", "510.061");
            var payAntarjemput = SeedPayableItem(context, "Antarjemput", "510.062");
            var payTaawunguru = SeedPayableItem(context, "Taawunguru", "510.063");
            var paySetordanakeyayasan = SeedPayableItem(context, "Setordanakeyayasan", "510.064");
            var paySetorinfaqkeyayasan = SeedPayableItem(context, "Setorinfaqkeyayasan", "510.065");
            var payK3S = SeedPayableItem(context, "K3S", "510.066");
            var payKesehatan = SeedPayableItem(context, "Kesehatan", "510.067");
            var payDanaSosial = SeedPayableItem(context, "DanaSosial", "510.068");
            var payPinjamanguru = SeedPayableItem(context, "Pinjamanguru", "510.069");
            var payPinjamankaryawan = SeedPayableItem(context, "Pinjamankaryawan", "510.070");
            context.SaveChanges();

            // Account for payables
            var accGajitransportUM = SeedRefAccount(context, "510.001", "Gaji / transport / UM");
            var accRapat = SeedRefAccount(context, "510.002", "Rapat");
            var accTelepon = SeedRefAccount(context, "510.003", "Telepon");
            var accRumahtanggasekolah = SeedRefAccount(context, "510.004", "Rumah tangga sekolah");
            var accAlattuliskantorATK = SeedRefAccount(context, "510.005", "Alat tulis kantor (ATK)");
            var accPerlengkapanKantor = SeedRefAccount(context, "510.006", "Perlengkapan Kantor ");
            var accPerlengkapanOlahraga = SeedRefAccount(context, "510.007", "Perlengkapan Olahraga ");
            var accPerlengkapanKelas = SeedRefAccount(context, "510.008", "Perlengkapan Kelas ");
            var accPerlengkapanLaboratorium = SeedRefAccount(context, "510.009", "Perlengkapan Laboratorium ");
            var accPerlengkapanPerpustakaan = SeedRefAccount(context, "510.010", "Perlengkapan Perpustakaan ");
            var accPerlengkapanUKS = SeedRefAccount(context, "510.011", "Perlengkapan UKS ");
            var accPerlengkapanElektronika = SeedRefAccount(context, "510.012", "Perlengkapan Elektronika ");
            var accFotoCopy = SeedRefAccount(context, "510.013", "FotoCopy ");
            var accPenggandaanSoal = SeedRefAccount(context, "510.014", "Penggandaan Soal ");
            var accBiayaSoalDiknas = SeedRefAccount(context, "510.015", "Biaya Soal Diknas ");
            var accPerawatansekolah = SeedRefAccount(context, "510.016", "Perawatan sekolah");
            var accPemeliharaan = SeedRefAccount(context, "510.017", "Pemeliharaan ");
            var accPemeliharanGedung = SeedRefAccount(context, "510.018", "Pemeliharan Gedung ");
            var accPemeliharaanPeralatan = SeedRefAccount(context, "510.019", "Pemeliharaan Peralatan ");
            var accListrik = SeedRefAccount(context, "510.020", "Listrik");
            var accKatering = SeedRefAccount(context, "510.021", "Katering");
            var accKegiatan = SeedRefAccount(context, "510.022", "Kegiatan");
            var accPHBN = SeedRefAccount(context, "510.023", "PHBN");
            var accPHBI = SeedRefAccount(context, "510.024", "PHBI");
            var accPesan = SeedRefAccount(context, "510.025", "Pesan");
            var accPekanolahragadankesenian = SeedRefAccount(context, "510.026", "Pekan olahraga dan kesenian");
            var accOutbond = SeedRefAccount(context, "510.027", "Outbond");
            var accMedia = SeedRefAccount(context, "510.028", "Media");
            var accTaawunsiswa = SeedRefAccount(context, "510.029", "Ta'awun siswa");
            var accPembinaanguru = SeedRefAccount(context, "510.030", "Pembinaan guru");
            var accPentasKreasiSeniAnakIslam = SeedRefAccount(context, "510.031", "Pentas Kreasi Seni Anak Islam");
            var accMLM = SeedRefAccount(context, "510.032", "MLM ");
            var accOutingclass = SeedRefAccount(context, "510.033", "Outing class");
            var accEkskul = SeedRefAccount(context, "510.034", "Ekskul");
            var accRihlahakhirtahun = SeedRefAccount(context, "510.035", "Rihlah akhir tahun");
            var accPerpisahankelas6 = SeedRefAccount(context, "510.036", "Perpisahan kelas 6");
            var accLatihanqurban = SeedRefAccount(context, "510.037", "Latihan qurban");
            var accKegiatanYayasan = SeedRefAccount(context, "510.038", "Kegiatan Yayasan ");
            var accUjianTengahSemesterUTS = SeedRefAccount(context, "510.039", "Ujian Tengah Semester (UTS)");
            var accUjianAkhirSemesterUAS = SeedRefAccount(context, "510.040", "Ujian Akhir Semester (UAS)");
            var accSiswaberprestasi = SeedRefAccount(context, "510.041", "Siswa berprestasi");
            var accKonsumsi = SeedRefAccount(context, "510.042", "Konsumsi");
            var accKonsumsiSiswa = SeedRefAccount(context, "510.043", "Konsumsi Siswa ");
            var accKonsumsiGuru = SeedRefAccount(context, "510.044", "Konsumsi Guru ");
            var accSnackSiswa = SeedRefAccount(context, "510.045", "Snack Siswa ");
            var accAirMinumSiswa = SeedRefAccount(context, "510.046", "Air Minum Siswa ");
            var accKonsumsiYayasan = SeedRefAccount(context, "510.047", "Konsumsi Yayasan ");
            var accKonsumsiTamu = SeedRefAccount(context, "510.048", "Konsumsi Tamu ");
            var accOperasionalKebutuhanDapur = SeedRefAccount(context, "510.049", "Operasional Kebutuhan Dapur ");
            var accPDAM = SeedRefAccount(context, "510.050", "PDAM");
            var accPerjalanandinas = SeedRefAccount(context, "510.051", "Perjalanan dinas");
            var accIuranYAB = SeedRefAccount(context, "510.052", "Iuran YAB");
            var accIuransekolah = SeedRefAccount(context, "510.053", "Iuran sekolah");
            var accJaminanHariTuaJHT = SeedRefAccount(context, "510.054", "Jaminan Hari Tua (JHT)");
            var accKomiteSekolah = SeedRefAccount(context, "510.055", "Komite Sekolah");
            var accKomputer = SeedRefAccount(context, "510.056", "Komputer");
            var accBukuguru = SeedRefAccount(context, "510.057", "Buku guru");
            var accBukusiswa = SeedRefAccount(context, "510.058", "Buku siswa");
            var accSeragam = SeedRefAccount(context, "510.059", "Seragam");
            var accSeragamguru = SeedRefAccount(context, "510.060", "Seragam guru");
            var accSeragamsiswa = SeedRefAccount(context, "510.061", "Seragam siswa");
            var accAntarjemput = SeedRefAccount(context, "510.062", "Antar jemput");
            var accTaawunguru = SeedRefAccount(context, "510.063", "Ta'awun guru");
            var accSetordanakeyayasan = SeedRefAccount(context, "510.064", "Setor dana ke yayasan");
            var accSetorinfaqkeyayasan = SeedRefAccount(context, "510.065", "Setor infaq ke yayasan");
            var accK3S = SeedRefAccount(context, "510.066", "K3S");
            var accKesehatan = SeedRefAccount(context, "510.067", "Kesehatan ");
            var accDanaSosial = SeedRefAccount(context, "510.068", "Dana Sosial ");
            var accPinjamanguru = SeedRefAccount(context, "510.069", "Pinjaman guru");
            var accPinjamankaryawan = SeedRefAccount(context, "510.070", "Pinjaman karyawan");

            context.SaveChanges();

            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payGajitransportUM.refpayable_id, accGajitransportUM);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payRapat.refpayable_id, accRapat);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payTelepon.refpayable_id, accTelepon);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payRumahtanggasekolah.refpayable_id, accRumahtanggasekolah);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payAlattuliskantorATK.refpayable_id, accAlattuliskantorATK);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPerlengkapanKantor.refpayable_id, accPerlengkapanKantor);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPerlengkapanOlahraga.refpayable_id, accPerlengkapanOlahraga);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPerlengkapanKelas.refpayable_id, accPerlengkapanKelas);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPerlengkapanLaboratorium.refpayable_id, accPerlengkapanLaboratorium);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPerlengkapanPerpustakaan.refpayable_id, accPerlengkapanPerpustakaan);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPerlengkapanUKS.refpayable_id, accPerlengkapanUKS);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPerlengkapanElektronika.refpayable_id, accPerlengkapanElektronika);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payFotoCopy.refpayable_id, accFotoCopy);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPenggandaanSoal.refpayable_id, accPenggandaanSoal);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payBiayaSoalDiknas.refpayable_id, accBiayaSoalDiknas);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPerawatansekolah.refpayable_id, accPerawatansekolah);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPemeliharaan.refpayable_id, accPemeliharaan);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPemeliharanGedung.refpayable_id, accPemeliharanGedung);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPemeliharaanPeralatan.refpayable_id, accPemeliharaanPeralatan);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payListrik.refpayable_id, accListrik);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKatering.refpayable_id, accKatering);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKegiatan.refpayable_id, accKegiatan);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPHBN.refpayable_id, accPHBN);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPHBI.refpayable_id, accPHBI);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPesan.refpayable_id, accPesan);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPekanolahragadankesenian.refpayable_id, accPekanolahragadankesenian);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payOutbond.refpayable_id, accOutbond);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payMedia.refpayable_id, accMedia);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payTaawunsiswa.refpayable_id, accTaawunsiswa);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPembinaanguru.refpayable_id, accPembinaanguru);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPentasKreasiSeniAnakIslam.refpayable_id, accPentasKreasiSeniAnakIslam);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payMLM.refpayable_id, accMLM);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payOutingclass.refpayable_id, accOutingclass);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payEkskul.refpayable_id, accEkskul);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payRihlahakhirtahun.refpayable_id, accRihlahakhirtahun);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPerpisahankelas6.refpayable_id, accPerpisahankelas6);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payLatihanqurban.refpayable_id, accLatihanqurban);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKegiatanYayasan.refpayable_id, accKegiatanYayasan);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payUjianTengahSemesterUTS.refpayable_id, accUjianTengahSemesterUTS);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payUjianAkhirSemesterUAS.refpayable_id, accUjianAkhirSemesterUAS);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, paySiswaberprestasi.refpayable_id, accSiswaberprestasi);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKonsumsi.refpayable_id, accKonsumsi);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKonsumsiSiswa.refpayable_id, accKonsumsiSiswa);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKonsumsiGuru.refpayable_id, accKonsumsiGuru);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, paySnackSiswa.refpayable_id, accSnackSiswa);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payAirMinumSiswa.refpayable_id, accAirMinumSiswa);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKonsumsiYayasan.refpayable_id, accKonsumsiYayasan);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKonsumsiTamu.refpayable_id, accKonsumsiTamu);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payOperasionalKebutuhanDapur.refpayable_id, accOperasionalKebutuhanDapur);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPDAM.refpayable_id, accPDAM);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPerjalanandinas.refpayable_id, accPerjalanandinas);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payIuranYAB.refpayable_id, accIuranYAB);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payIuransekolah.refpayable_id, accIuransekolah);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payJaminanHariTuaJHT.refpayable_id, accJaminanHariTuaJHT);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKomiteSekolah.refpayable_id, accKomiteSekolah);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKomputer.refpayable_id, accKomputer);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payBukuguru.refpayable_id, accBukuguru);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payBukusiswa.refpayable_id, accBukusiswa);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, paySeragam.refpayable_id, accSeragam);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, paySeragamguru.refpayable_id, accSeragamguru);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, paySeragamsiswa.refpayable_id, accSeragamsiswa);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payAntarjemput.refpayable_id, accAntarjemput);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payTaawunguru.refpayable_id, accTaawunguru);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, paySetordanakeyayasan.refpayable_id, accSetordanakeyayasan);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, paySetorinfaqkeyayasan.refpayable_id, accSetorinfaqkeyayasan);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payK3S.refpayable_id, accK3S);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payKesehatan.refpayable_id, accKesehatan);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payDanaSosial.refpayable_id, accDanaSosial);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPinjamanguru.refpayable_id, accPinjamanguru);
            SeedAccountClassSetting(AccountClassificationEnum.BEBAN, payPinjamankaryawan.refpayable_id, accPinjamankaryawan);

            context.SaveChanges();
        }

        private RefPayable SeedPayableItem(FinoDBContext context, string pNama, string pCode)
        {
            var refPayable = new RefPayable
            {
                aktif = true,
                code = pCode,
                name = pNama
            };
            refPayable = context.RefPayables.Add(refPayable);
            return refPayable;
        }

        private void SeedDummyData(FinoDBContext context)
        {
            SeedSiswa(context, "150201", "Chase Meridian", true, false);
            SeedSiswa(context, "150202", "Tessa", false, true);
            SeedSiswa(context, "150203", "Fulan", false, false);
            SeedSiswa(context, "130314", "Alres", true, true);
            SeedSiswa(context, "150201", "Gupta", true, true);
            SeedSiswa(context, "150206", "Eka Hana", true, false);
            SeedSiswa(context, "150207", "Shinta Wijaya", false, false);
            SeedSiswa(context, "150208", "Nasa Lus", false, true);

            SeedSiswaKelas(context, 1, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-07-24"), 1);
            SeedSiswaKelas(context, 2, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-06-24"), 2);
            SeedSiswaKelas(context, 3, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-07-12"), 11);
            SeedSiswaKelas(context, 4, 3, false, DateTime.Parse("2015-09-20"), DateTime.Parse("2015-06-11"), 14);
            SeedSiswaKelas(context, 5, 3, false, DateTime.Parse("2015-09-20"), DateTime.Parse("2015-06-17"), 14);
            SeedSiswaKelas(context, 6, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-07-24"), 16);
            SeedSiswaKelas(context, 7, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-07-24"), 5);
            SeedSiswaKelas(context, 8, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-07-24"), 6);
            context.SaveChanges();

            SeedAgtLayanan(context, 1, 1, DateTime.Parse("2014-10-01"), DateTime.Parse("2015-08-31"), DateTime.Parse("2014-08-14"), 100);
            SeedAgtLayanan(context, 1, 2, DateTime.Parse("2014-10-01"), DateTime.Parse("2015-08-31"), DateTime.Parse("2014-08-14"), 100);
            SeedAgtLayanan(context, 2, 2, DateTime.Parse("2014-10-01"), DateTime.Parse("2015-08-31"), DateTime.Parse("2014-08-14"), 100);

            SeedPosBiaya(context, 1, 1, "SPP", DateTime.Parse("2015-01-12"), 1000000, 0, false, 0, false, true, null, 0);

            var rekening1 = SeedRefRekening(context, "114235678943", 1, DateTime.Parse("2014-10-14"));




            try
            {
                context.SaveChanges();

                SeedSiswaRekening(context, 1, rekening1);

                SeedRefAccountDummy(context, "TEST01", "TEST01");
                SeedRefAccountDummy(context, "TEST02", "TEST02");
                SeedRefAccountDummy(context, "TEST03", "TEST03");
                SeedRefAccountDummy(context, "TEST04", "TEST04");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        private void SeedBiayaNilaiOption(FinoDBContext context, RefBiaya pBiaya, int pOption, int pNilai)
        {
            var bNilai = new BiayaNilaiOption
            {
                biaya_id = pBiaya.biaya_id,
                nilai = pNilai,
                option = pOption
            };

            context.BiayaNilaiOptions.Add(bNilai);
        }

        private void SeedRefKelas(FinoDBContext context)
        {
            var sub = new string[] { "A", "B", "C" };
            for (var i = 0; i < 6; i++)
            {
                foreach (var s in sub)
                {
                    var sb = new StringBuilder(string.Empty);
                    var refKelas = new RefKelas
                    {
                        nama = sb.Append((i + 1)).Append(s).ToString(),
                        kelas_code = sb.Append(i).Append(s).ToString(),
                        tingkat = i + 1
                    };
                    context.RefKelases.Add(refKelas);
                }
            }
        }

        private void SeedBiayaLayanan(FinoDBContext context, RefBiaya pBiaya, RefLayanan pLayanan)
        {
            var biayaLayanan = new BiayaLayanan()
            {
                layanan_id = pLayanan.layanan_id,
                biaya_id = pBiaya.biaya_id
            };
            context.BiayaLayanans.Add(biayaLayanan);
        }

        private RefLayanan SeedRefLayanan(FinoDBContext context, string pNama, string pCode)
        {
            var refLayanan = new RefLayanan
            {
                nama = pNama,
                code = pCode
            };
            refLayanan = context.RefLayanans.Add(refLayanan);
            return refLayanan;
        }

        private void SeedRefTahunAjaran(FinoDBContext context)
        {
            int current = DateTime.Now.AddYears(-1).Year;

            for (int i = current; i <= current + 10; i++)
            {
                for (int sem = 0; sem < 2; sem++)
                {
                    var sb = new StringBuilder("Tahun Ajaran ")
                            .Append((i)).Append("/").Append(i + 1)
                            .Append(" Sem. ").Append((sem + 1));
                    var refTh = new RefTahunAjaran
                    {
                        nama = sb.ToString(),
                        mulai_bulan = (sem.Equals(0) ? 7 : 1),
                        mulai_tahun = (sem.Equals(0) ? (i) : i + 1),
                        hingga_bulan = (sem.Equals(0) ? 12 : 6),
                        hingga_tahun = (sem.Equals(0) ? (i) : i + 1),
                        semcawu = sem + 1,
                        status = 0
                    };

                    context.RefTahunAjarans.Add(refTh);
                }
            }
        }

        private void SeedBiayaToGrup(FinoDBContext context, RefBiaya pBiaya, RefGrupBiaya pGrup)
        {
            var grupBiaya = new GrupBiaya
            {
                GrupBiaya_Id = pGrup.Grupbiaya_id,
                Biaya_Id = pBiaya.biaya_id
            };

            context.GrupBiayas.Add(grupBiaya);
        }

        private RefGrupBiaya SeedGrupBiaya(FinoDBContext context, string pNama, string pCode)
        {
            var refGrupBiaya = new RefGrupBiaya
            {
                nama = pNama,
                code = pCode,
                active = true
            };
            refGrupBiaya = context.RefGrupBiayas.Add(refGrupBiaya);
            return refGrupBiaya;
        }

        private RefBiaya SeedBiaya(FinoDBContext context, string pNama, string pCode, int pJtBulan, int pJtTanggal, int pRepetisi, bool pNoposbiaya, bool pBisaCicil = false)
        {
            var refBiaya = new RefBiaya
            {
                nama = pNama,
                code = pCode,
                jt_bulan = pJtBulan,
                jt_tanggal = pJtTanggal,
                mulai_tanggal = DateTime.Now.Date,
                repetisi = pRepetisi,
                aktif = true,
                noposbiaya = pNoposbiaya,
                bisacicil = pBisaCicil
            };
            refBiaya = context.RefBiayas.Add(refBiaya);
            return refBiaya;
        }

        private void SeedUser(FinoDBContext context)
        {
            var ContactList = new List<SysUserContact>();
            ContactList.Add(new SysUserContact
            {
                contact = "fino@nitrocodeus.com",
                type = (int)ContactTypeEnum.EMAIL
            });

            context.Users.Add(new SysUser
            {
                name = "system",
                password = "incrediblehulk",
                fullname = "System Administrator",
                role = Constants.AUTHORIZATION_ROLE_ADMIN,
                Contacts = ContactList
            });

            var fContactList = new List<SysUserContact>();
            ContactList.Add(new SysUserContact
            {
                contact = "fathrurohman@nitrocodeus.com",
                type = (int)ContactTypeEnum.EMAIL
            });

            context.Users.Add(new SysUser
            {
                name = "Fathurrohman",
                password = "secure",
                fullname = "Fathurrohman",
                role = Constants.AUTHORIZATION_ROLE_ADMIN,
                Contacts = fContactList
            });
        }

        private void SeedSiswa(FinoDBContext context, string siswa_code, string nama, bool jkelamin, bool ispindahan)
        {
            context.RefSiswas.Add(new RefSiswa
            {
                siswa_code = siswa_code,
                nama = nama,
                jkelamin = jkelamin,
                ispindahan = ispindahan
            });
        }

        private void SeedSiswaKelas(FinoDBContext context, int siswa_id, int tahun_ajaran_id, bool is_siswa_baru, DateTime mulai_tanggal, DateTime tgl_daftar, int kelas_id)
        {
            context.SiswaKelases.Add(new SiswaKelas
            {
                siswa_id = siswa_id,
                tahun_ajaran_id = tahun_ajaran_id,
                is_siswa_baru = is_siswa_baru,
                mulai_tanggal = mulai_tanggal,
                tgl_daftar = tgl_daftar,
                kelas_id = kelas_id
            });
        }

        private void SeedAgtLayanan(FinoDBContext context, int siswa_id, int layanan_id, DateTime mulai_tanggal, DateTime hingga_tanggal, DateTime tgl_daftar, double nilai)
        {
            context.AgtLayanans.Add(new AgtLayanan
            {
                siswa_id = siswa_id,
                layanan_id = layanan_id,
                mulai_tanggal = mulai_tanggal,
                hingga_tanggal = hingga_tanggal,
                tgl_daftar = tgl_daftar,
                nilai = nilai,
            });
        }

        private void SeedPosBiaya(FinoDBContext context, int siswa_id, int biaya_id, string deskripsi, DateTime jTempo, double nilai, double potongan, bool isPersen, double nilaiPotongan, bool isPaid, bool isActive, DateTime? datePaid, int status_id)
        {
            context.PosBiayas.Add(new PosBiaya
            {
                Siswa_Id = siswa_id,
                Biaya_Id = biaya_id,
                Deskripsi = deskripsi,
                JTempo = jTempo,
                Nilai = nilai,
                Potongan = potongan,
                IsPersen = isPersen,
                NilaiPotongan = nilaiPotongan,
                IsPaid = isPaid,
                IsActive = isActive,
                DatePaid = datePaid,
                Status_Id = status_id
            });
        }

        private RefRekening SeedRefRekening(FinoDBContext context, string rekening_code, int status, DateTime tgl_buka)
        {
            RefRekening rekening = new RefRekening
            {
                rekening_code = rekening_code,
                status = status,
                tgl_buka = tgl_buka
            };

            context.Rekenings.Add(rekening);

            return rekening;
        }

        private void SeedSiswaRekening(FinoDBContext context, int siswa_id, RefRekening rekening)
        {
            context.SiswaRekenings.Add(new SiswaRekening
            {
                siswa_id = siswa_id,
                Rekening = rekening
            });
        }

        private void SeedRefAccountDummy(FinoDBContext context, string account_code, string account_name)
        {
            context.RefAccounts.Add(new RefAccount
                {
                    account_code = account_code,
                    account_name = account_name
                });
        }

        private void SeedAccountingPeriod(FinoDBContext context)
        {
            var year = DateTime.Now.Year;
            for (int i = year; i < year + 10; i++)
            {
                for (int month = 1; month <= 12; month++)
                {
                    var periodStart = new DateTime(i, month, 1);
                    var periodEnd = periodStart.AddMonths(1).AddDays(-1);
                    var now = DateTime.Now;
                    var isCurrent = periodStart <= now && periodEnd >= now;
                    var isPast = periodEnd < now;
                    var period = new DateTime(i, month, 1);
                    context.RefAccPeriods.Add(new RefAccPeriod
                    {
                        period_name = string.Format("Periode {0}", period.ToString("MMM yyyy")),
                        period_start = periodStart,
                        period_end = periodEnd,
                        status = isCurrent ? (int)RefAccPeriodStatusEnum.Opened : (isPast ? (int)RefAccPeriodStatusEnum.Closed : (int)RefAccPeriodStatusEnum.New)
                    });
                }
            }
        }

        private RefAccount SeedRefAccount(FinoDBContext context, string p_Code, string p_Name)
        {
            var newAcc = new RefAccount
            {
                account_code = p_Code,
                account_name = p_Name
            };
            context.RefAccounts.Add(newAcc);

            return newAcc;
        }

        private void SeedAccountClassSetting(AccountClassificationEnum p_AccountClass, int p_ParamId, RefAccount p_Account)
        {
            var newSetting = new AccountClassSetting
            {
                acc_class_id = (int)p_AccountClass,
                param_id = p_ParamId
            };
            p_Account.AccountBiayaSetting = newSetting;
        }
    }
}
