﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditRefSiswaView : BaseForm, IEditRefSiswaView
    {
        #region Events

        public event Action BtnBatalClicked;
        public event Action BtnSimpanClicked;

        #endregion Events

        #region Public Properties

        public SiswaExplorerModel SelectedSiswaExplorer { get; private set; }
        public ISiswaExplorerView SiswaExplorerView { get; set; }
        public EditRefSiswaModel SelectedSiswa { get; private set; }

        #endregion Public Properties

        #region Constructors

        public EditRefSiswaView()
        {
            InitializeComponent();

            this.btnSimpan.Click += btnSimpan_Click;
            this.btnBatal.Click += btnBatal_Click;
        }

        void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        #endregion Constructors

        public void SetBinding()
        {
            txtKodeSiswa.DataBindings.Add("Text", SelectedSiswa, EditRefSiswaModel.KodeSiswaPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);

            txtNamaSiswa.DataBindings.Add("Text", SelectedSiswa, EditRefSiswaModel.NamaSiswaPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void ClearData()
        {
            
        }

        public void ShowForm(SiswaExplorerModel selectedSiswaExplorerModel)
        {
            SelectedSiswa = new EditRefSiswaModel
            {
                SiswaId = selectedSiswaExplorerModel.SiswaId,
                KodeSiswa = selectedSiswaExplorerModel.SiswaCode,
                NamaSiswa = selectedSiswaExplorerModel.NamaSiswa,
                JeniKelamin  = true
            };

            this.Show();
        }
    }
}
