﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class RefPayableModel : BaseModel
    {
        const string NAMA_NOTEMPTY = "VLD_REFPAYABLENAME_NOTEMPTY";
        const string NAMA_LENGTH = "VLD_REFPAYABLENAME_LENGTH";
        const string CODE_NOTEMPTY = "VLD_REFPAYABLECODE_NOTEMPTY";
        const string CODE_LENGTH = "VLD_REFPAYABLECODE_LENGTH";

        public static readonly string RefPayableIdPropertyName = "RefpayableId";
        public static readonly string NamePropertyName = "Nama";
        public static readonly string CodePropertyName = "Code";
        public static readonly string AktifPropertyName = "Aktif";

        int refpayableId;
        string name;
        string code;
        bool aktif;

        public int RefpayableId
        {
            get { return refpayableId; }
            set { refpayableId = value; }
        }

        [Display(Name = "Nama")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NAMA_NOTEMPTY)]
        [StringLength(25, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NAMA_LENGTH)]
        public string Nama
        {
            get 
            { 
                return name; 
            }
            set 
            { 
                name = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamePropertyName));
                ValidateProperty(value, NamePropertyName);
            }
        }

        [Display(Name = "Code")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = CODE_NOTEMPTY)]
        [StringLength(10, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = CODE_LENGTH)]
        public string Code
        {
            get 
            { 
                return code; 
            }
            set 
            { 
                code = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(CodePropertyName));
                ValidateProperty(value, CodePropertyName);
            }
        }        

        public bool Aktif
        {
            get 
            { 
                return aktif; 
            }
            set 
            { 
                aktif = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(AktifPropertyName));                
            }
        }
    }
}
