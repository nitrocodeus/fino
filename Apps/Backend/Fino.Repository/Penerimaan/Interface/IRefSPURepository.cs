﻿using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.Repository
{
    public interface IRefSPURepository : IRepository
    {
        void DeleteSPU(RefSumberPUModel entity);
        List<RefSumberPUModel> GetAllRefSPU();
        RefSumberPUModel GetVendor(int refSPUId);
        void UpdateSPU(RefSumberPUModel entity);
    }
}
