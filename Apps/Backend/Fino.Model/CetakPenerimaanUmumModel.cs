﻿using Fino.Lib.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class CetakPenerimaanUmumModel
    {
        int _notaPenerimaanUmumId;        
        string _alamat;        
        string _nama;
        string _kelas;
        DateTime _tanggalPenerimaan;
        string _tahunAjaran;
        string _deskripsi;
        double _totalJumlah;
        UserModel _user;
        int _sumberPenerimaanId;

        public UserModel User
        {
            get { return _user; }
            set { _user = value; }
        }

        public int SumberPenerimaanId
        {
            get { return _sumberPenerimaanId; }
            set { _sumberPenerimaanId = value; }
        }

        public string TotalTerbilang
        {
            get 
            {                
                return ((int)_totalJumlah).ToText(); 
            }
        }
        
        public double TotalJumlah
        {
            get { return _totalJumlah; }
            set { _totalJumlah = value; }
        }

        public string JenisPenerimaan { get; set; }

        public string Alamat
        {
            get { return _alamat; }
            set { _alamat = value; }
        }

        public int NotaPenerimaanUmumId
        {
            get { return _notaPenerimaanUmumId; }
            set { _notaPenerimaanUmumId = value; }
        }

        public string TahunAjaran
        {
            get { return _tahunAjaran; }
            set { _tahunAjaran = value; }
        }

        public string NoPenerimaan
        {
            get 
            {
                return _notaPenerimaanUmumId.ToString("X6"); 
            }            
        }

        public string Nama
        {
            get { return _nama; }
            set { _nama = value; }
        }

        public string Kelas
        {
            get { return _kelas; }
            set { _kelas = value; }
        }        

        public DateTime TanggalPenerimaan
        {
            get { return _tanggalPenerimaan; }
            set { _tanggalPenerimaan = value; }
        }

        public string Deskripsi
        {
            get { return _deskripsi; }
            set { _deskripsi = value; }
        }

    }
}
