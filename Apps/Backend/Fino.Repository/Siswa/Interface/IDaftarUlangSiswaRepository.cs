﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;
using Fino.Model;

namespace Fino.Repository
{
    public interface IDaftarUlangSiswaRepository : IRepository
    {
        DaftarUlangSiswaModel TambahDaftarUlang(DaftarUlangSiswaModel model);
        DaftarUlangSiswaModel SelectSiswaByCode(string pCode);
    }
}
