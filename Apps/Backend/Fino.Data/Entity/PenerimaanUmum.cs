﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class PenerimaanUmum
    {
        public int penerimaanumum_Id { get; set; }
        public int refsumberpenerimaan_id { get; set; }
        public int Biaya_Id { get; set; }
        public string Deskripsi { get; set; }
        public double Nilai { get; set; }
        public bool IsPaid { get; set; }
        public bool IsActive { get; set; }
        public DateTime? DatePaid { get; set; }
        public int Status_Id { get; set; }
        public DateTime? Status_Date { get; set; }
        public int? nota_penerimaan_id { get; set; }

        public virtual RefSumberPenerimaanUmum SumberPenerimaan { get; set; }
        public virtual RefBiaya Biaya { get; set; }
        public virtual NotaPenerimaanUmum NotaPenerimaanUmum { get; set; }
    }
}
