﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IReportSaldoView : IView
    {
        event Action BtnOkClicked;
        event Action BtnTutupClicked;

        DateTime SelectedBulan { get; }

        void ShowRPP(List<LaporanSaldoModel> p_Model);
    }
}
