﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using Fino.BusinessLogic;
using Fino.Presenter;

namespace Fino.Cient
{
    public partial class PendaftaranTabunganView : BaseForm, IPendaftaranTabunganView
    {
        public PendaftaranTabunganView()
        {
            InitializeComponent();
            pencarianSiswa.HideControl += pencarianSiswa_HideControl;
            InitialiseSelectSiswa();

        }

        public event Action SaveButtonClicked;
        public event Action SaveNextButtonClicked;
        public event Action NewDataButtonClicked;
        public event Action SearchSiswaButtonClicked;
        public event Action CloseButtonClicked;
        public event Action txtSiswaKeyPressed;
        public event Action SelectingSiswaDone;

        public PendaftaranTabunganModel ViewModel { get; set; }
        public bool IsSearchMode { get; set; }
        public int SiswaId { get; set; }

        public string NoInduk
        {
            get
            {
                return txtNoInduk.Text.Trim();
            }
            set
            {
                txtNoInduk.Text = value.Trim();
            }
        }

        public IPencarianSiswaView PencarianSiswaView
        {
            get { return pencarianSiswa; }
        }

        public string TextSearchSiswa
        {
            get
            {
                return txtSiswa.Text;
            }
        }

        public string NamaSiswa
        {
            get
            {
                return txtNamaSiswa.Text.Trim();
            }
            set
            {
                txtNamaSiswa.Text = value.Trim();
            }
        }

        public bool JenisKelamin
        {
            get
            {
                return rbLaki.Checked;
            }
            set
            {
                rbLaki.Checked = value;
            }
        }

        public double NilaiSetoran
        {
            get
            {
                return (double)nmNilai.Value;
            }
            set
            {
                nmNilai.Value = (decimal)value;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
            {
                SaveButtonClicked();
            }
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveNextButtonClicked != null)
            {
                SaveNextButtonClicked();
            }
        }


        public void ClearView()
        {
            SetBinding();

        }

        private void InitialiseSelectSiswa()
        {
            SiswaProcess processSiswa = new SiswaProcess();
            PencarianSiswaPresenter presenterSiswa = new PencarianSiswaPresenter(pencarianSiswa, processSiswa);
        }

        public void ShowErrorMessage(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowInformation(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void pencarianSiswa_HideControl()
        {
            if (SelectingSiswaDone != null && pencarianSiswa.SelectedSiswa != null)
            {
                SelectingSiswaDone();
            }
        }

        public void SetBinding()
        {
            try
            {
                txtSiswa.Text = string.Empty;

                txtNoInduk.DataBindings.Clear();
                txtNoInduk.DataBindings.Add("Text", ViewModel, PendaftaranTabunganModel.SiswaCodePropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                txtNamaSiswa.DataBindings.Clear();
                txtNamaSiswa.DataBindings.Add("Text", ViewModel, PendaftaranTabunganModel.SiswaNamaPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                rbLaki.Checked = ViewModel.jkelamin;
                rbPerempuan.Checked = !ViewModel.jkelamin;

                nmNilai.DataBindings.Clear();
                nmNilai.DataBindings.Add("Value", ViewModel, PendaftaranTabunganModel.MutasiNilaiPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                txtRekNo.Text = string.IsNullOrEmpty(ViewModel.rekening_code) ? string.Empty : ViewModel.rekening_code;
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message, "Error Binding");
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (NewDataButtonClicked != null)
            {
                NewDataButtonClicked();
            }
        }


        public void ShowHeader()
        {
            throw new NotImplementedException();
        }

        public void SetSearchMode()
        {
            txtNoInduk.ReadOnly = false;
            txtSiswa.ReadOnly = false;

            ControlHelper.ChangeEditable(false, gbSiswa, new string[] { "txtSiswa" });
            ControlHelper.ChangeEditable(false, gbTabungan);
            ControlHelper.ChangeEditable(false, this, new string[] { "btnClose"});
            IsSearchMode = true;
            btnNew.Enabled = !IsSearchMode;
        }

        public void SetEntryMode()
        {
            ControlHelper.ChangeEditable(false, gbSiswa);
            ControlHelper.ChangeEditable(true, gbTabungan, new string[] {"txtRekNo"});
            ControlHelper.ChangeEditable(true, this);
            IsSearchMode = false;
            btnNew.Enabled = !IsSearchMode;
        }

        public void SetViewMode()
        {
            SetBinding();
            ControlHelper.ChangeEditable(false, gbSiswa);
            ControlHelper.ChangeEditable(false, gbTabungan);
            ControlHelper.ChangeEditable(false, this, new string[] { "btnClose" });
            btnNew.Enabled = !IsSearchMode;
        }

        private void btnSearchSiswa_Click(object sender, EventArgs e)
        {
            if (SearchSiswaButtonClicked != null)
            {
                SearchSiswaButtonClicked();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        private void txtSiswa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && txtSiswaKeyPressed != null)
            {
                txtSiswaKeyPressed();
            }
        }
    }
}
