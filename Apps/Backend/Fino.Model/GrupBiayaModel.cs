﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class GrupBiayaModel : BaseModel
    {
        const string NAMAGRUPBIAYA_NOTEMPTY = "VLD_NAMAGRUPBIAYA_NOTEMPTY";
        const string NAMAGRUPBIAYA_LENGTH = "VLD_NAMAGRUPBIAYA_LENGTH";
        const string KODEGRUPBIAYA_NOTEMPTY = "VLD_KODEGRUPBIAYA_NOTEMPTY";
        const string KODEGRUPBIAYA_LENGTH = "VLD_KODEGRUPBIAYA_LENGTH";

        public static readonly string GrupBiayaIdPropertyName = "GrupBiayaId";
        public static readonly string NamaPropertyName = "Nama";
        public static readonly string CodePropertyName = "Code";
        public static readonly string AktifPropertyName = "Aktif";

        int _grupBiayaId;
        string _nama;
        string _code;
        bool _aktif;

        public int GrupBiayaId 
        { 
            get
            {
                return _grupBiayaId;
            }
            set
            {
                _grupBiayaId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(GrupBiayaIdPropertyName));
            }
        }

        [Display(Name = "Nama grup biaya")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NAMAGRUPBIAYA_NOTEMPTY)]
        [StringLength(25, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NAMAGRUPBIAYA_LENGTH)]
        public string Nama 
        { 
            get
            {
                return _nama;
            }
            set
            {
                _nama = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamaPropertyName));
                ValidateProperty(value, NamaPropertyName);
            }
        }

        [Display(Name = "Kode grup biaya")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = KODEGRUPBIAYA_NOTEMPTY)]
        [StringLength(10, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = KODEGRUPBIAYA_LENGTH)]
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(CodePropertyName));
                ValidateProperty(value, CodePropertyName);
            }
        }
        
        public bool Aktif 
        {
            get
            {
                return _aktif;
            }
            set
            {
                _aktif = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(AktifPropertyName));
            }
        }

        public List<RefBiayaModel> RefBiayas { get; set; }
    }
}
