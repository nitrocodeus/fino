﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;

namespace Fino.Repository
{
    public class DaftarSiswaBaruRepository : BaseRepository, IDaftarSiswaBaruRepository
    {
        public DaftarSiswaBaruModel TambahSiswaBaru(DaftarSiswaBaruModel model)
        {
            var isExists = false;
            var returnModel = model;
            var siswaKelasList = new List<SiswaKelas>();
            IPosBiayaRepository posBiayaRepo = new PosBiayaRepository();
            IGrupBiayaRepository grupBiayaRepo = new GrupBiayaRepository();
            var siswaKelasEntity = new SiswaKelas
            {
                kelas_id = model.Kelas_Id,
                is_siswa_baru = true,
                mulai_tanggal = model.Mulai_Tanggal.Date,
                tahun_ajaran_id = model.TahunAjaran_Id,
                tgl_daftar = DateTime.Now,
                option_tingkat = model.OptionTingkat_Id
            };
            siswaKelasList.Add(siswaKelasEntity);

            var siswaEntity = new RefSiswa
            {
                nama = model.Nama,
                siswa_code = model.Code,
                jkelamin = model.JKelamin,
                SiswaKelas = siswaKelasList,
                PosBiayas = new List<PosBiaya>(),
                ispindahan = model.IsPindahan
            };

            isExists = Context.RefSiswas.Any(e => e.siswa_code.Equals(siswaEntity.siswa_code));
            if (!isExists)
            {
                
                // Get Tahun Ajaran range
                var ta = Context.RefTahunAjarans.Find(model.TahunAjaran_Id);
                var kelas = Context.RefKelases.Find(model.Kelas_Id);

                // Generate pos biaya here
                DateTime endOfTA = model.Mulai_Tanggal;
                List<RefBiaya> biayaList = null;
                grupBiayaRepo.Context = Context;

                if (ta.semcawu.Equals(1))
                {
                    biayaList = grupBiayaRepo.GetBiayaOfGrupCode(AppResource.SYS_GRUPBIAYA_SISWA_BARU);
                    var taNext = Context.RefTahunAjarans.Where(e => e.mulai_tahun.Equals(ta.mulai_tahun + 1) && e.hingga_tahun.Equals(ta.hingga_tahun + 1)).FirstOrDefault();
                    if (taNext != null)
                    {
                        endOfTA = new DateTime(taNext.hingga_tahun, taNext.hingga_bulan, model.Mulai_Tanggal.Day);
                    }
                    else
                    {
                        throw new ApplicationException(AppResource.MSG_SAVING_SEMESTER_2_NOTFOUND);
                    }
                }
                else
                {
                    if (model.IsPindahan)
                    {
                        biayaList = grupBiayaRepo.GetBiayaOfGrupCode(AppResource.SYS_GRUPBIAYA_PINDAHAN_PERTENGAHAN);
                        endOfTA = new DateTime(ta.hingga_tahun, ta.hingga_bulan, model.Mulai_Tanggal.Day);
                    }
                    else
                    {
                        throw new ApplicationException(AppResource.MSG_SAVING_SISWABARU_INVALID_SEMESTER);
                    }
                }

                foreach (var b in biayaList)
                {
                    posBiayaRepo.Context = Context;
                    int tingkat = 0;
                    if (siswaEntity.ispindahan)
                    {
                        if (siswaKelasEntity.option_tingkat > 0)
                        {
                            tingkat = siswaKelasEntity.option_tingkat;
                        }
                        else
                        {
                            throw new ApplicationException(AppResource.MSG_SAVING_PINDAHAN_WITHOUT_OPTION);
                        }
                    }
                    else
                    {
                        tingkat = kelas.tingkat;
                    }

                    siswaEntity = Context.RefSiswas.Add(siswaEntity);
                    posBiayaRepo.GeneratePosBiaya(model.Mulai_Tanggal.Date, endOfTA, tingkat, b, siswaEntity);
                }
                
                returnModel.Siswa_Id = siswaEntity.siswa_id;
            }
            else
            {
                throw new ApplicationException(AppResource.MSG_SAVING_SISWA_BARU_DUPLICATE);
            }

            Context.SaveChanges();
            return returnModel;
        }

        
    }
}
