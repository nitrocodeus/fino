﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IPembatalanPembayaranPelunasanProcess
    {
        IPosBiayaRepository PosBiayaRepo { get; set; }

        ProcessResult GetPembatalanPenerimaanPelunasanList(DateTime p_FromDate, DateTime p_ToDate);
        ProcessResult GetPosBiaya(int p_PosBiayaId);
        ProcessResult GetPosBiayaPaid(int p_PosBiayaId);
        ProcessResult UpdatePosBiaya(PosBiayaModel entity);
        ProcessResult DeletePosBiayaPaid(PosBiayaPaidModel entity);
        ProcessResult AddPosBiayaPaidCancel(PosBiayaPaidCancelModel entity);
    }
}
