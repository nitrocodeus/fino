﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Fino.Lib.Win32
{
    public class WinApi
    {
        [DllImport("user32")]
        public static extern int RegisterWindowMessage(string message);

        public static int RegisterWindowMessage(string format, params object[] args)
        {
            string message = String.Format(format, args);
            return RegisterWindowMessage(message);
        }

        public const int HWND_BROADCAST = 0xffff;
        public const int SW_SHOWNORMAL = 1;

        [DllImport("user32")]
        public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);

        [DllImportAttribute("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImportAttribute("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void ShowToFront(IntPtr window)
        {
            ShowWindow(window, SW_SHOWNORMAL);
            SetForegroundWindow(window);
        }

        public static void ShowErrorMessage(string p_Text, string p_Title)
        {
            MessageBox.Show(p_Text, p_Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowWarningMessage(string p_Text, string p_Title)
        {
            MessageBox.Show(p_Text, p_Title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void ShowInformationMessage(string p_Text, string p_Title)
        {
            MessageBox.Show(p_Text, p_Title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static DialogResult ShowConfirmationMessage(string p_Text, string p_Title)
        {
            return MessageBox.Show(p_Text, p_Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }
    }
}
