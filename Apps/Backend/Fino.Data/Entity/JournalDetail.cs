﻿
namespace Fino.Datalib.Entity
{
    public class JournalDetail
    {
        public int journaldetail_id { get; set; }
        public int journal_id { get; set; }
        public int account_id { get; set; }
        public int factor { get; set; }
        public double amount { get; set; }
        public string detail_name { get; set; }
        public int status { get; set; }

        public virtual JournalHeader Header { get; set; }
        public virtual RefAccount Account { get; set; }
        public virtual JournalGLAccount JournalAccountGL { get; set; }
    }

}
