﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class PencarianSiswaPresenter : BasePresenter
    {
        IPencarianSiswaView _view;
        ISiswaProcess _process;

        public PencarianSiswaPresenter(IPencarianSiswaView p_view, ISiswaProcess p_process) : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.Load += Load;
            _view.gvSiswaClicked += GvSiswaClicked;
            _view.CariRaised += CariRaised;
        }

        void CariRaised()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _process.GetSiswa(_view.StringSiswa)),
                        AppResource.MSG_LOADING_REF_BIAYA);            

            if (result.IsSucess)
            {
                List<SiswaModel> data = result.DataResult as List<SiswaModel>;

                _view.gvSiswaSource = data;
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        void GvSiswaClicked()
        {
            _view.Hide();
            _view.RaiseHideControl();
        }

        void Load(object sender, EventArgs e)
        {

        }
    }
}
