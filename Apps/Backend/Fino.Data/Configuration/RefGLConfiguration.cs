﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefGLConfiguration
    {
        internal static void ConfigureRefGL(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefGL>().HasKey(e => e.gl_id);
            modelBuilder.Entity<RefGL>().Property(e => e.gl_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RefGL>().Property(e => e.gl_name).HasMaxLength(100);

            modelBuilder.Entity<RefGL>()
                    .HasRequired(e => e.Period)
                    .WithMany(e => e.GeneralLedgers)
                    .HasForeignKey(e => e.period_id);
        }
    }
}
