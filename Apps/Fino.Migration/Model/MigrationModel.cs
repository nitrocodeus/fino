﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Migration.Model
{
    public class MigrationModel
    {
        public string Type { get; set; }
        public string SourceFile { get; set; }
        public string TableName { get; set; }
        public List<MappingModel> Mappings { get; set; }

        public MigrationModel()
        {
            Mappings = new List<MappingModel>();
        }
    }
}
