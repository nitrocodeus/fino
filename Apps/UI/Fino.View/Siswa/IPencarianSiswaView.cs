﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IPencarianSiswaView : IView
    {
        event Action CariRaised;
        event Action gvSiswaClicked;
        event Action HideControl;

        string StringSiswa { get; set; }

        bool Visible { get; set; }
        List<SiswaModel> gvSiswaSource { get; set; }
        SiswaModel SelectedSiswa { get; }

        void Cari();
        void RaiseHideControl();
        void ClearSelected();
    }
}
