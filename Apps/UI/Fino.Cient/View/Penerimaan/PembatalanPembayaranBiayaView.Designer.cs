﻿namespace Fino.Cient
{
    partial class PembatalanPembayaranBiayaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PembatalanPembayaranBiayaView));
            this.lblDari = new System.Windows.Forms.Label();
            this.lblSampai = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.btnCari = new System.Windows.Forms.Button();
            this.btnTutup = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBatal = new System.Windows.Forms.Button();
            this.gvPosBiaya = new System.Windows.Forms.DataGridView();
            this.posBiayaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.selectedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.posBiayaIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.siswaIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namaSiswaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deskripsiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datePaidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nilaiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.potonganDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jumlahDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.notaPembayaranIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jTempoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPosBiaya)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.posBiayaBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDari
            // 
            this.lblDari.AutoSize = true;
            this.lblDari.Location = new System.Drawing.Point(7, 32);
            this.lblDari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDari.Name = "lblDari";
            this.lblDari.Size = new System.Drawing.Size(99, 17);
            this.lblDari.TabIndex = 11;
            this.lblDari.Text = "Dari tanggal:";
            // 
            // lblSampai
            // 
            this.lblSampai.AutoSize = true;
            this.lblSampai.Location = new System.Drawing.Point(238, 32);
            this.lblSampai.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSampai.Name = "lblSampai";
            this.lblSampai.Size = new System.Drawing.Size(31, 17);
            this.lblSampai.TabIndex = 12;
            this.lblSampai.Text = "s/d";
            // 
            // dtFrom
            // 
            this.dtFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(114, 31);
            this.dtFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(116, 24);
            this.dtFrom.TabIndex = 0;
            // 
            // dtTo
            // 
            this.dtTo.CustomFormat = "dd-MMM-yyyy";
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(277, 31);
            this.dtTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(161, 24);
            this.dtTo.TabIndex = 1;
            // 
            // btnCari
            // 
            this.btnCari.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnCari.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCari.ForeColor = System.Drawing.Color.White;
            this.btnCari.Location = new System.Drawing.Point(446, 31);
            this.btnCari.Margin = new System.Windows.Forms.Padding(4);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(100, 28);
            this.btnCari.TabIndex = 2;
            this.btnCari.Text = "Cari";
            this.btnCari.UseVisualStyleBackColor = false;
            // 
            // btnTutup
            // 
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(554, 31);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(4);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(100, 28);
            this.btnTutup.TabIndex = 1;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.btnAll);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btnBatal);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(963, 136);
            this.panel1.TabIndex = 15;
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.ForeColor = System.Drawing.Color.Black;
            this.btnReset.Location = new System.Drawing.Point(149, 88);
            this.btnReset.Margin = new System.Windows.Forms.Padding(2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(110, 39);
            this.btnReset.TabIndex = 14;
            this.btnReset.Text = "Kosongkan";
            this.btnReset.UseVisualStyleBackColor = false;
            // 
            // btnAll
            // 
            this.btnAll.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAll.ForeColor = System.Drawing.Color.White;
            this.btnAll.Location = new System.Drawing.Point(11, 88);
            this.btnAll.Margin = new System.Windows.Forms.Padding(2);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(133, 39);
            this.btnAll.TabIndex = 15;
            this.btnAll.Text = "Pilih Semua";
            this.btnAll.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblDari);
            this.groupBox1.Controls.Add(this.btnTutup);
            this.groupBox1.Controls.Add(this.dtFrom);
            this.groupBox1.Controls.Add(this.lblSampai);
            this.groupBox1.Controls.Add(this.btnCari);
            this.groupBox1.Controls.Add(this.dtTo);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(931, 71);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informasi periode penerimaan pelunasan";
            // 
            // btnBatal
            // 
            this.btnBatal.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.ForeColor = System.Drawing.Color.White;
            this.btnBatal.Location = new System.Drawing.Point(265, 88);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 39);
            this.btnBatal.TabIndex = 0;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = false;
            // 
            // gvPosBiaya
            // 
            this.gvPosBiaya.AllowUserToAddRows = false;
            this.gvPosBiaya.AllowUserToDeleteRows = false;
            this.gvPosBiaya.AllowUserToResizeRows = false;
            this.gvPosBiaya.AutoGenerateColumns = false;
            this.gvPosBiaya.BackgroundColor = System.Drawing.Color.White;
            this.gvPosBiaya.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvPosBiaya.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectedDataGridViewCheckBoxColumn,
            this.posBiayaIdDataGridViewTextBoxColumn,
            this.siswaIdDataGridViewTextBoxColumn,
            this.namaSiswaDataGridViewTextBoxColumn,
            this.deskripsiDataGridViewTextBoxColumn,
            this.datePaidDataGridViewTextBoxColumn,
            this.nilaiDataGridViewTextBoxColumn,
            this.potonganDataGridViewTextBoxColumn,
            this.jumlahDataGridViewTextBoxColumn,
            this.notaPembayaranIdDataGridViewTextBoxColumn,
            this.jTempoDataGridViewTextBoxColumn,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvPosBiaya.DataSource = this.posBiayaBindingSource;
            this.gvPosBiaya.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvPosBiaya.Location = new System.Drawing.Point(0, 0);
            this.gvPosBiaya.Name = "gvPosBiaya";
            this.gvPosBiaya.RowHeadersVisible = false;
            this.gvPosBiaya.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvPosBiaya.Size = new System.Drawing.Size(963, 266);
            this.gvPosBiaya.TabIndex = 16;
            // 
            // posBiayaBindingSource
            // 
            this.posBiayaBindingSource.DataSource = typeof(Fino.Model.PembatalanPenerimaanPelunasanModel);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gvPosBiaya);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 136);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(963, 266);
            this.panel2.TabIndex = 17;
            // 
            // selectedDataGridViewCheckBoxColumn
            // 
            this.selectedDataGridViewCheckBoxColumn.DataPropertyName = "Selected";
            this.selectedDataGridViewCheckBoxColumn.Frozen = true;
            this.selectedDataGridViewCheckBoxColumn.HeaderText = "";
            this.selectedDataGridViewCheckBoxColumn.Name = "selectedDataGridViewCheckBoxColumn";
            this.selectedDataGridViewCheckBoxColumn.Width = 30;
            // 
            // posBiayaIdDataGridViewTextBoxColumn
            // 
            this.posBiayaIdDataGridViewTextBoxColumn.DataPropertyName = "PosBiaya_Id";
            this.posBiayaIdDataGridViewTextBoxColumn.HeaderText = "PosBiaya_Id";
            this.posBiayaIdDataGridViewTextBoxColumn.Name = "posBiayaIdDataGridViewTextBoxColumn";
            this.posBiayaIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.posBiayaIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // siswaIdDataGridViewTextBoxColumn
            // 
            this.siswaIdDataGridViewTextBoxColumn.DataPropertyName = "Siswa_Id";
            this.siswaIdDataGridViewTextBoxColumn.HeaderText = "Siswa_Id";
            this.siswaIdDataGridViewTextBoxColumn.Name = "siswaIdDataGridViewTextBoxColumn";
            this.siswaIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.siswaIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // namaSiswaDataGridViewTextBoxColumn
            // 
            this.namaSiswaDataGridViewTextBoxColumn.DataPropertyName = "Nama_Siswa";
            this.namaSiswaDataGridViewTextBoxColumn.HeaderText = "Nama Siswa";
            this.namaSiswaDataGridViewTextBoxColumn.Name = "namaSiswaDataGridViewTextBoxColumn";
            this.namaSiswaDataGridViewTextBoxColumn.ReadOnly = true;
            this.namaSiswaDataGridViewTextBoxColumn.Width = 250;
            // 
            // deskripsiDataGridViewTextBoxColumn
            // 
            this.deskripsiDataGridViewTextBoxColumn.DataPropertyName = "Deskripsi";
            this.deskripsiDataGridViewTextBoxColumn.HeaderText = "Nama Transaksi";
            this.deskripsiDataGridViewTextBoxColumn.Name = "deskripsiDataGridViewTextBoxColumn";
            this.deskripsiDataGridViewTextBoxColumn.ReadOnly = true;
            this.deskripsiDataGridViewTextBoxColumn.Width = 200;
            // 
            // datePaidDataGridViewTextBoxColumn
            // 
            this.datePaidDataGridViewTextBoxColumn.DataPropertyName = "DatePaid";
            dataGridViewCellStyle1.Format = "dd MMM yyyy";
            this.datePaidDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.datePaidDataGridViewTextBoxColumn.HeaderText = "Tanggal";
            this.datePaidDataGridViewTextBoxColumn.Name = "datePaidDataGridViewTextBoxColumn";
            this.datePaidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nilaiDataGridViewTextBoxColumn
            // 
            this.nilaiDataGridViewTextBoxColumn.DataPropertyName = "Nilai";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##";
            this.nilaiDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.nilaiDataGridViewTextBoxColumn.HeaderText = "Nilai";
            this.nilaiDataGridViewTextBoxColumn.Name = "nilaiDataGridViewTextBoxColumn";
            this.nilaiDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // potonganDataGridViewTextBoxColumn
            // 
            this.potonganDataGridViewTextBoxColumn.DataPropertyName = "Potongan";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##";
            this.potonganDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.potonganDataGridViewTextBoxColumn.HeaderText = "Potongan";
            this.potonganDataGridViewTextBoxColumn.Name = "potonganDataGridViewTextBoxColumn";
            this.potonganDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jumlahDataGridViewTextBoxColumn
            // 
            this.jumlahDataGridViewTextBoxColumn.DataPropertyName = "Jumlah";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##";
            this.jumlahDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.jumlahDataGridViewTextBoxColumn.HeaderText = "Jumlah";
            this.jumlahDataGridViewTextBoxColumn.Name = "jumlahDataGridViewTextBoxColumn";
            this.jumlahDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // notaPembayaranIdDataGridViewTextBoxColumn
            // 
            this.notaPembayaranIdDataGridViewTextBoxColumn.DataPropertyName = "NotaPembayaranId";
            dataGridViewCellStyle5.Format = "#;#;-";
            this.notaPembayaranIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.notaPembayaranIdDataGridViewTextBoxColumn.HeaderText = "No Nota Pembayaran";
            this.notaPembayaranIdDataGridViewTextBoxColumn.Name = "notaPembayaranIdDataGridViewTextBoxColumn";
            this.notaPembayaranIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jTempoDataGridViewTextBoxColumn
            // 
            this.jTempoDataGridViewTextBoxColumn.DataPropertyName = "JTempo";
            this.jTempoDataGridViewTextBoxColumn.HeaderText = "JTempo";
            this.jTempoDataGridViewTextBoxColumn.Name = "jTempoDataGridViewTextBoxColumn";
            this.jTempoDataGridViewTextBoxColumn.ReadOnly = true;
            this.jTempoDataGridViewTextBoxColumn.Visible = false;
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.ReadOnly = true;
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // PembatalanPembayaranBiayaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 402);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(971, 360);
            this.Name = "PembatalanPembayaranBiayaView";
            this.Text = "Cari Pembatalan Penerimaan Pelunasan";
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPosBiaya)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.posBiayaBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDari;
        private System.Windows.Forms.Label lblSampai;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Button btnCari;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView gvPosBiaya;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.BindingSource posBiayaBindingSource;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn posBiayaIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn siswaIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaSiswaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deskripsiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datePaidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nilaiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn potonganDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jumlahDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn notaPembayaranIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jTempoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
    }
}