﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class LaporanSaldoModel
    {
        public string Kelas { get; set; }
		public string NoRekening { get; set; }
        public string Siswa { get; set; }
        public double Setoran { get; set; }
        public double Penarikan { get; set; }
    }
}
