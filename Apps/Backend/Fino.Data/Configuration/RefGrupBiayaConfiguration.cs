﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefGrupBiayaConfiguration
    {
        internal static void ConfigureRefGrupBiaya(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefGrupBiaya>().HasKey(e => e.Grupbiaya_id);
            modelBuilder.Entity<RefGrupBiaya>().Property(e => e.Grupbiaya_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RefGrupBiaya>().Property(e => e.nama).HasMaxLength(70);
            modelBuilder.Entity<RefGrupBiaya>().Property(e => e.code).HasMaxLength(10);
        }
    }
}
