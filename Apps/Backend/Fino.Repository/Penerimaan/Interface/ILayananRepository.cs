﻿using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public interface ILayananRepository : IRepository
    {
        List<RefLayanan> GetAllLayanan();
        bool CheckPendaftaranLayanan(DaftarLayananModel p_model);
        DaftarLayananModel SimpanDaftarLayanan(DaftarLayananModel p_model);
    }
}
