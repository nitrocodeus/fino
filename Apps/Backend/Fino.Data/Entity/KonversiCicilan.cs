﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class KonversiCicilan
    {
        public int PosBiaya_Id { get; set; }
        public int Tenor { get; set; }
        public DateTime MulaiTanggal { get; set; }
        public double NilaiCicilan { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsActive { get; set; }
        public int Status_Id { get; set; }

        public virtual PosBiaya ConvertedPosBiaya { get; set; }
        public virtual ICollection<KonversiCicilanPosBiaya> KonversiCicilanPosBiayas { get; set; }
    }
}
