﻿using Fino.Lib.Core;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class AccountClassSettingProcess : IAccountClassSettingProcess
    {
        public IAccountClassSettingRepository AccountClassSettingRepo { get; set; }
        public IKonfigurasiBiayaRepository KonfigurasiBiayaRepo { get; set; }
        public IRefPayableRepository RefPayableRepo { get; set; }
        public IRefAccountRepository RefAccountRepo { get; set; }

        public ProcessResult GetAccountClassSetting(int accountId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = AccountClassSettingRepo.GetAccountClassSetting(accountId);
                result.IsSucess = true;
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllAccountClassSettingByAccClassId(int accClassId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var allAccClass = AccountClassSettingRepo.GetAllAccountClassSettingByAccClassId(accClassId);
                var allRefPayable = RefPayableRepo.RefPayableData();
                var allRefBiaya = KonfigurasiBiayaRepo.KonfigurasiBiayaData();

                foreach(var accClass in allAccClass)
                {
                    switch(accClass.AccClassId)
                    {
                        case (int)AccountClassificationEnum.PENDAPATAN:
                        case (int)AccountClassificationEnum.PIUTANG:
                            accClass.ParamName = allRefBiaya.Where(x => x.biaya_id == accClass.ParamId).Select(x => x.nama).FirstOrDefault();
                            break;
                        case (int)AccountClassificationEnum.BEBAN:
                            accClass.ParamName = allRefPayable.Where(x => x.refpayable_id == accClass.ParamId).Select(x => x.name).FirstOrDefault();
                            break;
                        case (int)AccountClassificationEnum.KAS:
                            accClass.ParamName = "Default";
                            break;
                        default:
                            accClass.ParamName = "";
                            break;
                    }
                }

                result.DataResult = allAccClass;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdateAccountClassSetting(Model.AccountClassSettingModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                AccountClassSettingRepo.UpdateAccountClassSetting(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult DeleteAccountClassSetting(Model.AccountClassSettingModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                AccountClassSettingRepo.DeleteAccountClassSetting(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAccountData()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = RefAccountRepo.GetAllRefAccount().Select(x => new { Id = x.Account_id, Value = string.Format("{0} - {1}", x.Account_code, x.Account_name) }).ToList();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetParamFromRefBiaya()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = KonfigurasiBiayaRepo.KonfigurasiBiayaData().Select(x => new { Id = x.biaya_id, Value = x.nama }).ToList();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetParamFromRefPayable()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = RefPayableRepo.RefPayableData().Select(x => new { Id = x.refpayable_id, Value = x.name }).ToList();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
