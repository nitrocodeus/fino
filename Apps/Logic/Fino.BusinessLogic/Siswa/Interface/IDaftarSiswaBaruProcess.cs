﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public interface IDaftarSiswaBaruProcess
    {
        ITahunAjaranRepository TahunAjaranRepo { get; set; }
        IDaftarSiswaBaruRepository DaftarSiswaBaruRepo { get; set; }
        IRefKelasRepository RefKelasRepo { get; set; }
        ProcessResult TambahSiswaBaru(DaftarSiswaBaruModel model);
        ProcessResult GetTahunAjaran(DateTime currentDate);
        ProcessResult GetAllKelas();
    }
}
