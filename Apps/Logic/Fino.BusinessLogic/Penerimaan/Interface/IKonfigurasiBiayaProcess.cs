﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IKonfigurasiBiayaProcess
    {
        IKonfigurasiBiayaRepository KonfigurasiBiayaRepo { get; set; }

        ProcessResult GetRefBiaya();

        ProcessResult GetRefBiaya(int p_Id);

        ProcessResult TambahRefBiaya(RefBiayaModel p_KonfigurasiBiayaModel);

        ProcessResult UpdateRefBiaya(RefBiayaModel p_KonfigurasiBiayaModel);

        ProcessResult DeleteRefBiaya(RefBiayaModel p_KonfigurasiBiayaModel);

        ProcessResult GetGrupBiaya();

        ProcessResult GetGrupBiaya(int p_Id);

        ProcessResult GetMappingBiaya(int p_Id);

        ProcessResult TambahGrupBiaya(GrupBiayaModel p_grupBiayaModel);

        ProcessResult UpdateGrupBiaya(GrupBiayaModel p_grupBiayaModel);

        ProcessResult UpdateMappingGrupBiaya(GrupBiayaModel p_grupBiayaModel);

        ProcessResult DeleteGrupBiaya(GrupBiayaModel p_grupBiayaModel);
    }
}
