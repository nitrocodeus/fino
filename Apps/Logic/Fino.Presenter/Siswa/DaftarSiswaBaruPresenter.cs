﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class DaftarSiswaBaruPresenter : BasePresenter
    {
        private readonly IDaftarSiswaBaruProcess _daftarSiswaBaruProcess;
        private readonly IDaftarSiswaBaruView _daftarSiswaBaruView;

        public DaftarSiswaBaruPresenter(IDaftarSiswaBaruProcess p_Process, IDaftarSiswaBaruView p_View) : base(p_View)
        {
            _daftarSiswaBaruProcess = p_Process;
            _daftarSiswaBaruView = p_View;
            _daftarSiswaBaruView.Load += p_View_Load;
            _daftarSiswaBaruView.PotonganButtonClicked += _daftarSiswaBaruView_PotonganButtonClicked;
            _daftarSiswaBaruView.JemputanButtonClicked += _daftarSiswaBaruView_JemputanButtonClicked;
            _daftarSiswaBaruView.SaveButtonClicked += _daftarSiswaBaruView_SaveButtonClicked;
            _daftarSiswaBaruView.SaveNextButtonClicked += _daftarSiswaBaruView_SaveNextButtonClicked;
            _daftarSiswaBaruView.NewDataButtonClicked += _daftarSiswaBaruView_NewDataButtonClicked;
            _daftarSiswaBaruView.CloseButtonClicked += _daftarSiswaBaruView_CloseButtonClicked;
            _daftarSiswaBaruView.PindahanCheckChanged += _daftarSiswaBaruView_PindahanCheckChanged;
        }

        void _daftarSiswaBaruView_PindahanCheckChanged()
        {
            _daftarSiswaBaruView.SetOptionEnable(_daftarSiswaBaruView.IsPindahan);
        }

        void _daftarSiswaBaruView_CloseButtonClicked()
        {
            _daftarSiswaBaruView.Close();
        }

        private void _daftarSiswaBaruView_NewDataButtonClicked()
        {
            AssignNewViewModel();
            _daftarSiswaBaruView.ClearView();
            _daftarSiswaBaruView.ChangeEditable(true);
            _daftarSiswaBaruView.SetOptionEnable(_daftarSiswaBaruView.IsPindahan);
        }

        void p_View_Load(object sender, EventArgs e)
        {
            _daftarSiswaBaruView.KelasDataSource = GetListKelasDatasource();
            _daftarSiswaBaruView.OptionDataSource = GetListTingkatDatasource();
            AssignNewViewModel();
            _daftarSiswaBaruView.ClearView();
            _daftarSiswaBaruView.SetBinding();
            _daftarSiswaBaruView.SetOptionEnable(_daftarSiswaBaruView.IsPindahan);
        }

        private object GetListTingkatDatasource()
        {
            var tingkatList = new List<ValueList>();
            var enumList = Enum.GetValues(typeof(KelasTingkatEnum)).OfType<KelasTingkatEnum>().ToList();
            for (int i = 1; i <= enumList.Count; i++)
            {
                tingkatList.Add(new ValueList
                {
                    Id = i,
                    Value = enumList[i - 1].ToString()
                });
            }

            return tingkatList;
        }

        private void AssignNewViewModel()
        {
            var refTA = (ValueList)_daftarSiswaBaruProcess.GetTahunAjaran(DateTime.Now).DataResult;
            _daftarSiswaBaruView.ViewModel = new DaftarSiswaBaruModel
            {
                TahunAjaran_Id = (int)refTA.Id,
                TahunAjaran_Nama = (string)refTA.Value,
                Mulai_Tanggal = DateTime.Now
            };
        }

        private object GetListKelasDatasource()
        {
            return _daftarSiswaBaruProcess.GetAllKelas().DataResult;
        }

        void _daftarSiswaBaruView_SaveNextButtonClicked()
        {
            if (SaveData())
            {
                AssignNewViewModel();
                _daftarSiswaBaruView.ChangeEditable(true);
                _daftarSiswaBaruView.ClearView();
                _daftarSiswaBaruView.SetOptionEnable(_daftarSiswaBaruView.IsPindahan);
            }
        }


        bool SaveData()
        {
            var saved = false;

            var model = _daftarSiswaBaruView.ViewModel;
            ProcessResult procResult = _daftarSiswaBaruProcess.TambahSiswaBaru(model);

            if (procResult.ProcessException != null)
            {
                _daftarSiswaBaruView.ShowErrorMessage(procResult.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
            else
            {
                saved = true;
                _daftarSiswaBaruView.ShowInformation(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.CAP_PROCESS_INFORMATION);
            }

            return saved;
        }

        void _daftarSiswaBaruView_SaveButtonClicked()
        {
            if (SaveData())
            {
                _daftarSiswaBaruView.ChangeEditable(false);
            }
        }

        void _daftarSiswaBaruView_JemputanButtonClicked()
        {
            throw new NotImplementedException();
        }

        void _daftarSiswaBaruView_PotonganButtonClicked()
        {
            throw new NotImplementedException();
        }


    }
}
