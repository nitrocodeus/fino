﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class MutasiTabunganPresenter : BasePresenter
    {
        private readonly IMutasiTabunganProcess _mutasiTabunganProcess;
        private readonly IMutasiTabunganView _mutasiTabunganView;

        public MutasiTabunganPresenter(IMutasiTabunganProcess p_Process, IMutasiTabunganView p_View) : base(p_View)
        {
            try
            {
                _mutasiTabunganProcess = p_Process;
                _mutasiTabunganView = p_View;

                _mutasiTabunganView.Load += p_View_Load;
                _mutasiTabunganView.CloseButtonClicked += _mutasiTabunganView_CloseButtonClicked;
                _mutasiTabunganView.SearchSiswaButtonClicked += _mutasiTabunganView_SearchSiswaButtonClicked;

                _mutasiTabunganView.NewDataButtonClicked += _mutasiTabunganView_NewDataButtonClicked;
                _mutasiTabunganView.SaveButtonClicked += _mutasiTabunganView_SaveButtonClicked;
                _mutasiTabunganView.SaveNextButtonClicked +=_mutasiTabunganView_SaveNextButtonClicked;
                _mutasiTabunganView.CariSiswaTextKeypressed += _mutasiTabunganView_CariSiswaTextKeypressed;
                _mutasiTabunganView.SelectingSiswaDone += _mutasiTabunganView_SelectingSiswaDone;

                AssignNewViewModel();
                _mutasiTabunganView.SetBinding();

                _mutasiTabunganView.SetSearchMode();

            }
            catch (Exception ex)
            {
                _mutasiTabunganView.ShowErrorMessage(ex.Message, AppResource.CAP_PROCESS_ERROR);
            }
        }

        void _mutasiTabunganView_SelectingSiswaDone()
        {
            _mutasiTabunganView.NoRekening = _mutasiTabunganView.PencarianSiswaView.SelectedSiswa.NoRekening;
            _mutasiTabunganView_SearchSiswaButtonClicked();
        }

        void _mutasiTabunganView_CariSiswaTextKeypressed()
        {
            _mutasiTabunganView.PencarianSiswaView.Visible = true;
            _mutasiTabunganView.PencarianSiswaView.StringSiswa = _mutasiTabunganView.Keyword;

            _mutasiTabunganView.PencarianSiswaView.Cari();
        }

        void _mutasiTabunganView_CloseButtonClicked()
        {
            _mutasiTabunganView.Close();
        }


        void _mutasiTabunganView_SearchSiswaButtonClicked()
        {            
            if (_mutasiTabunganView.IsSearchMode)
            {
                var procResult = _mutasiTabunganProcess.InitiateMutasiByNoRekening(_mutasiTabunganView.NoRekening);
                if (procResult != null && procResult.DataResult != null)
                {
                    AssignNewViewModel();
                    _mutasiTabunganView.ViewModel = (MutasiTabunganModel)procResult.DataResult;
                    _mutasiTabunganView.SetBinding();
                    _mutasiTabunganView.SetEntryMode();
                    _mutasiTabunganView.Keyword = string.Empty;
                }
                else
                {
                    _mutasiTabunganView.ShowInformation(AppResource.MSG_LOADING_ACCOUNTSISWA_NOT_FOUND, AppResource.CAP_PROCESS_INFORMATION);
                }


            }
            else
            {
                AssignNewViewModel();
                _mutasiTabunganView.ClearView();
                _mutasiTabunganView.SetSearchMode();
            }
        }

        private void _mutasiTabunganView_NewDataButtonClicked()
        {
            _mutasiTabunganView_SearchSiswaButtonClicked();
        }

        void p_View_Load(object sender, EventArgs e)
        {
            AssignNewViewModel();
            _mutasiTabunganView.ClearView();
            _mutasiTabunganView.JMutasiDatasource = GetListJenisTransaksiDatasource();
            _mutasiTabunganView.SetBinding();
            _mutasiTabunganView.SetSearchMode();
        }

        private void AssignNewViewModel()
        {
            _mutasiTabunganView.ViewModel = new MutasiTabunganModel();
        }

        void _mutasiTabunganView_SaveNextButtonClicked()
        {
            _mutasiTabunganView_SaveButtonClicked();
            _mutasiTabunganView_NewDataButtonClicked();
        }


        void SaveData()
        {
            var model = _mutasiTabunganView.ViewModel;
            ProcessResult procResult = _mutasiTabunganProcess.NewTransaksiRekening(model);

            if (procResult.ProcessException != null)
            {
                _mutasiTabunganView.ShowErrorMessage(procResult.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
            else
            {
                _mutasiTabunganView.SetViewMode();
                _mutasiTabunganView.ViewModel = (MutasiTabunganModel)procResult.DataResult;
                _mutasiTabunganView.ShowInformation(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.CAP_PROCESS_INFORMATION);
            }
        }

        void _mutasiTabunganView_SaveButtonClicked()
        {
            SaveData();
        }

        private object GetListJenisTransaksiDatasource()
        {
            var jMutasiList = new List<ValueList>();
            var enumList = Enum.GetValues(typeof(JenisMutasiEnum)).OfType<JenisMutasiEnum>().ToList();
            for (int i = 1; i <= enumList.Count; i++)
            {
                jMutasiList.Add(new ValueList
                {
                    Id = i,
                    Value = enumList[i - 1].ToString()
                });
            }

            return jMutasiList;
        }
    }
}
