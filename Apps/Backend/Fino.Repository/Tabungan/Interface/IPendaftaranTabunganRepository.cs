﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Model;

namespace Fino.Repository
{
    public interface IPendaftaranTabunganRepository : IRepository
    {
        PendaftaranTabunganModel NewRekening(PendaftaranTabunganModel p_Model);
    }
}
