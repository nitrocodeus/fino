﻿using Fino.Lib.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class CetakPembayaranModel
    {
        int _notaPembayaranId;        
        string _noInduk;
        int _siswaId;
        string _namaSiswa;
        string _kelas;
        DateTime _tanggalPembayaran;
        string _tahunAjaran;
        List<PosBiayaModel> _daftarPembayaran;
        int _totalJumlah;
        UserModel _user;

        public UserModel User
        {
            get { return _user; }
            set { _user = value; }
        }

        public string TotalTerbilang
        {
            get 
            {                
                return _totalJumlah.ToText(); 
            }
        }
        
        public int TotalJumlah
        {
            get { return _totalJumlah; }
            set { _totalJumlah = value; }
        }

        public int SiswaId
        {
            get { return _siswaId; }
            set { _siswaId = value; }
        }

        public int NotaPembayaranId
        {
            get { return _notaPembayaranId; }
            set { _notaPembayaranId = value; }
        }

        public string TahunAjaran
        {
            get { return _tahunAjaran; }
            set { _tahunAjaran = value; }
        }

        public string NoPembayaran
        {
            get 
            {
                return _notaPembayaranId.ToString("X6"); 
            }            
        }

        public string NoInduk
        {
            get { return _noInduk; }
            set { _noInduk = value; }
        }

        public string NamaSiswa
        {
            get { return _namaSiswa; }
            set { _namaSiswa = value; }
        }

        public string Kelas
        {
            get { return _kelas; }
            set { _kelas = value; }
        }        

        public DateTime TanggalPembayaran
        {
            get { return _tanggalPembayaran; }
            set { _tanggalPembayaran = value; }
        }

        public List<PosBiayaModel> DaftarPembayaran
        {
            get { return _daftarPembayaran; }
            set { _daftarPembayaran = value; }
        }

    }
}
