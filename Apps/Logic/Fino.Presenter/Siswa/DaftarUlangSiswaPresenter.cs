﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class DaftarUlangSiswaPresenter : BasePresenter
    {
        private readonly IDaftarUlangSiswaProcess _daftarUlangSiswaProcess;
        private readonly IDaftarUlangSiswaView _daftarUlangSiswaView;

        public DaftarUlangSiswaPresenter(IDaftarUlangSiswaProcess p_Process, IDaftarUlangSiswaView p_View) : base(p_View)
        {
            _daftarUlangSiswaProcess = p_Process;
            _daftarUlangSiswaView = p_View;
            _daftarUlangSiswaView.Load += p_View_Load;
            _daftarUlangSiswaView.PotonganButtonClicked += _daftarUlangSiswaView_PotonganButtonClicked;
            _daftarUlangSiswaView.SaveButtonClicked += _daftarUlangSiswaView_SaveButtonClicked;
            _daftarUlangSiswaView.SaveNextButtonClicked += _daftarUlangSiswaView_SaveNextButtonClicked;
            _daftarUlangSiswaView.NewDataButtonClicked += _daftarUlangSiswaView_NewDataButtonClicked;
            _daftarUlangSiswaView.SearchSiswaButtonClicked += _daftarUlangSiswaView_SearchSiswaButtonClicked;
            _daftarUlangSiswaView.SiswaPindahanCheckedChanged += _daftarUlangSiswaView_SiswaPindahanCheckedChanged;
            _daftarUlangSiswaView.CloseButtonClicked += _daftarUlangSiswaView_CloseButtonClicked;
            _daftarUlangSiswaView.CariSiswaTextKeypressed += _daftarUlangSiswaView_CariSiswaTextKeypressed;
            _daftarUlangSiswaView.SelectingSiswaDone += _daftarUlangSiswaView_SelectingSiswaDone;
        }

        void _daftarUlangSiswaView_SelectingSiswaDone()
        {
            _daftarUlangSiswaView.NoInduk = _daftarUlangSiswaView.PencarianSiswaView.SelectedSiswa.Code;
            _daftarUlangSiswaView_SearchSiswaButtonClicked();
        }

        void _daftarUlangSiswaView_CariSiswaTextKeypressed()
        {
            _daftarUlangSiswaView.PencarianSiswaView.Visible = true;
            _daftarUlangSiswaView.PencarianSiswaView.StringSiswa = _daftarUlangSiswaView.Keyword;

            _daftarUlangSiswaView.PencarianSiswaView.Cari();
        }

        void _daftarUlangSiswaView_CloseButtonClicked()
        {
            _daftarUlangSiswaView.Close();
        }

        void _daftarUlangSiswaView_SiswaPindahanCheckedChanged()
        {
            _daftarUlangSiswaView.SetOptionEnable(_daftarUlangSiswaView.IsPindahan);
        }

        void _daftarUlangSiswaView_SearchSiswaButtonClicked()
        {            
            if (_daftarUlangSiswaView.IsSearchMode)
            {
                var procResult = _daftarUlangSiswaProcess.GetSiswaByNoInduk(_daftarUlangSiswaView.NoInduk);
                if (procResult != null && procResult.DataResult != null )
                {
                    AssignNewViewModel();
                    var returnModel = (DaftarUlangSiswaModel)procResult.DataResult;
                    _daftarUlangSiswaView.ViewModel.Code = returnModel.Code;
                    _daftarUlangSiswaView.ViewModel.IsPindahan = returnModel.IsPindahan;
                    _daftarUlangSiswaView.ViewModel.JKelamin = returnModel.JKelamin;
                    _daftarUlangSiswaView.ViewModel.Nama = returnModel.Nama;
                    _daftarUlangSiswaView.ViewModel.Siswa_Id = returnModel.Siswa_Id;

                    _daftarUlangSiswaView.SetBinding();
                    _daftarUlangSiswaView.SetEntryMode();
                }
                else
                {
                    _daftarUlangSiswaView.ShowInformation(AppResource.MSG_LOADING_SISWA_NOT_FOUND, AppResource.CAP_PROCESS_INFORMATION);
                }
                
                _daftarUlangSiswaView.SetOptionEnable(_daftarUlangSiswaView.IsPindahan);
            }
            else
            {
                AssignNewViewModel();
                _daftarUlangSiswaView.ClearView();
                _daftarUlangSiswaView.SetSearchMode();
            }
        }

        private void _daftarUlangSiswaView_NewDataButtonClicked()
        {
            //AssignNewViewModel();
            //_daftarUlangSiswaView.ClearView();
            //_daftarUlangSiswaView.ChangeEditable(true);
            _daftarUlangSiswaView_SearchSiswaButtonClicked();
        }

        void p_View_Load(object sender, EventArgs e)
        {
            _daftarUlangSiswaView.KelasDataSource = GetListKelasDatasource();
            _daftarUlangSiswaView.OptionDataSource = GetListTingkatDatasource();
            AssignNewViewModel();
            _daftarUlangSiswaView.ClearView();
            _daftarUlangSiswaView.SetBinding();
            _daftarUlangSiswaView.SetSearchMode();
            _daftarUlangSiswaView.SetOptionEnable(_daftarUlangSiswaView.IsPindahan);
        }

        private void AssignNewViewModel()
        {
            var refTA = (ValueList)_daftarUlangSiswaProcess.GetTahunAjaran(DateTime.Now).DataResult;
            _daftarUlangSiswaView.ViewModel = new DaftarUlangSiswaModel
            {
                TahunAjaran_Id = (int)refTA.Id,
                TahunAjaran_Nama = (string)refTA.Value,
                Mulai_Tanggal = DateTime.Now
            };
        }

        private object GetListKelasDatasource()
        {
            return _daftarUlangSiswaProcess.GetAllKelas().DataResult;
        }

        void _daftarSiswaBaruView_SaveNextButtonClicked()
        {
            SaveData();
            AssignNewViewModel();
            _daftarUlangSiswaView.ChangeEditable(true);
            _daftarUlangSiswaView.ClearView();
        }

        private object GetListTingkatDatasource()
        {
            var tingkatList = new List<ValueList>();
            var enumList = Enum.GetValues(typeof(KelasTingkatEnum)).OfType<KelasTingkatEnum>().ToList();
            for(int i=1; i <= enumList.Count;i++ )
            {
                tingkatList.Add(new ValueList
                {
                    Id = i,
                    Value = enumList[i - 1].ToString()
                });
            }

            return tingkatList;
        }

        void _daftarUlangSiswaView_SaveNextButtonClicked()
        {
            SaveData();
            _daftarUlangSiswaView_SearchSiswaButtonClicked();
        }

        
        void SaveData()
        {
            var model = _daftarUlangSiswaView.ViewModel;
            ProcessResult procResult = _daftarUlangSiswaProcess.TambahDaftarUlang(model);

            if (procResult.ProcessException != null)
            {
                _daftarUlangSiswaView.ShowErrorMessage(procResult.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
            else
            {
                _daftarUlangSiswaView.ShowInformation(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.CAP_PROCESS_INFORMATION);
            }
        }

        void _daftarUlangSiswaView_SaveButtonClicked()
        {
            SaveData();
            _daftarUlangSiswaView.ChangeEditable(false);
        }


        void _daftarUlangSiswaView_PotonganButtonClicked()
        {
            throw new NotImplementedException();
        }
    }
}
