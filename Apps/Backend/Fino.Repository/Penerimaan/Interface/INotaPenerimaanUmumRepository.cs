﻿using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public interface INotaPenerimaanUmumRepository
    {
        void DeleteNotaPenerimaanUmum(int p_NotaPenerimaanUmumId);
    }
}
