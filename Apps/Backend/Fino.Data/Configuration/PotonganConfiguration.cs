﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class PotonganConfiguration
    {
        internal static void ConfigurePotongan(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Potongan>().HasKey(e => e.potongan_id);
            modelBuilder.Entity<Potongan>().Property(e => e.potongan_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Potongan>()
                .HasRequired(e => e.Biayas)
                .WithMany(e => e.Potongans)
                .HasForeignKey(e => e.biaya_id);

            modelBuilder.Entity<Potongan>()
                .HasRequired(e => e.Siswas)
                .WithMany(e => e.Potongans)
                .HasForeignKey(e => e.siswa_id);
        }
    }
}
