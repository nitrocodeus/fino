﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IRefAccountView : IView
    {
        event Action GvRefAccountDoubleClick;
        event Action BtnTambahClick;
        event Action BtnHapusClick;
        event Action BtnTutupClick;
        event Action ReloadData;

        void SetData(List<RefAccountModel> listRefAccount);
        void RefreshDataGrid();
        RefAccountModel GetSelectedRefAccount();

        void ReloadDataEvent();
        void ShowEditRefBiayaView();
        void ShowTambahRefBiayaView();
        void ShowErrorMessage(string message, string caption);
    }
}
