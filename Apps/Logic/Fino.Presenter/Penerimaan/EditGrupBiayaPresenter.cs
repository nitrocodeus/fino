﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Core.Messaging;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{    
    public class EditGrupBiayaPresenter : BasePresenter
    {
        IKonfigurasiBiayaProcess _process;
        IEditGrupBiayaView _view;

        public EditGrupBiayaPresenter(IEditGrupBiayaView p_view, IKonfigurasiBiayaProcess p_process) : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.Load += Load;
            _view.BtnBatalClicked += BtnBatalClicked;
            _view.BtnSimpanClicked += BtnSimpanClicked;
        }

        public void BtnSimpanClicked()
        {
            var context = new ValidationContext(_view.SelectedGrupBiaya, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(_view.SelectedGrupBiaya, context, results, true);

            if (isValid)
            {
                if (_view.SelectedGrupBiaya.GrupBiayaId == 0)
                {
                    Save();
                }
                else
                {
                    Update();
                }

                _view.SelectedGrupBiaya.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                _view.SelectedGrupBiaya.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        public void BtnBatalClicked()
        {
            _view.GrupBiayaView.ReloadDataEvent();
            _view.Close();
        }

        public void Load(object sender, EventArgs e)
        {
            if (_view.SelectedGrupBiaya != null)
            {
                _view.Nama = _view.SelectedGrupBiaya.Nama;
                _view.Kode = _view.SelectedGrupBiaya.Code;
            }

            _view.SelectedGrupBiaya.ErrorValidation += SelectedGrupBiaya_ErrorValidation;
            _view.SetBinding();
        }

        private void SelectedGrupBiaya_ErrorValidation(object sender, Lib.Core.CustomEventArgs<string> e)
        {
            WinApi.ShowErrorMessage(e.Data, AppResource.CAP_VALIDASI_ERROR);
        }

        private void Update()
        {
            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _process.UpdateGrupBiaya(_view.SelectedGrupBiaya)),
                        AppResource.MSG_SAVING_GRUP_BIAYA);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                _view.GrupBiayaView.ReloadDataEvent();
                _view.Close();

                MessageData messageData = new MessageData(MessageTypeEnum.WithKey, _view.SelectedGrupBiaya);
                Mediator.Instance.NotifyColleagues(MessageKey.UPDATE_CURRENT_SELECTED_DATA, messageData);
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        private void Save()
        {
            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _process.TambahGrupBiaya(_view.SelectedGrupBiaya)),
                        AppResource.MSG_SAVING_GRUP_BIAYA);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                _view.GrupBiayaView.ReloadDataEvent();
                _view.Close();

                MessageData messageData = new MessageData(MessageTypeEnum.WithKey, result.DataResult);
                Mediator.Instance.NotifyColleagues(MessageKey.RELOAD_DATA, messageData);

            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }
    }
}
