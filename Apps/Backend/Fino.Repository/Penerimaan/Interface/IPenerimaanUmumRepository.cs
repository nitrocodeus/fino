﻿using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.Repository
{
    public interface IPenerimaanUmumRepository : IRepository
    {
        PenerimaanUmumModel UpdatePenerimaanUmum(PenerimaanUmumModel entity);
        CetakPenerimaanUmumModel AddNotaPenerimaanUmum(CetakPenerimaanUmumModel entity, PenerimaanUmumModel penerimaanUmumModel);
        List<PembatalanPenerimaanUmumModel> GetPembatalanPenerimaanUmumList(DateTime p_FromDate, DateTime p_ToDate);
        void DeletePenerimaanUmum(PenerimaanUmumModel entity);
        PenerimaanUmumModel GetPenerimaanUmum(int p_PenerimaanUmumId);
    }
}
