﻿namespace Fino.Cient
{
    partial class RefBiayaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefBiayaView));
            this.btnTutup = new System.Windows.Forms.Button();
            this.btnHapus = new System.Windows.Forms.Button();
            this.btnTambah = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gvKonfigurasiBiaya = new System.Windows.Forms.DataGridView();
            this.posBiayaModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.biayaidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.repetisiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jttanggalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jtbulanDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aktifDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.mulaitanggalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsTambahan = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BisaDiCicil = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvKonfigurasiBiaya)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.posBiayaModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTutup
            // 
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(16, 108);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(138, 35);
            this.btnTutup.TabIndex = 2;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            this.btnTutup.Click += new System.EventHandler(this.btnTutup_Click);
            // 
            // btnHapus
            // 
            this.btnHapus.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnHapus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHapus.ForeColor = System.Drawing.Color.White;
            this.btnHapus.Location = new System.Drawing.Point(16, 62);
            this.btnHapus.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(138, 35);
            this.btnHapus.TabIndex = 1;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.UseVisualStyleBackColor = false;
            this.btnHapus.Click += new System.EventHandler(this.btnHapus_Click);
            // 
            // btnTambah
            // 
            this.btnTambah.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnTambah.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTambah.ForeColor = System.Drawing.Color.White;
            this.btnTambah.Location = new System.Drawing.Point(16, 18);
            this.btnTambah.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnTambah.Name = "btnTambah";
            this.btnTambah.Size = new System.Drawing.Size(138, 35);
            this.btnTambah.TabIndex = 0;
            this.btnTambah.Text = "Tambah";
            this.btnTambah.UseVisualStyleBackColor = false;
            this.btnTambah.Click += new System.EventHandler(this.btnTambah_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnTambah);
            this.panel1.Controls.Add(this.btnTutup);
            this.panel1.Controls.Add(this.btnHapus);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(170, 488);
            this.panel1.TabIndex = 2;
            // 
            // gvKonfigurasiBiaya
            // 
            this.gvKonfigurasiBiaya.AllowUserToAddRows = false;
            this.gvKonfigurasiBiaya.AllowUserToDeleteRows = false;
            this.gvKonfigurasiBiaya.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvKonfigurasiBiaya.AutoGenerateColumns = false;
            this.gvKonfigurasiBiaya.BackgroundColor = System.Drawing.Color.White;
            this.gvKonfigurasiBiaya.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvKonfigurasiBiaya.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.biayaidDataGridViewTextBoxColumn,
            this.codeDataGridViewTextBoxColumn,
            this.namaDataGridViewTextBoxColumn,
            this.repetisiDataGridViewTextBoxColumn,
            this.jttanggalDataGridViewTextBoxColumn,
            this.jtbulanDataGridViewTextBoxColumn,
            this.aktifDataGridViewCheckBoxColumn,
            this.mulaitanggalDataGridViewTextBoxColumn,
            this.IsTambahan,
            this.BisaDiCicil,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvKonfigurasiBiaya.DataSource = this.posBiayaModelBindingSource;
            this.gvKonfigurasiBiaya.Location = new System.Drawing.Point(182, 14);
            this.gvKonfigurasiBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.gvKonfigurasiBiaya.Name = "gvKonfigurasiBiaya";
            this.gvKonfigurasiBiaya.ReadOnly = true;
            this.gvKonfigurasiBiaya.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvKonfigurasiBiaya.Size = new System.Drawing.Size(784, 460);
            this.gvKonfigurasiBiaya.TabIndex = 3;
            // 
            // posBiayaModelBindingSource
            // 
            this.posBiayaModelBindingSource.DataSource = typeof(Fino.Model.RefBiayaModel);
            // 
            // biayaidDataGridViewTextBoxColumn
            // 
            this.biayaidDataGridViewTextBoxColumn.DataPropertyName = "Biaya_id";
            this.biayaidDataGridViewTextBoxColumn.HeaderText = "Biaya_id";
            this.biayaidDataGridViewTextBoxColumn.Name = "biayaidDataGridViewTextBoxColumn";
            this.biayaidDataGridViewTextBoxColumn.ReadOnly = true;
            this.biayaidDataGridViewTextBoxColumn.Visible = false;
            // 
            // codeDataGridViewTextBoxColumn
            // 
            this.codeDataGridViewTextBoxColumn.DataPropertyName = "Code";
            this.codeDataGridViewTextBoxColumn.HeaderText = "Kode";
            this.codeDataGridViewTextBoxColumn.Name = "codeDataGridViewTextBoxColumn";
            this.codeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // namaDataGridViewTextBoxColumn
            // 
            this.namaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.namaDataGridViewTextBoxColumn.DataPropertyName = "Nama";
            this.namaDataGridViewTextBoxColumn.HeaderText = "Nama";
            this.namaDataGridViewTextBoxColumn.Name = "namaDataGridViewTextBoxColumn";
            this.namaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // repetisiDataGridViewTextBoxColumn
            // 
            this.repetisiDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.repetisiDataGridViewTextBoxColumn.DataPropertyName = "Repetisi";
            this.repetisiDataGridViewTextBoxColumn.HeaderText = "Repetisi";
            this.repetisiDataGridViewTextBoxColumn.Name = "repetisiDataGridViewTextBoxColumn";
            this.repetisiDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jttanggalDataGridViewTextBoxColumn
            // 
            this.jttanggalDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.jttanggalDataGridViewTextBoxColumn.DataPropertyName = "Jt_tanggal";
            this.jttanggalDataGridViewTextBoxColumn.HeaderText = "Tanggal jatuh tempo";
            this.jttanggalDataGridViewTextBoxColumn.Name = "jttanggalDataGridViewTextBoxColumn";
            this.jttanggalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jtbulanDataGridViewTextBoxColumn
            // 
            this.jtbulanDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.jtbulanDataGridViewTextBoxColumn.DataPropertyName = "Jt_bulan";
            this.jtbulanDataGridViewTextBoxColumn.HeaderText = "Bulan jatuh tempo";
            this.jtbulanDataGridViewTextBoxColumn.Name = "jtbulanDataGridViewTextBoxColumn";
            this.jtbulanDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // aktifDataGridViewCheckBoxColumn
            // 
            this.aktifDataGridViewCheckBoxColumn.DataPropertyName = "Aktif";
            this.aktifDataGridViewCheckBoxColumn.HeaderText = "Aktif";
            this.aktifDataGridViewCheckBoxColumn.Name = "aktifDataGridViewCheckBoxColumn";
            this.aktifDataGridViewCheckBoxColumn.ReadOnly = true;
            this.aktifDataGridViewCheckBoxColumn.Visible = false;
            // 
            // mulaitanggalDataGridViewTextBoxColumn
            // 
            this.mulaitanggalDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.mulaitanggalDataGridViewTextBoxColumn.DataPropertyName = "Mulai_tanggal";
            dataGridViewCellStyle1.Format = "dd-MMM-yy";
            this.mulaitanggalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.mulaitanggalDataGridViewTextBoxColumn.HeaderText = "Mulai tanggal";
            this.mulaitanggalDataGridViewTextBoxColumn.Name = "mulaitanggalDataGridViewTextBoxColumn";
            this.mulaitanggalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // IsTambahan
            // 
            this.IsTambahan.DataPropertyName = "IsTambahan";
            this.IsTambahan.FalseValue = "false";
            this.IsTambahan.HeaderText = "Tambahan";
            this.IsTambahan.Name = "IsTambahan";
            this.IsTambahan.ReadOnly = true;
            this.IsTambahan.TrueValue = "true";
            // 
            // BisaDiCicil
            // 
            this.BisaDiCicil.DataPropertyName = "BisaDiCicil";
            this.BisaDiCicil.FalseValue = "false";
            this.BisaDiCicil.HeaderText = "Bisa Dicicil";
            this.BisaDiCicil.Name = "BisaDiCicil";
            this.BisaDiCicil.ReadOnly = true;
            this.BisaDiCicil.TrueValue = "true";
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.ReadOnly = true;
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // RefBiayaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 488);
            this.Controls.Add(this.gvKonfigurasiBiaya);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "RefBiayaView";
            this.Text = "Jenis Penerimaan";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvKonfigurasiBiaya)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.posBiayaModelBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.Button btnTambah;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView gvKonfigurasiBiaya;
        private System.Windows.Forms.BindingSource posBiayaModelBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn biayaidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn repetisiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jttanggalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jtbulanDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn aktifDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mulaitanggalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsTambahan;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BisaDiCicil;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
    }
}