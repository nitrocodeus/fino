﻿using System;
using System.ComponentModel;
using Fino.Lib.Core;

namespace Fino.Model
{
    public class KonversiCicilanDetailModel: BaseModel
    {
        public static readonly string PosBiayaIdPropertyName = "PosBiayaId";
        public static readonly string NamaBiayaPropertyName = "NamaBiaya";
        public static readonly string JTempoPropertyName = "JTempo";
        public static readonly string NilaiBiayaPropertyName = "NilaiBiaya";

        private int _posBiayaId;
        private string _namaBiaya;
        private DateTime _jtempo;
        private double _nilaiBiaya;
        private int _refBiayaId;

        public int RefBiayaId
        {
            get { return _refBiayaId; }
            set { _refBiayaId = value; }
        }


        public int PosBiayaId
        {
            get
            {
                return _posBiayaId;
            }
            set
            {
                _posBiayaId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PosBiayaIdPropertyName));
            }
        }


        public string NamaBiaya
        {
            get
            {
                return _namaBiaya;
            }
            set
            {
                _namaBiaya = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamaBiayaPropertyName));
            }
        }

        public DateTime JTempo
        {
            get
            {
                return _jtempo;
            }
            set
            {
                _jtempo = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(JTempoPropertyName));
            }
        }

        public double NilaiBiaya
        {
            get
            {
                return _nilaiBiaya;
            }
            set
            {
                _nilaiBiaya = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NilaiBiayaPropertyName));
            }
        }

    }
}
