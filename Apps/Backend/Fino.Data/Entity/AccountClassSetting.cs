﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class AccountClassSetting
    {
        // public int acc_class_biaya_id { get; set; }
        public int acc_class_id { get; set; }
        public int account_id { get; set; }
        public int param_id { get; set; }

        public virtual RefAccount Account { get; set; }
    }
}
