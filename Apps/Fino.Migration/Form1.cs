﻿using Fino.Migration.Helper;
using Fino.Migration.Interface;
using Fino.Migration.Model;
using Fino.Migration.Processor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Migration
{
    public partial class Form1 : Form
    {
        private const string MIGRATION_CONFIG_PATH = @"Config\MigrationConfig.xml";

        private IMigrationEngine _migrationEngine = new MigrationEngine();

        public Form1()
        {
            InitializeComponent();

            _migrationEngine.InitializeEngine(Path.Combine(EnvironmentHelper.Instance.GetExecutingAssemblyDirectory(), MIGRATION_CONFIG_PATH));

            SetBinding();
        }

        private void SetBinding()
        {
            comboBoxMigrationType.DataSource = _migrationEngine.MigrationTypeDataSource;
            comboBoxMigrationType.DisplayMember = "Value";
            comboBoxMigrationType.ValueMember = "Id";
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            MigrationModel selectedMigrationModel = comboBoxMigrationType.SelectedValue as MigrationModel;

            if (selectedMigrationModel != null)
            {
                _migrationEngine.ExecuteMigration(selectedMigrationModel);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
