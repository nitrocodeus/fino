﻿namespace Fino.Cient
{
    partial class PenerimaanUmumView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PenerimaanUmumView));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDeskripsi = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnVendorBaru = new System.Windows.Forms.Button();
            this.nmNilai = new System.Windows.Forms.NumericUpDown();
            this.cboJenisPenerimaan = new System.Windows.Forms.ComboBox();
            this.cboSumberPenerimaan = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSaveNew = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNilai)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtDeskripsi);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.btnVendorBaru);
            this.groupBox2.Controls.Add(this.nmNilai);
            this.groupBox2.Controls.Add(this.cboJenisPenerimaan);
            this.groupBox2.Controls.Add(this.cboSumberPenerimaan);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(15, 14);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(743, 362);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detil Penerimaan";
            // 
            // txtDeskripsi
            // 
            this.txtDeskripsi.Location = new System.Drawing.Point(246, 114);
            this.txtDeskripsi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDeskripsi.MaxLength = 500;
            this.txtDeskripsi.Multiline = true;
            this.txtDeskripsi.Name = "txtDeskripsi";
            this.txtDeskripsi.Size = new System.Drawing.Size(315, 96);
            this.txtDeskripsi.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(129, 114);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "Deksripsi:";
            // 
            // btnVendorBaru
            // 
            this.btnVendorBaru.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnVendorBaru.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnVendorBaru.ForeColor = System.Drawing.Color.White;
            this.btnVendorBaru.Location = new System.Drawing.Point(569, 37);
            this.btnVendorBaru.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnVendorBaru.Name = "btnVendorBaru";
            this.btnVendorBaru.Size = new System.Drawing.Size(155, 30);
            this.btnVendorBaru.TabIndex = 1;
            this.btnVendorBaru.Text = "Sumber Baru";
            this.btnVendorBaru.UseVisualStyleBackColor = false;
            // 
            // nmNilai
            // 
            this.nmNilai.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nmNilai.Location = new System.Drawing.Point(246, 218);
            this.nmNilai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.nmNilai.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.nmNilai.Name = "nmNilai";
            this.nmNilai.Size = new System.Drawing.Size(252, 28);
            this.nmNilai.TabIndex = 11;
            this.nmNilai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmNilai.ThousandsSeparator = true;
            this.nmNilai.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // cboJenisPenerimaan
            // 
            this.cboJenisPenerimaan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJenisPenerimaan.FormattingEnabled = true;
            this.cboJenisPenerimaan.Location = new System.Drawing.Point(246, 76);
            this.cboJenisPenerimaan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboJenisPenerimaan.Name = "cboJenisPenerimaan";
            this.cboJenisPenerimaan.Size = new System.Drawing.Size(315, 28);
            this.cboJenisPenerimaan.TabIndex = 3;
            // 
            // cboSumberPenerimaan
            // 
            this.cboSumberPenerimaan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSumberPenerimaan.FormattingEnabled = true;
            this.cboSumberPenerimaan.Location = new System.Drawing.Point(246, 39);
            this.cboSumberPenerimaan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboSumberPenerimaan.Name = "cboSumberPenerimaan";
            this.cboSumberPenerimaan.Size = new System.Drawing.Size(315, 28);
            this.cboSumberPenerimaan.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(131, 220);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "Nilai (Rp):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 80);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Jenis Penerimaan:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 42);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sumber Penerimaan:";
            // 
            // btnSaveNew
            // 
            this.btnSaveNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveNew.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSaveNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSaveNew.ForeColor = System.Drawing.Color.White;
            this.btnSaveNew.Location = new System.Drawing.Point(227, 383);
            this.btnSaveNew.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSaveNew.Name = "btnSaveNew";
            this.btnSaveNew.Size = new System.Drawing.Size(243, 41);
            this.btnSaveNew.TabIndex = 2;
            this.btnSaveNew.Text = "Simpan dan Data Baru";
            this.btnSaveNew.UseVisualStyleBackColor = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Location = new System.Drawing.Point(623, 383);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(136, 41);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Tutup";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNew.Location = new System.Drawing.Point(478, 383);
            this.btnNew.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(136, 41);
            this.btnNew.TabIndex = 3;
            this.btnNew.Text = "Data Baru";
            this.btnNew.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(103, 383);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(116, 41);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Simpan";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // PenerimaanUmumView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 439);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnSaveNew);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PenerimaanUmumView";
            this.Text = "Penerimaan Umum";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNilai)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cboJenisPenerimaan;
        private System.Windows.Forms.ComboBox cboSumberPenerimaan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveNew;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.NumericUpDown nmNilai;
        private System.Windows.Forms.Button btnVendorBaru;
        private System.Windows.Forms.TextBox txtDeskripsi;
        private System.Windows.Forms.Label label5;
    }
}