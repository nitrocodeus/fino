﻿namespace Fino.Cient
{
    partial class DaftarUlangSiswaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DaftarUlangSiswaView));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSearchSiswa = new System.Windows.Forms.Button();
            this.chkPindahan = new System.Windows.Forms.CheckBox();
            this.dtpMulaiTanggal = new System.Windows.Forms.DateTimePicker();
            this.cboOpsi = new System.Windows.Forms.ComboBox();
            this.cboKelas = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rbPerempuan = new System.Windows.Forms.RadioButton();
            this.rbLaki = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamaSiswa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTahunAjaran = new System.Windows.Forms.TextBox();
            this.txtNoInduk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSaveNew = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtCari = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pencarianSiswaControlView1 = new Fino.Cient.Common.PencarianSiswaControlView();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtCari);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.btnSearchSiswa);
            this.groupBox2.Controls.Add(this.chkPindahan);
            this.groupBox2.Controls.Add(this.dtpMulaiTanggal);
            this.groupBox2.Controls.Add(this.cboOpsi);
            this.groupBox2.Controls.Add(this.cboKelas);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.rbPerempuan);
            this.groupBox2.Controls.Add(this.rbLaki);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtNamaSiswa);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtTahunAjaran);
            this.groupBox2.Controls.Add(this.txtNoInduk);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(17, 11);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(653, 413);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data Siswa";
            // 
            // btnSearchSiswa
            // 
            this.btnSearchSiswa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchSiswa.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchSiswa.Image")));
            this.btnSearchSiswa.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSearchSiswa.Location = new System.Drawing.Point(441, 28);
            this.btnSearchSiswa.Name = "btnSearchSiswa";
            this.btnSearchSiswa.Size = new System.Drawing.Size(63, 62);
            this.btnSearchSiswa.TabIndex = 2;
            this.btnSearchSiswa.Text = "Cari";
            this.btnSearchSiswa.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSearchSiswa.UseVisualStyleBackColor = true;
            this.btnSearchSiswa.Visible = false;
            this.btnSearchSiswa.Click += new System.EventHandler(this.btnSearchSiswa_Click);
            // 
            // chkPindahan
            // 
            this.chkPindahan.AutoSize = true;
            this.chkPindahan.Location = new System.Drawing.Point(180, 294);
            this.chkPindahan.Name = "chkPindahan";
            this.chkPindahan.Size = new System.Drawing.Size(167, 24);
            this.chkPindahan.TabIndex = 15;
            this.chkPindahan.Text = "Siswa pindahan";
            this.chkPindahan.UseVisualStyleBackColor = true;
            this.chkPindahan.CheckedChanged += new System.EventHandler(this.chkPindahan_CheckedChanged);
            // 
            // dtpMulaiTanggal
            // 
            this.dtpMulaiTanggal.CustomFormat = "dd-MMM-yyyy";
            this.dtpMulaiTanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMulaiTanggal.Location = new System.Drawing.Point(180, 222);
            this.dtpMulaiTanggal.Margin = new System.Windows.Forms.Padding(4);
            this.dtpMulaiTanggal.Name = "dtpMulaiTanggal";
            this.dtpMulaiTanggal.Size = new System.Drawing.Size(169, 28);
            this.dtpMulaiTanggal.TabIndex = 12;
            // 
            // cboOpsi
            // 
            this.cboOpsi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOpsi.FormattingEnabled = true;
            this.cboOpsi.Location = new System.Drawing.Point(176, 325);
            this.cboOpsi.Margin = new System.Windows.Forms.Padding(4);
            this.cboOpsi.Name = "cboOpsi";
            this.cboOpsi.Size = new System.Drawing.Size(134, 28);
            this.cboOpsi.TabIndex = 17;
            // 
            // cboKelas
            // 
            this.cboKelas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKelas.FormattingEnabled = true;
            this.cboKelas.Location = new System.Drawing.Point(180, 185);
            this.cboKelas.Margin = new System.Windows.Forms.Padding(4);
            this.cboKelas.Name = "cboKelas";
            this.cboKelas.Size = new System.Drawing.Size(252, 28);
            this.cboKelas.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 263);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Tahun Ajaran:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 328);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 20);
            this.label7.TabIndex = 16;
            this.label7.Text = "Penyetaraan:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 228);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Mulai Tanggal:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(102, 188);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Kelas:";
            // 
            // rbPerempuan
            // 
            this.rbPerempuan.AutoSize = true;
            this.rbPerempuan.Location = new System.Drawing.Point(301, 151);
            this.rbPerempuan.Margin = new System.Windows.Forms.Padding(4);
            this.rbPerempuan.Name = "rbPerempuan";
            this.rbPerempuan.Size = new System.Drawing.Size(127, 24);
            this.rbPerempuan.TabIndex = 8;
            this.rbPerempuan.TabStop = true;
            this.rbPerempuan.Text = "Perempuan";
            this.rbPerempuan.UseVisualStyleBackColor = true;
            // 
            // rbLaki
            // 
            this.rbLaki.AutoSize = true;
            this.rbLaki.Location = new System.Drawing.Point(180, 151);
            this.rbLaki.Margin = new System.Windows.Forms.Padding(4);
            this.rbLaki.Name = "rbLaki";
            this.rbLaki.Size = new System.Drawing.Size(102, 24);
            this.rbLaki.TabIndex = 7;
            this.rbLaki.TabStop = true;
            this.rbLaki.Text = "Laki-laki";
            this.rbLaki.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(79, 153);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Kelamin:";
            // 
            // txtNamaSiswa
            // 
            this.txtNamaSiswa.Location = new System.Drawing.Point(180, 113);
            this.txtNamaSiswa.Margin = new System.Windows.Forms.Padding(4);
            this.txtNamaSiswa.MaxLength = 25;
            this.txtNamaSiswa.Name = "txtNamaSiswa";
            this.txtNamaSiswa.Size = new System.Drawing.Size(252, 28);
            this.txtNamaSiswa.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 117);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nama Siswa:";
            // 
            // txtTahunAjaran
            // 
            this.txtTahunAjaran.Location = new System.Drawing.Point(180, 259);
            this.txtTahunAjaran.Margin = new System.Windows.Forms.Padding(4);
            this.txtTahunAjaran.MaxLength = 10;
            this.txtTahunAjaran.Name = "txtTahunAjaran";
            this.txtTahunAjaran.ReadOnly = true;
            this.txtTahunAjaran.Size = new System.Drawing.Size(377, 28);
            this.txtTahunAjaran.TabIndex = 14;
            // 
            // txtNoInduk
            // 
            this.txtNoInduk.Location = new System.Drawing.Point(180, 76);
            this.txtNoInduk.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoInduk.MaxLength = 10;
            this.txtNoInduk.Name = "txtNoInduk";
            this.txtNoInduk.Size = new System.Drawing.Size(169, 28);
            this.txtNoInduk.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 79);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "No Induk:";
            // 
            // btnSaveNew
            // 
            this.btnSaveNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveNew.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSaveNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSaveNew.ForeColor = System.Drawing.Color.White;
            this.btnSaveNew.Location = new System.Drawing.Point(138, 449);
            this.btnSaveNew.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveNew.Name = "btnSaveNew";
            this.btnSaveNew.Size = new System.Drawing.Size(243, 41);
            this.btnSaveNew.TabIndex = 2;
            this.btnSaveNew.Text = "Simpan dan Data Baru";
            this.btnSaveNew.UseVisualStyleBackColor = false;
            this.btnSaveNew.Click += new System.EventHandler(this.btnSaveNew_Click);
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNew.Location = new System.Drawing.Point(390, 449);
            this.btnNew.Margin = new System.Windows.Forms.Padding(4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(136, 41);
            this.btnNew.TabIndex = 3;
            this.btnNew.Text = "Data Baru";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(15, 449);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(116, 41);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Simpan";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Location = new System.Drawing.Point(534, 449);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(136, 41);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Tutup";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtCari
            // 
            this.txtCari.Location = new System.Drawing.Point(182, 28);
            this.txtCari.Margin = new System.Windows.Forms.Padding(4);
            this.txtCari.MaxLength = 25;
            this.txtCari.Name = "txtCari";
            this.txtCari.Size = new System.Drawing.Size(252, 28);
            this.txtCari.TabIndex = 1;
            this.txtCari.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCari_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(65, 31);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "Cari Siswa:";
            // 
            // pencarianSiswaControlView1
            // 
            this.pencarianSiswaControlView1.Font = new System.Drawing.Font("Verdana", 10F);
            this.pencarianSiswaControlView1.Location = new System.Drawing.Point(198, 70);
            this.pencarianSiswaControlView1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.pencarianSiswaControlView1.MdiParent = null;
            this.pencarianSiswaControlView1.Name = "pencarianSiswaControlView1";
            this.pencarianSiswaControlView1.Size = new System.Drawing.Size(452, 412);
            this.pencarianSiswaControlView1.StringSiswa = null;
            this.pencarianSiswaControlView1.TabIndex = 11;
            this.pencarianSiswaControlView1.Visible = false;
            // 
            // DaftarUlangSiswaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 503);
            this.Controls.Add(this.pencarianSiswaControlView1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnSaveNew);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DaftarUlangSiswaView";
            this.Text = "Daftar Ulang Siswa";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkPindahan;
        private System.Windows.Forms.DateTimePicker dtpMulaiTanggal;
        private System.Windows.Forms.ComboBox cboKelas;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbPerempuan;
        private System.Windows.Forms.RadioButton rbLaki;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNamaSiswa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTahunAjaran;
        private System.Windows.Forms.TextBox txtNoInduk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveNew;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cboOpsi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSearchSiswa;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtCari;
        private System.Windows.Forms.Label label8;
        private Common.PencarianSiswaControlView pencarianSiswaControlView1;
    }
}