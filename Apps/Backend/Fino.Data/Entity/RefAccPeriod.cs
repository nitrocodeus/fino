﻿using System;
using System.Collections.Generic;

namespace Fino.Datalib.Entity
{
    public class RefAccPeriod
    {
        public int period_id { get; set; }
        public string period_name { get; set; }
        public DateTime period_start { get; set; }
        public DateTime period_end { get; set; }
        public int status { get; set; }

        public virtual ICollection<JournalHeader> JournalHeaders { get; set; }
        public virtual ICollection<RefGL> GeneralLedgers { get; set; }
    }
}
