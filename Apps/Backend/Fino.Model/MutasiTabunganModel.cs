﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class MutasiTabunganModel
    {
        public static readonly string RekeningIdPropertyName = "rekening_id";
        public static readonly string RekeningCodePropertyName = "rekening_code";
        public static readonly string StatusPropertyName = "status";
        public static readonly string MutasiNilaiPropertyName = "MutasiNilai";

        public static readonly string SiswaIdPropertyName = "siswa_id";
        public static readonly string SiswaCodePropertyName = "siswa_code";
        public static readonly string SiswaNamaPropertyName = "siswa_nama";
        public static readonly string JKelaminPropertyName = "jkelamin";
        public static readonly string JMutasiPropertyName = "JenisMutasi";
        public static readonly string SaldoPropertyName = "Saldo";

        public int rekening_id { get; set; }
        public string rekening_code { get; set; }
        public int siswa_id { get; set; }
        public int status { get; set; }
        public string siswa_code { get; set; }
        public string siswa_nama { get; set; }
        public bool jkelamin { get; set; }
        public double Saldo { get; set; }

        
        double _mutasi_nilai;
        [Display(Name = "Nilai Setoran")]
        [Required]
        public double MutasiNilai
        {
            get
            {
                return _mutasi_nilai;
            }

            set
            {
                _mutasi_nilai = value;
            }
        }


        int _jenis_mutasi;
        [Display(Name = "Jenis Transaksi")]
        [Required]
        public int JenisMutasi
        {
            get
            {
                return _jenis_mutasi;
            }

            set
            {
                _jenis_mutasi = value;
            }
        }
    }
}
