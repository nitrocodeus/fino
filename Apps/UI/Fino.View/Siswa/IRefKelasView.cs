﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View.Siswa
{
    public interface IRefKelasView : IView
    {
        event Action GvRefKelasDoubleClick;
        event Action BtnTambahClick;
        event Action BtnHapusClick;
        event Action BtnTutupClick;
        event Action ReloadData;

        RefKelasModel ViewModel { get; set; }
        object TingkatDataObject { get; set; }

        void SetData(List<RefKelasModel> listRefKelas);
        void RefreshDataGrid();
        void SetBinding();
        RefKelasModel GetSelectedRefKelas();

        void ReloadDataEvent();
        void ShowEditRefKelasView();
        void ShowTambahRefKelasView();
        void ShowErrorMessage(string message, string caption);
    }
}
