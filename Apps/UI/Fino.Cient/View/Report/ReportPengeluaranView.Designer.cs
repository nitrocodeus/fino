﻿namespace Fino.Cient.Report
{
    partial class ReportPengeluaranView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportPengeluaranView));
            this.LaporanPengeluaranModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RekapitulasiPiutangDanPenerimaanModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblDari = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.btnTutup = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblSampai = new System.Windows.Forms.Label();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rptRekapPP = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.LaporanPengeluaranModelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RekapitulasiPiutangDanPenerimaanModelBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // LaporanPengeluaranModelBindingSource
            // 
            this.LaporanPengeluaranModelBindingSource.DataSource = typeof(Fino.Model.LaporanPengeluaranModel);
            // 
            // RekapitulasiPiutangDanPenerimaanModelBindingSource
            // 
            this.RekapitulasiPiutangDanPenerimaanModelBindingSource.DataSource = typeof(Fino.Model.RekapitulasiPiutangDanPenerimaanModel);
            // 
            // lblDari
            // 
            this.lblDari.AutoSize = true;
            this.lblDari.Location = new System.Drawing.Point(7, 20);
            this.lblDari.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblDari.Name = "lblDari";
            this.lblDari.Size = new System.Drawing.Size(123, 20);
            this.lblDari.TabIndex = 0;
            this.lblDari.Text = "Dari tanggal:";
            // 
            // dtFrom
            // 
            this.dtFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(146, 16);
            this.dtFrom.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(147, 28);
            this.dtFrom.TabIndex = 1;
            // 
            // btnTutup
            // 
            this.btnTutup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(705, 16);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(138, 35);
            this.btnTutup.TabIndex = 2;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            this.btnTutup.Click += new System.EventHandler(this.btnTutup_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(556, 16);
            this.btnOk.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(138, 35);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblSampai
            // 
            this.lblSampai.AutoSize = true;
            this.lblSampai.Location = new System.Drawing.Point(305, 22);
            this.lblSampai.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblSampai.Name = "lblSampai";
            this.lblSampai.Size = new System.Drawing.Size(37, 20);
            this.lblSampai.TabIndex = 0;
            this.lblSampai.Text = "s/d";
            // 
            // dtTo
            // 
            this.dtTo.CustomFormat = "dd-MMM-yyyy";
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(355, 18);
            this.dtTo.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(152, 28);
            this.dtTo.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.lblDari);
            this.panel1.Controls.Add(this.lblSampai);
            this.panel1.Controls.Add(this.btnTutup);
            this.panel1.Controls.Add(this.dtFrom);
            this.panel1.Controls.Add(this.dtTo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(876, 81);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rptRekapPP);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 81);
            this.panel2.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(876, 323);
            this.panel2.TabIndex = 4;
            // 
            // rptRekapPP
            // 
            this.rptRekapPP.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "LaporanPengeluaran";
            reportDataSource1.Value = this.LaporanPengeluaranModelBindingSource;
            this.rptRekapPP.LocalReport.DataSources.Add(reportDataSource1);
            this.rptRekapPP.LocalReport.ReportEmbeddedResource = "Fino.Cient.View.Report.LaporanPengeluaran.rdlc";
            this.rptRekapPP.Location = new System.Drawing.Point(0, 0);
            this.rptRekapPP.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.rptRekapPP.Name = "rptRekapPP";
            this.rptRekapPP.Size = new System.Drawing.Size(876, 323);
            this.rptRekapPP.TabIndex = 0;
            // 
            // ReportPengeluaranView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 404);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "ReportPengeluaranView";
            this.Text = "Laporan Pengeluaran Sekolah";
            this.Load += new System.EventHandler(this.RepRekapPPView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LaporanPengeluaranModelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RekapitulasiPiutangDanPenerimaanModelBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDari;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblSampai;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.Reporting.WinForms.ReportViewer rptRekapPP;
        private System.Windows.Forms.BindingSource RekapitulasiPiutangDanPenerimaanModelBindingSource;
        private System.Windows.Forms.BindingSource LaporanPengeluaranModelBindingSource;
    }
}