﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public interface IRefKelasRepository : IRepository
    {
        List<RefKelas> SelectAllKelas();
        RefKelas GetRefKelas(int kelasId);
        void UpdateRefKelas(RefKelas entity);
        void DeleteRefKelas(RefKelas entity);
    }
}
