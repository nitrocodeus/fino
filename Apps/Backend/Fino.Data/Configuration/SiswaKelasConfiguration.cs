﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class SiswaKelasConfiguration
    {
        internal static void ConfigureSiswaKelas(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SiswaKelas>().HasKey(e => e.siswa_kelas_id);
            modelBuilder.Entity<SiswaKelas>().Property(e => e.siswa_kelas_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<SiswaKelas>()
                .HasRequired(e => e.Siswa)
                .WithMany(e => e.SiswaKelas)
                .HasForeignKey(e => e.siswa_id);

            modelBuilder.Entity<SiswaKelas>()
                .HasRequired(e => e.Kelas)
                .WithMany(e => e.SiswaKelas)
                .HasForeignKey(e => e.kelas_id);

            modelBuilder.Entity<SiswaKelas>()
                .HasRequired(e => e.TahunAjaran)
                .WithMany(e => e.SiswaKelas)
                .HasForeignKey(e => e.tahun_ajaran_id);
        }
    }
}
