﻿using Excel;
using Fino.Migration.Constants;
using Fino.Migration.Helper;
using Fino.Migration.Interface;
using Fino.Migration.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Fino.Migration.Processor
{
    public class MigrationEngine : IMigrationEngine
    {
        private List<MigrationModel> _migrationList;
        private SqlCeConnection _conn;

        public object MigrationTypeDataSource
        {
            get
            {
                return (from x in _migrationList
                        select new
                            {
                                Value = x.Type,
                                Id = x
                            }).ToList();
            }
        }

        public MigrationEngine()
        {
            _migrationList = new List<MigrationModel>();
        }

        public void InitializeEngine(string migrationConfigFileName)
        {
            XDocument doc = XDocument.Load(migrationConfigFileName);

            XElement parentElement = doc.Element(XmlConstants.PARENT_ELEMENT_NAME);

            if (parentElement != null)
            {
                InitializeSqlCeConnection(parentElement);

                var migrationElements = parentElement.Elements(XmlConstants.MIGRATION_ELEMENT_NAME);

                foreach (var migrationElement in migrationElements)
                {
                    MigrationModel migrationModel = new MigrationModel();

                    migrationModel.Type = migrationElement.Attribute(XmlConstants.MIGRATION_ELEMENT_TYPE_ATTR_NAME).Value;
                    migrationModel.SourceFile = migrationElement.Attribute(XmlConstants.MIGRATION_ELEMENT_SOURCE_FILE_ATTR_NAME).Value;

                    var mappingsElement = migrationElement.Element(XmlConstants.MAPPINGS_ELEMENT_NAME);

                    if (mappingsElement != null)
                    {
                        migrationModel.TableName = mappingsElement.Attribute(XmlConstants.MAPPINGS_ELEMENT_TABLE_ATTR_NAME).Value;

                        var mappingElements = mappingsElement.Elements(XmlConstants.MAPPING_ELEMENT_NAME);

                        foreach (var mapping in mappingElements)
                        {
                            MappingModel mappingModel = new MappingModel
                                {
                                    Index = Int32.Parse(mapping.Attribute(XmlConstants.MAPPING_ELEMENT_INDEX_ATTR_NAME).Value),
                                    SourceColumn = mapping.Attribute(XmlConstants.MAPPING_ELEMENT_SOURCE_COLUMN_ATTR_NAME).Value,
                                    TargetColumn = mapping.Attribute(XmlConstants.MAPPING_ELEMENT_TARGET_COLUMN_ATTR_NAME).Value,
                                    DataType = mapping.Attribute(XmlConstants.MAPPING_ELEMENT_DATA_TYPE_ATTR_NAME).Value
                                };

                            var converters = mapping.Elements(XmlConstants.MAPPING_ELEMENT_CONVERTER);
                            ConverterModel converterModel = new ConverterModel();

                            foreach(var converter in converters)
                            {
                                converterModel.AddConverter(
                                    converter.Attribute(XmlConstants.MAPPING_ELEMENT_CONVERTER_SOURCE_VALUE_ATTR_NAME).Value,
                                    converter.Attribute(XmlConstants.MAPPING_ELEMENT_CONVERTER_TARGET_VALUE_ATTR_NAME).Value);
                            }
                            mappingModel.Converter = converterModel;

                            migrationModel.Mappings.Add(mappingModel);
                        }
                    }

                    _migrationList.Add(migrationModel);
                }
            }
        }

        public void ExecuteMigration(MigrationModel migrationModel)
        {
            FileStream stream = File.Open(migrationModel.SourceFile, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = null;

            string sourceFileExtension = Path.GetExtension(migrationModel.SourceFile);

            if (sourceFileExtension == ".xls")
            {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            }
            else if (sourceFileExtension == ".xlsx")
            {
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            }

            excelReader.IsFirstRowAsColumnNames = true;
            DataSet result = excelReader.AsDataSet();

            List<string[]> migrationDataList = new List<string[]>();

            for(int rowIndex = 0; rowIndex < result.Tables[0].Rows.Count; rowIndex++)
            {
                List<string> itemList = new List<string>();

                foreach (var mapping in migrationModel.Mappings)
                {
                    itemList.Add(result.Tables[0].Rows[rowIndex][mapping.Index].ToString());
                }

                migrationDataList.Add(itemList.ToArray());
            }

            excelReader.Close();

            foreach(var migrationData in migrationDataList)
            {
                SqlCeCommand comm = _conn.CreateCommand();
                comm.CommandText = ConstructQuery(migrationModel);

                for (int i = 0; i < migrationModel.Mappings.Count; i++)
                {
                    var mappingModel = migrationModel.Mappings[i];

                    if (mappingModel.Converter.Count > 0)
                    {
                        comm.Parameters.AddWithValue("@" + mappingModel.TargetColumn, DataTypeHelper.Instance.ConvertToDataType(mappingModel.Converter[migrationData[i]], mappingModel.DataType));
                    }
                    else
                    {
                        comm.Parameters.AddWithValue("@" + mappingModel.TargetColumn, DataTypeHelper.Instance.ConvertToDataType(migrationData[i], mappingModel.DataType));
                    }
                }
                int rowAffected = comm.ExecuteNonQuery();
                comm.Dispose();
            }

            _conn.Close();
        }

        #region private Methods

        private void InitializeSqlCeConnection(XElement parentElement)
        {
            XElement connStringElement = parentElement.Element(XmlConstants.CONNECTION_STRING_ELEMENT_NAME);

            if (connStringElement != null)
            {
                SqlCeConnectionStringBuilder connStringBuilder = new SqlCeConnectionStringBuilder();
                connStringBuilder.DataSource = connStringElement.Attribute(XmlConstants.CONNECTION_STRING_DATABASE_ATTR_NAME).Value;

                _conn = new SqlCeConnection(connStringBuilder.ConnectionString);
                _conn.Open();
            }
        }

        private string ConstructQuery(MigrationModel migrationModel)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(string.Format("INSERT INTO {0} (", migrationModel.TableName));

            List<string> colNames = new List<string>();
            List<string> paramList = new List<string>();

            foreach (var mapping in migrationModel.Mappings)
            {
                colNames.Add(mapping.TargetColumn);
                paramList.Add("@" + mapping.TargetColumn);
            }
            
            stringBuilder.Append(string.Join(",", colNames.ToArray()));
            stringBuilder.Append(") VALUES (");
            stringBuilder.Append(string.Join(",", paramList.ToArray()));
            stringBuilder.Append(")");

            return stringBuilder.ToString();
        }

        #endregion
    }
}
