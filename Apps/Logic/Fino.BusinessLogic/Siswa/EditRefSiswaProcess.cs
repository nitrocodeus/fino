﻿using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class EditRefSiswaProcess : IEditRefSiswaProcess
    {
        public IRefSiswaRepository RefSiswaRepo { get; set; }

        public ProcessResult UpdateSiswa(EditRefSiswaModel model)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefSiswaRepo.UpdateSiswa(new RefSiswa
                    {
                        siswa_id = model.SiswaId,
                        siswa_code = model.KodeSiswa,
                        nama = model.NamaSiswa,
                        jkelamin = model.JeniKelamin
                    });
                result.IsSucess = true;
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            RefSiswaRepo.DisposeDBContext();

            return result;
        }

        public ProcessResult GetSiswaByCode(string siswa_code)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var entity = RefSiswaRepo.GetSiswaByCode(siswa_code);

                if (entity != null)
                {
                    result.DataResult = new EditRefSiswaModel
                    {
                        SiswaId = entity.siswa_id,
                        KodeSiswa = entity.siswa_code,
                        NamaSiswa = entity.nama,
                        JeniKelamin = entity.jkelamin
                    };
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            RefSiswaRepo.DisposeDBContext();

            return result;
        }
    }
}
