﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IRepDailyJournalView : IView
    {
        event Action BtnOkClicked;
        event Action BtnTutupClicked;

        DateTime FromDate { get; set; }
        DateTime ToDate { get; set; }

        void ShowRPP(List<DailyJournalModel> p_Model);
    }
}
