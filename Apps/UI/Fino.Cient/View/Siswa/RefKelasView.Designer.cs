﻿namespace Fino.Cient
{
    partial class RefKelasView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefKelasView));
            this.gvRefKelas = new System.Windows.Forms.DataGridView();
            this.btnTambah = new System.Windows.Forms.Button();
            this.btnTutup = new System.Windows.Forms.Button();
            this.btnHapus = new System.Windows.Forms.Button();
            this.cboTingkat = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.refKelasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.kelasIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kelasCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tingkatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvRefKelas)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.refKelasBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gvRefKelas
            // 
            this.gvRefKelas.AllowUserToAddRows = false;
            this.gvRefKelas.AllowUserToDeleteRows = false;
            this.gvRefKelas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvRefKelas.AutoGenerateColumns = false;
            this.gvRefKelas.BackgroundColor = System.Drawing.Color.White;
            this.gvRefKelas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvRefKelas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kelasIdDataGridViewTextBoxColumn,
            this.kelasCodeDataGridViewTextBoxColumn,
            this.namaDataGridViewTextBoxColumn,
            this.tingkatDataGridViewTextBoxColumn,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvRefKelas.DataSource = this.refKelasBindingSource;
            this.gvRefKelas.Location = new System.Drawing.Point(187, 42);
            this.gvRefKelas.Margin = new System.Windows.Forms.Padding(5);
            this.gvRefKelas.Name = "gvRefKelas";
            this.gvRefKelas.ReadOnly = true;
            this.gvRefKelas.RowHeadersVisible = false;
            this.gvRefKelas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvRefKelas.Size = new System.Drawing.Size(357, 425);
            this.gvRefKelas.TabIndex = 11;
            // 
            // btnTambah
            // 
            this.btnTambah.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnTambah.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTambah.ForeColor = System.Drawing.Color.White;
            this.btnTambah.Location = new System.Drawing.Point(16, 17);
            this.btnTambah.Margin = new System.Windows.Forms.Padding(5);
            this.btnTambah.Name = "btnTambah";
            this.btnTambah.Size = new System.Drawing.Size(133, 34);
            this.btnTambah.TabIndex = 0;
            this.btnTambah.Text = "Tambah";
            this.btnTambah.UseVisualStyleBackColor = false;
            // 
            // btnTutup
            // 
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(16, 106);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(5);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(133, 34);
            this.btnTutup.TabIndex = 2;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            // 
            // btnHapus
            // 
            this.btnHapus.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnHapus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHapus.ForeColor = System.Drawing.Color.White;
            this.btnHapus.Location = new System.Drawing.Point(16, 62);
            this.btnHapus.Margin = new System.Windows.Forms.Padding(5);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(133, 34);
            this.btnHapus.TabIndex = 1;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.UseVisualStyleBackColor = false;
            // 
            // cboTingkat
            // 
            this.cboTingkat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTingkat.FormattingEnabled = true;
            this.cboTingkat.Location = new System.Drawing.Point(273, 7);
            this.cboTingkat.Margin = new System.Windows.Forms.Padding(4);
            this.cboTingkat.Name = "cboTingkat";
            this.cboTingkat.Size = new System.Drawing.Size(160, 24);
            this.cboTingkat.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(183, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Tingkat";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnTambah);
            this.panel1.Controls.Add(this.btnTutup);
            this.panel1.Controls.Add(this.btnHapus);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(165, 480);
            this.panel1.TabIndex = 10;
            // 
            // refKelasBindingSource
            // 
            this.refKelasBindingSource.DataSource = typeof(Fino.Model.RefKelasModel);
            // 
            // kelasIdDataGridViewTextBoxColumn
            // 
            this.kelasIdDataGridViewTextBoxColumn.DataPropertyName = "KelasId";
            this.kelasIdDataGridViewTextBoxColumn.HeaderText = "KelasId";
            this.kelasIdDataGridViewTextBoxColumn.Name = "kelasIdDataGridViewTextBoxColumn";
            this.kelasIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.kelasIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // kelasCodeDataGridViewTextBoxColumn
            // 
            this.kelasCodeDataGridViewTextBoxColumn.DataPropertyName = "KelasCode";
            this.kelasCodeDataGridViewTextBoxColumn.HeaderText = "Kode Kelas";
            this.kelasCodeDataGridViewTextBoxColumn.Name = "kelasCodeDataGridViewTextBoxColumn";
            this.kelasCodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.kelasCodeDataGridViewTextBoxColumn.Width = 150;
            // 
            // namaDataGridViewTextBoxColumn
            // 
            this.namaDataGridViewTextBoxColumn.DataPropertyName = "Nama";
            this.namaDataGridViewTextBoxColumn.HeaderText = "Nama";
            this.namaDataGridViewTextBoxColumn.Name = "namaDataGridViewTextBoxColumn";
            this.namaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tingkatDataGridViewTextBoxColumn
            // 
            this.tingkatDataGridViewTextBoxColumn.DataPropertyName = "Tingkat";
            this.tingkatDataGridViewTextBoxColumn.HeaderText = "Tingkat";
            this.tingkatDataGridViewTextBoxColumn.Name = "tingkatDataGridViewTextBoxColumn";
            this.tingkatDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.ReadOnly = true;
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // RefKelasView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 480);
            this.Controls.Add(this.gvRefKelas);
            this.Controls.Add(this.cboTingkat);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RefKelasView";
            this.Text = "Pengelolaan Kelas";
            ((System.ComponentModel.ISupportInitialize)(this.gvRefKelas)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.refKelasBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gvRefKelas;
        private System.Windows.Forms.Button btnTambah;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.ComboBox cboTingkat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.BindingSource refKelasBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn kelasIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kelasCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tingkatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
    }
}