﻿using Fino.Lib.Core;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Fino.Model
{
    public class RefVendorModel : BaseModel
    {
        public static readonly string VendorNamePropertyName = "VendorName";
        public static readonly string VendorAddrPropertyName = "VendorAddr";
        public static readonly string VendorEmailPropertyName = "VendorEmail";
        public static readonly string VendorPhonePropertyName = "VendorPhone";

        public int VendorId { get; set; }

        private string _vendorName;
        [Required]
        public string VendorName
        {
            get
            {
                return _vendorName;
            }
            set
            {
                _vendorName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(VendorNamePropertyName));
            }
        }

        private string _vendorAddr;
        [Required]
        public string VendorAddr
        {
            get
            {
                return _vendorAddr;
            }
            set
            {
                _vendorAddr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(VendorAddrPropertyName));
            }
        }

        private string _vendorEmail;
        [Required]
        public string VendorEmail
        {
            get
            {
                return _vendorEmail;
            }
            set
            {
                _vendorEmail = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(VendorEmailPropertyName));
            }
        }

        private string _vendorPhone;
        [Required]
        public string VendorPhone
        {
            get
            {
                return _vendorPhone;
            }
            set
            {
                _vendorPhone = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(VendorPhonePropertyName));
            }
        }
    }
}