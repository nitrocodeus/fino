﻿namespace Fino.Cient
{
    partial class RefAccPeriodView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefAccPeriodView));
            this.gvRefAccPeriod = new System.Windows.Forms.DataGridView();
            this.refAccPeriodModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnTambah = new System.Windows.Forms.Button();
            this.btnTutup = new System.Windows.Forms.Button();
            this.btnDeaktivasi = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnTutupPeriode = new System.Windows.Forms.Button();
            this.btnBukaPeriode = new System.Windows.Forms.Button();
            this.btnUbah = new System.Windows.Forms.Button();
            this.periodidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodstartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodendDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvRefAccPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refAccPeriodModelBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gvRefAccPeriod
            // 
            this.gvRefAccPeriod.AllowUserToAddRows = false;
            this.gvRefAccPeriod.AllowUserToDeleteRows = false;
            this.gvRefAccPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvRefAccPeriod.AutoGenerateColumns = false;
            this.gvRefAccPeriod.BackgroundColor = System.Drawing.Color.White;
            this.gvRefAccPeriod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvRefAccPeriod.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.periodidDataGridViewTextBoxColumn,
            this.periodnameDataGridViewTextBoxColumn,
            this.periodstartDataGridViewTextBoxColumn,
            this.periodendDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvRefAccPeriod.DataSource = this.refAccPeriodModelBindingSource;
            this.gvRefAccPeriod.Location = new System.Drawing.Point(137, 11);
            this.gvRefAccPeriod.Margin = new System.Windows.Forms.Padding(4);
            this.gvRefAccPeriod.Name = "gvRefAccPeriod";
            this.gvRefAccPeriod.ReadOnly = true;
            this.gvRefAccPeriod.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvRefAccPeriod.Size = new System.Drawing.Size(494, 368);
            this.gvRefAccPeriod.TabIndex = 5;
            // 
            // refAccPeriodModelBindingSource
            // 
            this.refAccPeriodModelBindingSource.DataSource = typeof(Fino.Model.RefAccPeriodModel);
            // 
            // btnTambah
            // 
            this.btnTambah.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnTambah.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTambah.ForeColor = System.Drawing.Color.White;
            this.btnTambah.Location = new System.Drawing.Point(12, 14);
            this.btnTambah.Margin = new System.Windows.Forms.Padding(4);
            this.btnTambah.Name = "btnTambah";
            this.btnTambah.Size = new System.Drawing.Size(100, 28);
            this.btnTambah.TabIndex = 0;
            this.btnTambah.Text = "Tambah";
            this.btnTambah.UseVisualStyleBackColor = false;
            // 
            // btnTutup
            // 
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(12, 220);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(4);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(100, 28);
            this.btnTutup.TabIndex = 5;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            // 
            // btnDeaktivasi
            // 
            this.btnDeaktivasi.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnDeaktivasi.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDeaktivasi.ForeColor = System.Drawing.Color.White;
            this.btnDeaktivasi.Location = new System.Drawing.Point(12, 184);
            this.btnDeaktivasi.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeaktivasi.Name = "btnDeaktivasi";
            this.btnDeaktivasi.Size = new System.Drawing.Size(100, 28);
            this.btnDeaktivasi.TabIndex = 4;
            this.btnDeaktivasi.Text = "Deaktivasi";
            this.btnDeaktivasi.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnTutupPeriode);
            this.panel1.Controls.Add(this.btnBukaPeriode);
            this.panel1.Controls.Add(this.btnUbah);
            this.panel1.Controls.Add(this.btnTambah);
            this.panel1.Controls.Add(this.btnTutup);
            this.panel1.Controls.Add(this.btnDeaktivasi);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(124, 390);
            this.panel1.TabIndex = 4;
            // 
            // btnTutupPeriode
            // 
            this.btnTutupPeriode.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnTutupPeriode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutupPeriode.ForeColor = System.Drawing.Color.White;
            this.btnTutupPeriode.Location = new System.Drawing.Point(12, 134);
            this.btnTutupPeriode.Margin = new System.Windows.Forms.Padding(4);
            this.btnTutupPeriode.Name = "btnTutupPeriode";
            this.btnTutupPeriode.Size = new System.Drawing.Size(100, 42);
            this.btnTutupPeriode.TabIndex = 3;
            this.btnTutupPeriode.Text = "Tutup Periode";
            this.btnTutupPeriode.UseVisualStyleBackColor = false;
            // 
            // btnBukaPeriode
            // 
            this.btnBukaPeriode.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnBukaPeriode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBukaPeriode.ForeColor = System.Drawing.Color.White;
            this.btnBukaPeriode.Location = new System.Drawing.Point(12, 84);
            this.btnBukaPeriode.Margin = new System.Windows.Forms.Padding(4);
            this.btnBukaPeriode.Name = "btnBukaPeriode";
            this.btnBukaPeriode.Size = new System.Drawing.Size(100, 42);
            this.btnBukaPeriode.TabIndex = 2;
            this.btnBukaPeriode.Text = "Buka Periode";
            this.btnBukaPeriode.UseVisualStyleBackColor = false;
            // 
            // btnUbah
            // 
            this.btnUbah.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnUbah.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUbah.ForeColor = System.Drawing.Color.White;
            this.btnUbah.Location = new System.Drawing.Point(12, 50);
            this.btnUbah.Margin = new System.Windows.Forms.Padding(4);
            this.btnUbah.Name = "btnUbah";
            this.btnUbah.Size = new System.Drawing.Size(100, 28);
            this.btnUbah.TabIndex = 1;
            this.btnUbah.Text = "Ubah";
            this.btnUbah.UseVisualStyleBackColor = false;
            // 
            // periodidDataGridViewTextBoxColumn
            // 
            this.periodidDataGridViewTextBoxColumn.DataPropertyName = "Period_id";
            this.periodidDataGridViewTextBoxColumn.HeaderText = "Period_id";
            this.periodidDataGridViewTextBoxColumn.Name = "periodidDataGridViewTextBoxColumn";
            this.periodidDataGridViewTextBoxColumn.ReadOnly = true;
            this.periodidDataGridViewTextBoxColumn.Visible = false;
            // 
            // periodnameDataGridViewTextBoxColumn
            // 
            this.periodnameDataGridViewTextBoxColumn.DataPropertyName = "Period_name";
            this.periodnameDataGridViewTextBoxColumn.HeaderText = "Nama Periode";
            this.periodnameDataGridViewTextBoxColumn.MinimumWidth = 130;
            this.periodnameDataGridViewTextBoxColumn.Name = "periodnameDataGridViewTextBoxColumn";
            this.periodnameDataGridViewTextBoxColumn.ReadOnly = true;
            this.periodnameDataGridViewTextBoxColumn.Width = 130;
            // 
            // periodstartDataGridViewTextBoxColumn
            // 
            this.periodstartDataGridViewTextBoxColumn.DataPropertyName = "Period_start";
            this.periodstartDataGridViewTextBoxColumn.HeaderText = "Mulai Periode";
            this.periodstartDataGridViewTextBoxColumn.MinimumWidth = 130;
            this.periodstartDataGridViewTextBoxColumn.Name = "periodstartDataGridViewTextBoxColumn";
            this.periodstartDataGridViewTextBoxColumn.ReadOnly = true;
            this.periodstartDataGridViewTextBoxColumn.Width = 130;
            // 
            // periodendDataGridViewTextBoxColumn
            // 
            this.periodendDataGridViewTextBoxColumn.DataPropertyName = "Period_end";
            this.periodendDataGridViewTextBoxColumn.HeaderText = "Akhir Periode";
            this.periodendDataGridViewTextBoxColumn.Name = "periodendDataGridViewTextBoxColumn";
            this.periodendDataGridViewTextBoxColumn.ReadOnly = true;
            this.periodendDataGridViewTextBoxColumn.Width = 130;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.ReadOnly = true;
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // RefAccPeriodView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 390);
            this.Controls.Add(this.gvRefAccPeriod);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RefAccPeriodView";
            this.Text = "Periode Akuntansi";
            ((System.ComponentModel.ISupportInitialize)(this.gvRefAccPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refAccPeriodModelBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gvRefAccPeriod;
        private System.Windows.Forms.BindingSource refAccPeriodModelBindingSource;
        private System.Windows.Forms.Button btnTambah;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Button btnDeaktivasi;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnTutupPeriode;
        private System.Windows.Forms.Button btnBukaPeriode;
        private System.Windows.Forms.Button btnUbah;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodstartDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodendDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
    }
}