namespace Fino.Datalib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTable_PosBiayaPaidCancel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PosBiayaPaidCancels",
                c => new
                    {
                        PosBiayaPaidCancel_Id = c.Int(nullable: false, identity: true),
                        PosBiaya_Id = c.Int(nullable: false),
                        datepaid = c.DateTime(nullable: false),
                        paynilai = c.Double(nullable: false),
                        status = c.Int(nullable: false),
                        nota_pembayaran_id = c.Int(),
                    })
                .PrimaryKey(t => t.PosBiayaPaidCancel_Id)
                .ForeignKey("dbo.NotaPembayarans", t => t.nota_pembayaran_id)
                .ForeignKey("dbo.PosBiayas", t => t.PosBiaya_Id)
                .Index(t => t.PosBiaya_Id)
                .Index(t => t.nota_pembayaran_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PosBiayaPaidCancels", "PosBiaya_Id", "dbo.PosBiayas");
            DropForeignKey("dbo.PosBiayaPaidCancels", "nota_pembayaran_id", "dbo.NotaPembayarans");
            DropIndex("dbo.PosBiayaPaidCancels", new[] { "nota_pembayaran_id" });
            DropIndex("dbo.PosBiayaPaidCancels", new[] { "PosBiaya_Id" });
            DropTable("dbo.PosBiayaPaidCancels");
        }
    }
}
