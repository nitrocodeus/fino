﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class PenerimaanUmumProcess : IPenerimaanUmumProcess
    {
        public IPenerimaanUmumRepository PenerimaanUmumRepo { get; set; }
        public IRefSPURepository RefSPURepository { get; set; }
        public IKonfigurasiBiayaRepository KonfigurasiBiayaRepo { get; set; }
        public IRefKelasRepository RefKelasRepo { get; set; }

        public ProcessResult GetAllRefSPU()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = RefSPURepository.GetAllRefSPU().Select(x => new 
                { 
                    Id = x.Id, 
                    Value = x.Nama 
                }).ToList();
                result.IsSucess = true;
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllRefBiaya()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = KonfigurasiBiayaRepo.KonfigurasiBiayaData().Where(x => x.noposbiaya).Select(x => new 
                { 
                    Id = x.biaya_id, 
                    Value = x.nama 
                }).ToList();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllKelas()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = RefKelasRepo.SelectAllKelas().Where(x => x.SumberPenerimaanUmums.Count() < 1).Select(x => new
                {
                    Id = x.kelas_id,
                    Value = x.nama
                }).ToList();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdatePenerimaanUmum(PenerimaanUmumModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = PenerimaanUmumRepo.UpdatePenerimaanUmum(entity);
                result.IsSucess = true;

                PenerimaanUmumRepo.DisposeDBContext();
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult CreateNewRefSPU(RefSumberPUModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefSPURepository.UpdateSPU(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult AddNotaPenerimaanUmum(CetakPenerimaanUmumModel entity, PenerimaanUmumModel penerimaanUmumModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                CetakPenerimaanUmumModel dataResult = PenerimaanUmumRepo.AddNotaPenerimaanUmum(entity, penerimaanUmumModel);
                result.IsSucess = true;
                result.DataResult = dataResult;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
