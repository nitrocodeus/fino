using Fino.Datalib.Dbmodel;
using System;

namespace Fino.Repository
{
    public class BaseRepository
    {
        FinoDBContext _context;

        public FinoDBContext Context 
        { 
            get
            {
                if (_context == null || _context.IsDisposed)
                {
                    _context = new FinoDBContext();
                }

                _context.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

                return _context;
            }
            set
            {
                _context = value;
            }
        }

        public void DisposeDBContext()
        {            
            Context.Dispose();
            GC.SuppressFinalize(Context);
        }
    }
}
