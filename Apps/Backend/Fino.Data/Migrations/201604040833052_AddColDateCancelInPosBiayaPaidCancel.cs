namespace Fino.Datalib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColDateCancelInPosBiayaPaidCancel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PosBiayaPaidCancels", "DateCancel", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PosBiayaPaidCancels", "DateCancel");
        }
    }
}
