﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace Fino.Datalib.Configuration
{
    internal static class RefSumberPenerimaanUmumConfiguration
    {
        internal static void ConfigureRefSumberPenerimaanUmumConfiguration(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefSumberPenerimaanUmum>().HasKey(e => e.refsumberpenerimaan_id);
            modelBuilder.Entity<RefSumberPenerimaanUmum>().Property(e => e.refsumberpenerimaan_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<RefSumberPenerimaanUmum>().Property(e => e.refsumberpenerimaan_name).HasMaxLength(50);
            modelBuilder.Entity<RefSumberPenerimaanUmum>().Property(e => e.refsumberpenerimaan_alamat).HasMaxLength(200);

            modelBuilder.Entity<RefSumberPenerimaanUmum>()
                .HasOptional(e => e.Kelas)
                .WithMany(e => e.SumberPenerimaanUmums)
                .HasForeignKey(e => e.refsumberpenerimaan_kelasid);
        }
    }
}
