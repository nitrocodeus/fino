﻿namespace Fino.Cient
{
    partial class EditRefSPUView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditRefSPUView));
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.rbExternal = new System.Windows.Forms.RadioButton();
            this.gbExternal = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtVendorAddr = new System.Windows.Forms.TextBox();
            this.lblNamaBiaya = new System.Windows.Forms.Label();
            this.txtVendorName = new System.Windows.Forms.TextBox();
            this.rbKelas = new System.Windows.Forms.RadioButton();
            this.gbKelas = new System.Windows.Forms.GroupBox();
            this.cboKelas = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gbExternal.SuspendLayout();
            this.gbKelas.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(155, 318);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(138, 35);
            this.btnSimpan.TabIndex = 4;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(305, 318);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(138, 35);
            this.btnBatal.TabIndex = 5;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            // 
            // rbExternal
            // 
            this.rbExternal.AutoSize = true;
            this.rbExternal.Location = new System.Drawing.Point(31, 152);
            this.rbExternal.Name = "rbExternal";
            this.rbExternal.Size = new System.Drawing.Size(168, 24);
            this.rbExternal.TabIndex = 1;
            this.rbExternal.Text = "Penerimaan lain";
            this.rbExternal.UseVisualStyleBackColor = true;
            // 
            // gbExternal
            // 
            this.gbExternal.Controls.Add(this.label1);
            this.gbExternal.Controls.Add(this.txtVendorAddr);
            this.gbExternal.Controls.Add(this.lblNamaBiaya);
            this.gbExternal.Controls.Add(this.txtVendorName);
            this.gbExternal.Enabled = false;
            this.gbExternal.Location = new System.Drawing.Point(31, 173);
            this.gbExternal.Name = "gbExternal";
            this.gbExternal.Size = new System.Drawing.Size(412, 106);
            this.gbExternal.TabIndex = 3;
            this.gbExternal.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 62);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Alamat:";
            // 
            // txtVendorAddr
            // 
            this.txtVendorAddr.Location = new System.Drawing.Point(124, 59);
            this.txtVendorAddr.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtVendorAddr.Name = "txtVendorAddr";
            this.txtVendorAddr.Size = new System.Drawing.Size(218, 28);
            this.txtVendorAddr.TabIndex = 3;
            // 
            // lblNamaBiaya
            // 
            this.lblNamaBiaya.AutoSize = true;
            this.lblNamaBiaya.Location = new System.Drawing.Point(40, 22);
            this.lblNamaBiaya.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblNamaBiaya.Name = "lblNamaBiaya";
            this.lblNamaBiaya.Size = new System.Drawing.Size(67, 20);
            this.lblNamaBiaya.TabIndex = 0;
            this.lblNamaBiaya.Text = "Nama:";
            // 
            // txtVendorName
            // 
            this.txtVendorName.Location = new System.Drawing.Point(124, 19);
            this.txtVendorName.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtVendorName.Name = "txtVendorName";
            this.txtVendorName.Size = new System.Drawing.Size(218, 28);
            this.txtVendorName.TabIndex = 1;
            // 
            // rbKelas
            // 
            this.rbKelas.AutoSize = true;
            this.rbKelas.Checked = true;
            this.rbKelas.Location = new System.Drawing.Point(31, 24);
            this.rbKelas.Name = "rbKelas";
            this.rbKelas.Size = new System.Drawing.Size(220, 24);
            this.rbKelas.TabIndex = 0;
            this.rbKelas.TabStop = true;
            this.rbKelas.Text = "Penerimaan dari kelas";
            this.rbKelas.UseVisualStyleBackColor = true;
            this.rbKelas.CheckedChanged += new System.EventHandler(this.rbKelas_CheckedChanged);
            // 
            // gbKelas
            // 
            this.gbKelas.Controls.Add(this.cboKelas);
            this.gbKelas.Controls.Add(this.label2);
            this.gbKelas.Location = new System.Drawing.Point(31, 54);
            this.gbKelas.Name = "gbKelas";
            this.gbKelas.Size = new System.Drawing.Size(412, 70);
            this.gbKelas.TabIndex = 2;
            this.gbKelas.TabStop = false;
            // 
            // cboKelas
            // 
            this.cboKelas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKelas.FormattingEnabled = true;
            this.cboKelas.Location = new System.Drawing.Point(124, 19);
            this.cboKelas.Margin = new System.Windows.Forms.Padding(4);
            this.cboKelas.Name = "cboKelas";
            this.cboKelas.Size = new System.Drawing.Size(217, 28);
            this.cboKelas.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Kelas:";
            // 
            // EditRefSPUView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 369);
            this.Controls.Add(this.gbKelas);
            this.Controls.Add(this.gbExternal);
            this.Controls.Add(this.rbKelas);
            this.Controls.Add(this.rbExternal);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "EditRefSPUView";
            this.Text = "Tambah Sumber Penerimaan";
            this.gbExternal.ResumeLayout(false);
            this.gbExternal.PerformLayout();
            this.gbKelas.ResumeLayout(false);
            this.gbKelas.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.RadioButton rbExternal;
        private System.Windows.Forms.GroupBox gbExternal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtVendorAddr;
        private System.Windows.Forms.Label lblNamaBiaya;
        private System.Windows.Forms.TextBox txtVendorName;
        private System.Windows.Forms.RadioButton rbKelas;
        private System.Windows.Forms.GroupBox gbKelas;
        private System.Windows.Forms.ComboBox cboKelas;
        private System.Windows.Forms.Label label2;
    }
}