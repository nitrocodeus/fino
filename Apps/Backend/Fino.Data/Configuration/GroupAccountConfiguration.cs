﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class GroupAccountConfiguration
    {
        internal static void ConfigureGroupAccount(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GroupAccount>().HasKey(e => e.groupaccount_id);
            modelBuilder.Entity<GroupAccount>().Property(e => e.groupaccount_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            
            modelBuilder.Entity<GroupAccount>()
                    .HasRequired(e => e.AccountGroup)
                    .WithMany(e => e.GroupOfAccounts)
                    .HasForeignKey(e => e.accgroup_id);

            modelBuilder.Entity<GroupAccount>()
                .HasRequired(e => e.Account)
                .WithMany(e => e.GroupOfAccounts)
                .HasForeignKey(e => e.account_id);
        }
    }
}
