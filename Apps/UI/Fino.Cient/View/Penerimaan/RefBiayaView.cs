﻿using Fino.Cient.Startup;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class RefBiayaView : BaseForm, IRefBiayaView
    {
        public event Action GvRefBiayaDoubleClick;
        public event Action BtnTambahClick;
        public event Action BtnHapusClick;
        public event Action BtnTutupClick;
        public event Action ReloadData;

        public RefBiayaView()
        {
            InitializeComponent();
            gvKonfigurasiBiaya.CellDoubleClick += gvKonfigurasiBiaya_CellDoubleClick;
        }

        void gvKonfigurasiBiaya_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (GvRefBiayaDoubleClick != null)
            {
                GvRefBiayaDoubleClick();
            }
        }

        public void SetData(List<Model.RefBiayaModel> p_ListKonfigurasiBiaya)
        {
            gvKonfigurasiBiaya.DataSource = p_ListKonfigurasiBiaya;
        }

        public void RefreshDataGrid()
        {
            gvKonfigurasiBiaya.Refresh();
        }        

        public void ShowEditRefBiayaView()
        {
            if (gvKonfigurasiBiaya.SelectedCells.Count > 0)
            {
                ViewLocator.Instance.EditPosBiayaView.KonfigurasiBiayaView = this;
                ViewLocator.Instance.EditPosBiayaView.ShowForm(GetSelectedKonfigurasiBiaya());                
            }
        }

        public void ShowTambahRefBiayaView()
        {
            ViewLocator.Instance.EditPosBiayaView.KonfigurasiBiayaView = this;
            ViewLocator.Instance.EditPosBiayaView.ShowForm(null);            
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            if (BtnTambahClick != null)
            {
                BtnTambahClick();
            }
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            if (BtnHapusClick != null)
            {
                BtnHapusClick();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (BtnTutupClick != null)
            {
                BtnTutupClick();
            }
        }

        public RefBiayaModel GetSelectedKonfigurasiBiaya()
        {
            RefBiayaModel result = new RefBiayaModel();

            if (gvKonfigurasiBiaya.SelectedCells.Count > 0)
            {
                result = (RefBiayaModel)gvKonfigurasiBiaya.Rows[gvKonfigurasiBiaya.SelectedCells[0].RowIndex].DataBoundItem; 
            }

            return result;
        }

        public void ReloadDataEvent()
        {
            if (ReloadData != null)
            {
                ReloadData();
            }
        }
    }
}
