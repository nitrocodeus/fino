﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class NotaPembayaran
    {
        public int nota_pembayaran_Id { get; set; }
        public DateTime tanggal_cetak { get; set; }
        public int status { get; set; }
        public int siswa_id { get; set; }
        public int user_id { get; set; }

        public virtual RefSiswa Siswa { get; set; }
        public virtual SysUser User { get; set; }
        public virtual ICollection<PosBiayaPaid> PosBiayaPaids { get; set; }
    }
}
