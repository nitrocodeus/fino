
Library:
	Fino.Lib.Core
		berisi common library yang akan digunakan untuk aplikasi fino
		contoh IView yang akan digunakan sebagai base untuk semua Interface view
	Fino.Lib.Win32
		berisi common library yang lebih spesifik ke winforms
		contoh splash screen, ShowMessageBox (wrapper message box yang nanti digunakan oleh presenter)

Backend:
	Fino.Model
		berisi semua model yang digunakan oleh fino
	Fino.Repository
		berisis repository

Logic:
	Fino.BusinessLogic
		berisi semua bisnis proses
	Fino.Presenter
		berisi semua presenter class

UI
	Fino.Client
		berisi fino aplikasi
	Fine.View
		berisi semua interface view yang digunakan oleh aplikasi fino

Lankah development
- buat model
- buat repository
- buat business logic
- buat interface view
- buat presenter
- buat view
- koneksi antara view dan presenter 