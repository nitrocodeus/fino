﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Core.Settings;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class PembayaranBiayaPresenter : BasePresenter
    {
        private IPembayaranBiayaView _view;
        private IPosBiayaProcess _process;

        public PembayaranBiayaPresenter(IPembayaranBiayaView p_view, IPosBiayaProcess p_process) : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.btnTutupClicked += _view_btnTutupClicked;
            _view.btnSimpanClicked += _view_btnSimpanClicked;
            _view.txtSiswaKeyPressed += _view_txtSiswaKeyPressed;
            _view.SelectingSiswaDone += _view_SelectingSiswaDone;
            _view.AddingNewPosBiaya += _view_AddingNewPosBiaya;
            _view.btnTambahBiayaClicked += _view_btnTambahBiayaClicked;
            _view.CurrentPosBiayaView.CellSelectedClicked += CurrentPosBiayaView_CellSelectedClicked;
        }

        void CurrentPosBiayaView_CellSelectedClicked(object sender, CustomEventArgs<PosBiayaModel> e)
        {
            double totalBiaya = 0;

            foreach (PosBiayaModel item in _view.CurrentPosBiayaView.SelectedPosBiaya)
            {
                if (item.Selected)
                {
                    totalBiaya += item.JumlahBiaya;
                }
            }

            // _view.TotalBiaya = String.Format(System.Globalization.CultureInfo.CurrentCulture, "{0:C2}", totalBiaya);
            _view.TotalBiaya = String.Format("{0:n0}", totalBiaya);
        }

        void _view_btnTambahBiayaClicked()
        {
            _view.AddPosBiayaMode(true);
            _view.AddPosBiayaView.Visible = true;

            _view.AddPosBiayaView.SiswaId = _view.SiswaId;
            _view.AddPosBiayaView.Cari();
        }

        void _view_AddingNewPosBiaya()
        {
            List<PosBiayaModel> current = _view.CurrentPosBiayaView.gvPosBiayaSource;
            List<PosBiayaModel> selectedAddPosBiaya = _view.AddPosBiayaView.SelectedPosBiaya;
            List<PosBiayaModel> addedPosBiaya = new List<PosBiayaModel>();

            foreach (PosBiayaModel item in selectedAddPosBiaya)
            {
                PosBiayaModel itemExist = current.Where(x => x.PosBiayaId == item.PosBiayaId && x.RefBiayaId == item.RefBiayaId).FirstOrDefault();

                if (itemExist == null)
                {
                    current.Add(item);
                    item.Selected = false;
                    addedPosBiaya.Add(item);
                }
            }

            _view.CurrentPosBiayaView.gvPosBiayaSource = current;
            _view.AddPosBiayaView.Visible = false;
            _view.AddPosBiayaMode(false);
        }

        void _view_SelectingSiswaDone()
        {
            _view.NamaSiswa = _view.PencarianSiswaView.SelectedSiswa.Nama;
            _view.NoInduk = _view.PencarianSiswaView.SelectedSiswa.Code;
            _view.CurrentPosBiayaView.SiswaId = _view.SiswaId;
            _view.CurrentPosBiayaView.Cari();
            _view.TotalBiaya = string.Empty;
        }

        void _view_txtSiswaKeyPressed()
        {
            _view.PencarianSiswaView.Visible = true;
            _view.PencarianSiswaView.StringSiswa = _view.TextSearchSiswa;

            _view.PencarianSiswaView.Cari();
        }

        void _view_btnSimpanClicked()
        {
            if (_view.SiswaId > 0)
            {
                int siswaId = _view.SiswaId;
                List<PosBiayaModel> model = _view.CurrentPosBiayaView.SelectedPosBiaya;

                if (model != null && model.Count > 0)
                {
                    ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => _process.UpdatePembayaranPosBiaya(model, siswaId)),
                                AppResource.MSG_SAVING_MAPPING_GRUP_BIAYA);

                    List<PosBiayaPaidModel> dataResult = result.DataResult as List<PosBiayaPaidModel>;

                    if (dataResult == null)
                    {
                        result.IsSucess = false;
                    }

                    if (result.IsSucess)
                    {                        
                        if (MessageBox.Show(AppResource.MSG_KONF_CETAKNOTA, AppResource.MSG_CETAKNOTA_CAPTION,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            CetakNota(dataResult);
                        }

                        CleanAll();
                    }
                    else
                    {
                        WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
                    }
                }
                else
                {
                    WinApi.ShowErrorMessage(AppResource.VLD_PEMBAYARAN_PILIHBIAYA, AppResource.CAP_VALIDASI_ERROR);
                }
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.VLD_DAFTARLAYANAN_PILIHSISWA, AppResource.CAP_VALIDASI_ERROR);
            }
        }

        private void CleanAll()
        {
            _view.NamaSiswa = string.Empty;
            _view.NoInduk = string.Empty;
            _view.CurrentPosBiayaView.gvPosBiayaSource = null;
            _view.AddPosBiayaView.ClearSelected();
            _view.PencarianSiswaView.ClearSelected();
            _view.TotalBiaya = string.Empty;
        }

        private void CetakNota(List<PosBiayaPaidModel> dataResult)
        {
            CetakPembayaranModel cpm = new CetakPembayaranModel();

            cpm.Kelas = _view.Kelas;
            cpm.SiswaId = _view.SiswaId;
            cpm.NamaSiswa = _view.NamaSiswa;
            cpm.NoInduk = _view.NoInduk;
            cpm.TanggalPembayaran = DateTime.Today;
            cpm.TahunAjaran = _view.TahunAjaran;
            cpm.DaftarPembayaran = _view.CurrentPosBiayaView.SelectedPosBiaya;
            cpm.TotalJumlah = int.Parse((string.IsNullOrEmpty(_view.TotalBiaya)) ? "0" : _view.TotalBiaya, System.Globalization.NumberStyles.AllowThousands);
            cpm.User = AppVariable.Instance.AppGlobal[AppResource.APP_VARIABEL_LOGGEDONUSER] as UserModel;

            ProcessResult cetakNotaProcess = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _process.SimpanCetakPembayaran(cpm, dataResult)),
                AppResource.MSG_CETAK_ONPROGRESS);

            _view.CetakNota(cpm);

            MessageBox.Show(AppResource.MSG_CETAKNOTA_BERHASIL, AppResource.APP_TITLE, MessageBoxButtons.OK);
        }

        void _view_btnTutupClicked()
        {
            _view.Close();
        }
    }
}
