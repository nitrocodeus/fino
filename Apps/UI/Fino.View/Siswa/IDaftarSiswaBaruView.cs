﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IDaftarSiswaBaruView: IView
    {
        event Action SaveButtonClicked;
        event Action SaveNextButtonClicked;
        event Action NewDataButtonClicked;
        event Action PotonganButtonClicked;
        event Action KateringButtonClicked;
        event Action JemputanButtonClicked;
        event Action CloseButtonClicked;
        event Action PindahanCheckChanged;

        string NoInduk { get; set; }
        string NamaSiswa { get; set; }
        bool JenisKelamin { get; set; }
        int KelasId { get;set; }
        DateTime MulaiTanggal { get; set; }
        int TahunAjaranId { get; set; }
        string TahunAjaranName { get; set; }
        bool IsPindahan { get; set; }

        DaftarSiswaBaruModel ViewModel { get; set; }
        object KelasDataSource { get; set; }
        object OptionDataSource { get; set; }

        void SetBinding();
        void ClearView();
        void ChangeEditable(bool enabled);
        void ShowErrorMessage(string message, string caption);
        void ShowInformation(string message, string caption);
        void SetOptionEnable(bool enabled);

    }
}
