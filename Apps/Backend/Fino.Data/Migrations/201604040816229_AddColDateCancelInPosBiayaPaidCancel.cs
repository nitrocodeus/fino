namespace Fino.Datalib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColDateCancelInPosBiayaPaidCancel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PosBiayaPaidCancels",
                c => new
                    {
                        PosBiayaPaidCancel_Id = c.Int(nullable: false, identity: true),
                        PosBiaya_Id = c.Int(nullable: false),
                        datepaid = c.DateTime(nullable: false),
                        paynilai = c.Double(nullable: false),
                        status = c.Int(nullable: false),
                        nota_pembayaran_id = c.Int(),
                        DateCancel = c.DateTime(),
                    })
                .PrimaryKey(t => t.PosBiayaPaidCancel_Id)
                .ForeignKey("dbo.NotaPembayarans", t => t.nota_pembayaran_id)
                .ForeignKey("dbo.PosBiayas", t => t.PosBiaya_Id)
                .Index(t => t.PosBiaya_Id)
                .Index(t => t.nota_pembayaran_id);
            
            AddColumn("dbo.PenerimaanUmums", "Status_Date", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PosBiayaPaidCancels", "PosBiaya_Id", "dbo.PosBiayas");
            DropForeignKey("dbo.PosBiayaPaidCancels", "nota_pembayaran_id", "dbo.NotaPembayarans");
            DropIndex("dbo.PosBiayaPaidCancels", new[] { "nota_pembayaran_id" });
            DropIndex("dbo.PosBiayaPaidCancels", new[] { "PosBiaya_Id" });
            DropColumn("dbo.PenerimaanUmums", "Status_Date");
            DropTable("dbo.PosBiayaPaidCancels");
        }
    }
}
