﻿using System;
using System.Collections.Generic;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Fino.BusinessLogic
{
    [TestClass]
    public class PostJournal_Test
    {
        [TestMethod]
        public void PostingJournal_Success()
        {
            IDaftarSiswaBaruProcess process = new DaftarSiswaBaruProcess();
            var ta = (ValueList)process.GetTahunAjaran(DateTime.Now).DataResult;
            var cd = DateTime.Now.ToString("yyddhhmmss");

            // Create siswa
            DaftarSiswaBaruModel model = new DaftarSiswaBaruModel
            {
                Code = cd,
                IsPindahan = false,
                JKelamin = true,
                Kelas_Id = 3,
                Mulai_Tanggal = new DateTime(2015, 8, 1),
                Nama = "Fulan  " + cd,
                OptionTingkat_Id = 0,
                TahunAjaran_Id = (int)ta.Id,
                TahunAjaran_Nama = (string)ta.Value
            };
            var siswaBaruResult = process.TambahSiswaBaru(model);

            if (siswaBaruResult.IsSucess)
            {
                // Post journal
                IPostTransactionToJournalProcess journalProcess = new PostTransactionToJournalProcess();
                DateTime dtmStart = new DateTime(2015, 1, 1);
                DateTime dtmEnd = DateTime.Now;

                var trxlist = (List<TransaksiToJurnalModel>)journalProcess.GetActiveTransactionForJournal(dtmStart, dtmEnd).DataResult;
                var postResult = journalProcess.PostTransactionForJournal(trxlist);
                Assert.IsTrue(postResult.IsSucess);
            }
            else
            {
                Assert.IsTrue(false);
            }
            
        }
    }
}
