﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IRefPayableView : IView
    {
        event Action GvRefPayableDoubleClick;
        event Action BtnTambahClick;
        event Action BtnHapusClick;
        event Action BtnTutupClick;
        event Action ReloadData;

        void SetData(List<RefPayableModel> p_ListRefPayable);
        void RefreshDataGrid();
        void ShowEditRefPayableView();
        void ShowTambahRefPayableView();
        void ReloadDataEvent();
        RefPayableModel GetSelectedRefPayable();
    }
}
