﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Core.Settings;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class PenerimaanUmumPresenter : BasePresenter
    {
        IPenerimaanUmumProcess _penerimaanUmumProcess;
        IPenerimaanUmumView _PenerimaanUmumView;

        public PenerimaanUmumPresenter(IPenerimaanUmumView p_View, IPenerimaanUmumProcess p_Process)
            : base (p_View)
        {
            this._PenerimaanUmumView = p_View;
            this._penerimaanUmumProcess = p_Process;

            this._PenerimaanUmumView.AddNewSPUButtonClicked += _PenerimaanUmumView_AddNewSPUButtonClicked;
            this._PenerimaanUmumView.CloseButtonClicked += _PenerimaanUmumView_CloseButtonClicked;
            this._PenerimaanUmumView.NewDataButtonClicked += _PenerimaanUmumView_NewDataButtonClicked;
            this._PenerimaanUmumView.SaveButtonClicked += _penerimaanUmumView_SaveButtonClicked;
            this._PenerimaanUmumView.SaveNextButtonClicked += _penerimaanUmumView_SaveNextButtonClicked;
            this._PenerimaanUmumView.ReloadDataEvent += _penerimaanUmumView_ReloadDataEvent;
            this._PenerimaanUmumView.Load += _penerimaanUmumView_Load;
        }

        #region Control Events

        void _penerimaanUmumView_ReloadDataEvent()
        {
            this._PenerimaanUmumView.RefSPUDataSource = GetRefSPUDataSource();
            this._PenerimaanUmumView.SetBinding();
        }

        void _penerimaanUmumView_Load(object sender, EventArgs e)
        {
            this._PenerimaanUmumView.RefSPUDataSource = GetRefSPUDataSource();
            this._PenerimaanUmumView.RefBiayaDataSource = GetRefBiayaDataSource();
            AssignNewViewModel();
            this._PenerimaanUmumView.ClearView();
            this._PenerimaanUmumView.SetBinding();
        }

        void _penerimaanUmumView_SaveNextButtonClicked()
        {
            if (SaveData())
            {
                AssignNewViewModel();
                this._PenerimaanUmumView.ChangeEditable(true);
                this._PenerimaanUmumView.ClearView();
            }
        }

        void _penerimaanUmumView_SaveButtonClicked()
        {
            if (SaveData())
            {
                this._PenerimaanUmumView.ChangeEditable(false);
            }
        }

        void _PenerimaanUmumView_NewDataButtonClicked()
        {
            AssignNewViewModel();
            this._PenerimaanUmumView.ClearView();
            this._PenerimaanUmumView.ChangeEditable(true);
        }

        void _PenerimaanUmumView_CloseButtonClicked()
        {
            this._PenerimaanUmumView.Close();
        }

        void _PenerimaanUmumView_AddNewSPUButtonClicked()
        {
            this._PenerimaanUmumView.ShowCreateNewRefSPUView();
        }

        #endregion Control Events

        #region Private Methods

        private void AssignNewViewModel()
        {
            this._PenerimaanUmumView.ViewModel = new PenerimaanUmumModel();
            this._PenerimaanUmumView.ViewModel.DatePaid = DateTime.Now;
        }

        private object GetRefSPUDataSource()
        {
            return this._penerimaanUmumProcess.GetAllRefSPU().DataResult;
        }

        private object GetRefBiayaDataSource()
        {
            return this._penerimaanUmumProcess.GetAllRefBiaya().DataResult;
        }

        private bool Save()
        {
            var saved = false;

            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => this._penerimaanUmumProcess.UpdatePenerimaanUmum(this._PenerimaanUmumView.ViewModel)),
                        AppResource.MSG_SAVING_PENERIMAANUMUM);

            if (result.ProcessException != null)
            {
                this._PenerimaanUmumView.ShowErrorMessage(result.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
            else
            {
                saved = true;
                this._PenerimaanUmumView.ShowInformation(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.CAP_PROCESS_INFORMATION);

                PenerimaanUmumModel pum = result.DataResult as PenerimaanUmumModel;

                if (pum != null && MessageBox.Show(AppResource.MSG_KONF_CETAKNOTAPENERIMAANUMUM, AppResource.MSG_CETAKNOTA_CAPTION,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    CetakNota(pum);
                }
            }

            return saved;
        }

        private void CetakNota(PenerimaanUmumModel penerimaanUmumModel)
        {
            if (penerimaanUmumModel != null)
            {
                CetakPenerimaanUmumModel cetakModel = new CetakPenerimaanUmumModel();

                cetakModel.Nama = _PenerimaanUmumView.RefSPU;
                cetakModel.JenisPenerimaan = _PenerimaanUmumView.RefBiaya;
                cetakModel.Deskripsi = _PenerimaanUmumView.Deskripsi;
                cetakModel.TanggalPenerimaan = DateTime.Now;
                cetakModel.TotalJumlah = _PenerimaanUmumView.Nilai;
                cetakModel.User = AppVariable.Instance.AppGlobal[AppResource.APP_VARIABEL_LOGGEDONUSER] as UserModel;
                //cetakModel.Alamat = 

                ProcessResult cetakNotaProcess = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _penerimaanUmumProcess.AddNotaPenerimaanUmum(cetakModel, penerimaanUmumModel)),
                        AppResource.MSG_CETAK_ONPROGRESS);

                _PenerimaanUmumView.CetakNota(cetakModel);

                MessageBox.Show(AppResource.MSG_CETAKNOTA_BERHASIL, AppResource.APP_TITLE, MessageBoxButtons.OK);
            }
        }

        private bool SaveData()
        {
            var saved = false;
            var context = new ValidationContext(this._PenerimaanUmumView.ViewModel, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            // for some reasons the text property is unexpectedly not functioning
            // need to assign the value explicitly, here:
            //_PenerimaanUmumView.ViewModel.RefBiayaId = _payablePaidView.RefPayableName;

            var isValid = Validator.TryValidateObject(this._PenerimaanUmumView.ViewModel, context, results, true);

            // Later, this should be done in data annotation
            isValid = isValid && (this._PenerimaanUmumView.ViewModel.SumberPenerimaanUmumId > 0);
            if (isValid)
            {
                saved = Save();

                this._PenerimaanUmumView.ViewModel.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }
                if (this._PenerimaanUmumView.ViewModel.SumberPenerimaanUmumId <= 0)
                {
                    sb.Append("Sumber penerimaan is required");
                }
                this._PenerimaanUmumView.ViewModel.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }

            return saved;
        }

        #endregion Private Methods
    }
}
