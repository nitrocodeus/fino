﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IEditRefAccPeriodView : IView
    {
        event Action SaveButtonClicked;
        event Action CloseButtonClicked;

        RefAccPeriodModel SelectedRefAccPeriod { get; }
        IRefAccPeriodView RefAccPeriodView { get; set; }

        string PeriodName { get; set; }
        DateTime PeriodStart { get; set; }
        DateTime PeriodEnd { get; set; }

        void ShowForm(RefAccPeriodModel p_RefAccPeriod);
        void SetBinding();
        void ClearData();
    }
}
