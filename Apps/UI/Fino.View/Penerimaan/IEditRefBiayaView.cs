﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IEditRefBiayaView : IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;
        event Action CmbRepetisiValueChanged;

        RefBiayaModel SelectedKonfigurasiBiaya { get; }
        IRefBiayaView KonfigurasiBiayaView { get; set; }

        string NamaBiaya { get; set; }
        string KodeBiaya { get; set; }
        int Repetisi { get; set; }
        int TanggalJatuhTempo { get; set; }
        int BulanJatuhTempo { get; set; }
        DateTime MulaiTanggal { get; set; }
        bool CmbJTBulanEnabled { get; set; }
        bool IsTambahan { get; set; }
        bool BisaDiCicil { get; set; }

        void SetBinding();
        void ClearData();        
    }
}
