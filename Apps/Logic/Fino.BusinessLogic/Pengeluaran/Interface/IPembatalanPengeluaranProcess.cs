﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IPembatalanPengeluaranProcess
    {
        IPayableRepository PayableRepo { get; set; }
        IPayablePaidRepository PayablePaidRepo { get; set; }

        ProcessResult GetPembatalanPengeluaranList(DateTime p_FromDate, DateTime p_ToDate);
        ProcessResult GetPayable(int p_PayableId);
        ProcessResult UpdatePayable(PayableModel entity);
        ProcessResult DeletePayable(PayableModel entity);
    }
}
