﻿using Fino.Datalib.Entity;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Fino.Repository
{
    public class RefPayableRepository : BaseRepository, IRefPayableRepository
    {
        public List<RefPayable> RefPayableData()
        {
            return (from a in Context.RefPayables
                    select a).ToList();
        }

        public RefPayable RefPayableData(int p_Id)
        {
            return (from a in Context.RefPayables
                    where a.refpayable_id == p_Id
                    select a).FirstOrDefault();
        }

        public void AddRefPayableData(RefPayable p_RefPayable)
        {
            Context.RefPayables.Add(p_RefPayable);
        }

        public void DeleteRefPayableData(RefPayable p_RefPayable)
        {
            Context.Entry<RefPayable>(p_RefPayable).State = System.Data.Entity.EntityState.Deleted;
        }

        public void UpdateRefPayableData(RefPayable p_RefPayable)
        {
            Context.RefPayables.Attach(p_RefPayable);
            Context.Entry(p_RefPayable).Property(x => x.aktif).IsModified = true;
        }

        public void SaveChanges()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                throw new UpdateException(e.Message, e);
            }
        }
    }
}
