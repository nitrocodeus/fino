﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefLayananConfiguration
    {
        internal static void ConfigureRefLayanan(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefLayanan>().HasKey(e => e.layanan_id);
            modelBuilder.Entity<RefLayanan>().Property(e => e.layanan_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RefLayanan>().Property(e => e.nama).HasMaxLength(25);
            modelBuilder.Entity<RefLayanan>().Property(e => e.code).HasMaxLength(10);
        }
    }
}
