﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class PembatalanPenerimaanUmumView : BaseForm, IPembatalanPenerimaanUmumView
    {
        private BindingSource _bindData;

        public event Action BtnCariClicked;
        public event Action BtnTutupClicked;
        public event Action BtnBatalClicked;
        public event Action BtnAllClicked;
        public event Action BtnResetClicked;

        #region Public Properties

        public DateTime DateFrom
        {
            get
            {
                DateTime result = new DateTime(dtFrom.Value.Year, dtFrom.Value.Month, dtFrom.Value.Day);
                return result;
            }
        }

        public DateTime DateTo
        {
            get
            {
                DateTime result = new DateTime(dtTo.Value.Year, dtTo.Value.Month, dtTo.Value.Day);
                return result;
            }
        }

        #endregion Public Properties

        #region Contructors

        public PembatalanPenerimaanUmumView()
        {
            InitializeComponent();

            btnCari.Click += btnCari_Click;
            btnTutup.Click += btnTutup_Click;
            btnBatal.Click += btnBatal_Click;
            btnAll.Click += btnAll_Click;
            btnReset.Click += btnReset_Click;
        }

        #endregion Contructors

        #region Control Events

        void btnReset_Click(object sender, EventArgs e)
        {
            if (BtnResetClicked != null)
            {
                BtnResetClicked();
            }
        }

        void btnAll_Click(object sender, EventArgs e)
        {
            if (BtnAllClicked != null)
            {
                BtnAllClicked();
            }
        }

        void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        void btnTutup_Click(object sender, EventArgs e)
        {
            if (BtnTutupClicked != null)
            {
                BtnTutupClicked();
            }
        }

        void btnCari_Click(object sender, EventArgs e)
        {
            if (BtnCariClicked != null)
            {
                BtnCariClicked();
            }
        }

        #endregion Control Events

        public void SetData(List<Model.PembatalanPenerimaanUmumModel> p_ListPenerimaanUmum)
        {
            _bindData = new BindingSource();
            _bindData.DataSource = p_ListPenerimaanUmum;

            gvPenerimaanUmum.DataSource = _bindData;
        }

        public void RefreshDataGrid()
        {
            gvPenerimaanUmum.Refresh();
        }

        public void SelectAllItem()
        {
            foreach (DataGridViewRow row in gvPenerimaanUmum.Rows)
            {
                row.Cells[0].Value = true;
            }
        }

        public void UnselectAllItem()
        {
            foreach (DataGridViewRow row in gvPenerimaanUmum.Rows)
            {
                row.Cells[0].Value = false;
            }
        }

        public List<PembatalanPenerimaanUmumModel> GetSelectedPenerimaanUmum()
        {
            var result = new List<PembatalanPenerimaanUmumModel>();
            var dataSource = _bindData.DataSource as List<PembatalanPenerimaanUmumModel>;

            if (dataSource != null)
            {
                result = dataSource.Where(x => x.Selected).ToList();
            }

            return result;
        }

        public List<PembatalanPenerimaanUmumModel> GetGVPenerimaanUmumDataSource()
        {
            return (_bindData != null) ? _bindData.DataSource as List<PembatalanPenerimaanUmumModel> : null;
        }
    }
}
