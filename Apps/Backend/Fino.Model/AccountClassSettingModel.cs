﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class AccountClassSettingModel : BaseModel
    {
        const string ACCOUNTID_NOTEMPTY = "VLD_ACCOUNTID_NOTEMPTY";
        const string PARAMID_NOTEMPTY = "VLD_PARAMID_NOTEMTPY";

        public static readonly string AccountIdPropertyName = "AccountId";
        public static readonly string AccClassIdPropertyName = "AccClassId";
        public static readonly string ParamIdPropertyName = "ParamId";

        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public string ParamName { get; set; }

        private int _accountId;
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = ACCOUNTID_NOTEMPTY)]
        public int AccountId
        {
            get
            {
                return _accountId;
            }
            set
            {
                _accountId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(AccountIdPropertyName));
                ValidateProperty(value, AccountIdPropertyName);
            }
        }

        private int _accClassId;
        public int AccClassId
        {
            get
            {
                return _accClassId;
            }
            set
            {
                _accClassId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(AccClassIdPropertyName));
                ValidateProperty(value, AccClassIdPropertyName);
            }
        }

        private int _paramId;
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = PARAMID_NOTEMPTY)]
        public int ParamId
        {
            get
            {
                return _paramId;
            }
            set
            {
                _paramId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(ParamIdPropertyName));
                ValidateProperty(value, ParamIdPropertyName);
            }
        }
    }
}
