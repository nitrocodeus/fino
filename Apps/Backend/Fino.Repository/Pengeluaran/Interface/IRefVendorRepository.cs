﻿using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public interface IRefVendorRepository
    {
        RefVendorModel GetVendor(int refVendorId);
        List<RefVendorModel> GetAllVendor();
        void UpdateVendor(RefVendorModel entity);
        void DeleteVendor(RefVendorModel entity);
    }
}
