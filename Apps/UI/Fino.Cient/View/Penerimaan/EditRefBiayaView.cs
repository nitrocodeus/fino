﻿using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditRefBiayaView : BaseForm, IEditRefBiayaView
    {
        public event Action BtnBatalClicked;
        public event Action BtnSimpanClicked;
        public event Action CmbRepetisiValueChanged;

        public IRefBiayaView KonfigurasiBiayaView { get; set; }
        public RefBiayaModel SelectedKonfigurasiBiaya { get; private set; }

        public string NamaBiaya
        {
            get
            {
                return txtNamaBiaya.Text;
            }
            set
            {
                txtNamaBiaya.Text = value;
            }
        }

        public string KodeBiaya
        {
            get
            {
                return txtKodeBiaya.Text;
            }
            set
            {
                txtKodeBiaya.Text = value;
            }
        }

        public int Repetisi
        {
            get
            {
                if (cmbRepetisi.SelectedValue != null)
                {
                    return Int32.Parse(cmbRepetisi.SelectedValue.ToString());
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                cmbRepetisi.SelectedValue = value.ToString();
            }
        }

        public int TanggalJatuhTempo
        {
            get
            {
                return Int32.Parse(txtTanggalJatuhTempo.Text);
            }
            set
            {
                txtTanggalJatuhTempo.Text = value.ToString();
            }
        }

        public int BulanJatuhTempo
        {
            get
            {
                if (cmbBulanJatuhTempo.SelectedValue != null)
                {
                    return Int32.Parse(cmbBulanJatuhTempo.SelectedValue.ToString());
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                cmbBulanJatuhTempo.SelectedValue = value.ToString();
            }
        }

        public DateTime MulaiTanggal
        {
            get
            {
                return dtTanggalMulai.Value;
            }
            set
            {
                dtTanggalMulai.Value = value;
            }
        }

        public bool IsTambahan
        {
            get
            {
                return chkTambahan.Checked;
            }
            set
            {
                chkTambahan.Checked= value;
            }
        }
        public bool BisaDiCicil
        {
            get
            {
                return chkBisaDiCicil.Checked;
            }
            set
            {
                chkBisaDiCicil.Checked = value;
            }
        }

        public EditRefBiayaView()
        {
            InitializeComponent();

            cmbBulanJatuhTempo.DataSource = Constants.Instance.Bulan;
            cmbBulanJatuhTempo.DisplayMember = "Value";
            cmbBulanJatuhTempo.ValueMember = "Id";

            cmbRepetisi.DataSource = Constants.Instance.Repetisi;
            cmbRepetisi.DisplayMember = "Value";
            cmbRepetisi.ValueMember = "Id";
        }

        public void SetBinding()
        {
            txtNamaBiaya.DataBindings.Add("Text", SelectedKonfigurasiBiaya, RefBiayaModel.NamaPropertyName, 
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtKodeBiaya.DataBindings.Add("Text", SelectedKonfigurasiBiaya, RefBiayaModel.CodePropertyName, 
                false, DataSourceUpdateMode.OnPropertyChanged);
            cmbRepetisi.DataBindings.Add("SelectedValue", SelectedKonfigurasiBiaya, RefBiayaModel.RepetisiPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtTanggalJatuhTempo.DataBindings.Add("Text", SelectedKonfigurasiBiaya, RefBiayaModel.JTTanggalPropertyName);
            cmbBulanJatuhTempo.DataBindings.Add(
                "SelectedValue", SelectedKonfigurasiBiaya, RefBiayaModel.JTBulanPropertyName, 
                false, DataSourceUpdateMode.OnPropertyChanged);
            dtTanggalMulai.DataBindings.Add("Value", SelectedKonfigurasiBiaya, RefBiayaModel.MulaiTanggalPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            chkTambahan.DataBindings.Add("Checked", SelectedKonfigurasiBiaya, RefBiayaModel.IsTambahanPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            chkBisaDiCicil.DataBindings.Add("Checked", SelectedKonfigurasiBiaya, RefBiayaModel.BisaDiCicilPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void ClearData()
        {
        }

        public void ShowForm(RefBiayaModel p_KonfigurasiBiaya)
        {
            if (p_KonfigurasiBiaya != null)
            {
                SelectedKonfigurasiBiaya = p_KonfigurasiBiaya;
            }
            else
            {
                SelectedKonfigurasiBiaya = new RefBiayaModel();
            }
            this.Show();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        public bool CmbJTBulanEnabled
        {
            get
            {
                return cmbBulanJatuhTempo.Enabled;
            }
            set
            {
                cmbBulanJatuhTempo.Enabled = value;
            }
        }

        private void cmbRepetisi_SelectedValueChanged(object sender, EventArgs e)
        {
            if (CmbRepetisiValueChanged != null)
            {
                CmbRepetisiValueChanged();
            }
        }
    }
}
