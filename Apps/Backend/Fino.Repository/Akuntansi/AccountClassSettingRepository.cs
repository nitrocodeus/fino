﻿using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class AccountClassSettingRepository : BaseRepository, IAccountClassSettingRepository
    {
        private AccountClassSetting GetExistingAccountClassSetting(int accountId)
        {
            return this.Context.AccClassificationSettings.Where(x => x.account_id == accountId).FirstOrDefault();
        }

        public AccountClassSettingModel GetAccountClassSetting(int accountId)
        {
            var existingAccountClassSetting = GetExistingAccountClassSetting(accountId);

            if (existingAccountClassSetting != null)
            {
                return new AccountClassSettingModel
                {
                    AccountId = existingAccountClassSetting.account_id,
                    AccountCode = existingAccountClassSetting.Account.account_code,
                    AccountName = existingAccountClassSetting.Account.account_name,
                    AccClassId = existingAccountClassSetting.acc_class_id,
                    ParamId = existingAccountClassSetting.param_id
                };
            }

            return null;
        }

        public List<AccountClassSettingModel> GetAllAccountClassSettingByAccClassId(int accClassId)
        {
            List<AccountClassSettingModel> result = new List<AccountClassSettingModel>();

            var allAccountClassSetting = this.Context.AccClassificationSettings.Where(x => x.acc_class_id == accClassId).ToList();

            foreach(var accountClassSetting in allAccountClassSetting)
            {
                result.Add(new AccountClassSettingModel
                {
                    AccountId = accountClassSetting.account_id,
                    AccountCode = accountClassSetting.Account.account_code,
                    AccountName = accountClassSetting.Account.account_name,
                    AccClassId = accountClassSetting.acc_class_id,
                    ParamId = accountClassSetting.param_id
                });
            }

            return result;
        }

        public void UpdateAccountClassSetting(AccountClassSettingModel entity)
        {
            var existingAccClassSetting = GetExistingAccountClassSetting(entity.AccountId);

            if (existingAccClassSetting == null)
            {
                this.Context.AccClassificationSettings.Add(new AccountClassSetting
                    {
                        account_id = entity.AccountId,
                        acc_class_id = entity.AccClassId,
                        param_id = entity.ParamId
                    });
            }
            else
            {
                existingAccClassSetting.acc_class_id = entity.AccClassId;
                existingAccClassSetting.param_id = entity.ParamId;
            }

            this.Context.SaveChanges();
        }

        public void DeleteAccountClassSetting(Model.AccountClassSettingModel entity)
        {
            var existingAccClassSetting = GetExistingAccountClassSetting(entity.AccountId);

            if (existingAccClassSetting != null)
            {
                this.Context.AccClassificationSettings.Remove(existingAccClassSetting);
                this.Context.SaveChanges();
            }
        }
    }
}
