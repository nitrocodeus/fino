﻿using Fino.Lib.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Presenter
{
    public abstract class BasePresenter
    {
        public IView View { get; private set; }

        public BasePresenter(IView pView)
        {
            View = pView;
        }
    }
}
