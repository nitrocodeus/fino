﻿using System;

namespace Fino.Lib.Core
{
    [Serializable]
    public class EnvironmentInfo
    {
        public string Manufacturer { get; set; }
        public string Motherboard { get; set; }
        public string BIOSSerialNumber { get; set; }
    }
}
