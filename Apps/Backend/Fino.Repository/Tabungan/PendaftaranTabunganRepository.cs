﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;

namespace Fino.Repository
{
    public class PendaftaranTabunganRepository : BaseRepository, IPendaftaranTabunganRepository
    {
        public Model.PendaftaranTabunganModel NewRekening(Model.PendaftaranTabunganModel p_Model)
        {
            var pResult = p_Model;
            var exist = false;

            exist = IsExist(p_Model.siswa_id);

            if (!exist)
            {
                var rekeningEntity = new RefRekening
                {
                    rekening_code = GenerateRekeningCode(p_Model.siswa_id),
                    status = 1,
                    tgl_buka = DateTime.Now
                };

                rekeningEntity.RekeningOfSiswa = new SiswaRekening
                {
                    siswa_id = p_Model.siswa_id
                };
                rekeningEntity.Mutasi = new List<MutasiRekening>();
                rekeningEntity.Mutasi.Add(new MutasiRekening
                {
                    factor = 1,
                    is_awal = true,
                    jenis_mutasi = (int)JenisMutasiEnum.SETORAN,
                    mutasi_nilai = p_Model.MutasiNilai,
                    tgl_mutasi = DateTime.Now
                });

                Context.Rekenings.Add(rekeningEntity);
                Context.SaveChanges();
                pResult.rekening_id = rekeningEntity.rekening_id;
                pResult.rekening_code = rekeningEntity.rekening_code;
            }

            if (exist)
            {
                throw new ApplicationException(AppResource.MSG_SAVING_REKENING_ALREADY_EXIST);
            }
            return pResult;
        }

        private bool IsExist(int p_SiswaId)
        {
            return Context.SiswaRekenings.Any(e => e.siswa_id.Equals(p_SiswaId));
        }

        private string GenerateRekeningCode(int p_SiswaId)
        {
            var result = string.Empty;
            var nw = DateTime.Now;
            var siswaCode = Context.RefSiswas.Find(p_SiswaId).siswa_code;
            result = string.Format("{0}{1}", nw.ToString("yyhhss"), siswaCode);
            return result;
        }
    }
}
