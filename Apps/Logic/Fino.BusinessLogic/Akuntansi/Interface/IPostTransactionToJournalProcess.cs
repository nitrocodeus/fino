﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public interface IPostTransactionToJournalProcess
    {
        ITransactionToJournalRepository TransactionToJournalRepo { get; set; }
        ProcessResult GetActiveTransactionForJournal(DateTime p_Start, DateTime p_End);
        ProcessResult PostTransactionForJournal(List<TransaksiToJurnalModel> p_Model);
        ProcessResult GetNeracaSaldo(int p_PeriodId);
        ProcessResult GetActiveAccountingPeriod();
        ProcessResult GetDailyJournal(DateTime p_Start, DateTime p_End);
        ProcessResult GetAvailablePeriod();
    }
}
