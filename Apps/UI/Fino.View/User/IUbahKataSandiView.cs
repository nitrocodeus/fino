﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Windows.Forms;

namespace Fino.View
{
    public interface IUbahKataSandiView : IView
    {
        event Action BtnSimpanClicked;
        event Action BtnTutupClicked;
        event Action BtnBrowseClicked;

        ChangePasswordModel CurrentModel { get; set; }
        RestorePasswordModel RestorePasswordModel { get; set; }
        TextBox txtUsername { get; set; }

        string Username { get; set; }
        string OldPassword { get; set; }
        string NewPassword { get; set; }
        string FileKunci { get; set; }
        bool IsRestorePassword { get; set; }

        void SetBinding();
        void SetRestorePasswordBinding();
        void LoadData();
        void ShowLupaKataSandiForm();
    }
}
