﻿using Fino.Lib.Core;
using System.Drawing;
using System.Windows.Forms;

namespace Fino.Lib.Win32
{
    public class BaseForm : Form
    {
        public BaseForm()
        {
            this.Font = new Font(Constants.FontName, Constants.FontSize);
        }
    }
}
