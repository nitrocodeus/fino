﻿
namespace Fino.Datalib.Entity
{
    public class BiayaLayanan
    {
        public int biaya_id {get;set;}
        public int layanan_id { get; set; }
        public virtual RefLayanan Layanan { get; set; }
        public virtual RefBiaya Biaya {get;set;}
    }
}
