﻿using Fino.Lib.Core;
using Fino.Lib.Core.Util;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class UbahKataSandiProcess : IUbahKataSandiProcess
    {
        public IUserRepository UserRepo { get; set; }

        private bool ValidateData(ChangePasswordModel entity)
        {
            if (entity.NewPassword != entity.RepeatNewPassword)
            {
                throw new ApplicationException(AppResource.VLD_NEWPASSWORD_NOTMATCH);
            }

            return true;
        }

        private bool ValidateRestorePassword(RestorePasswordModel entity)
        {
            string storedKey = File.ReadAllText(entity.KeyFile);

            if (EnvironmentHelper.Instance.GetEnvironmentHashID() != storedKey)
            {
                throw new ApplicationException(AppResource.VLD_KEYFILE_INVALID);
            }

            return true;
        }


        public ProcessResult UpdatePassword(ChangePasswordModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                if (ValidateData(entity))
                {
                    bool updateSuccessful = UserRepo.UpdatePassword(entity);

                    result.DataResult = updateSuccessful;
                    result.IsSucess = true;
                }
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult SetNewPassword(RestorePasswordModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                if (ValidateRestorePassword(entity))
                {                    
                    bool updateSuccessful = UserRepo.SetNewPassword(entity);

                    result.DataResult = updateSuccessful;
                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
