﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefAccountConfiguration
    {
        internal static void ConfigureRefAccount(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefAccount>().HasKey(e => e.account_id);
            modelBuilder.Entity<RefAccount>().Property(e => e.account_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RefAccount>().Property(e => e.account_code).HasMaxLength(20);
            modelBuilder.Entity<RefAccount>().Property(e => e.account_name).HasMaxLength(100);
        }
    }
}
