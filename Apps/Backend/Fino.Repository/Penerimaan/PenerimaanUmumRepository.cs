﻿using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class PenerimaanUmumRepository : BaseRepository, IPenerimaanUmumRepository
    {
        private PenerimaanUmum GetExistingPenerimaanUmum(int penerimaanUmumId)
        {
            return this.Context.PenerimaanUmums.Where(x => x.penerimaanumum_Id == penerimaanUmumId).FirstOrDefault();
        }

        public PenerimaanUmumModel UpdatePenerimaanUmum(PenerimaanUmumModel entity)
        {
            var existingPenerimaanUmum = GetExistingPenerimaanUmum(entity.PenerimaanUmumId);

            if (existingPenerimaanUmum == null)
            {
                PenerimaanUmum penerimaanUmum = new PenerimaanUmum
                {
                    penerimaanumum_Id = entity.PenerimaanUmumId,
                    Deskripsi = entity.Deskripsi,
                    Nilai = entity.NilaiBiaya,
                    DatePaid = entity.DatePaid,
                    Biaya_Id = entity.RefBiayaId,
                    IsActive = true,
                    IsPaid = true,
                    refsumberpenerimaan_id = entity.SumberPenerimaanUmumId
                };

                existingPenerimaanUmum = this.Context.PenerimaanUmums.Add(penerimaanUmum);
            }
            else
            {
                existingPenerimaanUmum.Deskripsi = entity.Deskripsi;
                existingPenerimaanUmum.Nilai = entity.NilaiBiaya;
                existingPenerimaanUmum.DatePaid = entity.DatePaid;
                existingPenerimaanUmum.IsPaid = entity.IsPaid.HasValue ? entity.IsPaid.Value : false;
                existingPenerimaanUmum.Status_Id = entity.Status_Id;
                existingPenerimaanUmum.Status_Date = entity.Status_Date;
            }

            this.Context.SaveChanges();

            entity.PenerimaanUmumId = existingPenerimaanUmum.penerimaanumum_Id;

            return entity;
        }

        public CetakPenerimaanUmumModel AddNotaPenerimaanUmum(CetakPenerimaanUmumModel entity, PenerimaanUmumModel penerimaanUmumModel)
        {
            NotaPenerimaanUmum penerimaanUmum = new NotaPenerimaanUmum
                {
                    SumberPenerimaanId = penerimaanUmumModel.SumberPenerimaanUmumId,
                    user_id = entity.User.UserId,
                    tanggal_cetak = entity.TanggalPenerimaan
                };

            NotaPenerimaanUmum npu = this.Context.NotaPenerimaanUmums.Add(penerimaanUmum);

            this.Context.SaveChanges();

            entity.NotaPenerimaanUmumId = npu.nota_penerimaan_Id;

            PenerimaanUmum pbp = new PenerimaanUmum();
            pbp.penerimaanumum_Id = penerimaanUmumModel.PenerimaanUmumId;

            Context.PenerimaanUmums.Attach(pbp);

            pbp.nota_penerimaan_id = npu.nota_penerimaan_Id;

            this.Context.SaveChanges();

            return entity;
        }


        public List<PembatalanPenerimaanUmumModel> GetPembatalanPenerimaanUmumList(DateTime p_FromDate, DateTime p_ToDate)
        {
            var startDate = new DateTime(p_FromDate.Year, p_FromDate.Month, p_FromDate.Day, 0, 0, 0);
            var endDate = new DateTime(p_ToDate.Year, p_ToDate.Month, p_ToDate.Day, 23, 59, 59);

            var pembatalanPenerimaanUmumList = this.Context.PenerimaanUmums.Where(x => x.DatePaid.HasValue && x.DatePaid.Value >= startDate && x.DatePaid.Value < endDate && x.IsActive && x.IsPaid && x.Status_Id < 2).ToList();

            List<PembatalanPenerimaanUmumModel> result = new List<PembatalanPenerimaanUmumModel>();

            foreach (var pembatalanPenerimaanUmum in pembatalanPenerimaanUmumList)
            {
                result.Add(new PembatalanPenerimaanUmumModel
                {
                    PenerimaanUmum_Id = pembatalanPenerimaanUmum.penerimaanumum_Id,
                    RefSumberPenerimaan_Id = pembatalanPenerimaanUmum.refsumberpenerimaan_id,
                    RefSumberPenerimaan_Name = pembatalanPenerimaanUmum.SumberPenerimaan.refsumberpenerimaan_name,
                    Deskripsi = pembatalanPenerimaanUmum.Deskripsi,
                    Nilai = pembatalanPenerimaanUmum.Nilai,
                    NotaPenerimaanUmumId = pembatalanPenerimaanUmum.nota_penerimaan_id.HasValue ? pembatalanPenerimaanUmum.nota_penerimaan_id.Value : 0,
                    DatePaid = pembatalanPenerimaanUmum.DatePaid,
                });
            }

            return result;
        }

        public void DeletePenerimaanUmum(PenerimaanUmumModel entity)
        {
            var existingPenerimaanUmum = GetExistingPenerimaanUmum(entity.PenerimaanUmumId);

            if (existingPenerimaanUmum != null)
            {
                this.Context.PenerimaanUmums.Remove(existingPenerimaanUmum);
                this.Context.SaveChanges();
            }
        }

        public PenerimaanUmumModel GetPenerimaanUmum(int p_PenerimaanUmumId)
        {
            PenerimaanUmumModel result = null;

            var existingPenerimaanUmum = GetExistingPenerimaanUmum(p_PenerimaanUmumId);

            if (existingPenerimaanUmum != null)
            {
                result = new PenerimaanUmumModel
                {
                    PenerimaanUmumId = existingPenerimaanUmum.penerimaanumum_Id,
                    NamaBiaya = existingPenerimaanUmum.Biaya.nama,
                    NilaiBiaya = existingPenerimaanUmum.Nilai,
                    IsPaid = existingPenerimaanUmum.IsPaid,
                    NoPosBiaya = existingPenerimaanUmum.Biaya.noposbiaya,
                    RefBiayaId = existingPenerimaanUmum.Biaya_Id,
                    SumberPenerimaanUmumId = existingPenerimaanUmum.refsumberpenerimaan_id,
                    Deskripsi = existingPenerimaanUmum.Deskripsi,
                    DatePaid = existingPenerimaanUmum.DatePaid,
                    Status_Id = existingPenerimaanUmum.Status_Id
                };
            }

            return result;
        }
    }
}
