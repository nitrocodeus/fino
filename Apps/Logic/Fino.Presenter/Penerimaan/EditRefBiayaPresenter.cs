﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class EditRefBiayaPresenter : BasePresenter
    {        
        private readonly IKonfigurasiBiayaProcess _konfigurasiBiayaProcess;
        private readonly IEditRefBiayaView _editKonfigurasiBiayaView;

        public EditRefBiayaPresenter(IKonfigurasiBiayaProcess p_Process, IEditRefBiayaView p_View) : base(p_View)
        {
            _konfigurasiBiayaProcess = p_Process;
            _editKonfigurasiBiayaView = p_View;

            _editKonfigurasiBiayaView.Load += p_View_Load;
            _editKonfigurasiBiayaView.BtnBatalClicked += _editKonfigurasiBiayaView_BtnBatalClicked;
            _editKonfigurasiBiayaView.BtnSimpanClicked += _editKonfigurasiBiayaView_BtnSimpanClicked;
            _editKonfigurasiBiayaView.CmbRepetisiValueChanged += CmbRepetisiValueChanged;
        }

        public void CmbRepetisiValueChanged()
        {
            if ((RepetisiEnum)_editKonfigurasiBiayaView.Repetisi == RepetisiEnum.NONE)
            {
                _editKonfigurasiBiayaView.CmbJTBulanEnabled = false;
            }
            else
            {
                _editKonfigurasiBiayaView.CmbJTBulanEnabled = true;
            }
        }

        void SelectedKonfigurasiBiaya_ErrorValidation(object sender, CustomEventArgs<string> e)
        {
            WinApi.ShowErrorMessage(e.Data, AppResource.CAP_VALIDASI_ERROR);
        }

        void _editKonfigurasiBiayaView_BtnSimpanClicked()
        {            
            var context = new ValidationContext(_editKonfigurasiBiayaView.SelectedKonfigurasiBiaya, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(_editKonfigurasiBiayaView.SelectedKonfigurasiBiaya, context, results, true);

            if (isValid)
            {
                if (_editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Biaya_id == 0)
                {
                    // Call save process here
                    ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => _konfigurasiBiayaProcess.TambahRefBiaya(_editKonfigurasiBiayaView.SelectedKonfigurasiBiaya)),
                                AppResource.MSG_SAVING_REF_BIAYA);

                    if (result.IsSucess)
                    {
                        WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                        _editKonfigurasiBiayaView.KonfigurasiBiayaView.ReloadDataEvent();
                        _editKonfigurasiBiayaView.Close();
                    }
                    else
                    {
                        WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
                    }
                }
                else
                {
                    // Call save process here
                    ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => _konfigurasiBiayaProcess.UpdateRefBiaya(_editKonfigurasiBiayaView.SelectedKonfigurasiBiaya)),
                                AppResource.MSG_SAVING_REF_BIAYA);

                    if (result.IsSucess)
                    {
                        WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                        _editKonfigurasiBiayaView.KonfigurasiBiayaView.ReloadDataEvent();
                        _editKonfigurasiBiayaView.Close();
                    }
                    else
                    {
                        WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
                    }
                }
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        void _editKonfigurasiBiayaView_BtnBatalClicked()
        {
            _editKonfigurasiBiayaView.KonfigurasiBiayaView.ReloadDataEvent();
            _editKonfigurasiBiayaView.Close();
        }

        void p_View_Load(object sender, EventArgs e)
        {            
            if (_editKonfigurasiBiayaView.SelectedKonfigurasiBiaya != null)
            {
                _editKonfigurasiBiayaView.NamaBiaya = _editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Nama;
                _editKonfigurasiBiayaView.KodeBiaya = _editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Code;

                if (_editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Repetisi > 0)
                {
                    _editKonfigurasiBiayaView.Repetisi = _editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Repetisi;
                }
                else
                {
                    _editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Repetisi = 0;
                    _editKonfigurasiBiayaView.CmbJTBulanEnabled = false;
                }

                _editKonfigurasiBiayaView.TanggalJatuhTempo = _editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Jt_tanggal;

                if (_editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Jt_bulan > 0)
                {
                    _editKonfigurasiBiayaView.BulanJatuhTempo = _editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Jt_bulan;
                }
                else
                {
                    _editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Jt_bulan = 0; 
                }

                if (_editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Mulai_tanggal != DateTime.MinValue)
                {
                    _editKonfigurasiBiayaView.MulaiTanggal = _editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Mulai_tanggal;
                }
                else
                {
                    _editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.Mulai_tanggal = DateTime.Today;
                }
            }

            _editKonfigurasiBiayaView.SelectedKonfigurasiBiaya.ErrorValidation += SelectedKonfigurasiBiaya_ErrorValidation;
            _editKonfigurasiBiayaView.SetBinding();
        }
    }
}
