﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public interface IMutasiTabunganProcess
    {
        IMutasiTabunganRepository MutasiTabunganRepo { get; set; }
        ProcessResult NewTransaksiRekening(MutasiTabunganModel p_Model);
        ProcessResult InitiateMutasiByNoRekening(string pNoRekening);
        ProcessResult GetAllSaldoAsOn(DateTime p_Date);
        ProcessResult GetAllMutasiAsOn(DateTime p_Date);
    }
}
