﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public interface IRefPayableProcess
    {
        IRefPayableRepository RefPayableRepo { get; set; }

        ProcessResult GetRefPayable();

        ProcessResult GetRefPayable(int p_Id);

        ProcessResult TambahRefPayable(RefPayableModel p_RefPayableModel);

        ProcessResult UpdateRefPayable(RefPayableModel p_RefPayableModel);

        ProcessResult DeleteRefPayable(RefPayableModel p_RefPayableModel);
    }
}
