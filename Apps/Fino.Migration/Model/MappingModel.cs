﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Migration.Model
{
    public class MappingModel
    {
        public int Index { get; set; }
        public string SourceColumn { get; set; }
        public string TargetColumn { get; set; }
        public string DataType { get; set; }
        public ConverterModel Converter { get; set; }
    }
}
