namespace Fino.Datalib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColStatusDateInPayable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payables", "status_date", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Payables", "status_date");
        }
    }
}
