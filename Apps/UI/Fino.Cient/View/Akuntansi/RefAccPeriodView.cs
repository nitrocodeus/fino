﻿using Fino.Cient.Startup;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;

namespace Fino.Cient
{
    public partial class RefAccPeriodView : BaseForm, IRefAccPeriodView
    {
        #region Events

        public event Action AddNewButtonClicked;
        public event Action EditButtonClicked;
        public event Action OpenPeriodButtonClicked;
        public event Action ClosePeriodButtonClicked;
        public event Action DeactivateButtonClicked;
        public event Action CloseButtonClicked;
        public event Action GvAccPeriodDoubleClicked;
        public event Action GvAccPeriodSelectionChanged;
        public event Action ReloadData;

        #endregion Events

        public RefAccPeriodView()
        {
            InitializeComponent();

            this.btnTambah.Click += btnTambah_Click;
            this.btnUbah.Click += btnUbah_Click;
            this.btnBukaPeriode.Click += btnBukaPeriode_Click;
            this.btnTutupPeriode.Click += btnTutupPeriode_Click;
            this.btnDeaktivasi.Click += btnDeaktivasi_Click;
            this.btnTutup.Click += btnTutup_Click;
            this.gvRefAccPeriod.DoubleClick += gvRefAccPeriod_DoubleClick;
            this.gvRefAccPeriod.SelectionChanged += gvRefAccPeriod_SelectionChanged;
        }

        #region Control Events

        private void gvRefAccPeriod_DoubleClick(object sender, EventArgs e)
        {
            if (GvAccPeriodDoubleClicked != null)
            {
                GvAccPeriodDoubleClicked();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        private void btnDeaktivasi_Click(object sender, EventArgs e)
        {
            if (DeactivateButtonClicked != null)
            {
                DeactivateButtonClicked();
            }
        }

        private void btnTutupPeriode_Click(object sender, EventArgs e)
        {
            if (ClosePeriodButtonClicked != null)
            {
                ClosePeriodButtonClicked();
            }
        }

        private void btnBukaPeriode_Click(object sender, EventArgs e)
        {
            if (OpenPeriodButtonClicked != null)
            {
                OpenPeriodButtonClicked();
            }
        }

        private void btnUbah_Click(object sender, EventArgs e)
        {
            if (EditButtonClicked != null)
            {
                EditButtonClicked();
            }
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            if (AddNewButtonClicked != null)
            {
                AddNewButtonClicked();
            }
        }

        private void gvRefAccPeriod_SelectionChanged(object sender, EventArgs e)
        {
            if (GvAccPeriodSelectionChanged != null)
            {
                GvAccPeriodSelectionChanged();
            }
        }

        #endregion Control Events

        #region IRefAccPeriodView

        public void SetData(List<RefAccPeriodModel> p_ListRefAccPeriod)
        {
            gvRefAccPeriod.DataSource = p_ListRefAccPeriod;
        }

        public void RefreshDataGrid()
        {
            gvRefAccPeriod.Refresh();
        }

        public void ShowEditRefAcPeriodView()
        {
            if (gvRefAccPeriod.SelectedCells.Count > 0)
            {
                ViewLocator.Instance.EditRefAccPeriodView.RefAccPeriodView = this;
                ViewLocator.Instance.EditRefAccPeriodView.ShowForm(GetSelectedRefAccPeriod());
            }
        }

        public void ShowTambahRefAccPeriodView()
        {
            ViewLocator.Instance.EditRefAccPeriodView.RefAccPeriodView = this;
            ViewLocator.Instance.EditRefAccPeriodView.ShowForm(null);
        }

        public void ReloadDataEvent()
        {
            if (ReloadData != null)
            {
                ReloadData();
            }
        }

        public RefAccPeriodModel GetSelectedRefAccPeriod()
        {
            RefAccPeriodModel result = null;

            if (gvRefAccPeriod.SelectedCells.Count > 0)
            {
                result = (RefAccPeriodModel)gvRefAccPeriod.Rows[gvRefAccPeriod.SelectedCells[0].RowIndex].DataBoundItem;
            }

            return result;
        }

        public void ChangeControlEnabled(bool btnUbahEnabled, bool btnBukaPeriodeEnabled, bool btnTutupPeriodeEnabled, bool btnDeaktivasiEnabled)
        {
            this.btnUbah.Enabled = btnUbahEnabled;
            this.btnBukaPeriode.Enabled = btnBukaPeriodeEnabled;
            this.btnTutupPeriode.Enabled = btnTutupPeriodeEnabled;
            this.btnDeaktivasi.Enabled = btnDeaktivasiEnabled;
        }

        #endregion
    }
}
