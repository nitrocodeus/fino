﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class PenerimaanUmumConfiguration
    {
        internal static void ConfigurePenerimaanUmum(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PenerimaanUmum>().HasKey(e => e.penerimaanumum_Id);
            modelBuilder.Entity<PenerimaanUmum>().Property(e => e.penerimaanumum_Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<PenerimaanUmum>().Property(e => e.Deskripsi).HasMaxLength(70);
            
            modelBuilder.Entity<PenerimaanUmum>()
                .HasRequired(e => e.Biaya)
                .WithMany(e => e.PenerimaanUmums)
                .HasForeignKey(e => e.Biaya_Id);

            modelBuilder.Entity<PenerimaanUmum>()
                .HasRequired(e => e.SumberPenerimaan)
                .WithMany(e => e.PenerimaanUmums)
                .HasForeignKey(e => e.refsumberpenerimaan_id);

            modelBuilder.Entity<PenerimaanUmum>()
                .HasOptional(e => e.NotaPenerimaanUmum)
                .WithMany(e => e.PenerimaanUmums)
                .HasForeignKey(e => e.nota_penerimaan_id);
        }
    }
}
