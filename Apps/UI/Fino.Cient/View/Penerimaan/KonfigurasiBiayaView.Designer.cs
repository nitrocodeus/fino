﻿namespace Fino.Cient
{
    partial class KonfigurasiBiayaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KonfigurasiBiayaView));
            this.gvMasterRefBiaya = new System.Windows.Forms.DataGridView();
            this.biayaidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.repetisiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jttanggalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jtbulanDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aktifDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.mulaitanggalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refBiayaModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvSelectedRefBiaya = new System.Windows.Forms.DataGridView();
            this.biayaidDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.repetisiDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jttanggalDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jtbulanDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aktifDataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.mulaitanggalDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAddPosBiaya = new System.Windows.Forms.Button();
            this.btnAddAllPosBiaya = new System.Windows.Forms.Button();
            this.btnDeletePosBiaya = new System.Windows.Forms.Button();
            this.btnDeleteAllPosBiaya = new System.Windows.Forms.Button();
            this.btnTutup = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gvMasterRefBiaya)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refBiayaModelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSelectedRefBiaya)).BeginInit();
            this.SuspendLayout();
            // 
            // gvMasterRefBiaya
            // 
            this.gvMasterRefBiaya.AllowUserToAddRows = false;
            this.gvMasterRefBiaya.AllowUserToDeleteRows = false;
            this.gvMasterRefBiaya.AutoGenerateColumns = false;
            this.gvMasterRefBiaya.BackgroundColor = System.Drawing.Color.White;
            this.gvMasterRefBiaya.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvMasterRefBiaya.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.biayaidDataGridViewTextBoxColumn,
            this.codeDataGridViewTextBoxColumn,
            this.namaDataGridViewTextBoxColumn,
            this.repetisiDataGridViewTextBoxColumn,
            this.jttanggalDataGridViewTextBoxColumn,
            this.jtbulanDataGridViewTextBoxColumn,
            this.aktifDataGridViewCheckBoxColumn,
            this.mulaitanggalDataGridViewTextBoxColumn,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvMasterRefBiaya.DataSource = this.refBiayaModelBindingSource;
            this.gvMasterRefBiaya.Location = new System.Drawing.Point(23, 20);
            this.gvMasterRefBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.gvMasterRefBiaya.MultiSelect = false;
            this.gvMasterRefBiaya.Name = "gvMasterRefBiaya";
            this.gvMasterRefBiaya.ReadOnly = true;
            this.gvMasterRefBiaya.RowHeadersVisible = false;
            this.gvMasterRefBiaya.Size = new System.Drawing.Size(368, 231);
            this.gvMasterRefBiaya.TabIndex = 0;
            // 
            // biayaidDataGridViewTextBoxColumn
            // 
            this.biayaidDataGridViewTextBoxColumn.DataPropertyName = "Biaya_id";
            this.biayaidDataGridViewTextBoxColumn.HeaderText = "Biaya_id";
            this.biayaidDataGridViewTextBoxColumn.Name = "biayaidDataGridViewTextBoxColumn";
            this.biayaidDataGridViewTextBoxColumn.ReadOnly = true;
            this.biayaidDataGridViewTextBoxColumn.Visible = false;
            // 
            // codeDataGridViewTextBoxColumn
            // 
            this.codeDataGridViewTextBoxColumn.DataPropertyName = "Code";
            this.codeDataGridViewTextBoxColumn.HeaderText = "Kode";
            this.codeDataGridViewTextBoxColumn.Name = "codeDataGridViewTextBoxColumn";
            this.codeDataGridViewTextBoxColumn.ReadOnly = true;
            this.codeDataGridViewTextBoxColumn.Width = 70;
            // 
            // namaDataGridViewTextBoxColumn
            // 
            this.namaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.namaDataGridViewTextBoxColumn.DataPropertyName = "Nama";
            this.namaDataGridViewTextBoxColumn.HeaderText = "Nama";
            this.namaDataGridViewTextBoxColumn.Name = "namaDataGridViewTextBoxColumn";
            this.namaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // repetisiDataGridViewTextBoxColumn
            // 
            this.repetisiDataGridViewTextBoxColumn.DataPropertyName = "Repetisi";
            this.repetisiDataGridViewTextBoxColumn.HeaderText = "Repetisi";
            this.repetisiDataGridViewTextBoxColumn.Name = "repetisiDataGridViewTextBoxColumn";
            this.repetisiDataGridViewTextBoxColumn.ReadOnly = true;
            this.repetisiDataGridViewTextBoxColumn.Visible = false;
            // 
            // jttanggalDataGridViewTextBoxColumn
            // 
            this.jttanggalDataGridViewTextBoxColumn.DataPropertyName = "Jt_tanggal";
            this.jttanggalDataGridViewTextBoxColumn.HeaderText = "Jt_tanggal";
            this.jttanggalDataGridViewTextBoxColumn.Name = "jttanggalDataGridViewTextBoxColumn";
            this.jttanggalDataGridViewTextBoxColumn.ReadOnly = true;
            this.jttanggalDataGridViewTextBoxColumn.Visible = false;
            // 
            // jtbulanDataGridViewTextBoxColumn
            // 
            this.jtbulanDataGridViewTextBoxColumn.DataPropertyName = "Jt_bulan";
            this.jtbulanDataGridViewTextBoxColumn.HeaderText = "Jt_bulan";
            this.jtbulanDataGridViewTextBoxColumn.Name = "jtbulanDataGridViewTextBoxColumn";
            this.jtbulanDataGridViewTextBoxColumn.ReadOnly = true;
            this.jtbulanDataGridViewTextBoxColumn.Visible = false;
            // 
            // aktifDataGridViewCheckBoxColumn
            // 
            this.aktifDataGridViewCheckBoxColumn.DataPropertyName = "Aktif";
            this.aktifDataGridViewCheckBoxColumn.HeaderText = "Aktif";
            this.aktifDataGridViewCheckBoxColumn.Name = "aktifDataGridViewCheckBoxColumn";
            this.aktifDataGridViewCheckBoxColumn.ReadOnly = true;
            this.aktifDataGridViewCheckBoxColumn.Visible = false;
            // 
            // mulaitanggalDataGridViewTextBoxColumn
            // 
            this.mulaitanggalDataGridViewTextBoxColumn.DataPropertyName = "Mulai_tanggal";
            this.mulaitanggalDataGridViewTextBoxColumn.HeaderText = "Mulai_tanggal";
            this.mulaitanggalDataGridViewTextBoxColumn.Name = "mulaitanggalDataGridViewTextBoxColumn";
            this.mulaitanggalDataGridViewTextBoxColumn.ReadOnly = true;
            this.mulaitanggalDataGridViewTextBoxColumn.Visible = false;
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.ReadOnly = true;
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // refBiayaModelBindingSource
            // 
            this.refBiayaModelBindingSource.DataSource = typeof(Fino.Model.RefBiayaModel);
            // 
            // gvSelectedRefBiaya
            // 
            this.gvSelectedRefBiaya.AllowUserToAddRows = false;
            this.gvSelectedRefBiaya.AllowUserToDeleteRows = false;
            this.gvSelectedRefBiaya.AutoGenerateColumns = false;
            this.gvSelectedRefBiaya.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvSelectedRefBiaya.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.biayaidDataGridViewTextBoxColumn1,
            this.codeDataGridViewTextBoxColumn1,
            this.namaDataGridViewTextBoxColumn1,
            this.repetisiDataGridViewTextBoxColumn1,
            this.jttanggalDataGridViewTextBoxColumn1,
            this.jtbulanDataGridViewTextBoxColumn1,
            this.aktifDataGridViewCheckBoxColumn1,
            this.mulaitanggalDataGridViewTextBoxColumn1,
            this.validationMessageDataGridViewTextBoxColumn1});
            this.gvSelectedRefBiaya.DataSource = this.refBiayaModelBindingSource;
            this.gvSelectedRefBiaya.Location = new System.Drawing.Point(483, 16);
            this.gvSelectedRefBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.gvSelectedRefBiaya.Name = "gvSelectedRefBiaya";
            this.gvSelectedRefBiaya.ReadOnly = true;
            this.gvSelectedRefBiaya.RowHeadersVisible = false;
            this.gvSelectedRefBiaya.Size = new System.Drawing.Size(380, 231);
            this.gvSelectedRefBiaya.TabIndex = 1;
            // 
            // biayaidDataGridViewTextBoxColumn1
            // 
            this.biayaidDataGridViewTextBoxColumn1.DataPropertyName = "Biaya_id";
            this.biayaidDataGridViewTextBoxColumn1.HeaderText = "Biaya_id";
            this.biayaidDataGridViewTextBoxColumn1.Name = "biayaidDataGridViewTextBoxColumn1";
            this.biayaidDataGridViewTextBoxColumn1.ReadOnly = true;
            this.biayaidDataGridViewTextBoxColumn1.Visible = false;
            // 
            // codeDataGridViewTextBoxColumn1
            // 
            this.codeDataGridViewTextBoxColumn1.DataPropertyName = "Code";
            this.codeDataGridViewTextBoxColumn1.HeaderText = "Kode";
            this.codeDataGridViewTextBoxColumn1.Name = "codeDataGridViewTextBoxColumn1";
            this.codeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.codeDataGridViewTextBoxColumn1.Width = 70;
            // 
            // namaDataGridViewTextBoxColumn1
            // 
            this.namaDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.namaDataGridViewTextBoxColumn1.DataPropertyName = "Nama";
            this.namaDataGridViewTextBoxColumn1.HeaderText = "Nama";
            this.namaDataGridViewTextBoxColumn1.Name = "namaDataGridViewTextBoxColumn1";
            this.namaDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // repetisiDataGridViewTextBoxColumn1
            // 
            this.repetisiDataGridViewTextBoxColumn1.DataPropertyName = "Repetisi";
            this.repetisiDataGridViewTextBoxColumn1.HeaderText = "Repetisi";
            this.repetisiDataGridViewTextBoxColumn1.Name = "repetisiDataGridViewTextBoxColumn1";
            this.repetisiDataGridViewTextBoxColumn1.ReadOnly = true;
            this.repetisiDataGridViewTextBoxColumn1.Visible = false;
            // 
            // jttanggalDataGridViewTextBoxColumn1
            // 
            this.jttanggalDataGridViewTextBoxColumn1.DataPropertyName = "Jt_tanggal";
            this.jttanggalDataGridViewTextBoxColumn1.HeaderText = "Jt_tanggal";
            this.jttanggalDataGridViewTextBoxColumn1.Name = "jttanggalDataGridViewTextBoxColumn1";
            this.jttanggalDataGridViewTextBoxColumn1.ReadOnly = true;
            this.jttanggalDataGridViewTextBoxColumn1.Visible = false;
            // 
            // jtbulanDataGridViewTextBoxColumn1
            // 
            this.jtbulanDataGridViewTextBoxColumn1.DataPropertyName = "Jt_bulan";
            this.jtbulanDataGridViewTextBoxColumn1.HeaderText = "Jt_bulan";
            this.jtbulanDataGridViewTextBoxColumn1.Name = "jtbulanDataGridViewTextBoxColumn1";
            this.jtbulanDataGridViewTextBoxColumn1.ReadOnly = true;
            this.jtbulanDataGridViewTextBoxColumn1.Visible = false;
            // 
            // aktifDataGridViewCheckBoxColumn1
            // 
            this.aktifDataGridViewCheckBoxColumn1.DataPropertyName = "Aktif";
            this.aktifDataGridViewCheckBoxColumn1.HeaderText = "Aktif";
            this.aktifDataGridViewCheckBoxColumn1.Name = "aktifDataGridViewCheckBoxColumn1";
            this.aktifDataGridViewCheckBoxColumn1.ReadOnly = true;
            this.aktifDataGridViewCheckBoxColumn1.Visible = false;
            // 
            // mulaitanggalDataGridViewTextBoxColumn1
            // 
            this.mulaitanggalDataGridViewTextBoxColumn1.DataPropertyName = "Mulai_tanggal";
            this.mulaitanggalDataGridViewTextBoxColumn1.HeaderText = "Mulai_tanggal";
            this.mulaitanggalDataGridViewTextBoxColumn1.Name = "mulaitanggalDataGridViewTextBoxColumn1";
            this.mulaitanggalDataGridViewTextBoxColumn1.ReadOnly = true;
            this.mulaitanggalDataGridViewTextBoxColumn1.Visible = false;
            // 
            // validationMessageDataGridViewTextBoxColumn1
            // 
            this.validationMessageDataGridViewTextBoxColumn1.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn1.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn1.Name = "validationMessageDataGridViewTextBoxColumn1";
            this.validationMessageDataGridViewTextBoxColumn1.ReadOnly = true;
            this.validationMessageDataGridViewTextBoxColumn1.Visible = false;
            // 
            // btnAddPosBiaya
            // 
            this.btnAddPosBiaya.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnAddPosBiaya.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddPosBiaya.ForeColor = System.Drawing.Color.White;
            this.btnAddPosBiaya.Location = new System.Drawing.Point(406, 51);
            this.btnAddPosBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnAddPosBiaya.Name = "btnAddPosBiaya";
            this.btnAddPosBiaya.Size = new System.Drawing.Size(59, 35);
            this.btnAddPosBiaya.TabIndex = 2;
            this.btnAddPosBiaya.Text = ">";
            this.btnAddPosBiaya.UseVisualStyleBackColor = false;
            this.btnAddPosBiaya.Click += new System.EventHandler(this.btnAddPosBiaya_Click);
            // 
            // btnAddAllPosBiaya
            // 
            this.btnAddAllPosBiaya.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnAddAllPosBiaya.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddAllPosBiaya.ForeColor = System.Drawing.Color.White;
            this.btnAddAllPosBiaya.Location = new System.Drawing.Point(406, 95);
            this.btnAddAllPosBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnAddAllPosBiaya.Name = "btnAddAllPosBiaya";
            this.btnAddAllPosBiaya.Size = new System.Drawing.Size(59, 35);
            this.btnAddAllPosBiaya.TabIndex = 2;
            this.btnAddAllPosBiaya.Text = ">>";
            this.btnAddAllPosBiaya.UseVisualStyleBackColor = false;
            this.btnAddAllPosBiaya.Click += new System.EventHandler(this.btnAddAllPosBiaya_Click);
            // 
            // btnDeletePosBiaya
            // 
            this.btnDeletePosBiaya.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDeletePosBiaya.Location = new System.Drawing.Point(406, 165);
            this.btnDeletePosBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnDeletePosBiaya.Name = "btnDeletePosBiaya";
            this.btnDeletePosBiaya.Size = new System.Drawing.Size(59, 35);
            this.btnDeletePosBiaya.TabIndex = 2;
            this.btnDeletePosBiaya.Text = "<";
            this.btnDeletePosBiaya.UseVisualStyleBackColor = true;
            this.btnDeletePosBiaya.Click += new System.EventHandler(this.btnDeletePosBiaya_Click);
            // 
            // btnDeleteAllPosBiaya
            // 
            this.btnDeleteAllPosBiaya.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDeleteAllPosBiaya.Location = new System.Drawing.Point(406, 209);
            this.btnDeleteAllPosBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnDeleteAllPosBiaya.Name = "btnDeleteAllPosBiaya";
            this.btnDeleteAllPosBiaya.Size = new System.Drawing.Size(59, 35);
            this.btnDeleteAllPosBiaya.TabIndex = 2;
            this.btnDeleteAllPosBiaya.Text = "<<";
            this.btnDeleteAllPosBiaya.UseVisualStyleBackColor = true;
            this.btnDeleteAllPosBiaya.Click += new System.EventHandler(this.btnDeleteAllPosBiaya_Click);
            // 
            // btnTutup
            // 
            this.btnTutup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(711, 282);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(138, 35);
            this.btnTutup.TabIndex = 2;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            this.btnTutup.Click += new System.EventHandler(this.btnTutup_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(561, 282);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(138, 35);
            this.btnSimpan.TabIndex = 2;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // KonfigurasiBiayaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 340);
            this.Controls.Add(this.btnDeleteAllPosBiaya);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnTutup);
            this.Controls.Add(this.btnDeletePosBiaya);
            this.Controls.Add(this.btnAddAllPosBiaya);
            this.Controls.Add(this.btnAddPosBiaya);
            this.Controls.Add(this.gvSelectedRefBiaya);
            this.Controls.Add(this.gvMasterRefBiaya);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "KonfigurasiBiayaView";
            this.Text = "Konfigurasi Penerimaan";
            ((System.ComponentModel.ISupportInitialize)(this.gvMasterRefBiaya)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refBiayaModelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSelectedRefBiaya)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gvMasterRefBiaya;
        private System.Windows.Forms.DataGridView gvSelectedRefBiaya;
        private System.Windows.Forms.Button btnAddPosBiaya;
        private System.Windows.Forms.Button btnAddAllPosBiaya;
        private System.Windows.Forms.Button btnDeletePosBiaya;
        private System.Windows.Forms.Button btnDeleteAllPosBiaya;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.BindingSource refBiayaModelBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn biayaidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn repetisiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jttanggalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jtbulanDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn aktifDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mulaitanggalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn biayaidDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn repetisiDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn jttanggalDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn jtbulanDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn aktifDataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn mulaitanggalDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn1;
    }
}