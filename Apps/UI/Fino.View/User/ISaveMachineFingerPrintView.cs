﻿using Fino.Lib.Win32;
using System;

namespace Fino.View
{
    public interface ISaveMachineFingerPrintView : IView
    {
        event Action ButtonSaveClicked;

        void UpdateFirstRun(bool isFirstRun);
    }
}
