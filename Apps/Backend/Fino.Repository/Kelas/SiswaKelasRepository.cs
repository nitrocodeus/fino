﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class SiswaKelasRepository : BaseRepository, ISiswaKelasRepository
    {
        public SiswaKelas GetExistingSiswaKelas(int pSiswaId, int pTahunAjaranId)
        {
            SiswaKelas returnData = null;

            var siswaKelasEntity = Context.SiswaKelases.Where(e => e.siswa_id.Equals(pSiswaId) && e.tahun_ajaran_id.Equals(pTahunAjaranId)).FirstOrDefault();
            if (siswaKelasEntity != null)
            {
                returnData = siswaKelasEntity;
            }

            return returnData;
        }
    }
}