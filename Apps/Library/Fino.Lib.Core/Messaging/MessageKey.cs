﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core.Messaging
{
    public static class MessageKey
    {
        public const string RELOAD_DATA = "ReloadData";
        public const string UPDATE_CURRENT_SELECTED_DATA = "UpdateCurrentSelectedData";
    }
}
