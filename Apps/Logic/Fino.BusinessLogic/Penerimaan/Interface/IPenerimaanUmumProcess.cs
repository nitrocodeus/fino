﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;

namespace Fino.BusinessLogic
{
    public interface IPenerimaanUmumProcess
    {
        ProcessResult CreateNewRefSPU(RefSumberPUModel entity);
        ProcessResult GetAllRefBiaya();
        ProcessResult GetAllRefSPU();
        ProcessResult GetAllKelas();
        IKonfigurasiBiayaRepository KonfigurasiBiayaRepo { get; set; }
        IPenerimaanUmumRepository PenerimaanUmumRepo { get; set; }
        IRefSPURepository RefSPURepository { get; set; }
        IRefKelasRepository RefKelasRepo { get; set; }
        ProcessResult UpdatePenerimaanUmum(PenerimaanUmumModel entity);
        ProcessResult AddNotaPenerimaanUmum(CetakPenerimaanUmumModel entity, PenerimaanUmumModel penerimaanUmumModel);
    }
}
