﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IReportMutasiView : IView
    {
        event Action BtnOkClicked;
        event Action BtnTutupClicked;

        DateTime SelectedBulan { get; }

        void ShowRPP(List<LaporanMutasiModel> p_Model);
    }
}
