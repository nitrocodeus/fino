﻿using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public interface IUserRepository : IRepository
    {
        SysUser UserLogon(string p_UserName, string p_Password);
        bool UpdatePassword(ChangePasswordModel entity);
        bool SetNewPassword(RestorePasswordModel entity);
    }
}
