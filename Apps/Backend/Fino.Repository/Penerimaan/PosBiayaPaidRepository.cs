﻿using System.Linq;
using Fino.Datalib.Entity;
using Fino.Model;

namespace Fino.Repository.Penerimaan
{
    public class PosBiayaPaidRepository : BaseRepository, IPosBiayaPaidRepository
    {
        private PosBiayaPaid GetExistingPosBiayaPaid(int p_PosBiayaId)
        {
            return this.Context.PosBiayaPaids.Where(x => x.PosBiaya_Id == p_PosBiayaId).FirstOrDefault();
        }

        public void DeletePosBiayaPaid(PosBiayaPaidModel entity)
        {
            var existingPosBiaya = GetExistingPosBiayaPaid(entity.PosBiayaId);

            if (existingPosBiaya != null)
            {
                Context.PosBiayaPaids.Remove(existingPosBiaya);
                Context.SaveChanges();
            }
        }
    }
}
