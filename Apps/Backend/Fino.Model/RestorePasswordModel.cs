﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class RestorePasswordModel : BaseModel
    {
        const string USERNAME_NOTEMPTY = "VLD_USERNAME_NOTEMPTY";
        const string USERNAME_LENGTH = "VLD_USERNAME_LENGTH";
        const string NEWPASSWORD_NOTEMPTY = "VLD_NEWPASSWORD_NOTEMPTY";
        const string NEWPASSWORD_LENGTH = "VLD_NEWPASSWORD_LENGTH";
        const string REPEATNEWPASSWORD_NOTEMPTY = "VLD_REPEATNEWPASSWORD_NOTEMPTY";
        const string REPEATNEWPASSWORD_LENGTH = "VLD_REPEATNEWPASSWORD_LENGTH";
        const string KEYFILE_NOTEMPTY = "VLD_KEYFILE_NOTEMPTY";

        public static readonly string UsernamePropertyName = "Username";
        public static readonly string NewPasswordPropertyName = "NewPassword";
        public static readonly string RepeatNewPasswordPropertyName = "RepeatNewPassword";
        public static readonly string KeyFilePropertyName = "KeyFile";

        string _username;
        string _newPassword;
        string _repeatNewPassword;
        string _keyFile;

        [Display(Name = "Username")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = USERNAME_NOTEMPTY)]
        [StringLength(15, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = USERNAME_LENGTH)]
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(UsernamePropertyName));
                ValidateProperty(value, UsernamePropertyName);
            }
        }

        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NEWPASSWORD_NOTEMPTY)]
        [StringLength(100, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NEWPASSWORD_LENGTH)]
        public string NewPassword
        {
            get
            {
                return _newPassword;
            }
            set
            {
                _newPassword = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NewPasswordPropertyName));
                ValidateProperty(value, NewPasswordPropertyName);
            }
        }

        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = REPEATNEWPASSWORD_NOTEMPTY)]
        [StringLength(100, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = REPEATNEWPASSWORD_LENGTH)]
        public string RepeatNewPassword
        {
            get
            {
                return _repeatNewPassword;
            }
            set
            {
                _repeatNewPassword = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(RepeatNewPasswordPropertyName));
                ValidateProperty(value, RepeatNewPasswordPropertyName);
            }
        }

        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = KEYFILE_NOTEMPTY)]
        public string KeyFile
        {
            get
            {
                return _keyFile;
            }
            set
            {
                _keyFile = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(KeyFilePropertyName));
                ValidateProperty(value, KeyFilePropertyName);
            }
        }
    }
}
