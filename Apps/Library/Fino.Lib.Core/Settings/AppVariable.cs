﻿using Fino.Lib.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core.Settings
{
    public class AppVariable
    {
        #region Singleton
        private static readonly Lazy<AppVariable> lazy = new Lazy<AppVariable>(() => new AppVariable());

        public static AppVariable Instance { get { return lazy.Value; } }

        private AppVariable() { }
        #endregion

        public Dictionary<string, object> AppGlobal { get; set; }

        public IUserModel GetLoggedOnUser()
        {
            return AppGlobal[AppResource.APP_VARIABEL_LOGGEDONUSER] as IUserModel;
        }

        public ITahunAjaranModel GetTahunAjaran()
        {
            return AppGlobal[AppResource.APP_VARIABEL_TAHUNAJARANAKTIF] as ITahunAjaranModel;
        }

        public string GetLoggedOnUserFullName()
        {
            IUserModel loggedOnUser = AppGlobal[AppResource.APP_VARIABEL_LOGGEDONUSER] as IUserModel;

            return (loggedOnUser != null) ? loggedOnUser.FullName : string.Empty;
        }

        public string GetTahunAjaranAktif()
        {
            ITahunAjaranModel aktif = AppGlobal[AppResource.APP_VARIABEL_TAHUNAJARANAKTIF] as ITahunAjaranModel;

            return (aktif != null) ? aktif.Nama : string.Empty;
        }
    }
}
