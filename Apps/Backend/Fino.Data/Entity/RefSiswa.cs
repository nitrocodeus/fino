﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class RefSiswa
    {
        public int siswa_id {get;set;}
        public string siswa_code {get;set;}
        public string nama {get;set;}
        public bool jkelamin { get; set; }
        public bool ispindahan { get; set; }

        public virtual ICollection<AgtLayanan> AnggotaLayanans { get; set; }
        public virtual SiswaAlamat Alamat { get; set; }
        public virtual ICollection<Potongan> Potongans { get; set; }
        public virtual ICollection<SiswaKelas> SiswaKelas { get; set; }
        public virtual ICollection<PosBiaya> PosBiayas { get; set; }
        public virtual ICollection<NotaPembayaran> NotaPembayarans { get; set; }
        public virtual SiswaRekening RekeningTabungan { get; set; }
    }
}
