﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Migration.Model
{
    public class ConverterModel
    {
        private Dictionary<string, string> _dictionary;

        public int Count
        {
            get
            {
                return _dictionary.Count;
            }
        }

        public ConverterModel()
        {
            _dictionary = new Dictionary<string, string>();
        }

        public void AddConverter(string sourceValue, string targetValue)
        {
            if (_dictionary.ContainsKey(sourceValue))
            {
                _dictionary[sourceValue] = targetValue;
            }
            else
            {
                _dictionary.Add(sourceValue, targetValue);
            }
        }

        public string this[string sourceValue]
        {
            get
            {
                if (_dictionary.ContainsKey(sourceValue))
                {
                    return _dictionary[sourceValue];
                }
                return "";
            }
        }
    }
}
