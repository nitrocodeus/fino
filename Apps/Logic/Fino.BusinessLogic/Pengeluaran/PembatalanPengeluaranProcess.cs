﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class PembatalanPengeluaranProcess : IPembatalanPengeluaranProcess
    {
        public IPayableRepository PayableRepo { get; set; }
        public IPayablePaidRepository PayablePaidRepo { get; set; }

        public ProcessResult GetPembatalanPengeluaranList(DateTime p_FromDate, DateTime p_ToDate)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<PembatalanPengeluaranModel> dataResult = PayableRepo.GetPembatalanPengeluaranList(p_FromDate, p_ToDate);

                result.DataResult = dataResult.OrderBy(x => x.RefPayable_Name).ThenBy(x => x.JTempo).ThenBy(x => x.DatePaid).ToList();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetPayable(int p_PayableId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = PayableRepo.GetPayable(p_PayableId);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdatePayable(PayableModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                PayableRepo.UpdatePayable(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult DeletePayable(PayableModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                PayableRepo.DeletePayable(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
