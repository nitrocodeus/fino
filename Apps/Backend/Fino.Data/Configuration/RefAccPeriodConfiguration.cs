﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefAccPeriodConfiguration
    {
        internal static void ConfigureRefAccPeriod(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefAccPeriod>().HasKey(e => e.period_id);
            modelBuilder.Entity<RefAccPeriod>().Property(e => e.period_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RefAccPeriod>().Property(e => e.period_name).HasMaxLength(20);
        }
    }
}
