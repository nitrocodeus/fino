﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefRekeningConfiguration
    {
        internal static void ConfigureRefRekening(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefRekening>().HasKey(e => e.rekening_id);
            modelBuilder.Entity<RefRekening>().Property(e => e.rekening_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RefRekening>().Property(e => e.rekening_code).HasMaxLength(20);

        }
    }
}
