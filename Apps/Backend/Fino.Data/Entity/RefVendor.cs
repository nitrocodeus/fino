﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class RefVendor
    {
        public int refvendor_id { get; set; }
        public string vendorname { get; set; }
        public string vendor_addr { get; set; }
        public string vendor_email { get; set; }
        public string vendor_phone { get; set; }

        public virtual ICollection<Payable> payables { get; set; }
    }
}
