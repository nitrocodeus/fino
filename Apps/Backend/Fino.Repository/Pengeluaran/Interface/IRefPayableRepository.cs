﻿using Fino.Datalib.Entity;
using System.Collections.Generic;

namespace Fino.Repository
{
    public interface IRefPayableRepository : IRepository
    {
        List<RefPayable> RefPayableData();
        RefPayable RefPayableData(int p_Id);
        void AddRefPayableData(RefPayable p_RefPayable);
        void DeleteRefPayableData(RefPayable p_RefPayable);
        void UpdateRefPayableData(RefPayable p_RefPayable);

        void SaveChanges();
    }
}
