﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using Fino.View.Siswa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditRefKelasView : BaseForm, IEditRefKelasView
    {
        #region Events

        public event Action BtnBatalClicked;
        public event Action BtnSimpanClicked;

        #endregion Events

        #region Constructors

        public EditRefKelasView()
        {
            InitializeComponent();

            this.btnSimpan.Click += btnSimpan_Click;
            this.btnBatal.Click += btnBatal_Click;
        }

        #region Control Events

        void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        #endregion Control Events

        #endregion Contructors

        #region Public Properties

        public RefKelasModel SelectedRefKelas { get; private set; }
        public IRefKelasView RefKelasView { get; set; }
        public object TingkatDataSource { get; set; }

        public int KelasId { get; set; }

        public string KelasCode
        {
            get
            {
                return txtKelasKode.Text;
            }
            set
            {
                txtKelasKode.Text = value;
            }
        }

        public string Nama
        {
            get
            {
                return txtNama.Text;
            }
            set
            {
                txtNama.Text = value;
            }
        }

        public int Tingkat
        {
            get
            {
                return (int)cboTingkat.SelectedValue;
            }
            set
            {
                cboTingkat.SelectedValue = value;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public void ShowForm(RefKelasModel refKelasModel)
        {
            if (refKelasModel != null)
            {
                SelectedRefKelas = refKelasModel;
                this.Text = "Edit Kelas";
            }
            else
            {
                SelectedRefKelas = new RefKelasModel { Tingkat = RefKelasView.ViewModel.Tingkat };
                this.Text = "Tambah Kelas";
            }

            this.Show();
        }

        #endregion Public Methods

        #region IEditRefKelasView

        public void SetBinding()
        {
            txtKelasKode.DataBindings.Add("Text", SelectedRefKelas, RefKelasModel.KelasCodePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);

            txtNama.DataBindings.Add("Text", SelectedRefKelas, RefKelasModel.NamaPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);

            cboTingkat.DataSource = TingkatDataSource;
            cboTingkat.DisplayMember = "Value";
            cboTingkat.ValueMember = "Id";
            cboTingkat.DataBindings.Clear();
            cboTingkat.DataBindings.Add("SelectedValue", SelectedRefKelas, RefKelasModel.TingkatPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void ClearData()
        {
            
        }

        #endregion IEditRefKelasView
    }
}
