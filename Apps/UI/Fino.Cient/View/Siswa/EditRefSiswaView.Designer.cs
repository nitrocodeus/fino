﻿namespace Fino.Cient
{
    partial class EditRefSiswaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditRefSiswaView));
            this.txtNamaSiswa = new System.Windows.Forms.TextBox();
            this.txtKodeSiswa = new System.Windows.Forms.TextBox();
            this.labelKodeKelas = new System.Windows.Forms.Label();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.lblTingkat = new System.Windows.Forms.Label();
            this.lblNama = new System.Windows.Forms.Label();
            this.rbLakiLaki = new System.Windows.Forms.RadioButton();
            this.rbPerempuan = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // txtNamaSiswa
            // 
            this.txtNamaSiswa.Location = new System.Drawing.Point(153, 43);
            this.txtNamaSiswa.Name = "txtNamaSiswa";
            this.txtNamaSiswa.Size = new System.Drawing.Size(200, 24);
            this.txtNamaSiswa.TabIndex = 43;
            // 
            // txtKodeSiswa
            // 
            this.txtKodeSiswa.Location = new System.Drawing.Point(153, 13);
            this.txtKodeSiswa.Name = "txtKodeSiswa";
            this.txtKodeSiswa.Size = new System.Drawing.Size(200, 24);
            this.txtKodeSiswa.TabIndex = 42;
            // 
            // labelKodeKelas
            // 
            this.labelKodeKelas.AutoSize = true;
            this.labelKodeKelas.Location = new System.Drawing.Point(51, 16);
            this.labelKodeKelas.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelKodeKelas.Name = "labelKodeKelas";
            this.labelKodeKelas.Size = new System.Drawing.Size(95, 17);
            this.labelKodeKelas.TabIndex = 49;
            this.labelKodeKelas.Text = "Kode Siswa:";
            this.labelKodeKelas.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(121, 110);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(100, 28);
            this.btnSimpan.TabIndex = 45;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(230, 110);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 28);
            this.btnBatal.TabIndex = 46;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            // 
            // lblTingkat
            // 
            this.lblTingkat.AutoSize = true;
            this.lblTingkat.Location = new System.Drawing.Point(39, 76);
            this.lblTingkat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTingkat.Name = "lblTingkat";
            this.lblTingkat.Size = new System.Drawing.Size(107, 17);
            this.lblTingkat.TabIndex = 47;
            this.lblTingkat.Text = "Jenis Kelamin:";
            this.lblTingkat.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblNama
            // 
            this.lblNama.AutoSize = true;
            this.lblNama.Location = new System.Drawing.Point(48, 46);
            this.lblNama.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(98, 17);
            this.lblNama.TabIndex = 48;
            this.lblNama.Text = "Nama Siswa:";
            this.lblNama.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // rbLakiLaki
            // 
            this.rbLakiLaki.AutoSize = true;
            this.rbLakiLaki.Checked = true;
            this.rbLakiLaki.Location = new System.Drawing.Point(153, 74);
            this.rbLakiLaki.Name = "rbLakiLaki";
            this.rbLakiLaki.Size = new System.Drawing.Size(82, 21);
            this.rbLakiLaki.TabIndex = 51;
            this.rbLakiLaki.TabStop = true;
            this.rbLakiLaki.Text = "Laki-laki";
            this.rbLakiLaki.UseVisualStyleBackColor = true;
            // 
            // rbPerempuan
            // 
            this.rbPerempuan.AutoSize = true;
            this.rbPerempuan.Location = new System.Drawing.Point(241, 74);
            this.rbPerempuan.Name = "rbPerempuan";
            this.rbPerempuan.Size = new System.Drawing.Size(104, 21);
            this.rbPerempuan.TabIndex = 52;
            this.rbPerempuan.Text = "Perempuan";
            this.rbPerempuan.UseVisualStyleBackColor = true;
            // 
            // EditRefSiswaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 151);
            this.Controls.Add(this.rbPerempuan);
            this.Controls.Add(this.rbLakiLaki);
            this.Controls.Add(this.txtNamaSiswa);
            this.Controls.Add(this.txtKodeSiswa);
            this.Controls.Add(this.labelKodeKelas);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.lblTingkat);
            this.Controls.Add(this.lblNama);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "EditRefSiswaView";
            this.Text = "Edit Siswa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNamaSiswa;
        private System.Windows.Forms.TextBox txtKodeSiswa;
        private System.Windows.Forms.Label labelKodeKelas;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Label lblTingkat;
        private System.Windows.Forms.Label lblNama;
        private System.Windows.Forms.RadioButton rbLakiLaki;
        private System.Windows.Forms.RadioButton rbPerempuan;
    }
}