﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;
using Fino.Model;

namespace Fino.Repository
{
    public interface IKonversiCicilanRepository : IRepository
    {
        List<KonversiCicilanHeaderModel> GetJenisBiayaWithExistingPosBiaya(int p_SiswaId);
        List<KonversiCicilanHeaderModel> CreateInstallment(List<KonversiCicilanHeaderModel> p_Model);
        KonversiCicilanSiswaModel GetSiswaByCode(string pCode);
    }
}
