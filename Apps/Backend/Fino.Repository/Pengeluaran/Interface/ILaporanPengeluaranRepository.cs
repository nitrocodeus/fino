﻿using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public interface ILaporanPengeluaranRepository : IRepository
    {
        List<LaporanPengeluaranModel> GetDataPengeluaran(DateTime p_FromDate, DateTime p_ToDate);
    }
}
