﻿using Fino.Cient.Startup;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class LogonView : Form, ILogonView
    {
        public event Action OkButtonClicked;
        public event Action CancelButtonClicked;
        public event Action LinkForgotPasswordClicked;     
        
        public bool LogonResult { get; set; }
        public UserModel User { get; set; }

        public LogonView()
        {
            InitializeComponent();

            this.linkForgotPassword.Click += linkForgotPassword_Click;
        }

        public string Username
        {
            get
            {
                return txtUserName.Text;
            }
            set
            {
                txtUserName.Text = value;
            }
        }

        public string Password
        {
            get
            {
                return txtPassword.Text;
            }
            set
            {
                txtPassword.Text = value;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (CancelButtonClicked != null)
            {
                this.CancelButtonClicked();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (OkButtonClicked != null)
            {
                this.OkButtonClicked();
            }
        }

        private void linkForgotPassword_Click(object sender, EventArgs e)
        {
            if (LinkForgotPasswordClicked != null)
            {
                LinkForgotPasswordClicked();
            }
        }

        public void SetBinding()
        {
            txtUserName.DataBindings.Add("Text", User, UserModel.UsernamePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtPassword.DataBindings.Add("Text", User, UserModel.PasswordPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void ShowForgotPasswordView()
        {
            ViewLocator.Instance.UbahKataSandiView.ShowLupaKataSandiForm();
        }
    }
}
