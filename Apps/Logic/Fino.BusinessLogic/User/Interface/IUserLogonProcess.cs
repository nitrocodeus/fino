﻿using Fino.Lib.Core;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IUserLogonProcess
    {
        IUserRepository UserRepo { get; set; }
        
        ITahunAjaranRepository TahunAjaranRepo { get; set; }

        ProcessResult UserLogon(string p_UserName, string p_Password);

        ProcessResult UserLogoff(string p_UserName);

        ProcessResult GetTahunAjaranAktif();
    }
}
