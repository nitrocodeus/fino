﻿
using System;
namespace Fino.Datalib.Entity
{
    public class Potongan
    {
        public int potongan_id { get; set; }
        public int siswa_id { get; set; }
        public int biaya_id { get; set; }
        public double nilai { get; set; }
        public bool persen { get; set; }
        public DateTime mulai_tanggal { get; set; }
        public DateTime hingga_tanggal { get; set; }

        public virtual RefSiswa Siswas { get; set; }
        public virtual RefBiaya Biayas { get; set; }

    }
}
