﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public class PotonganBiayaProcess : IPotonganBiayaProcess
    {
        public IPotonganRepository PotonganRepo { get; set; }
        public IRefSiswaRepository RefSiswaRepo { get; set; }

        private void ValidateData(List<PotonganBiayaDetailModel> p_Model)
        {
            foreach (var m in p_Model)
            {
                if (m.Hingga_Tanggal < m.Mulai_Tanggal)
                {
                    throw new ApplicationException("Tanggal berakhir lebih kecil dari tanggal mulai");
                }
                if (m.Nilai > 100 && m.IsPersen)
                {
                    throw new ApplicationException("Maksimal nilai persen adalah 100");
                }
            }
        }

        public ProcessResult ManagePotonganBiaya(List<PotonganBiayaDetailModel> p_Model)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                ValidateData(p_Model);

                var potonganList = new List<Potongan>();
                foreach (var m in p_Model)
                {
                    potonganList.Add(new Potongan
                    {
                        biaya_id = m.Biaya_Id,
                        hingga_tanggal = m.Hingga_Tanggal,
                        mulai_tanggal = m.Mulai_Tanggal,
                        nilai = m.Nilai,
                        persen = m.IsPersen,
                        potongan_id = m.Potongan_Id,
                        siswa_id = m.Siswa_Id
                    });
                }

                result.DataResult = new List<PotonganBiayaDetailModel>();
                potonganList = PotonganRepo.UpdatePotongan(potonganList);
                foreach (var p in potonganList)
                {
                    ((List<PotonganBiayaDetailModel>)result.DataResult).Add(new PotonganBiayaDetailModel
                    {
                        Biaya_Id = p.biaya_id,
                        Hingga_Tanggal = p.hingga_tanggal,
                        IsPersen = p.persen,
                        Mulai_Tanggal = p.mulai_tanggal,
                        Nilai = p.nilai,
                        Potongan_Id = p.potongan_id,
                        Siswa_Id = p.siswa_id
                    });
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetSiswaByNoInduk(string pNoInduk)
        {
            ProcessResult result = new ProcessResult();
            try
            {                
                var model = RefSiswaRepo.GetSiswaByCode(pNoInduk);
                if (model != null)
                {
                    result.DataResult = new PotonganBiayaHeaderModel
                        {
                            Siswa_Id = model.siswa_id,
                            Code = model.siswa_code,
                            JKelamin = model.jkelamin,
                            Nama = model.nama,
                            PotonganBiayaDetails = new List<PotonganBiayaDetailModel>()
                        };

                    var potList = PotonganRepo.GetPotonganBySiswaId(model.siswa_id);
                    foreach (var p in potList)
                    {
                        ((PotonganBiayaHeaderModel)result.DataResult).PotonganBiayaDetails.Add(
                        new PotonganBiayaDetailModel
                        {
                            Biaya_Id = p.biaya_id,
                            Hingga_Tanggal = p.hingga_tanggal,
                            Mulai_Tanggal = p.mulai_tanggal,
                            IsPersen = p.persen,
                            Nilai = p.nilai,
                            Potongan_Id = p.potongan_id,
                            Siswa_Id = p.siswa_id
                        });
                    }
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }


        public ProcessResult GetBiayaList()
        {
            ProcessResult result = new ProcessResult();
            var listValue = new List<ValueList>();
            try
            {
                var model = PotonganRepo.GetRefBiaya();
                if (model != null)
                {
                    foreach (var b in model)
                    {
                        listValue.Add(new ValueList
                            {
                                Id = b.biaya_id,
                                Value = b.nama
                            });
                    }
                    result.DataResult = listValue;
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
