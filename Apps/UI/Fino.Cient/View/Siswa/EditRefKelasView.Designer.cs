﻿namespace Fino.Cient
{
    partial class EditRefKelasView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditRefKelasView));
            this.txtKelasKode = new System.Windows.Forms.TextBox();
            this.txtAccSettingId = new System.Windows.Forms.TextBox();
            this.labelKodeKelas = new System.Windows.Forms.Label();
            this.cboTingkat = new System.Windows.Forms.ComboBox();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.lblTingkat = new System.Windows.Forms.Label();
            this.lblNama = new System.Windows.Forms.Label();
            this.txtNama = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtKelasKode
            // 
            this.txtKelasKode.Location = new System.Drawing.Point(196, 13);
            this.txtKelasKode.Name = "txtKelasKode";
            this.txtKelasKode.Size = new System.Drawing.Size(200, 24);
            this.txtKelasKode.TabIndex = 0;
            // 
            // txtAccSettingId
            // 
            this.txtAccSettingId.Location = new System.Drawing.Point(196, 13);
            this.txtAccSettingId.Name = "txtAccSettingId";
            this.txtAccSettingId.ReadOnly = true;
            this.txtAccSettingId.Size = new System.Drawing.Size(200, 24);
            this.txtAccSettingId.TabIndex = 41;
            this.txtAccSettingId.TabStop = false;
            // 
            // labelKodeKelas
            // 
            this.labelKodeKelas.AutoSize = true;
            this.labelKodeKelas.Location = new System.Drawing.Point(94, 16);
            this.labelKodeKelas.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelKodeKelas.Name = "labelKodeKelas";
            this.labelKodeKelas.Size = new System.Drawing.Size(92, 17);
            this.labelKodeKelas.TabIndex = 40;
            this.labelKodeKelas.Text = "Kode Kelas:";
            this.labelKodeKelas.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cboTingkat
            // 
            this.cboTingkat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTingkat.FormattingEnabled = true;
            this.cboTingkat.Location = new System.Drawing.Point(196, 73);
            this.cboTingkat.Name = "cboTingkat";
            this.cboTingkat.Size = new System.Drawing.Size(200, 24);
            this.cboTingkat.TabIndex = 2;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(164, 110);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(100, 28);
            this.btnSimpan.TabIndex = 3;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(273, 110);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 28);
            this.btnBatal.TabIndex = 4;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            // 
            // lblTingkat
            // 
            this.lblTingkat.AutoSize = true;
            this.lblTingkat.Location = new System.Drawing.Point(123, 76);
            this.lblTingkat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTingkat.Name = "lblTingkat";
            this.lblTingkat.Size = new System.Drawing.Size(66, 17);
            this.lblTingkat.TabIndex = 38;
            this.lblTingkat.Text = "Tingkat:";
            this.lblTingkat.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblNama
            // 
            this.lblNama.AutoSize = true;
            this.lblNama.Location = new System.Drawing.Point(94, 46);
            this.lblNama.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(95, 17);
            this.lblNama.TabIndex = 39;
            this.lblNama.Text = "Nama Kelas:";
            this.lblNama.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNama
            // 
            this.txtNama.Location = new System.Drawing.Point(196, 43);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(200, 24);
            this.txtNama.TabIndex = 1;
            // 
            // EditRefKelasView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 151);
            this.Controls.Add(this.txtNama);
            this.Controls.Add(this.txtKelasKode);
            this.Controls.Add(this.txtAccSettingId);
            this.Controls.Add(this.labelKodeKelas);
            this.Controls.Add(this.cboTingkat);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.lblTingkat);
            this.Controls.Add(this.lblNama);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "EditRefKelasView";
            this.Text = "Edit Kelas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtKelasKode;
        private System.Windows.Forms.TextBox txtAccSettingId;
        private System.Windows.Forms.Label labelKodeKelas;
        private System.Windows.Forms.ComboBox cboTingkat;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Label lblTingkat;
        private System.Windows.Forms.Label lblNama;
        private System.Windows.Forms.TextBox txtNama;
    }
}