﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Model;
using Fino.Repository;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class PayableRepository : BaseRepository, IPayableRepository
    {
        private Payable GetExistingPayable(int p_PayableId)
        {
            return this.Context.Payables.Where(x => x.payable_id == p_PayableId).FirstOrDefault();
        }

        public List<RekapitulasiPengeluaranModel> GetRekapitulasiPengeluaran(DateTime p_FromDate, DateTime p_ToDate)
        {
            var startDate = p_FromDate.Date;
            var endDate = new DateTime(p_ToDate.Year, p_ToDate.Month, p_ToDate.Day, 23, 59, 59);
            var sqlCommand = new StringBuilder("select ")
                                   .Append("p.refpayable_id, ")
                                   .Append("r.name as payable_name, ")
                                   .Append("sum(p.nilai) as payable_value ")
                                   .Append("from payables p inner join ")
                                   .Append("     refpayables r on p.refpayable_id = r.refpayable_id ")
                                   .Append("where p.isactive = 1 ")
                                   .Append("      and p.created >= @p0 ")
                                   .Append("	  and p.created <= @p1 ")
                                   .Append("group by p.refpayable_id, r.name ");

            List<RekapitulasiPengeluaranModel> resultSql = Context.Database
                    .SqlQuery<RekapitulasiPengeluaranModel>(
                        sqlCommand.ToString(),
                        startDate.ToString("yyyy/MM/dd"),
                        endDate.ToString("yyyy/MM/dd HH:mm:ss")).ToList();

            return resultSql;
        }

        public PayableModel GetPayable(int p_PayableId)
        {
            var existingPayable = GetExistingPayable(p_PayableId);
            PayableModel result = null;

            if (existingPayable != null)
            {
                result = new PayableModel
                {
                    payable_id = existingPayable.payable_id,
                    refpayable_id = existingPayable.refpayable_id,
                    refvendor_id = existingPayable.refvendor_id,
                    deskripsi = existingPayable.deskripsi,
                    jtempo = existingPayable.jtempo,
                    nilai = existingPayable.nilai,
                    ispaid = existingPayable.ispaid,
                    isactive = existingPayable.isactive,
                    datepaid = existingPayable.datepaid,
                    created =existingPayable.created,
                    status_id = existingPayable.status_id,
                    status_date = existingPayable.status_date
                };
            }

            return result;
        }

        public void UpdatePayable(PayableModel entity)
        {
            var existingPayable = GetExistingPayable(entity.payable_id);

            if (existingPayable != null)
            {
                existingPayable.refpayable_id = entity.refpayable_id;
                existingPayable.refvendor_id = entity.refvendor_id;
                existingPayable.deskripsi = entity.deskripsi;
                existingPayable.jtempo = entity.jtempo;
                existingPayable.nilai = entity.nilai;
                existingPayable.ispaid = entity.ispaid;
                existingPayable.isactive = entity.isactive;
                existingPayable.datepaid = entity.datepaid;
                existingPayable.status_id = entity.status_id;
                existingPayable.status_date = entity.status_date;

                this.Context.SaveChanges();
            }
        }

        public void DeletePayable(PayableModel entity)
        {
            var existingPayable = GetExistingPayable(entity.payable_id);

            if (existingPayable != null)
            {
                this.Context.Payables.Remove(existingPayable);
                this.Context.SaveChanges();
            }
        }


        public List<PembatalanPengeluaranModel> GetPembatalanPengeluaranList(DateTime p_FromDate, DateTime p_ToDate)
        {
            var startDate = new DateTime(p_FromDate.Year, p_FromDate.Month, p_FromDate.Day, 0, 0, 0);
            var endDate = new DateTime(p_ToDate.Year, p_ToDate.Month, p_ToDate.Day, 23, 59, 59);

            var payableList = this.Context.Payables.Where(x => x.ispaid
                                                            && x.datepaid.HasValue 
                                                            && x.datepaid.Value >= startDate 
                                                            && x.datepaid.Value < endDate 
                                                            && x.isactive
                                                            && x.status_id < 2).ToList();

            List<PembatalanPengeluaranModel> result = new List<PembatalanPengeluaranModel>();

            foreach (var payable in payableList)
            {
                result.Add(new PembatalanPengeluaranModel
                    {
                        Payable_Id = payable.payable_id,
                        RefPayable_Id = payable.refpayable_id,
                        RefPayable_Name = payable.refpayable.name,
                        RefVendor_Id = payable.refvendor_id,
                        RefVendor_Name = payable.vendor.vendorname,
                        Deskripsi = payable.deskripsi,
                        Nilai = payable.nilai,
                        Jumlah = payable.payablepaid.paynilai,
                        ReferenceNo = payable.payablepaid.referenceno,
                        Status_Id = payable.status_id,
                        Status_Date = payable.status_date,
                        JTempo = payable.jtempo,
                        DatePaid = payable.datepaid
                    });
            }
            
            return result;
        }
    }
}
