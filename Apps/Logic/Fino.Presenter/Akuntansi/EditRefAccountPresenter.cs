﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class EditRefAccountPresenter : BasePresenter
    {
        private readonly IRefAccountProcess _refAccountProcess;
        private readonly IEditRefAccountView _editRefAccountView;

        public EditRefAccountPresenter(IRefAccountProcess p_Process, IEditRefAccountView p_View) : base(p_View)
        {
            this._refAccountProcess = p_Process;
            this._editRefAccountView = p_View;

            this._editRefAccountView.Load += _editRefAccountView_Load;
            this._editRefAccountView.BtnSimpanClicked += _editRefAccountView_BtnSimpanClicked;
            this._editRefAccountView.BtnBatalClicked += _editRefAccountView_BtnBatalClicked;
        }

        #region Private Methods

        public void Load()
        {
            if (_editRefAccountView.SelectedRefAccount != null)
            {
                _editRefAccountView.NamaAkun = _editRefAccountView.SelectedRefAccount.Account_name;
                _editRefAccountView.KodeAkun = _editRefAccountView.SelectedRefAccount.Account_code;
            }

            _editRefAccountView.SelectedRefAccount.ErrorValidation += SelectedRefAccount_ErrorValidation;
            _editRefAccountView.SetBinding();
        }

        private void SelectedRefAccount_ErrorValidation(object sender, Lib.Core.CustomEventArgs<string> e)
        {
            WinApi.ShowErrorMessage(e.Data, AppResource.CAP_VALIDASI_ERROR);
        }

        private void Save()
        {
            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _refAccountProcess.UpdateRefAccount(_editRefAccountView.SelectedRefAccount)),
                        AppResource.MSG_SAVING_GRUP_BIAYA);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                _editRefAccountView.RefAccountView.ReloadDataEvent();
                _editRefAccountView.Close();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        #endregion

        #region Control Events

        private void _editRefAccountView_Load(object sender, EventArgs e)
        {
            Load();
        }

        private void _editRefAccountView_BtnSimpanClicked()
        {
            var context = new ValidationContext(_editRefAccountView.SelectedRefAccount, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(_editRefAccountView.SelectedRefAccount, context, results, true);

            if (isValid)
            {
                Save();

                _editRefAccountView.SelectedRefAccount.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                _editRefAccountView.SelectedRefAccount.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        private void _editRefAccountView_BtnBatalClicked()
        {
            this._editRefAccountView.Close();
        }

        #endregion Control Events
    }
}
