﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IEditUserManagementView : IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;

        UserManagementModel SelectedUser { get; }
        IUserManagementView UserManagementView { get; set; }

        string Fullname { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        string Phone { get; set; }
        string Email { get; set; }
        object RoleDataSource { get; set; }

        void SetBinding();
    }
}
