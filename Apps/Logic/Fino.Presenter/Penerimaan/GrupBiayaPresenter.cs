﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Core.Messaging;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class GrupBiayaPresenter : BasePresenter
    {
        IGrupBiayaView _grupBiayaView;
        IKonfigurasiBiayaProcess _konfigurasiBiayaProcess;

        public GrupBiayaPresenter(IGrupBiayaView p_view, IKonfigurasiBiayaProcess p_process) : base(p_view)
        {
            _grupBiayaView = p_view;
            _konfigurasiBiayaProcess = p_process;

            _grupBiayaView.BtnTambahClick += BtnTambahClick;
            _grupBiayaView.BtnHapusClick += BtnHapusClick;
            _grupBiayaView.BtnTutupClick += BtnTutupClick;
            _grupBiayaView.GvGrupBiayaDoubleClick += GvGrupBiayaDoubleClick;
            _grupBiayaView.Load += Load;
            _grupBiayaView.ReloadData += ReloadData;
            _grupBiayaView.BtnKonfigurasiBiayaClick += BtnKonfigurasiBiayaClick;
        }

        public void BtnKonfigurasiBiayaClick()
        {
            _grupBiayaView.ShowKonfigurasiBiayaView();
        }

        public void ReloadData()
        {
            LoadData();
        }

        public void Load(object sender, EventArgs e)
        {
            LoadData();
        }

        public void GvGrupBiayaDoubleClick()
        {
            _grupBiayaView.ShowEditGrupBiayaView();
        }

        public void BtnTutupClick()
        {
            _grupBiayaView.Close();
        }

        public void BtnHapusClick()
        {
            GrupBiayaModel selectedModel = _grupBiayaView.GetSelectedGrupBiaya();

            if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_DLT_GRUPBIAYA, selectedModel.Nama),"Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                    () => _konfigurasiBiayaProcess.DeleteGrupBiaya(selectedModel)),
                    AppResource.MSG_DELETE_GRUP_BIAYA);

                if (result.IsSucess)
                {
                    LoadData();
                }
                else
                {
                    if (result.ProcessException != null && result.ProcessException.GetType() == typeof(UpdateException))
                    {
                        MessageBox.Show(string.Format(AppResource.MSG_DATA_TDKBISADIHAPUS, selectedModel.Nama, Environment.NewLine), AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
                    }
                }
            }
        }

        public void BtnTambahClick()
        {
            _grupBiayaView.ShowTambahGrupBiayaView();
        }

        private void LoadData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _konfigurasiBiayaProcess.GetGrupBiaya()),
                AppResource.MSG_LOADING_GRUP_BIAYA);

            if (result.IsSucess)
            {
                List<GrupBiayaModel> data = result.DataResult as List<GrupBiayaModel>;

                _grupBiayaView.SetData(data);
                _grupBiayaView.RefreshDataGrid();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        [MediatorMessageAttribute(MessageKey.RELOAD_DATA)]
        public void SelesaiSimpanGrupBiaya(object p_MessageData)
        {
            MessageData mData = p_MessageData as MessageData;

            if (mData.MessageType == MessageTypeEnum.WithKey)
            {
                GrupBiayaModel t = mData.Data as GrupBiayaModel;
                _grupBiayaView.GetGVGrupBiayaDataSource().Add(t);
                _grupBiayaView.RefreshDataGrid();
            }
        }

        [MediatorMessageAttribute(MessageKey.UPDATE_CURRENT_SELECTED_DATA)]
        public void SelesaiUpdateGrupBiaya(object p_MessageData)
        {
            MessageData mData = p_MessageData as MessageData;

            if (mData.MessageType == MessageTypeEnum.WithKey)
            {
                _grupBiayaView.RefreshDataGrid();
            }
        }

    }
}
