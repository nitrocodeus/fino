﻿namespace Fino.Cient.Common
{
    partial class PosBiayaView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gvPosBiaya = new System.Windows.Forms.DataGridView();
            this.posBiayaModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pnlPosBiayaToolbar = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnTambahkan = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.posBiayaIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.selectedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.namaBiayaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jTempoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nilaiBiayaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.potonganDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isPaidDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.jumlahBiayaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvPosBiaya)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.posBiayaModelBindingSource)).BeginInit();
            this.pnlPosBiayaToolbar.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gvPosBiaya
            // 
            this.gvPosBiaya.AllowUserToAddRows = false;
            this.gvPosBiaya.AllowUserToDeleteRows = false;
            this.gvPosBiaya.AllowUserToResizeColumns = false;
            this.gvPosBiaya.AllowUserToResizeRows = false;
            this.gvPosBiaya.AutoGenerateColumns = false;
            this.gvPosBiaya.BackgroundColor = System.Drawing.Color.White;
            this.gvPosBiaya.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvPosBiaya.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.posBiayaIdDataGridViewTextBoxColumn,
            this.selectedDataGridViewCheckBoxColumn,
            this.namaBiayaDataGridViewTextBoxColumn,
            this.jTempoDataGridViewTextBoxColumn,
            this.nilaiBiayaDataGridViewTextBoxColumn,
            this.potonganDataGridViewTextBoxColumn,
            this.isPaidDataGridViewCheckBoxColumn,
            this.jumlahBiayaDataGridViewTextBoxColumn,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvPosBiaya.DataSource = this.posBiayaModelBindingSource;
            this.gvPosBiaya.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvPosBiaya.Location = new System.Drawing.Point(0, 0);
            this.gvPosBiaya.Margin = new System.Windows.Forms.Padding(4);
            this.gvPosBiaya.Name = "gvPosBiaya";
            this.gvPosBiaya.RowHeadersVisible = false;
            this.gvPosBiaya.Size = new System.Drawing.Size(436, 276);
            this.gvPosBiaya.TabIndex = 1;
            this.gvPosBiaya.DataSourceChanged += new System.EventHandler(this.gvPosBiaya_DataSourceChanged);
            this.gvPosBiaya.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvPosBiaya_CellClick);
            this.gvPosBiaya.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvPosBiaya_CellEnter);
            this.gvPosBiaya.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvPosBiaya_CellValueChanged);
            this.gvPosBiaya.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.gvPosBiaya_EditingControlShowing);
            // 
            // posBiayaModelBindingSource
            // 
            this.posBiayaModelBindingSource.DataSource = typeof(Fino.Model.PosBiayaModel);
            // 
            // pnlPosBiayaToolbar
            // 
            this.pnlPosBiayaToolbar.Controls.Add(this.btnClose);
            this.pnlPosBiayaToolbar.Controls.Add(this.btnTambahkan);
            this.pnlPosBiayaToolbar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlPosBiayaToolbar.Location = new System.Drawing.Point(0, 276);
            this.pnlPosBiayaToolbar.Margin = new System.Windows.Forms.Padding(4);
            this.pnlPosBiayaToolbar.Name = "pnlPosBiayaToolbar";
            this.pnlPosBiayaToolbar.Size = new System.Drawing.Size(436, 50);
            this.pnlPosBiayaToolbar.TabIndex = 2;
            this.pnlPosBiayaToolbar.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.Gainsboro;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(388, 16);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(32, 28);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "X";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnTambahkan
            // 
            this.btnTambahkan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTambahkan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnTambahkan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTambahkan.ForeColor = System.Drawing.Color.White;
            this.btnTambahkan.Location = new System.Drawing.Point(280, 16);
            this.btnTambahkan.Margin = new System.Windows.Forms.Padding(4);
            this.btnTambahkan.Name = "btnTambahkan";
            this.btnTambahkan.Size = new System.Drawing.Size(100, 28);
            this.btnTambahkan.TabIndex = 0;
            this.btnTambahkan.Text = "Tambahkan";
            this.btnTambahkan.UseVisualStyleBackColor = false;
            this.btnTambahkan.Click += new System.EventHandler(this.btnTambahkan_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gvPosBiaya);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(436, 276);
            this.panel2.TabIndex = 3;
            // 
            // posBiayaIdDataGridViewTextBoxColumn
            // 
            this.posBiayaIdDataGridViewTextBoxColumn.DataPropertyName = "PosBiayaId";
            this.posBiayaIdDataGridViewTextBoxColumn.HeaderText = "PosBiayaId";
            this.posBiayaIdDataGridViewTextBoxColumn.Name = "posBiayaIdDataGridViewTextBoxColumn";
            this.posBiayaIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.posBiayaIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // selectedDataGridViewCheckBoxColumn
            // 
            this.selectedDataGridViewCheckBoxColumn.DataPropertyName = "Selected";
            this.selectedDataGridViewCheckBoxColumn.HeaderText = "";
            this.selectedDataGridViewCheckBoxColumn.Name = "selectedDataGridViewCheckBoxColumn";
            this.selectedDataGridViewCheckBoxColumn.Visible = false;
            this.selectedDataGridViewCheckBoxColumn.Width = 20;
            // 
            // namaBiayaDataGridViewTextBoxColumn
            // 
            this.namaBiayaDataGridViewTextBoxColumn.DataPropertyName = "NamaBiaya";
            this.namaBiayaDataGridViewTextBoxColumn.HeaderText = "Biaya";
            this.namaBiayaDataGridViewTextBoxColumn.Name = "namaBiayaDataGridViewTextBoxColumn";
            this.namaBiayaDataGridViewTextBoxColumn.ReadOnly = true;
            this.namaBiayaDataGridViewTextBoxColumn.Width = 250;
            // 
            // jTempoDataGridViewTextBoxColumn
            // 
            this.jTempoDataGridViewTextBoxColumn.DataPropertyName = "JTempo";
            this.jTempoDataGridViewTextBoxColumn.HeaderText = "JTempo";
            this.jTempoDataGridViewTextBoxColumn.Name = "jTempoDataGridViewTextBoxColumn";
            this.jTempoDataGridViewTextBoxColumn.ReadOnly = true;
            this.jTempoDataGridViewTextBoxColumn.Visible = false;
            // 
            // nilaiBiayaDataGridViewTextBoxColumn
            // 
            this.nilaiBiayaDataGridViewTextBoxColumn.DataPropertyName = "NilaiBiaya";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N0";
            dataGridViewCellStyle1.NullValue = null;
            this.nilaiBiayaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.nilaiBiayaDataGridViewTextBoxColumn.HeaderText = "Nilai (Rp)";
            this.nilaiBiayaDataGridViewTextBoxColumn.Name = "nilaiBiayaDataGridViewTextBoxColumn";
            this.nilaiBiayaDataGridViewTextBoxColumn.ReadOnly = true;
            this.nilaiBiayaDataGridViewTextBoxColumn.Width = 150;
            // 
            // potonganDataGridViewTextBoxColumn
            // 
            this.potonganDataGridViewTextBoxColumn.DataPropertyName = "Potongan";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.potonganDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.potonganDataGridViewTextBoxColumn.HeaderText = "Pot. (Rp)";
            this.potonganDataGridViewTextBoxColumn.Name = "potonganDataGridViewTextBoxColumn";
            this.potonganDataGridViewTextBoxColumn.ReadOnly = true;
            this.potonganDataGridViewTextBoxColumn.Width = 150;
            // 
            // isPaidDataGridViewCheckBoxColumn
            // 
            this.isPaidDataGridViewCheckBoxColumn.DataPropertyName = "IsPaid";
            this.isPaidDataGridViewCheckBoxColumn.HeaderText = "IsPaid";
            this.isPaidDataGridViewCheckBoxColumn.Name = "isPaidDataGridViewCheckBoxColumn";
            this.isPaidDataGridViewCheckBoxColumn.ReadOnly = true;
            this.isPaidDataGridViewCheckBoxColumn.Visible = false;
            // 
            // jumlahBiayaDataGridViewTextBoxColumn
            // 
            this.jumlahBiayaDataGridViewTextBoxColumn.DataPropertyName = "JumlahBiaya";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.jumlahBiayaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.jumlahBiayaDataGridViewTextBoxColumn.HeaderText = "Jumlah (Rp)";
            this.jumlahBiayaDataGridViewTextBoxColumn.Name = "jumlahBiayaDataGridViewTextBoxColumn";
            this.jumlahBiayaDataGridViewTextBoxColumn.ReadOnly = true;
            this.jumlahBiayaDataGridViewTextBoxColumn.Width = 150;
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.ReadOnly = true;
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // PosBiayaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlPosBiayaToolbar);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PosBiayaView";
            this.Size = new System.Drawing.Size(436, 326);
            ((System.ComponentModel.ISupportInitialize)(this.gvPosBiaya)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.posBiayaModelBindingSource)).EndInit();
            this.pnlPosBiayaToolbar.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gvPosBiaya;
        private System.Windows.Forms.BindingSource posBiayaModelBindingSource;
        private System.Windows.Forms.Panel pnlPosBiayaToolbar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnTambahkan;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn posBiayaIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaBiayaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jTempoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nilaiBiayaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn potonganDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isPaidDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jumlahBiayaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
    }
}
