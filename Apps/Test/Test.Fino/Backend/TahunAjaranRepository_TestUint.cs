﻿using System;
using Fino.Datalib.Dbmodel;
using Fino.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Fino.Backend
{
    [TestClass]
    public class TahunAjaranRepository_TestUint
    {
        [TestMethod]
        public void GetValidTahunAjaran_Valid()
        {
            var repo = new TahunAjaranRepository();

            var result = repo.GetValidTahunAjaran(DateTime.Now.Year, DateTime.Now.Month);
            Assert.IsNotNull(result);
        }
    }
}
