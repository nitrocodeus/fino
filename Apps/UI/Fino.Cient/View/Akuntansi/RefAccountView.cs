﻿using Fino.Cient.Startup;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class RefAccountView : BaseForm, IRefAccountView
    {
        #region Events

        public event Action GvRefAccountDoubleClick;
        public event Action BtnTambahClick;
        public event Action BtnHapusClick;
        public event Action BtnTutupClick;
        public event Action ReloadData;

        #endregion Events

        #region Public Properties



        #endregion Public Properties

        #region Constructors

        public RefAccountView()
        {
            InitializeComponent();

            this.btnTambah.Click += btnTambah_Click;
            this.btnHapus.Click += btnHapus_Click;
            this.btnTutup.Click += btnTutup_Click;
            this.gvKonfigurasiKodeAkun.DoubleClick += gvKonfigurasiKodeAkun_DoubleClick;

            this.gvKonfigurasiKodeAkun.MultiSelect = false;
            this.gvKonfigurasiKodeAkun.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        #endregion Constructors

        #region Public Methods

        public void ReloadDataEvent()
        {
            if (ReloadData != null)
            {
                ReloadData();
            }
        }

        #endregion Public Methods

        #region Control Events

        private void btnTambah_Click(object sender, EventArgs e)
        {
            if (this.BtnTambahClick != null)
            {
                BtnTambahClick();
            }
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            if (this.BtnHapusClick != null)
            {
                BtnHapusClick();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (this.BtnTutupClick != null)
            {
                BtnTutupClick();
            }
        }

        private void gvKonfigurasiKodeAkun_DoubleClick(object sender, EventArgs e)
        {
            if (GvRefAccountDoubleClick != null)
            {
                GvRefAccountDoubleClick();
            }
        }

        #endregion ControlEvents

        #region IRefAccountView

        public void ShowEditRefBiayaView()
        {
            if (gvKonfigurasiKodeAkun.SelectedCells.Count > 0)
            {
                ViewLocator.Instance.EditRefAccountView.RefAccountView = this;
                ViewLocator.Instance.EditRefAccountView.ShowForm(GetSelectedRefAccount());  
            }
        }

        public void ShowTambahRefBiayaView()
        {
            ViewLocator.Instance.EditRefAccountView.RefAccountView = this;
            ViewLocator.Instance.EditRefAccountView.ShowForm(null);    
        }

        public void ShowErrorMessage(string message, string caption)
        {
            WinApi.ShowErrorMessage(message, caption);
        }

        public void SetData(List<RefAccountModel> listRefAccount)
        {
            gvKonfigurasiKodeAkun.DataSource = listRefAccount;
        }

        public void RefreshDataGrid()
        {
            gvKonfigurasiKodeAkun.Refresh();
        }

        public RefAccountModel GetSelectedRefAccount()
        {
            RefAccountModel result = new RefAccountModel();

            if (gvKonfigurasiKodeAkun.SelectedCells.Count > 0)
            {
                result = (RefAccountModel)gvKonfigurasiKodeAkun.Rows[gvKonfigurasiKodeAkun.SelectedCells[0].RowIndex].DataBoundItem;
            }

            return result;
        }

        #endregion IRefAccountView
    }
}
