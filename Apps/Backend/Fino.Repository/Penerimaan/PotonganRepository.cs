﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class PotonganRepository : BaseRepository, IPotonganRepository
    {
        public List<Potongan> GetPotonganBySiswaId(int p_SiswaId)
        {
            List<Potongan> returnModel = new List<Potongan>();

            var potongans = Context.Potongans.Where(e => e.siswa_id.Equals(p_SiswaId)).ToList();
            if (potongans != null && potongans.Count > 0)
            {
                returnModel = potongans;
            }

            return returnModel;
        }

        public List<RefBiaya> GetRefBiaya()
        {
            List<RefBiaya> returnModel = new List<RefBiaya>();

            var biayaList = Context.RefBiayas.ToList();
            if (biayaList != null && biayaList.Count > 0)
            {
                returnModel = biayaList;
            }

            return returnModel;
        }

        public List<Potongan> UpdatePotongan(List<Potongan> p_Potongan)
        {
            var pResult = new List<Potongan>();

            foreach (var p in p_Potongan)
            {
                if (p.potongan_id.Equals(0))
                {
                    Context.Entry(p).State = System.Data.Entity.EntityState.Added;
                    UpdateExistingPosBiaya(p);

                    pResult.Add(p);
                }
                else if (p.potongan_id > 0)
                {
                    var existing = Context.Potongans.Find(p.potongan_id);
                    if (existing != null)
                    {
                        existing.biaya_id = p.biaya_id;
                        existing.hingga_tanggal = p.hingga_tanggal;
                        existing.mulai_tanggal = p.mulai_tanggal;
                        existing.nilai = p.nilai;
                        existing.persen = p.persen;
                        existing.siswa_id = p.siswa_id;
                    }
                    UpdateExistingPosBiaya(p);
                    pResult.Add(p);
                }
                else
                {

                    p.potongan_id = Math.Abs(p.potongan_id);
                    var existing = Context.Potongans.Find(p.potongan_id);
                    if (existing != null)
                    {
                        Context.Potongans.Remove(existing);
                    }
                    DeleteExistingPosBiaya(p);
                }
            }
            Context.SaveChanges();
            Context.Dispose();

            return pResult;
        }

        private List<PosBiaya> GetExistingPosBiaya(Potongan p_Potongan)
        {
            return Context.PosBiayas.Where(e => e.Siswa_Id.Equals(p_Potongan.siswa_id) &&
                                    e.Biaya_Id.Equals(p_Potongan.biaya_id) &&
                                    e.IsActive &&
                                    !e.IsPaid &&
                                    e.Status_Id.Equals(0) &&
                                    e.JTempo >= p_Potongan.mulai_tanggal.Date &&
                                    e.JTempo <= p_Potongan.hingga_tanggal.Date).ToList();
        }

        private void DeleteExistingPosBiaya(Potongan p_Potongan)
        {
            var existingPostBiaya = GetExistingPosBiaya(p_Potongan);

            foreach (var exist in existingPostBiaya)
            {
                exist.IsPersen = false;
                exist.Potongan = 0;
                exist.NilaiPotongan = 0;
            }

        }

        private void UpdateExistingPosBiaya(Potongan p_Potongan)
        {
            var existingPostBiaya = GetExistingPosBiaya(p_Potongan);

            foreach(var exist in existingPostBiaya)
            {
                exist.IsPersen = p_Potongan.persen;
                exist.Potongan = p_Potongan.nilai;
                if (p_Potongan.persen)
                {
                    exist.NilaiPotongan = exist.Nilai * (p_Potongan.nilai / 100);
                }
                else
                {
                    exist.NilaiPotongan = p_Potongan.nilai;
                }
            }

        }
    }
}
