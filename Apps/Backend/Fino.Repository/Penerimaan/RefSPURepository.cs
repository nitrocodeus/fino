﻿using Fino.Datalib.Entity;
using Fino.Model;
using System.Collections.Generic;
using System.Linq;

namespace Fino.Repository
{
    public class RefSPURepository : BaseRepository, IRefSPURepository
    {
        private RefSumberPenerimaanUmum GetExistingRefSPU(int refSPUId)
        {
            return this.Context.SumberPenerimaanUmums.Where(x => x.refsumberpenerimaan_id == refSPUId).FirstOrDefault();
        }

        public RefSumberPUModel GetVendor(int refSPUId)
        {
            var existingRefSPU = GetExistingRefSPU(refSPUId);

            if (existingRefSPU != null)
            {
                return new RefSumberPUModel
                {
                    Id = existingRefSPU.refsumberpenerimaan_id,
                    Nama = existingRefSPU.refsumberpenerimaan_name,
                    Alamat = existingRefSPU.refsumberpenerimaan_alamat,
                    KelasId = existingRefSPU.refsumberpenerimaan_kelasid
                };
            }

            return null;
        }

        public List<RefSumberPUModel> GetAllRefSPU()
        {
            List<RefSumberPUModel> result = new List<RefSumberPUModel>();

            var allSPU = this.Context.SumberPenerimaanUmums.ToList();

            foreach(var spu in allSPU)
            {
                result.Add(new RefSumberPUModel
                    {
                        Id = spu.refsumberpenerimaan_id,
                        Nama = spu.refsumberpenerimaan_name,
                        Alamat = spu.refsumberpenerimaan_alamat,
                        KelasId = spu.refsumberpenerimaan_kelasid
                    });
            }

            return result;
        }

        public void UpdateSPU(RefSumberPUModel entity)
        {
            var existingRefSPU = GetExistingRefSPU(entity.Id);

            if (existingRefSPU == null)
            {
                this.Context.SumberPenerimaanUmums.Add(new RefSumberPenerimaanUmum
                    {
                        refsumberpenerimaan_id = entity.Id,
                        refsumberpenerimaan_name = entity.Nama,
                        refsumberpenerimaan_alamat = entity.Alamat,
                        refsumberpenerimaan_kelasid = entity.KelasId
                    });
            }
            else
            {
                existingRefSPU.refsumberpenerimaan_name = entity.Nama;
                existingRefSPU.refsumberpenerimaan_alamat = entity.Alamat;
                existingRefSPU.refsumberpenerimaan_kelasid = entity.KelasId;
                existingRefSPU.refsumberpenerimaan_id = entity.Id;
            }

            this.Context.SaveChanges();
        }

        public void DeleteSPU(RefSumberPUModel entity)
        {
            var existingRefSPU = GetExistingRefSPU(entity.Id);

            if (existingRefSPU != null)
            {
                this.Context.SumberPenerimaanUmums.Remove(existingRefSPU);
            }

            this.Context.SaveChanges();
        }
    }
}
