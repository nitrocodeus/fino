using Fino.Datalib.Dbmodel;

namespace Fino.Repository
{
    public interface IRepository
    {
        FinoDBContext Context { get; set; }
        void DisposeDBContext();
    }
}
