﻿namespace Fino.Cient
{
    partial class UbahKataSandiView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UbahKataSandiView));
            this.lblKataSandiLama = new System.Windows.Forms.Label();
            this.txtSandiLama = new System.Windows.Forms.TextBox();
            this.txtSandiBaru = new System.Windows.Forms.TextBox();
            this.lblKataSandiBaru = new System.Windows.Forms.Label();
            this.txtUlangSandiBaru = new System.Windows.Forms.TextBox();
            this.lblUlangKataSandiBaru = new System.Windows.Forms.Label();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnTutup = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblKataSandiLama
            // 
            this.lblKataSandiLama.AutoSize = true;
            this.lblKataSandiLama.Location = new System.Drawing.Point(12, 27);
            this.lblKataSandiLama.Name = "lblKataSandiLama";
            this.lblKataSandiLama.Size = new System.Drawing.Size(119, 17);
            this.lblKataSandiLama.TabIndex = 0;
            this.lblKataSandiLama.Text = "Kata sandi lama";
            // 
            // txtSandiLama
            // 
            this.txtSandiLama.Location = new System.Drawing.Point(180, 24);
            this.txtSandiLama.Name = "txtSandiLama";
            this.txtSandiLama.PasswordChar = '*';
            this.txtSandiLama.Size = new System.Drawing.Size(202, 24);
            this.txtSandiLama.TabIndex = 1;
            // 
            // txtSandiBaru
            // 
            this.txtSandiBaru.Location = new System.Drawing.Point(180, 55);
            this.txtSandiBaru.Name = "txtSandiBaru";
            this.txtSandiBaru.PasswordChar = '*';
            this.txtSandiBaru.Size = new System.Drawing.Size(202, 24);
            this.txtSandiBaru.TabIndex = 3;
            // 
            // lblKataSandiBaru
            // 
            this.lblKataSandiBaru.AutoSize = true;
            this.lblKataSandiBaru.Location = new System.Drawing.Point(12, 58);
            this.lblKataSandiBaru.Name = "lblKataSandiBaru";
            this.lblKataSandiBaru.Size = new System.Drawing.Size(119, 17);
            this.lblKataSandiBaru.TabIndex = 2;
            this.lblKataSandiBaru.Text = "Kata sandi baru";
            // 
            // txtUlangSandiBaru
            // 
            this.txtUlangSandiBaru.Location = new System.Drawing.Point(180, 85);
            this.txtUlangSandiBaru.Name = "txtUlangSandiBaru";
            this.txtUlangSandiBaru.PasswordChar = '*';
            this.txtUlangSandiBaru.Size = new System.Drawing.Size(202, 24);
            this.txtUlangSandiBaru.TabIndex = 5;
            // 
            // lblUlangKataSandiBaru
            // 
            this.lblUlangKataSandiBaru.AutoSize = true;
            this.lblUlangKataSandiBaru.Location = new System.Drawing.Point(12, 88);
            this.lblUlangKataSandiBaru.Name = "lblUlangKataSandiBaru";
            this.lblUlangKataSandiBaru.Size = new System.Drawing.Size(161, 17);
            this.lblUlangKataSandiBaru.TabIndex = 4;
            this.lblUlangKataSandiBaru.Text = "Ulang kata sandi baru";
            // 
            // btnSimpan
            // 
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.Location = new System.Drawing.Point(226, 115);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(75, 28);
            this.btnSimpan.TabIndex = 6;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            // 
            // btnTutup
            // 
            this.btnTutup.BackColor = System.Drawing.SystemColors.Control;
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(307, 115);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(75, 28);
            this.btnTutup.TabIndex = 7;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = false;
            // 
            // UbahKataSandiView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 158);
            this.Controls.Add(this.btnTutup);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.txtUlangSandiBaru);
            this.Controls.Add(this.lblUlangKataSandiBaru);
            this.Controls.Add(this.txtSandiBaru);
            this.Controls.Add(this.lblKataSandiBaru);
            this.Controls.Add(this.txtSandiLama);
            this.Controls.Add(this.lblKataSandiLama);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UbahKataSandiView";
            this.Text = "Ubah Kata Sandi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKataSandiLama;
        private System.Windows.Forms.TextBox txtSandiLama;
        private System.Windows.Forms.TextBox txtSandiBaru;
        private System.Windows.Forms.Label lblKataSandiBaru;
        private System.Windows.Forms.TextBox txtUlangSandiBaru;
        private System.Windows.Forms.Label lblUlangKataSandiBaru;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnTutup;
    }
}