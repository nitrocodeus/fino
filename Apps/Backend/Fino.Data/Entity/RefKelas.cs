﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class RefKelas
    {
        public int kelas_id {get;set;}
        public string kelas_code {get;set;}
        public string nama {get;set;}
        public int tingkat { get; set; }

        public virtual ICollection<SiswaKelas> SiswaKelas { get; set; }
        public virtual ICollection<RefSumberPenerimaanUmum> SumberPenerimaanUmums { get; set; }
    }
}
