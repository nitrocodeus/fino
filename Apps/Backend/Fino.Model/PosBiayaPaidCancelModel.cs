﻿using System;

namespace Fino.Model
{
    public class PosBiayaPaidCancelModel
    {
        public int PosBiayaPaidCancel_Id { get; set; }
        public int PosBiaya_Id { get; set; }
        public DateTime datepaid { get; set; }
        public double paynilai { get; set; }
        public int status { get; set; }
        public int? nota_pembayaran_id { get; set; }
        public DateTime? DateCancel { get; set; }
    }
}
