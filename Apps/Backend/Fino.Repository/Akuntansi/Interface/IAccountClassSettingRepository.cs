﻿using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public interface IAccountClassSettingRepository
    {
        AccountClassSettingModel GetAccountClassSetting(int accountId);
        List<AccountClassSettingModel> GetAllAccountClassSettingByAccClassId(int accClassId);
        void UpdateAccountClassSetting(AccountClassSettingModel entity);
        void DeleteAccountClassSetting(AccountClassSettingModel entity);
    }
}
