﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public interface ITahunAjaranRepository : IRepository
    {
        RefTahunAjaran GetValidTahunAjaran(int currentYear, int currentMonth);
    }
}
