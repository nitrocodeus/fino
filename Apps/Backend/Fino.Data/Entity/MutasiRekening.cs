﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Entity
{
    public class MutasiRekening
    {
        public int mutasi_id { get; set; }
        public int rekening_id { get; set; }
        public int jenis_mutasi { get; set; }
        public DateTime tgl_mutasi { get; set; }
        public int factor { get; set; }
        public double mutasi_nilai { get; set;}
        public bool is_awal { get; set; }

        public virtual RefRekening Rekening { get; set; }
    }
}
