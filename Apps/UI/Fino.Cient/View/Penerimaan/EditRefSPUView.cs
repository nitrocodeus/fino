﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditRefSPUView : BaseForm, IEditRefSPUView
    {
        #region Events

        public event Action BtnBatalClicked;
        public event Action BtnSimpanClicked;
        public event Action CboKelasSelectedChanged;
        public event Action RbKelasSelectedChanged;

        #endregion Events

        #region Constructors

        public EditRefSPUView()
        {
            InitializeComponent();

            this.btnSimpan.Click += btnSimpan_Click;
            this.btnBatal.Click += btnBatal_Click;
            cboKelas.SelectedIndexChanged += cboKelas_SelectedIndexChanged;
        }

        void cboKelas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboKelasSelectedChanged != null)
            {
                CboKelasSelectedChanged();
            }            
        }

        private void rbKelas_CheckedChanged(object sender, EventArgs e)
        {
            if (RbKelasSelectedChanged != null)
            {
                RbKelasSelectedChanged();
            }
        }

        #region Control Events

        void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        #endregion Control Events

        #endregion Constructors

        #region Public Properties

        public RefSumberPUModel ViewModel { get; set; }
        public IPenerimaanUmumView PenerimaanUmumView { get; set; }
        public object RefKelasDataSource { get; set; }

        public string Nama
        {
            get
            {
                return txtVendorName.Text;
            }
            set
            {
                txtVendorName.Text = value;
            }
        }

        public string Alamat
        {
            get
            {
                return txtVendorAddr.Text;
            }
            set
            {
                txtVendorAddr.Text = value;
            }
        }

        public string KelasCode
        {
            get
            {
                return (cboKelas.SelectedIndex > -1) ? cboKelas.Text : string.Empty;
            }
        }

        public string KelasId
        {
            get
            {
                return (cboKelas.SelectedValue != null) ? cboKelas.SelectedValue.ToString() : string.Empty;
            }
            set
            {
                cboKelas.SelectedValue = value;
            }
        }

        public void KelasSelectedStateChanged(bool p_Enabled)
        {
            txtVendorName.Enabled = p_Enabled;
            txtVendorAddr.Enabled = p_Enabled;
        }

        public bool IsKelas
        {
            get
            {
                return rbKelas.Checked && !rbExternal.Checked;
            }
        }

        #endregion Public Properties

        #region IEditRefVendorView

        public void SetBinding()
        {
            txtVendorName.DataBindings.Add("Text", ViewModel, RefSumberPUModel.SPUNamaPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtVendorAddr.DataBindings.Add("Text", ViewModel, RefSumberPUModel.SPUAlamatPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);

            cboKelas.DataSource = RefKelasDataSource;
            cboKelas.DisplayMember = "Value";
            cboKelas.ValueMember = "Id";
            cboKelas.SelectedIndex = -1;
            //cboKelas.DataBindings.Add("SelectedValue", ViewModel, RefSumberPUModel.SPUKelasIdPropertyName,
            //    false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void SetEnableJenisSumber()
        {
            gbKelas.Enabled = IsKelas;
            gbExternal.Enabled = !IsKelas;
        }

        #endregion IEditRefVendorView
    }
}
