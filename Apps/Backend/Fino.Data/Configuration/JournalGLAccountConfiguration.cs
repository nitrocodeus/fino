﻿using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class JournalGLAccountConfiguration
    {
        internal static void ConfigureJournalGLAccount(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JournalGLAccount>().HasKey(e => e.journaldetail_id);
            modelBuilder.Entity<JournalGLAccount>()
                .HasRequired(e => e.DetailofJournal)
                .WithOptional(e => e.JournalAccountGL);

            modelBuilder.Entity<JournalGLAccount>().HasKey(e => e.glaccount_id);
            modelBuilder.Entity<JournalGLAccount>()
                .HasRequired(e => e.AccountOfGL)
                .WithOptional(e => e.JournalAccountGL);
        }
    }
}
