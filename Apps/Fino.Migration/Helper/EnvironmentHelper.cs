﻿using System;
using System.IO;
using System.Reflection;

namespace Fino.Migration.Helper
{
    public class EnvironmentHelper
    {
        #region Singleton
        private static readonly Lazy<EnvironmentHelper> lazy = new Lazy<EnvironmentHelper>(() => new EnvironmentHelper());

        public static EnvironmentHelper Instance { get { return lazy.Value; } }

        private EnvironmentHelper() { }
        #endregion

        public string GetExecutingAssemblyDirectory()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }
    }
}
