﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class RefKelasModel : BaseModel
    {
        const string CODE_NOTEMPTY = "VLD_REFKELASCODE_NOTEMPTY";
        const string NAME_NOTEMPTY = "VLD_REFKELASNAME_NOTEMPTY";

        public static readonly string KelasCodePropertyName = "KelasCode";
        public static readonly string NamaPropertyName = "Nama";
        public static readonly string TingkatPropertyName = "Tingkat";

        private int _kelasId;
        public int KelasId
        {
            get
            {
                return _kelasId;
            }
            set
            {
                _kelasId = value;
            }
        }

        private string _kelasCode;
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = CODE_NOTEMPTY)]
        public string KelasCode
        {
            get
            {
                return _kelasCode;
            }
            set
            {
                _kelasCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(KelasCodePropertyName));
                ValidateProperty(value, KelasCodePropertyName);
            }
        }

        private string _nama;
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NAME_NOTEMPTY)]
        public string Nama
        {
            get
            {
                return _nama;
            }
            set
            {
                _nama = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamaPropertyName));
                ValidateProperty(value, NamaPropertyName);
            }
        }

        private int _tingkat;
        public int Tingkat
        {
            get
            {
                return _tingkat;
            }
            set
            {
                _tingkat = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(TingkatPropertyName));
            }
        }
    }
}
