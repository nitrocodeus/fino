﻿using Castle.Windsor;
using Fino.BusinessLogic;
using Fino.Cient.Common;
using Fino.Cient.Configuration;
using Fino.Cient.Report;
using Fino.Lib.Core.Messaging;
using Fino.Lib.Win32;
using Fino.Presenter;
using Fino.View;
using System;

namespace Fino.Cient.Startup
{
    public class ViewLocator
    {
        private IWindsorContainer container = null;

        private T Resolve<T>()
        {
            if (container == null)
            {
                container = new WindsorContainer();
                container.Install(new WindsorRepositoryInstaller());
                container.Install(new WindsorBLInstaller());
                container.Install(new WindsorViewInstaller());
                container.Install(new WindsorPresenterInstaller());
            }
            
            return (T)container.Resolve(typeof(T));
        }

        EditRefBiayaView _editPosBiayaView;
        EditGrupBiayaView _editGrupBiayaView;
        RefBiayaView _refBiayaView;
        GrupBiayaView _grupBiayaView;
        KonfigurasiBiayaView _konfigurasiBiayaView;
        RepRekapPPView _reportRekapPPView;
        ReportKasDetailView _reportKasDetailView;
        ReportSaldoView _reportSaldoView;
        ReportNeracaSaldoView _reportNeracaSaldoView;
        ReportMutasiView _reportMutasiView;
        KonversiCicilanView _konversiCicilanView;
        RepRekapPengeluaranView _repRekapPengeluaranView;
        RepDailyJournalView _repDailyJournalView;
        DaftarUlangSiswaView _daftarUlangSiswaView;
        PembayaranBiayaView _pembayaranBiayaView;
        PotonganBiayaView _potonganBiayaView;
        DaftarLayananView _daftarLayananView;
        SiswaExplorerView _siswaExplorerView;
        PendaftaranTabunganView _daftarTabunganView;
        PengelolaanNilaiBiayaView _pengelolaanNilaiBiayaView;
        MutasiTabunganView _mutasiTabunganView;
        RefAccountView _refAcccountView;
        EditRefAccountView _editRefAccountView;
        RefAccPeriodView _refAccPeriodView;
        PostingJournalView _postingJournalView;
        EditRefAccPeriodView _editRefAccPeriodView;
        UbahKataSandiView _ubahKataSandiView;
        UserManagementView _userManagementView;
        EditUserManagementView _editUserManagementView;
        DaftarSiswaBaruView _daftarSiswaBaruView;
        // SaveMachineFingerPrintView _saveMachineFingerPrintView;
        PosBiayaView _posBiayaView;
        PencarianSiswaControlView _pencarianSiswaView;
        RefPayableView _refPayableView;
        EditRefPayableView _editRefPayableView;
        ReportPengeluaranView _reportPengeluaranView;
        PayablePaidView _payablePadiView;
        EditRefVendorView _editRefVendorView;
        AccountClassSettingView _accountClassSettingView;
        EditAccountClassSettingView _editAccountClassSettingView;
        RefKelasView _refKelasView;
        EditRefKelasView _editRefKelasView;
        ReportPenerimaanView _reportPenerimaanView;
        EditRefSiswaView _editRefSiswaView;
        PenerimaanUmumView _penerimaanUmumView;
        EditRefSPUView _editRefSPUView;
        PembatalanPembayaranBiayaView _cariPembatalanPembayaranBiayaView;
        PembatalanPenerimaanUmumView _pembatalanPenerimaanUmumView;
        PembatalanPengeluaranView _pembatalanPengeluaranView;

        public LogonView LogonView { get; private set; }
        public MainFormView MainFormView { get; private set; }

        public PenerimaanUmumView PenerimaanUmumView
        {
            get
            {
                if ((_penerimaanUmumView == null) || (_penerimaanUmumView != null && _penerimaanUmumView.IsDisposed))
                {
                    _penerimaanUmumView = CreateNewView<PenerimaanUmumView, PenerimaanUmumPresenter>();
                }
                else
                {
                    _penerimaanUmumView.BringToFront();
                }

                return _penerimaanUmumView;
            }
        }

        public EditRefSPUView EditRefSPUView
        {
            get
            {
                if ((_editRefSPUView == null) || (_editRefSPUView != null && _editRefSPUView.IsDisposed))
                {
                    _editRefSPUView = CreateNewView<EditRefSPUView, EditRefSPUPresenter>();
                }
                else
                {
                    _editRefSPUView.BringToFront();
                }

                return _editRefSPUView;
            }
        }

        public PendaftaranTabunganView DaftarTabunganView
        {
            get
            {
                if ((_daftarTabunganView == null) || (_daftarTabunganView != null && _daftarTabunganView.IsDisposed))
                {
                    _daftarTabunganView = CreateNewView<PendaftaranTabunganView, PendaftaranTabunganPresenter>();                   
                }
                else
                {
                    _daftarTabunganView.BringToFront();
                }

                return _daftarTabunganView;
            }
        }

        public MutasiTabunganView MutasiTabunganView
        {
            get
            {
                if ((_mutasiTabunganView == null) || (_mutasiTabunganView != null && _mutasiTabunganView.IsDisposed))
                {
                    _mutasiTabunganView = CreateNewView<MutasiTabunganView, MutasiTabunganPresenter>();                    
                }
                else
                {
                    _mutasiTabunganView.BringToFront();
                }

                return _mutasiTabunganView;
            }
        }

        public DaftarLayananView DaftarLayanView
        {
            get
            {
                if ((_daftarLayananView == null) || (_daftarLayananView != null && _daftarLayananView.IsDisposed))
                {
                    _daftarLayananView = CreateNewView<DaftarLayananView, DaftarLayananPresenter>();
                }
                else
                {
                    _daftarLayananView.BringToFront();
                }

                return _daftarLayananView;
            }
        }

        public PembayaranBiayaView PembayaranBiayaView
        {
            get
            {
                if ((_pembayaranBiayaView == null) || (_pembayaranBiayaView != null && _pembayaranBiayaView.IsDisposed))
                {
                    _pembayaranBiayaView = CreateNewView<PembayaranBiayaView, PembayaranBiayaPresenter>();
                }
                else
                {
                    _pembayaranBiayaView.BringToFront();
                }

                return _pembayaranBiayaView;
            }
        }

        public RepRekapPPView ReportRekapPPView
        {
            get
            {
                if ((_reportRekapPPView == null) || (_reportRekapPPView != null && _reportRekapPPView.IsDisposed))
                {
                    _reportRekapPPView = CreateNewView<RepRekapPPView, RekapPPPresenter>();
                }
                else
                {
                    _reportRekapPPView.BringToFront();
                }

                return _reportRekapPPView;
            }
        }

        public KonfigurasiBiayaView KonfigurasiBiayaView
        {
            get
            {
                if ((_konfigurasiBiayaView == null) || (_konfigurasiBiayaView != null && _konfigurasiBiayaView.IsDisposed))
                {
                    _konfigurasiBiayaView = CreateNewView<KonfigurasiBiayaView, KonfigurasiBiayaPresenter>();
                }
                else
                {
                    _konfigurasiBiayaView.BringToFront();
                }

                return _konfigurasiBiayaView;
            }
        }

        public DaftarUlangSiswaView DaftarUlangSiswaView
        {
            get
            {
                if ((_daftarUlangSiswaView == null) || (_daftarUlangSiswaView != null && _daftarUlangSiswaView.IsDisposed))
                {
                    _daftarUlangSiswaView = CreateNewView<DaftarUlangSiswaView, DaftarUlangSiswaPresenter>();
                }
                else
                {
                    _daftarUlangSiswaView.BringToFront();
                }

                return _daftarUlangSiswaView;
            }
        }

        public PotonganBiayaView PotonganBiayaView
        {
            get
            {
                if ((_potonganBiayaView == null) || (_potonganBiayaView != null && _potonganBiayaView.IsDisposed))
                {
                    _potonganBiayaView = CreateNewView<PotonganBiayaView, PotonganBiayaPresenter>();
                }
                else
                {
                    _daftarUlangSiswaView.BringToFront();
                }

                return _potonganBiayaView;
            }
        }

        public RefBiayaView RefBiayaView
        {
            get
            {
                if ((_refBiayaView == null) || (_refBiayaView != null && _refBiayaView.IsDisposed))
                {
                    _refBiayaView = CreateNewView<RefBiayaView, RefBiayaPresenter>();                    
                }
                else
                {
                    _refBiayaView.BringToFront();                    
                }

                return _refBiayaView;
            }
        }

        public PosBiayaView PosBiayaView
        {
            get
            {
                _posBiayaView = CreateNewView<PosBiayaView, PosBiayaPresenter>();
                _posBiayaView.BringToFront();

                return _posBiayaView;
            }
        }

        public PencarianSiswaControlView PencarianSiswaView
        {
            get
            {
                _pencarianSiswaView = CreateNewView<PencarianSiswaControlView, PencarianSiswaPresenter>();
                _pencarianSiswaView.BringToFront();

                return _pencarianSiswaView;
            }
        }

        public EditRefBiayaView EditPosBiayaView
        {
            get
            {
                if ((_editPosBiayaView == null) || (_editPosBiayaView != null && _editPosBiayaView.IsDisposed))
                {
                    _editPosBiayaView = CreateNewView<EditRefBiayaView, EditRefBiayaPresenter>();
                }
                else
                {
                    _editPosBiayaView.BringToFront();
                }

                return _editPosBiayaView;
            }
        }

        public EditGrupBiayaView EditGrupBiayaView
        {
            get
            {
                if ((_editGrupBiayaView == null) || (_editGrupBiayaView != null && _editGrupBiayaView.IsDisposed))
                {
                    _editGrupBiayaView = CreateNewView<EditGrupBiayaView, EditGrupBiayaPresenter>();
                }
                else
                {
                    _editGrupBiayaView.BringToFront();
                }

                return _editGrupBiayaView;
            }
        }

        public GrupBiayaView GrupBiayaView
        {
            get
            {
                if ((_grupBiayaView == null) || (_grupBiayaView != null && _grupBiayaView.IsDisposed))
                {
                    _grupBiayaView = CreateNewView<GrupBiayaView, GrupBiayaPresenter>();
                }
                else
                {
                    _grupBiayaView.BringToFront();
                }

                return _grupBiayaView;
            }
        }

        public SiswaExplorerView SiswaExplorerView
        {
            get
            {
                if (_siswaExplorerView == null || (_siswaExplorerView != null && _siswaExplorerView.IsDisposed))
                {
                    _siswaExplorerView = CreateNewView<SiswaExplorerView, SiswaExplorerPresenter>();
                }
                else
                {
                    _siswaExplorerView.BringToFront();
                }

                return _siswaExplorerView;
            }
        }

        public PengelolaanNilaiBiayaView PengelolaanNilaiBiayaView
        {
            get
            {
                if (_pengelolaanNilaiBiayaView == null || (_pengelolaanNilaiBiayaView != null && _pengelolaanNilaiBiayaView.IsDisposed))
                {
                    _pengelolaanNilaiBiayaView = CreateNewView<PengelolaanNilaiBiayaView, PengelolaanNilaiBiayaPresenter>();
                }
                else
                {
                    _pengelolaanNilaiBiayaView.BringToFront();
                }

                return _pengelolaanNilaiBiayaView;
            }
        }

        public RefAccountView RefAccountView
        {
            get
            {
                if (_refAcccountView == null || (_refAcccountView != null && _refAcccountView.IsDisposed))
                {
                    _refAcccountView = CreateNewView<RefAccountView, RefAccountPresenter>();
                }
                else
                {
                    _refAcccountView.BringToFront();
                }

                return _refAcccountView;
            }
        }

        public DaftarSiswaBaruView DaftarSiswaBaruView
        {
            get
            {
                if (_daftarSiswaBaruView == null || (_daftarSiswaBaruView != null && _daftarSiswaBaruView.IsDisposed))
                {
                    _daftarSiswaBaruView = CreateNewView<DaftarSiswaBaruView, DaftarSiswaBaruPresenter>();
                }
                else
                {
                    _daftarSiswaBaruView.BringToFront();
                }

                return _daftarSiswaBaruView;
            }
        }

        public EditRefAccountView EditRefAccountView
        {
            get
            {
                if (_editRefAccountView == null || (_editRefAccountView != null && _editRefAccountView.IsDisposed))
                {
                    _editRefAccountView = CreateNewView<EditRefAccountView, EditRefAccountPresenter>();
                }
                else
                {
                    _editRefAccountView.BringToFront();
                }

                return _editRefAccountView;
            }
        }

        public RefAccPeriodView RefAccPeriodView
        {
            get
            {
                if (_refAccPeriodView == null || (_refAccPeriodView != null && _refAccPeriodView.IsDisposed))
                {
                    _refAccPeriodView = CreateNewView<RefAccPeriodView, RefAccPeriodPresenter>();
                }
                else
                {
                    _refAccPeriodView.BringToFront();
                }
                return _refAccPeriodView;
            }
        }

        public EditRefAccPeriodView EditRefAccPeriodView
        {
            get
            {
                if (_editRefAccPeriodView == null || (_editRefAccPeriodView != null && _editRefAccPeriodView.IsDisposed))
                {
                    _editRefAccPeriodView = CreateNewView<EditRefAccPeriodView, EditRefAccPeriodPresenter>();
                }
                else
                {
                    _editRefAccPeriodView.BringToFront();
                }
                return _editRefAccPeriodView;
            }
        }

        public PostingJournalView PostingJournalView
        {
            get
            {
                if (_postingJournalView == null || (_postingJournalView != null && _postingJournalView.IsDisposed))
                {
                    _postingJournalView = CreateNewView<PostingJournalView, PostingJournalPresenter>();
                }
                else
                {
                    _postingJournalView.BringToFront();
                }

                return _postingJournalView;
            }
        }

        public ReportKasDetailView ReportKasDetailView
        {
            get
            {
                if (_reportKasDetailView == null || (_reportKasDetailView != null && _reportKasDetailView.IsDisposed))
                {
                    _reportKasDetailView = CreateNewView<ReportKasDetailView, ReportKasDetailPresenter>();
                }
                else
                {
                    _reportKasDetailView.BringToFront();
                }

                return _reportKasDetailView;
            }
        }

        public ReportPenerimaanView ReportPenerimaanView
        {
            get
            {
                if (_reportPenerimaanView == null || (_reportPenerimaanView != null && _reportPenerimaanView.IsDisposed))
                {
                    _reportPenerimaanView = CreateNewView<ReportPenerimaanView, ReportPenerimaanDetailPresenter>();
                }
                else
                {
                    _reportPenerimaanView.BringToFront();
                }

                return _reportPenerimaanView;
            }
        }

        public ReportSaldoView ReportSaldoView
        {
            get
            {
                if (_reportSaldoView == null || (_reportSaldoView != null && _reportSaldoView.IsDisposed))
                {
                    _reportSaldoView = CreateNewView<ReportSaldoView, ReportSaldoPresenter>();
                }
                else
                {
                    _reportSaldoView.BringToFront();
                }

                return _reportSaldoView;
            }
        }

        public ReportNeracaSaldoView ReportNeracaSaldoView
        {
            get
            {
                if (_reportNeracaSaldoView == null || (_reportNeracaSaldoView != null && _reportNeracaSaldoView.IsDisposed))
                {
                    _reportNeracaSaldoView = CreateNewView<ReportNeracaSaldoView, ReportNeracaSaldoPresenter>();
                }
                else
                {
                    _reportNeracaSaldoView.BringToFront();
                }

                return _reportNeracaSaldoView;
            }
        }
        public ReportMutasiView ReportMutasiView
        {
            get
            {
                if (_reportMutasiView == null || (_reportMutasiView != null && _reportMutasiView.IsDisposed))
                {
                    _reportMutasiView = CreateNewView<ReportMutasiView, ReportMutasiPresenter>();
                }
                else
                {
                    _reportMutasiView.BringToFront();
                }

                return _reportMutasiView;
            }
        }
        public KonversiCicilanView KonversiCicilanView
        {
            get
            {
                if (_konversiCicilanView == null || (_konversiCicilanView != null && _konversiCicilanView.IsDisposed))
                {
                    _konversiCicilanView = CreateNewView<KonversiCicilanView, KonversiCicilanPresenter>();
                }
                else
                {
                    _konversiCicilanView.BringToFront();
                }

                return _konversiCicilanView;
            }
        }

        public RepRekapPengeluaranView RepRekapPengeluaranView
        {
            get
            {
                if (_repRekapPengeluaranView == null || (_repRekapPengeluaranView != null && _repRekapPengeluaranView.IsDisposed))
                {
                    _repRekapPengeluaranView = CreateNewView<RepRekapPengeluaranView, RekapPengeluaranPresenter>();
                }
                else
                {
                    _repRekapPengeluaranView.BringToFront();
                }

                return _repRekapPengeluaranView;
            }
        }

        public RepDailyJournalView RepDailyJournalView
        {
            get
            {
                if (_repDailyJournalView == null || (_repDailyJournalView != null && _repDailyJournalView.IsDisposed))
                {
                    _repDailyJournalView = CreateNewView<RepDailyJournalView, RepDailyJournalPresenter>();
                }
                else
                {
                    _repDailyJournalView.BringToFront();
                }

                return _repDailyJournalView;
            }
        }

        public UbahKataSandiView UbahKataSandiView
        {
            get
            {
                if (_ubahKataSandiView == null || (_ubahKataSandiView != null && _ubahKataSandiView.IsDisposed))
                {
                    _ubahKataSandiView = CreateNewView<UbahKataSandiView, UbahKataSandiPresenter>();
                }
                else
                {
                    _ubahKataSandiView.BringToFront();
                }

                return _ubahKataSandiView;
            }
        }

        public UserManagementView UserManagementView
        {
            get
            {
                if (_userManagementView == null || (_userManagementView != null && _userManagementView.IsDisposed))
                {
                    _userManagementView = CreateNewView<UserManagementView, UserManagementPresenter>();
                }
                else
                {
                    _userManagementView.BringToFront();
                }

                return _userManagementView;
            }
        }

        public EditUserManagementView EditUserManagementView
        {
            get
            {
                if (_editUserManagementView == null || (_editUserManagementView != null && _editUserManagementView.IsDisposed))
                {
                    _editUserManagementView = CreateNewView<EditUserManagementView, EditUserManagementPresenter>();
                }
                else
                {
                    _editUserManagementView.BringToFront();
                }

                return _editUserManagementView;
            }
        }

        public RefPayableView RefPayableView
        {
            get
            {
                if (_refPayableView == null || (_refPayableView != null && _refPayableView.IsDisposed))
                {
                    _refPayableView = CreateNewView<RefPayableView, RefPayablePresenter>();
                }
                else
                {
                    _refPayableView.BringToFront();
                }

                return _refPayableView;
            }
        }

        public EditRefPayableView EditRefPayableView
        {
            get
            {
                if (_editRefPayableView == null || (_editRefPayableView != null && _editRefPayableView.IsDisposed))
                {
                    _editRefPayableView = CreateNewView<EditRefPayableView, EditRefPayablePresenter>();
                }
                else
                {
                    _editRefPayableView.BringToFront();
                }

                return _editRefPayableView;
            }
        }

        public ReportPengeluaranView ReportPengeluaranView
        {
            get
            {
                if (_reportPengeluaranView == null || (_reportPengeluaranView != null && _reportPengeluaranView.IsDisposed))
                {
                    _reportPengeluaranView = CreateNewView<ReportPengeluaranView, ReportPengeluaranPresenter>();
                }
                else
                {
                    _reportPengeluaranView.BringToFront();
                }

                return _reportPengeluaranView;
            }
        }

        public SaveMachineFingerPrintView SaveMachineFingerPrintView
        {
            get
            {
                return CreateNewView<SaveMachineFingerPrintView, SaveMachineFingerPrintPresenter>();
            }
        }

        public PayablePaidView PayablePaidView
        {
            get
            {
                if (_payablePadiView == null ||(_payablePadiView != null && _payablePadiView.IsDisposed))
                {
                    _payablePadiView = CreateNewView<PayablePaidView, PayablePaidPresenter>();
                }
                else
                {
                    _payablePadiView.BringToFront();
                }

                return _payablePadiView;
            }
        }

        public EditRefVendorView EditRefVendorView
        {
            get
            {
                if (_editRefVendorView == null || (_editRefVendorView != null && _editRefVendorView.IsDisposed))
                {
                    _editRefVendorView = CreateNewView<EditRefVendorView, EditRefVendorPresenter>();
                }
                else
                {
                    _editRefVendorView.BringToFront();
                }

                return _editRefVendorView;
            }
        }

        public AccountClassSettingView AccountClassSettingView
        {
            get
            {
                if (_accountClassSettingView == null || (_accountClassSettingView != null && _accountClassSettingView.IsDisposed))
                {
                    _accountClassSettingView = CreateNewView<AccountClassSettingView, AccountClassSettingPresenter>();
                }
                else
                {
                    _accountClassSettingView.BringToFront();
                }

                return _accountClassSettingView;
            }
        }

        public EditAccountClassSettingView EditAccountClassSettingView
        {
            get
            {
                if (_editAccountClassSettingView == null || (_editAccountClassSettingView != null && _editAccountClassSettingView.IsDisposed))
                {
                    _editAccountClassSettingView = CreateNewView<EditAccountClassSettingView, EditAccountClassSettingPresenter>();
                }
                else
                {
                    _editAccountClassSettingView.BringToFront();
                }

                return _editAccountClassSettingView;
            }
        }

        public RefKelasView RefKelasView
        {
            get
            {
                if (_refKelasView == null || (_refKelasView != null && _refKelasView.IsDisposed))
                {
                    _refKelasView = CreateNewView<RefKelasView, RefKelasPresenter>();
                }
                else
                {
                    _refKelasView.BringToFront();
                }

                return _refKelasView;
            }
        }

        public EditRefKelasView EditRefKelasView
        {
            get
            {
                if (_editRefKelasView == null ||(_editRefKelasView != null && _editRefKelasView.IsDisposed))
                {
                    _editRefKelasView = CreateNewView<EditRefKelasView, EditRefKelasPresenter>();
                }
                else
                {
                    _editRefKelasView.BringToFront();
                }

                return _editRefKelasView;
            }
        }

        public EditRefSiswaView EditRefSiswaView
        {
            get
            {
                if (_editRefSiswaView == null || (_editRefSiswaView != null && _editRefSiswaView.IsDisposed))
                {
                    _editRefSiswaView = CreateNewView<EditRefSiswaView, EditRefSiswaPresenter>();
                }
                else
                {
                    _editRefSiswaView.BringToFront();
                }

                return _editRefSiswaView;
            }
        }

        public PembatalanPembayaranBiayaView CariPembatalanPembayaranBiayaView
        {
            get
            {
                if (_cariPembatalanPembayaranBiayaView == null || (_cariPembatalanPembayaranBiayaView != null && _cariPembatalanPembayaranBiayaView.IsDisposed))
                {
                    _cariPembatalanPembayaranBiayaView = CreateNewView<PembatalanPembayaranBiayaView, PembatalanPembayaranBiayaPresenter>();
                }
                else
                {
                    _cariPembatalanPembayaranBiayaView.BringToFront();
                }

                return _cariPembatalanPembayaranBiayaView;
            }
        }

        public PembatalanPenerimaanUmumView PembatalanPenerimaanUmumView
        {
            get
            {
                if (_pembatalanPenerimaanUmumView == null || (_pembatalanPenerimaanUmumView != null && _pembatalanPenerimaanUmumView.IsDisposed))
                {
                    _pembatalanPenerimaanUmumView = CreateNewView<PembatalanPenerimaanUmumView, PembatalanPenerimaanUmumPresenter>();
                }
                else
                {
                    _pembatalanPenerimaanUmumView.BringToFront();
                }

                return _pembatalanPenerimaanUmumView;
            }
        }

        public PembatalanPengeluaranView PembatalanPengeluaranView
        {
            get
            {
                if (_pembatalanPengeluaranView == null || (_pembatalanPengeluaranView != null && _pembatalanPengeluaranView.IsDisposed))
                {
                    _pembatalanPengeluaranView = CreateNewView<PembatalanPengeluaranView, PembatalanPengeluaranPresenter>();
                }
                else
                {
                    _pembatalanPengeluaranView.BringToFront();
                }

                return _pembatalanPengeluaranView;
            }
        }

        #region Singleton
        private static readonly Lazy<ViewLocator> lazy = new Lazy<ViewLocator>(() => new ViewLocator());

        public static ViewLocator Instance { get { return lazy.Value; } }

        private ViewLocator() { }
        #endregion

        public void Initialise()
        {
            RegisterLogonView();
            RegisterMainView();
        }

        private void RegisterMainView()
        {               
            MainFormPresenter mainFormPresenter = Resolve<MainFormPresenter>();
            MainFormView = mainFormPresenter.View as MainFormView;    
        }

        private void RegisterLogonView()
        {                        
            LogonPresenter logonPresenter = Resolve<LogonPresenter>();
            LogonView = logonPresenter.View as LogonView;
        }

        private V CreateNewView<V, P>()
        {
            BasePresenter presenter = Resolve<P>() as BasePresenter;
            IView view = presenter.View;

            Mediator.Instance.Register(presenter);

            view.MdiParent = MainFormView;

            return (V)view;
        }

        public void ResetUbahKataSandiView()
        {
            this._ubahKataSandiView = null;
        }
    }
}
