﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;

namespace Fino.BusinessLogic
{
    public interface IPayableProcess
    {
        ProcessResult GetRekapitulasiPengeluaran(DateTime p_FromDate, DateTime p_ToDate);
    }
}
