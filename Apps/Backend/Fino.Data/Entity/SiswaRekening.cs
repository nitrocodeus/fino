﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class SiswaRekening
    {
        public int siswa_id { get; set; }
        public int rekening_id { get; set; }

        public virtual RefSiswa Siswa { get; set; }
        public virtual RefRekening Rekening { get; set; }
    }
}
