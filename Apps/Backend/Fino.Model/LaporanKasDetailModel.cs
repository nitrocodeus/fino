﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class LaporanKasDetailModel
    {
        public string Kelas { get; set; }
        public string Siswa { get; set; }
        public string Biaya { get; set; }
        public double Nilai { get; set; }
    }
}
