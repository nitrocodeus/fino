﻿using Fino.Datalib.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class NotaPembayaranConfiguration
    {
        internal static void ConfiguraNotaPembayaran(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NotaPembayaran>().HasKey(e => e.nota_pembayaran_Id);
            modelBuilder.Entity<NotaPembayaran>().Property(e => e.nota_pembayaran_Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<NotaPembayaran>()
                    .HasRequired(e => e.Siswa)
                    .WithMany(e => e.NotaPembayarans)
                    .HasForeignKey(e => e.siswa_id);

            modelBuilder.Entity<NotaPembayaran>()
                    .HasRequired(e => e.User)
                    .WithMany(e => e.NotaPembayarans)
                    .HasForeignKey(e => e.user_id);
        }
    }
}
