﻿using Fino.Datalib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository.Penerimaan
{
    public class NotaPenerimaanUmumRepository : BaseRepository, INotaPenerimaanUmumRepository
    {
        private NotaPenerimaanUmum GetExistingNotaPenerimaanUmum(int p_NotaPenerimaanUmumId)
        {
            return this.Context.NotaPenerimaanUmums.Where(x => x.nota_penerimaan_Id == p_NotaPenerimaanUmumId).FirstOrDefault();
        }

        public void DeleteNotaPenerimaanUmum(int p_NotaPenerimaanUmumId)
        {
            var existingNotaPenerimaanUmum = GetExistingNotaPenerimaanUmum(p_NotaPenerimaanUmumId);

            if (existingNotaPenerimaanUmum != null)
            {
                this.Context.NotaPenerimaanUmums.Remove(existingNotaPenerimaanUmum);
                this.Context.SaveChanges();
            }
        }
    }
}
