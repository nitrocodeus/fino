﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System.Collections.Generic;

namespace Fino.BusinessLogic
{
    public interface IRefAccountProcess
    {
        IRefAccountRepository RefAccountRepo { get; set; }
        ProcessResult UpdateRefAccount(RefAccountModel entity);
        ProcessResult GetRefAccount(int account_id);
        ProcessResult GetAllRefAccount();
        ProcessResult DeleteRefAccount(RefAccountModel entity);
    }
}
