﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IPembayaranBiayaView : IView
    {
        event Action btnSimpanClicked;
        event Action btnTutupClicked;
        event Action SelectingSiswaDone;
        event Action AddingNewPosBiaya;
        event Action txtSiswaKeyPressed;
        event Action btnTambahBiayaClicked;

        IPencarianSiswaView PencarianSiswaView { get; }
        IPosBiayaView CurrentPosBiayaView { get; }
        IPosBiayaView AddPosBiayaView { get; }

        int SiswaId { get; }
        string NamaSiswa { get; set; }
        string TextSearchSiswa { get; }
        string NoInduk { get; set; }
        string TotalBiaya { get; set; }
        int IdKelas { get; }
        string Kelas { get; }
        int IdTahunAjaran { get; }
        string TahunAjaran { get; }

        void AddPosBiayaMode(bool p_Mode);
        void CetakNota(CetakPembayaranModel p_Model);
    }
}
