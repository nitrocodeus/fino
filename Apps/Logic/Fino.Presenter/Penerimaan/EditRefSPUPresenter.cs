﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class EditRefSPUPresenter : BasePresenter
    {
        IEditRefSPUView _editRefSPUView;
        IPenerimaanUmumProcess _penerimaanUmumProcess;

        public EditRefSPUPresenter(IEditRefSPUView p_View, IPenerimaanUmumProcess p_Process)
            : base (p_View)
        {
            this._editRefSPUView = p_View;
            this._penerimaanUmumProcess = p_Process;

            this._editRefSPUView.BtnSimpanClicked += _editRefVendorView_BtnSimpanClicked;
            this._editRefSPUView.BtnBatalClicked += _editRefVendorView_BtnBatalClicked;
            this._editRefSPUView.Load += _editRefVendorView_Load;
            this._editRefSPUView.CboKelasSelectedChanged += _editRefSPUView_CboKelasSelectedChanged;
            this._editRefSPUView.RbKelasSelectedChanged += _editRefSPUView_RbKelasSelectedChanged;
        }

        void _editRefSPUView_RbKelasSelectedChanged()
        {
            this._editRefSPUView.SetEnableJenisSumber();
            SetSumberInfo();
        }

        void SetSumberInfo()
        {
            if (this._editRefSPUView.IsKelas)
            {
                this._editRefSPUView.Nama = "Kelas " + this._editRefSPUView.KelasCode;
                this._editRefSPUView.Alamat = "-";
            }
            else
            {
                this._editRefSPUView.Nama = "";
                this._editRefSPUView.Alamat = "";
            }
        }

        void _editRefSPUView_CboKelasSelectedChanged()
        {
            SetSumberInfo();
        }

        
        void _editRefVendorView_Load(object sender, EventArgs e)
        {
            this._editRefSPUView.RefKelasDataSource = GetRefKelasDataSource();
            this._editRefSPUView.ViewModel = new Model.RefSumberPUModel();
            this._editRefSPUView.SetBinding();
        }

        void _editRefVendorView_BtnBatalClicked()
        {
            this._editRefSPUView.Close();
        }

        void _editRefVendorView_BtnSimpanClicked()
        {
            var context = new ValidationContext(this._editRefSPUView.ViewModel, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(this._editRefSPUView.ViewModel, context, results, true);

            if (isValid)
            {
                Save();

                this._editRefSPUView.ViewModel.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                this._editRefSPUView.ViewModel.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        private void Save()
        {
            if (this._editRefSPUView.IsKelas)
            {
                this._editRefSPUView.ViewModel.KelasId = Convert.ToInt32(this._editRefSPUView.KelasId);
            }

            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => this._penerimaanUmumProcess.CreateNewRefSPU(this._editRefSPUView.ViewModel)),
                        AppResource.MSG_SAVING_SUMBERPENERIMAAN);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                this._editRefSPUView.PenerimaanUmumView.ReloadData();
                this._editRefSPUView.Close();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        private object GetRefKelasDataSource()
        {
            return this._penerimaanUmumProcess.GetAllKelas().DataResult;
        }
    }
}
