﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class RefAccPeriodPresenter : BasePresenter
    {
        private readonly IRefAccPeriodProcess _refAccPeriodProcess;
        private readonly IRefAccPeriodView _refAccPeriodView;

        public RefAccPeriodPresenter(IRefAccPeriodProcess p_Process, IRefAccPeriodView p_View) : base(p_View)
        {
            this._refAccPeriodProcess = p_Process;
            this._refAccPeriodView = p_View;

            this._refAccPeriodView.Load += _refAccPeriodView_Load;
            this._refAccPeriodView.AddNewButtonClicked += _refAccPeriodView_AddNewButtonClicked;
            this._refAccPeriodView.EditButtonClicked += _refAccPeriodView_EditButtonClicked;
            this._refAccPeriodView.OpenPeriodButtonClicked += _refAccPeriodView_OpenPeriodButtonClicked;
            this._refAccPeriodView.ClosePeriodButtonClicked += _refAccPeriodView_ClosePeriodButtonClicked;
            this._refAccPeriodView.DeactivateButtonClicked += _refAccPeriodView_DeactivateButtonClicked;
            this._refAccPeriodView.CloseButtonClicked += _refAccPeriodView_CloseButtonClicked;
            this._refAccPeriodView.GvAccPeriodDoubleClicked += _refAccPeriodView_GvAccPeriodDoubleClicked;
            this._refAccPeriodView.GvAccPeriodSelectionChanged += _refAccPeriodView_GvAccPeriodSelectionChanged;
            this._refAccPeriodView.ReloadData += _refAccPeriodView_ReloadData;
        }

        private void _refAccPeriodView_GvAccPeriodSelectionChanged()
        {
            var selectedRefAccPeriod = this._refAccPeriodView.GetSelectedRefAccPeriod();

            if (selectedRefAccPeriod != null)
            {
                bool btnUbahEnabled = selectedRefAccPeriod.Status == (int)RefAccPeriodStatusEnum.New;
                bool btnBukaEnabled = btnUbahEnabled && selectedRefAccPeriod.Period_end >= DateTime.Today;
                bool btnTutupEnabled = selectedRefAccPeriod.Status == (int)RefAccPeriodStatusEnum.Opened;
                bool btnDeaktivasi = btnUbahEnabled;
                this._refAccPeriodView.ChangeControlEnabled(btnUbahEnabled, btnBukaEnabled, btnTutupEnabled, btnDeaktivasi);
            }
        }

        #region Private Methods

        private void LoadData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => this._refAccPeriodProcess.GetAllRefAccPeriodModel()),
                AppResource.MSG_LOADING_REF_BIAYA);

            if (result.IsSucess)
            {
                List<RefAccPeriodModel> data = result.DataResult as List<RefAccPeriodModel>;

                this._refAccPeriodView.SetData(data);
                this._refAccPeriodView.RefreshDataGrid();
            }
            else
            {
                MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion Private Methods

        #region Control Events

        private void _refAccPeriodView_GvAccPeriodDoubleClicked()
        {
            RefAccPeriodModel selectedRefAcc = this._refAccPeriodView.GetSelectedRefAccPeriod();

            if (selectedRefAcc != null && selectedRefAcc.Status == 0)
            {
                this._refAccPeriodView.ShowEditRefAcPeriodView();
            }
        }

        private void _refAccPeriodView_CloseButtonClicked()
        {
            this._refAccPeriodView.Close();
        }

        private void _refAccPeriodView_DeactivateButtonClicked()
        {
            RefAccPeriodModel selectedRefAcc = this._refAccPeriodView.GetSelectedRefAccPeriod();

            if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_DEACT_REFACCPERIOD, selectedRefAcc.Period_name), "Deaktivasi", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => this._refAccPeriodProcess.UpdateRefAccPeriodStatus(
                                    selectedRefAcc.Period_id,
                                    (int)RefAccPeriodStatusEnum.Deactivated)),
                                AppResource.MSG_DEACT_REFACCPERIOD);

                if (result.IsSucess)
                {
                    LoadData();
                }
                else
                {
                    if (result.ProcessException != null && result.ProcessException.GetType() == typeof(UpdateException))
                    {
                        MessageBox.Show(string.Format(AppResource.MSG_DATA_TDKBISADIHAPUS, selectedRefAcc.Period_name, Environment.NewLine), AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void _refAccPeriodView_ClosePeriodButtonClicked()
        {
            RefAccPeriodModel selectedRefAcc = this._refAccPeriodView.GetSelectedRefAccPeriod();

            if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_CLOSE_REFACCPERIOD, selectedRefAcc.Period_name), "Deaktivasi", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => this._refAccPeriodProcess.UpdateRefAccPeriodStatus(
                                    selectedRefAcc.Period_id,
                                    (int)RefAccPeriodStatusEnum.Closed)),
                                AppResource.MSG_CLOSE_REFACCPERIOD);

                if (result.IsSucess)
                {
                    LoadData();
                }
                else
                {
                    if (result.ProcessException != null && result.ProcessException.GetType() == typeof(UpdateException))
                    {
                        MessageBox.Show(string.Format(AppResource.MSG_DATA_TDKBISADIHAPUS, selectedRefAcc.Period_name, Environment.NewLine), AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void _refAccPeriodView_OpenPeriodButtonClicked()
        {
            RefAccPeriodModel selectedRefAcc = this._refAccPeriodView.GetSelectedRefAccPeriod();

            if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_OPEN_REFACCPERIOD, selectedRefAcc.Period_name), "Deaktivasi", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => this._refAccPeriodProcess.UpdateRefAccPeriodStatus(
                                    selectedRefAcc.Period_id,
                                    (int)RefAccPeriodStatusEnum.Opened)),
                                AppResource.MSG_OPEN_REFACCPERIOD);

                if (result.IsSucess)
                {
                    LoadData();
                }
                else
                {
                    if (result.ProcessException != null && result.ProcessException.GetType() == typeof(UpdateException))
                    {
                        MessageBox.Show(string.Format(AppResource.MSG_DATA_TDKBISADIHAPUS, selectedRefAcc.Period_name, Environment.NewLine), AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(result.ProcessException.Message, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void _refAccPeriodView_EditButtonClicked()
        {
            this._refAccPeriodView.ShowEditRefAcPeriodView();
        }

        private void _refAccPeriodView_AddNewButtonClicked()
        {
            this._refAccPeriodView.ShowTambahRefAccPeriodView();
        }

        private void _refAccPeriodView_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void _refAccPeriodView_ReloadData()
        {
            LoadData();
        }

        #endregion Control Events
    }
}
