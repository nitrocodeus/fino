﻿using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class LaporanPengeluaranRepository : BaseRepository, ILaporanPengeluaranRepository
    {
        public List<LaporanPengeluaranModel> GetDataPengeluaran(DateTime p_FromDate, DateTime p_ToDate)
        {
            List<LaporanPengeluaranModel> result = new List<LaporanPengeluaranModel>();

            var startDate = p_FromDate.Date;
            var endDate = new DateTime(p_ToDate.Year, p_ToDate.Month, p_ToDate.Day, 23, 59, 59);

            var query = (from a in Context.Payables
                         where a.created >= startDate && a.created <= endDate
                         select new { a.created, a.refpayable.name, a.deskripsi, a.nilai });

            foreach (var item in query)
            {
                LaporanPengeluaranModel temp = new LaporanPengeluaranModel();
                temp.Tanggal = item.created;
                temp.JenisPengeluaran = item.name;
                temp.Deskripsi = item.deskripsi;
                temp.Nilai = item.nilai;

                result.Add(temp);
            }

            return result;
        }
    }
}
