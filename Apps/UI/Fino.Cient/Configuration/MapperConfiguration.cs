﻿using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Cient.Configuration
{
    public class MapperConfiguration
    {
        #region Singleton
        private static readonly Lazy<MapperConfiguration> lazy = new Lazy<MapperConfiguration>(() => new MapperConfiguration());
    
        public static MapperConfiguration Instance { get { return lazy.Value; } }

        private MapperConfiguration() { }
        #endregion

        public void SetupMapping()
        {
            SetupKonfigurasiBiayaMapping();
            SetupUserMapping();
            SetupSiswaMapping();
            SetupPosBiayaMapping();
            SetupLayananMapping();
            SetupPosBiayaPaidMapping();
            SetupCetakPembayaranMapping();
            SetupTahunAjaranMapping();
            SetupRefPayableMapping();
        }

        private void SetupTahunAjaranMapping()
        {
            AutoMapper.Mapper.CreateMap<RefTahunAjaran, TahunAjaranModel>()
                .ForMember(dest => dest.TahunAjaranId, opts => opts.MapFrom(src => src.tahunajaran_id))
                .ForMember(dest => dest.Nama, opts => opts.MapFrom(src => src.nama))
                .ForMember(dest => dest.HinggaBulan, opts => opts.MapFrom(src => src.hingga_bulan))
                .ForMember(dest => dest.HinggaTahun, opts => opts.MapFrom(src => src.hingga_tahun))
                .ForMember(dest => dest.MulaiBulan, opts => opts.MapFrom(src => src.mulai_bulan))
                .ForMember(dest => dest.MulaiTahun, opts => opts.MapFrom(src => src.mulai_tahun))
                .ForMember(dest => dest.SemCawu, opts => opts.MapFrom(src => src.semcawu))
                .ForMember(dest => dest.Status, opts => opts.MapFrom(src => src.status));

            AutoMapper.Mapper.CreateMap<TahunAjaranModel, RefTahunAjaran>()
                .ForMember(dest => dest.tahunajaran_id, opts => opts.MapFrom(src => src.TahunAjaranId))
                .ForMember(dest => dest.nama, opts => opts.MapFrom(src => src.Nama))
                .ForMember(dest => dest.hingga_bulan, opts => opts.MapFrom(src => src.HinggaBulan))
                .ForMember(dest => dest.hingga_tahun, opts => opts.MapFrom(src => src.HinggaTahun))
                .ForMember(dest => dest.mulai_bulan, opts => opts.MapFrom(src => src.MulaiBulan))
                .ForMember(dest => dest.mulai_tahun, opts => opts.MapFrom(src => src.MulaiTahun))
                .ForMember(dest => dest.semcawu, opts => opts.MapFrom(src => src.SemCawu))
                .ForMember(dest => dest.status, opts => opts.MapFrom(src => src.Status));
        }

        private void SetupCetakPembayaranMapping()
        {
            AutoMapper.Mapper.CreateMap<CetakPembayaranModel, NotaPembayaran>()
                .ForMember(dest => dest.nota_pembayaran_Id, opts => opts.MapFrom(src => src.NotaPembayaranId))
                .ForMember(dest => dest.siswa_id, opts => opts.MapFrom(src => src.SiswaId))
                .ForMember(dest => dest.tanggal_cetak, opts => opts.MapFrom(src => src.TanggalPembayaran));
        }

        private void SetupPosBiayaPaidMapping()
        {
            AutoMapper.Mapper.CreateMap<PosBiayaPaid, PosBiayaPaidModel>()
                .ForMember(dest => dest.PosBiayaId, opts => opts.MapFrom(src => src.PosBiaya_Id))
                .ForMember(dest => dest.DatePaid, opts => opts.MapFrom(src => src.datepaid))
                .ForMember(dest => dest.NilaiBayar, opts => opts.MapFrom(src => src.paynilai))
                .ForMember(dest => dest.Status, opts => opts.MapFrom(src => src.status))
                .ForMember(dest => dest.NotaPembayaranId, opts => opts.MapFrom(src => src.nota_pembayaran_id));

            AutoMapper.Mapper.CreateMap<PosBiayaPaidModel, PosBiayaPaid>()
                .ForMember(dest => dest.PosBiaya_Id, opts => opts.MapFrom(src => src.PosBiayaId))
                .ForMember(dest => dest.datepaid, opts => opts.MapFrom(src => src.DatePaid))
                .ForMember(dest => dest.paynilai, opts => opts.MapFrom(src => src.NilaiBayar))
                .ForMember(dest => dest.status, opts => opts.MapFrom(src => src.Status))
                .ForMember(dest => dest.nota_pembayaran_id, opts => opts.MapFrom(src => src.NotaPembayaranId));
        }

        private void SetupLayananMapping()
        {
            AutoMapper.Mapper.CreateMap<RefLayanan, RefLayananModel>()
                .ForMember(dest => dest.LayananId, opts => opts.MapFrom(src => src.layanan_id))
                .ForMember(dest => dest.BiayaId, opts => opts.MapFrom(src => src.BiayaOfLayanan.biaya_id))
                .ForMember(dest => dest.Code, opts => opts.MapFrom(src => src.code))
                .ForMember(dest => dest.Nama, opts => opts.MapFrom(src => src.nama));
        }

        private void SetupPosBiayaMapping()
        {
            AutoMapper.Mapper.CreateMap<PosBiaya, PosBiayaModel>()
                        .ForMember(dest => dest.PosBiayaId, opts => opts.MapFrom(src => src.PosBiaya_Id))
                        .ForMember(dest => dest.IsPaid, opts => opts.MapFrom(src => src.IsPaid))
                        .ForMember(dest => dest.JTempo, opts => opts.MapFrom(src => src.JTempo))
                        .ForMember(dest => dest.NamaBiaya, opts => opts.MapFrom(src => src.Deskripsi))
                        .ForMember(dest => dest.NilaiBiaya, opts => opts.MapFrom(src => src.Nilai))
                        .ForMember(dest => dest.Potongan, opts => opts.MapFrom(src => src.NilaiPotongan));

            AutoMapper.Mapper.CreateMap<PosBiayaModel, PosBiaya>()
                        .ForMember(dest => dest.PosBiaya_Id, opts => opts.MapFrom(src => src.PosBiayaId))
                        .ForMember(dest => dest.Biaya_Id, opts => opts.MapFrom(src => src.RefBiayaId))
                        .ForMember(dest => dest.Deskripsi, opts => opts.MapFrom(src => src.NamaBiaya))
                        .ForMember(dest => dest.Nilai, opts => opts.MapFrom(src => src.NilaiBiaya))
                        .ForMember(dest => dest.NilaiPotongan, opts => opts.MapFrom(src => src.Potongan))
                        .ForMember(dest => dest.JTempo, opts => opts.MapFrom(src => src.JTempo))
                        .ForMember(dest => dest.IsPaid, opts => opts.MapFrom(src => src.IsPaid));
        }

        private void SetupSiswaMapping()
        {
            AutoMapper.Mapper.CreateMap<RefSiswa, SiswaModel>()
                        .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.siswa_id))
                        .ForMember(dest => dest.Code, opts => opts.MapFrom(src => src.siswa_code))
                        .ForMember(dest => dest.Nama, opts => opts.MapFrom(src => src.nama))
                        .ForMember(dest => dest.Alamat, opts => opts.MapFrom(src => src.Alamat));                        
        }

        private void SetupUserMapping()
        {
            AutoMapper.Mapper.CreateMap<SysUser, UserModel>();                        
            AutoMapper.Mapper.CreateMap<UserModel, SysUser>();
        }

        private void SetupKonfigurasiBiayaMapping()
        {            
            AutoMapper.Mapper.CreateMap<RefBiaya, RefBiayaModel>()
                        .ForMember(dest => dest.Biaya_id, opts => opts.MapFrom(src => src.biaya_id))
                        .ForMember(dest => dest.Code, opts => opts.MapFrom(src => src.code))
                        .ForMember(dest => dest.Repetisi, opts => opts.MapFrom(src => src.repetisi))
                        .ForMember(dest => dest.Jt_tanggal, opts => opts.MapFrom(src => src.jt_tanggal))
                        .ForMember(dest => dest.Jt_bulan, opts => opts.MapFrom(src => src.jt_bulan))
                        .ForMember(dest => dest.Aktif, opts => opts.MapFrom(src => src.aktif))
                        .ForMember(dest => dest.Mulai_tanggal, opts => opts.MapFrom(src => src.mulai_tanggal))
                        .ForMember(dest => dest.Nama, opts => opts.MapFrom(src => src.nama))
                        .ForMember(dest => dest.IsTambahan, opts => opts.MapFrom(src => src.noposbiaya))
                        .ForMember(dest => dest.BisaDiCicil, opts => opts.MapFrom(src => src.bisacicil));

            AutoMapper.Mapper.CreateMap<RefBiayaModel, RefBiaya>()
                        .ForMember(dest => dest.biaya_id, opts => opts.MapFrom(src => src.Biaya_id))
                        .ForMember(dest => dest.nama, opts => opts.MapFrom(src => src.Nama))
                        .ForMember(dest => dest.code, opts => opts.MapFrom(src => src.Code))
                        .ForMember(dest => dest.repetisi, opts => opts.MapFrom(src => src.Repetisi))
                        .ForMember(dest => dest.jt_tanggal, opts => opts.MapFrom(src => src.Jt_tanggal))
                        .ForMember(dest => dest.jt_bulan, opts => opts.MapFrom(src => src.Jt_bulan))
                        .ForMember(dest => dest.aktif, opts => opts.MapFrom(src => src.Aktif))
                        .ForMember(dest => dest.mulai_tanggal, opts => opts.MapFrom(src => src.Mulai_tanggal))
                        .ForMember(dest => dest.noposbiaya, opts => opts.MapFrom(src => src.IsTambahan))
                        .ForMember(dest => dest.bisacicil, opts => opts.MapFrom(src => src.BisaDiCicil));

            AutoMapper.Mapper.CreateMap<RefGrupBiaya, GrupBiayaModel>()
                        .ForMember(dest => dest.GrupBiayaId, opts => opts.MapFrom(src => src.Grupbiaya_id))
                        .ForMember(dest => dest.Nama, opts => opts.MapFrom(src => src.nama))
                        .ForMember(dest => dest.Code, opts => opts.MapFrom(src => src.code))
                        .ForMember(dest => dest.Aktif, opts => opts.MapFrom(src => src.active));

            AutoMapper.Mapper.CreateMap<GrupBiayaModel, RefGrupBiaya>()
                        .ForMember(dest => dest.Grupbiaya_id, opts => opts.MapFrom(src => src.GrupBiayaId))
                        .ForMember(dest => dest.nama, opts => opts.MapFrom(src => src.Nama))
                        .ForMember(dest => dest.code, opts => opts.MapFrom(src => src.Code))
                        .ForMember(dest => dest.active, opts => opts.MapFrom(src => src.Aktif));
        }

        private void SetupRefPayableMapping()
        {
            AutoMapper.Mapper.CreateMap<RefPayable, RefPayableModel>()
                        .ForMember(dest => dest.RefpayableId, opts => opts.MapFrom(src => src.refpayable_id))
                        .ForMember(dest => dest.Nama, opts => opts.MapFrom(src => src.name))
                        .ForMember(dest => dest.Code, opts => opts.MapFrom(src => src.code))
                        .ForMember(dest => dest.Aktif, opts => opts.MapFrom(src => src.aktif));

            AutoMapper.Mapper.CreateMap<RefPayableModel, RefPayable>()
                        .ForMember(dest => dest.refpayable_id, opts => opts.MapFrom(src => src.RefpayableId))
                        .ForMember(dest => dest.name, opts => opts.MapFrom(src => src.Nama))
                        .ForMember(dest => dest.code, opts => opts.MapFrom(src => src.Code))
                        .ForMember(dest => dest.aktif, opts => opts.MapFrom(src => src.Aktif));
        }
    }
}
