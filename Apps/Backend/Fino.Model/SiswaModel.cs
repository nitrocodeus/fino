﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class SiswaModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Nama { get; set; }

        public string Alamat { get; set; }

        public DateTime TanggalLahir { get; set; }

        public int IdKelas { get; set; }

        public string Kelas { get; set; }

        public int IdTahunAjaran { get; set; }

        public string TahunAjaran { get; set; }

        public string NoRekening { get; set; }

        public bool JenisKelamin { get; set; }
    }
}
