﻿using Fino.Lib.Core;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Fino.Model
{
    public class PengelolaanNilaiBiayaModel : BaseModel
    {
        public static readonly string OptionPropertyName = "Option";
        public static readonly string NilaiPropertyName = "Nilai";

        public int BiayaNilaiOption_Id { get; set; }
        public int Biaya_Id { get; set; }

        private int _Option;
        [Display(Name = "Option")]
        [Required]
        public int Option
        {
            get
            {
                return _Option;
            }

            set
            {
                _Option = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(OptionPropertyName));
                ValidateProperty(value, OptionPropertyName);
            }
        }

        private int _Nilai;
        [Display(Name = "Nilai")]
        [Required]
        public int Nilai
        {
            get
            {
                return _Nilai;
            }

            set
            {
                _Nilai = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NilaiPropertyName));
                ValidateProperty(value, NilaiPropertyName);
            }
        }
    }
}
