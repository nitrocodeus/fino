﻿using Fino.Lib.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Fino.Model;

namespace Fino.View
{
    public interface ISiswaExplorerView : IView
    {
        List<SiswaExplorerModel> SiswaDatasource { set; }
        List<string> KelasDatasource { set; }

        void ReloadDataEvent();
        void InitializeDataGridView();
    }
}
