﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IDaftarUlangSiswaView: IView
    {
        event Action SaveButtonClicked;
        event Action SaveNextButtonClicked;
        event Action NewDataButtonClicked;
        event Action PotonganButtonClicked;
        event Action SearchSiswaButtonClicked;
        event Action SiswaPindahanCheckedChanged;
        event Action CloseButtonClicked;
        event Action CariSiswaTextKeypressed;
        event Action SelectingSiswaDone;

        int SiswaId { get; set; }
        string NoInduk { get; set; }
        string NamaSiswa { get; set; }
        bool JenisKelamin { get; set; }
        int KelasId { get; set; }
        DateTime MulaiTanggal { get; set; }
        int TahunAjaranId { get; set; }
        string TahunAjaranName { get; set; }
        bool IsPindahan { get; set; }
        int OpsiTingkat { get; set; }
        bool IsSearchMode { get; set; }

        DaftarUlangSiswaModel ViewModel { get; set; }
        object KelasDataSource { get; set; }
        object OptionDataSource { get; set; }
        string Keyword { get; }

        IPencarianSiswaView PencarianSiswaView { get; }

        void SetBinding();
        void ClearView();
        void ChangeEditable(bool enabled);
        void ShowErrorMessage(string message, string caption);
        void ShowInformation(string message, string caption);
        void ShowSiswaToEdit(int pSiswaId);
        void SetSearchMode();
        void SetEntryMode();
        void SetViewMode();
        void SetOptionEnable(bool enabled);
    }
}
