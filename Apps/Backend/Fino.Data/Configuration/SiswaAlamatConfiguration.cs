﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class SiswaAlamatConfiguration
    {
        internal static void ConfigureSiswaAlamat(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SiswaAlamat>().HasKey(e => e.siswa_id);
            modelBuilder.Entity<SiswaAlamat>().Property(e => e.alamat).HasMaxLength(50);
            modelBuilder.Entity<SiswaAlamat>().Property(e => e.kecamatan).HasMaxLength(50);
            modelBuilder.Entity<SiswaAlamat>().Property(e => e.kota).HasMaxLength(50);
            modelBuilder.Entity<SiswaAlamat>().Property(e => e.propinsi).HasMaxLength(50);

            modelBuilder.Entity<SiswaAlamat>()
                .HasRequired(e => e.Siswas)
                .WithOptional(e => e.Alamat);
        }
    }
}
