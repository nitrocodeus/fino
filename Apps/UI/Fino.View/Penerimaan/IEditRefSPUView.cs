﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IEditRefSPUView : IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;
        event Action CboKelasSelectedChanged;
        event Action RbKelasSelectedChanged;

        RefSumberPUModel ViewModel { get; set; }
        IPenerimaanUmumView PenerimaanUmumView { get; set; }
        object RefKelasDataSource { get; set; }

        string Nama { get; set; }
        string Alamat { get; set; }
        string KelasCode { get; }
        string KelasId { get; set; }
        bool IsKelas { get; }

        void SetBinding();
        void KelasSelectedStateChanged(bool p_Enabled);
        void SetEnableJenisSumber();
    }
}
