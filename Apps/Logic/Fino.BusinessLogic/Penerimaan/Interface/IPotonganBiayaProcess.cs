﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public interface IPotonganBiayaProcess
    {
        IPotonganRepository PotonganRepo { get; set; }
        IRefSiswaRepository RefSiswaRepo { get; set; }
        ProcessResult ManagePotonganBiaya(List<PotonganBiayaDetailModel> model);
        ProcessResult GetSiswaByNoInduk(string pNoInduk);
        ProcessResult GetBiayaList();
    }
}
