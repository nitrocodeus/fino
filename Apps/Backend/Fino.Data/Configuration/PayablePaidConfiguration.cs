﻿using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class PayablePaidConfiguration
    {
        internal static void ConfigurePayablePaid(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PayablePaid>().HasKey(e => e.payable_id);
            modelBuilder.Entity<PayablePaid>()
                .HasRequired(e => e.payable)
                .WithOptional(e => e.payablepaid);
        }
    }
}
