﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Lib.Win32
{
    public interface IView
    {
        event EventHandler Load;

        void Show();
        void Hide();
        void Close();
        bool Focus();

        DialogResult ShowDialog();
        DialogResult ShowDialog(IWin32Window owner);

        Form MdiParent { get; set; }
    }
}
