﻿using Fino.BusinessLogic;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Model;
using System.Data;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class SiswaExplorerPresenter : BasePresenter
    {
        private readonly ISiswaExplorerProcess _siswaExplorerProcess;
        private readonly ISiswaExplorerView _siswaExplorerView;

        public SiswaExplorerPresenter(ISiswaExplorerView p_View, ISiswaExplorerProcess p_Process) : base(p_View)
        {
            _siswaExplorerProcess = p_Process;
            _siswaExplorerView = p_View;
            _siswaExplorerView.Load += _siswaExplorerView_Load;
        }

        void _siswaExplorerView_Load(object sender, EventArgs e)
        {
            //_siswaExplorerView.InitializeDataGridView();
            _siswaExplorerView.KelasDatasource = InitializeComboBox();
            GetSiswaList();
        }
        private List<string> InitializeComboBox()
        {
            List<string> kelasList = new List<string>();

            kelasList.Add("Semua Kelas");
            var result = _siswaExplorerProcess.GetAllRefKelas();
            if (result.IsSucess)
            {
                foreach (var kelas in result.DataResult as List<RefKelasModel>)
                {
                    kelasList.Add(kelas.Nama);
                }
            }

            return kelasList;
        }

        private void GetSiswaList()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                  () => _siswaExplorerProcess.GetAllSiswa()),
                  AppResource.MSG_SISWAEXPLORER);

            if (result.IsSucess)
            {
                _siswaExplorerView.SiswaDatasource = (result.DataResult as List<SiswaExplorerModel>);
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        
    }
}
