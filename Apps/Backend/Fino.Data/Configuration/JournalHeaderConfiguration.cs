﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class JournalHeaderConfiguration
    {
        internal static void ConfigureJournalHeader(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JournalHeader>().HasKey(e => e.journal_id);
            modelBuilder.Entity<JournalHeader>().Property(e => e.journal_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<JournalHeader>().Property(e => e.journal_name).HasMaxLength(500);

            modelBuilder.Entity<JournalHeader>()
                    .HasRequired(e => e.AccountPeriod)
                    .WithMany(e => e.JournalHeaders)
                    .HasForeignKey(e => e.period_id);
        }
    }
}
