﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class KonversiCicilanPosBiaya
    {
        public int source_posbiaya_id { get; set; }
        public int destination_posbiaya_id { get; set; }

        public virtual KonversiCicilan Konversi { get; set; }
        public virtual PosBiaya DestinationPosBiaya { get; set; }
    }
}
