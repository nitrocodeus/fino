; installer.nsi
;
; Script installer for fino application

;--------------------------------

!include MUI2.nsh

; The name of the installer
Name "installer"

; The file to write
OutFile "..\FinoInstaller_${VERSION}.exe"

; The default installation directory
InstallDir $PROGRAMFILES\Fino

RequestExecutionLevel admin

;--------------------------------

; Pages

;Page directory
;Page instfiles

!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP orange.bmp
!define MUI_WELCOMEFINISHPAGE_BITMAP arrow.bmp
!define MUI_WELCOMEPAGE_TEXT "Anda sedang menginstall aplikasi pengendalian keuangan SDIT Baiturrahman Jakarta Utara"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

;UninstPage uninstConfirm
;UninstPage instfiles
!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_COMPONENTS
!insertmacro MUI_UNPAGE_DIRECTORY
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

;--------------------------------

; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File /r ..\Apps\UI\Fino.Cient\bin\Release\*.*  
  
  WriteUninstaller "uninstall.exe"
  
SectionEnd ; end the section

Section "SQLServerClearTypes" SEC01  
  File "SQLSysClrTypes.msi"
  ExecWait 'msiexec /i "$INSTDIR\SQLSysClrTypes.msi" /passive '
SectionEnd

Section "ReportViewer" SEC02
  File "ReportViewer.msi"
  ExecWait 'msiexec /i "$INSTDIR\ReportViewer.msi" /passive '
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Fino"
  CreateShortCut "$SMPROGRAMS\Fino\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\Fino\Fino.lnk" "$INSTDIR\Fino.Cient.exe" "" "$INSTDIR\Fino.Cient.exe" 0
  
SectionEnd

; Uninstaller

Section "Uninstall"

  ; Remove files and uninstaller
  Delete $INSTDIR\*.*  

  ; Remove directories used
  RMDir "$INSTDIR"

  Delete "$SMPROGRAMS\Fino\Uninstall.lnk"
  Delete "$SMPROGRAMS\Fino\Fino.lnk"
  RMDir  "$SMPROGRAMS\Fino"  

SectionEnd
