﻿using Fino.Cient.Startup;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class UserManagementView : BaseForm, IUserManagementView
    {
        #region Events

        public event Action GvUserManagementDoubleClick;
        public event Action BtnTambahClick;
        public event Action BtnHapusClick;
        public event Action BtnTutupClick;
        public event Action ReloadData;

        #endregion Events

        #region Constructors

        public UserManagementView()
        {
            InitializeComponent();

            this.btnTambah.Click += btnTambah_Click;
            this.btnHapus.Click += btnHapus_Click;
            this.btnTutup.Click += btnTutup_Click;
            this.gvUserManagement.DoubleClick += gvUserManagement_DoubleClick;
        }

        #endregion Constructors

        #region Public Methods

        public void ReloadDataEvent()
        {
            if (ReloadData != null)
            {
                ReloadData();
            }
        }

        #endregion Public Methods

        #region Control Events

        private void gvUserManagement_DoubleClick(object sender, EventArgs e)
        {
            if (GvUserManagementDoubleClick != null)
            {
                GvUserManagementDoubleClick();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (BtnTutupClick != null)
            {
                BtnTutupClick();
            }
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            if (BtnHapusClick != null)
            {
                BtnHapusClick();
            }
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            if (BtnTambahClick != null)
            {
                BtnTambahClick();
            }
        }

        #endregion Control Events

        #region IUserManagementModel

        public void SetData(List<UserManagementModel> listUserManagement)
        {
            gvUserManagement.DataSource = listUserManagement;
        }

        public void RefreshDataGrid()
        {
            gvUserManagement.Refresh();
        }

        public UserManagementModel GetSelectedUser()
        {
            UserManagementModel result = new UserManagementModel();

            if (gvUserManagement.SelectedCells.Count > 0)
            {
                result = (UserManagementModel)gvUserManagement.Rows[gvUserManagement.SelectedCells[0].RowIndex].DataBoundItem;
            }

            return result;
        }

        public void ShowEditUserView()
        {
            if (gvUserManagement.SelectedCells.Count > 0)
            {
                ViewLocator.Instance.EditUserManagementView.UserManagementView = this;
                ViewLocator.Instance.EditUserManagementView.ShowForm(GetSelectedUser());
            }
        }

        public void ShowTambahUserView()
        {
            ViewLocator.Instance.EditUserManagementView.UserManagementView = this;
            ViewLocator.Instance.EditUserManagementView.ShowForm(null);
        }

        public void ShowErrorMessage(string message, string caption)
        {
            WinApi.ShowErrorMessage(message, caption);
        }

        #endregion IUserManagementModel
    }
}
