﻿namespace Fino.Cient
{
    partial class EditAccountClassSettingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditAccountClassSettingView));
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.lblAccountName = new System.Windows.Forms.Label();
            this.cboAccountId = new System.Windows.Forms.ComboBox();
            this.cboParamId = new System.Windows.Forms.ComboBox();
            this.labelAccClassId = new System.Windows.Forms.Label();
            this.txtAccSettingId = new System.Windows.Forms.TextBox();
            this.txtAccClassSettingName = new System.Windows.Forms.TextBox();
            this.lblParam = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(257, 234);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(138, 35);
            this.btnSimpan.TabIndex = 3;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(407, 234);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(138, 35);
            this.btnBatal.TabIndex = 4;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            // 
            // lblAccountName
            // 
            this.lblAccountName.AutoSize = true;
            this.lblAccountName.Location = new System.Drawing.Point(27, 83);
            this.lblAccountName.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblAccountName.Name = "lblAccountName";
            this.lblAccountName.Size = new System.Drawing.Size(115, 20);
            this.lblAccountName.TabIndex = 25;
            this.lblAccountName.Text = "Nama akun:";
            // 
            // cboAccountId
            // 
            this.cboAccountId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboAccountId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAccountId.FormattingEnabled = true;
            this.cboAccountId.Location = new System.Drawing.Point(31, 107);
            this.cboAccountId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboAccountId.Name = "cboAccountId";
            this.cboAccountId.Size = new System.Drawing.Size(514, 28);
            this.cboAccountId.TabIndex = 1;
            // 
            // cboParamId
            // 
            this.cboParamId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboParamId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboParamId.FormattingEnabled = true;
            this.cboParamId.Location = new System.Drawing.Point(31, 181);
            this.cboParamId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboParamId.Name = "cboParamId";
            this.cboParamId.Size = new System.Drawing.Size(514, 28);
            this.cboParamId.TabIndex = 2;
            // 
            // labelAccClassId
            // 
            this.labelAccClassId.AutoSize = true;
            this.labelAccClassId.Location = new System.Drawing.Point(27, 16);
            this.labelAccClassId.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelAccClassId.Name = "labelAccClassId";
            this.labelAccClassId.Size = new System.Drawing.Size(223, 20);
            this.labelAccClassId.TabIndex = 31;
            this.labelAccClassId.Text = "Tipe setting akun jurnal:";
            // 
            // txtAccSettingId
            // 
            this.txtAccSettingId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccSettingId.Location = new System.Drawing.Point(31, 40);
            this.txtAccSettingId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAccSettingId.Name = "txtAccSettingId";
            this.txtAccSettingId.ReadOnly = true;
            this.txtAccSettingId.Size = new System.Drawing.Size(514, 28);
            this.txtAccSettingId.TabIndex = 32;
            this.txtAccSettingId.TabStop = false;
            // 
            // txtAccClassSettingName
            // 
            this.txtAccClassSettingName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccClassSettingName.Location = new System.Drawing.Point(31, 40);
            this.txtAccClassSettingName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAccClassSettingName.Name = "txtAccClassSettingName";
            this.txtAccClassSettingName.ReadOnly = true;
            this.txtAccClassSettingName.Size = new System.Drawing.Size(514, 28);
            this.txtAccClassSettingName.TabIndex = 0;
            // 
            // lblParam
            // 
            this.lblParam.AutoSize = true;
            this.lblParam.Location = new System.Drawing.Point(27, 157);
            this.lblParam.Name = "lblParam";
            this.lblParam.Size = new System.Drawing.Size(71, 20);
            this.lblParam.TabIndex = 33;
            this.lblParam.Text = "Param:";
            // 
            // EditAccountClassSettingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 285);
            this.Controls.Add(this.lblParam);
            this.Controls.Add(this.txtAccClassSettingName);
            this.Controls.Add(this.txtAccSettingId);
            this.Controls.Add(this.labelAccClassId);
            this.Controls.Add(this.cboParamId);
            this.Controls.Add(this.cboAccountId);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.lblAccountName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "EditAccountClassSettingView";
            this.Text = "Ubah setting akun jurnal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Label lblAccountName;
        private System.Windows.Forms.ComboBox cboAccountId;
        private System.Windows.Forms.ComboBox cboParamId;
        private System.Windows.Forms.Label labelAccClassId;
        private System.Windows.Forms.TextBox txtAccSettingId;
        private System.Windows.Forms.TextBox txtAccClassSettingName;
        private System.Windows.Forms.Label lblParam;

    }
}