﻿using Fino.Model;
using System.Collections.Generic;

namespace Fino.Repository
{
    public interface IRefAccountRepository : IRepository
    {
        void UpdateRefAccount(RefAccountModel entity);
        RefAccountModel GetRefAccount(int account_id);
        List<RefAccountModel> GetAllRefAccount();
        void DeleteRefAccount(RefAccountModel entity);
    }
}
