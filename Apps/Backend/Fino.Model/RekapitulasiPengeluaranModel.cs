﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class RekapitulasiPengeluaranModel
    {
        public int refpayable_id { get; set; }
        public string payable_name { get; set; }
        public double payable_value {get;set;}
    }
}
