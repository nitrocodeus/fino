﻿using System.Collections.Generic;

namespace Fino.Datalib.Entity
{
    public class RefAccount
    {
        public int account_id { get; set; }
        public string account_code { get; set; }
        public string account_name { get; set; }

        public virtual ICollection<GroupAccount> GroupOfAccounts { get; set; }
        public virtual ICollection<JournalDetail> JournalDetails { get; set; }
        public virtual ICollection<GLAccount> GLAccounts { get; set; }
        public virtual AccountClassSetting AccountBiayaSetting { get; set; }
    }
}
