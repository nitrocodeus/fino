﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public class SiswaExplorerProcess : ISiswaExplorerProcess
    {
        public IRefSiswaRepository repo {get;set;}
        public IRefKelasRepository RefKelasRepository { get; set; }

        public ProcessResult GetAllRefKelas()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var allKelas = this.RefKelasRepository.SelectAllKelas();

                List<RefKelasModel> dataResult = new List<RefKelasModel>();

                foreach (var kelas in allKelas)
                {
                    dataResult.Add(new RefKelasModel
                    {
                        KelasId = kelas.kelas_id,
                        KelasCode = kelas.kelas_code,
                        Nama = kelas.nama,
                        Tingkat = kelas.tingkat
                    });
                }

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            RefKelasRepository.DisposeDBContext();

            return result;
        }

        public ProcessResult GetAllSiswa()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var dataResult = this.repo.GetAllRefSiswaForSiswaExplorer();

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
            
        }
    }
}
