﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fino.Model;
using System.Collections.Generic;
using Moq;
using Fino.View;
using Fino.Presenter;
using Fino.BusinessLogic;
using Fino.Lib.Core;

namespace Test.Fino.UI
{
    [TestClass]
    public class KonfigurasiBiaya_TestUnit
    {
        public List<RefBiayaModel> _validDataKonfigurasiBiaya;

        private Mock<IRefBiayaView> _konfigurasiBiayaViewMock;
        private Mock<IEditRefBiayaView> _editKonfigurasiBiayaViewMock;
        private Mock<IKonfigurasiBiayaProcess> _konfigurasiBiayaProcessMock;

        [TestInitialize]
        public void InitTest()
        {
            _validDataKonfigurasiBiaya = new List<RefBiayaModel>();            
            PopulateValidKonfigurasiBiaya();

            _konfigurasiBiayaProcessMock = new Mock<IKonfigurasiBiayaProcess>();
            _konfigurasiBiayaViewMock = new Mock<IRefBiayaView>();
            _editKonfigurasiBiayaViewMock = new Mock<IEditRefBiayaView>();
        }

        private void PopulateValidKonfigurasiBiaya()
        {
            for (int i = 0; i < 100; i++)
            {
                RefBiayaModel data = new RefBiayaModel();
                data.Biaya_id = i;
                data.Nama = string.Format("TestNama{0}", i);
                data.Code = string.Format("TestCode{0}", i);
                data.Jt_bulan = new Random().Next(13);
                data.Jt_tanggal = new Random().Next(30);
                data.Aktif = true;
                data.Repetisi = new Random().Next(4);
                data.Mulai_tanggal = DateTime.Today;

                _validDataKonfigurasiBiaya.Add(data);
            }
        }

        [TestMethod]
        public void KonfigurasiBiayaView_FormLoadWillLoadAllKonfigurasiBiayaData()
        {
            ProcessResult result = new ProcessResult();

            result.IsSucess = true;
            result.DataResult = _validDataKonfigurasiBiaya;

            _konfigurasiBiayaProcessMock.Setup(process => process.GetRefBiaya()).Returns(result);

            RefBiayaPresenter presenter = new RefBiayaPresenter(
                _konfigurasiBiayaViewMock.Object, _konfigurasiBiayaProcessMock.Object);

            // Act
            _konfigurasiBiayaViewMock.Raise(view => view.Load += null,new EventArgs());

            // Assert
            _konfigurasiBiayaViewMock.Verify(view => view.SetData(_validDataKonfigurasiBiaya), "Data not set");
            _konfigurasiBiayaViewMock.Verify(view => view.RefreshDataGrid(), "Datagrid not refreshed");
        }

        [TestMethod]
        public void KonfigurasiBiayaView_DoubleClickOnDatagridCellWillCallEditKonfigurasiBiayaForm()
        {
            ProcessResult result = new ProcessResult();

            result.IsSucess = true;
            result.DataResult = _validDataKonfigurasiBiaya;

            _konfigurasiBiayaProcessMock.Setup(process => process.GetRefBiaya()).Returns(result);

            RefBiayaPresenter presenter = new RefBiayaPresenter(
                _konfigurasiBiayaViewMock.Object, _konfigurasiBiayaProcessMock.Object);

            // Act
            _konfigurasiBiayaViewMock.Raise(view => view.GvRefBiayaDoubleClick += null);

            // Assert
            _konfigurasiBiayaViewMock.Verify(view => view.ShowEditRefBiayaView(), "Edit Konfigurasi Biaya Form is not called");            
        }

        [TestMethod]
        public void KonfigurasiBiayaView_ClickOnCloseButtonWillCloseTheForm()
        {
            ProcessResult result = new ProcessResult();

            result.IsSucess = true;
            result.DataResult = _validDataKonfigurasiBiaya;

            _konfigurasiBiayaProcessMock.Setup(process => process.GetRefBiaya()).Returns(result);

            RefBiayaPresenter presenter = new RefBiayaPresenter(
                _konfigurasiBiayaViewMock.Object, _konfigurasiBiayaProcessMock.Object);

            // Act
            _konfigurasiBiayaViewMock.Raise(view => view.BtnTutupClick += null);

            // Assert
            _konfigurasiBiayaViewMock.Verify(view => view.Close(), "Close Form is not called");
        }

        [TestMethod]
        public void KonfigurasiBiayaView_ClickOnAddButtonWillCallTheEditKonfigurasiBiayaForm()
        {
            ProcessResult result = new ProcessResult();

            result.IsSucess = true;
            result.DataResult = _validDataKonfigurasiBiaya;

            _konfigurasiBiayaProcessMock.Setup(process => process.GetRefBiaya()).Returns(result);

            RefBiayaPresenter presenter = new RefBiayaPresenter(
                _konfigurasiBiayaViewMock.Object, _konfigurasiBiayaProcessMock.Object);

            // Act
            _konfigurasiBiayaViewMock.Raise(view => view.BtnTambahClick += null);

            // Assert
            _konfigurasiBiayaViewMock.Verify(view => view.ShowTambahRefBiayaView(), "Edit Konfigurasi Biaya Form is not called");
        }

        [TestMethod]
        public void KonfigurasiBiayaView_ClickOnDeleteButtonWillDeleteTheSelectedDataAndThenReloadTheData()
        {
            ProcessResult result = new ProcessResult();

            result.IsSucess = true;
            result.DataResult = _validDataKonfigurasiBiaya;

            ProcessResult deleteResult = new ProcessResult();

            deleteResult.IsSucess = true;            

            _konfigurasiBiayaProcessMock.Setup(process => process.GetRefBiaya()).Returns(result);
            _konfigurasiBiayaProcessMock.Setup(process => process.DeleteRefBiaya(It.IsAny<RefBiayaModel>())).Returns(deleteResult);

            RefBiayaPresenter presenter = new RefBiayaPresenter(
                _konfigurasiBiayaViewMock.Object, _konfigurasiBiayaProcessMock.Object);

            // Act
            _konfigurasiBiayaViewMock.Raise(view => view.BtnHapusClick += null);

            // Assert
            _konfigurasiBiayaViewMock.Verify(view => view.GetSelectedKonfigurasiBiaya(), "Edit Konfigurasi Biaya Form is not called");
            _konfigurasiBiayaViewMock.Verify(view => view.SetData(_validDataKonfigurasiBiaya), "Data not set");
            _konfigurasiBiayaViewMock.Verify(view => view.RefreshDataGrid(), "Datagrid not refreshed");
        }
    }
}
