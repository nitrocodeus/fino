﻿using System;
using System.ComponentModel;
using Fino.Lib.Core;

namespace Fino.Model
{
    public class PenerimaanUmumModel : BaseModel
    {
        public static readonly string PenerimaanUmumIdPropertyName = "PenerimaanUmumId";
        public static readonly string SelectedPropertyName = "Selected";
        public static readonly string NamaBiayaPropertyName = "NamaBiaya";
        public static readonly string JTempoPropertyName = "JTempo";
        public static readonly string NilaiBiayaPropertyName = "NilaiBiaya";
        public static readonly string IsPaidPropertyName = "IsPaid";
        public static readonly string SumberPenerimaanIdPropertyName = "SumberPenerimaanUmumId";
        public static readonly string SumberPenerimaanNamaPropertyName = "SumberPenerimaanUmumNama";
        public static readonly string RefBiayaIdPropertyName = "RefBiayaId";
        public static readonly string DeskripsiPropertyName = "Deskripsi";

        private int _penerimaanUmumId;
        private bool _selected;
        private string _namaBiaya;
        private double _nilaiBiaya;
        private bool? _isPaid;
        private bool? _noPosBiaya;
        private int _refBiayaId;
        private int _sumberPenerimaanUmumId;
        private string _sumberPenerimaanUmumNama;
        private string _deskripsi;

        public DateTime? DatePaid { get; set; }
        public int Status_Id { get; set; }
        public DateTime? Status_Date { get; set; }

        public int RefBiayaId
        {
            get { return _refBiayaId; }
            set { _refBiayaId = value; }
        }
        public bool? NoPosBiaya
        {
            get { return _noPosBiaya; }
            set { _noPosBiaya = value; }
        }

        public int SumberPenerimaanUmumId
        {
            get
            {
                return _sumberPenerimaanUmumId;
            }
            set
            {
                _sumberPenerimaanUmumId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(SumberPenerimaanIdPropertyName));
            }
        }

        public string SumberPenerimaanUmumNama
        {
            get
            {
                return _sumberPenerimaanUmumNama;
            }
            set
            {
                _sumberPenerimaanUmumNama = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(SumberPenerimaanNamaPropertyName));
            }
        }

        public string Deskripsi
        {
            get
            {
                return _deskripsi;
            }
            set
            {
                _deskripsi = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(DeskripsiPropertyName));
            }
        }

        public int PenerimaanUmumId
        {
            get
            {
                return _penerimaanUmumId;
            }
            set
            {
                _penerimaanUmumId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PenerimaanUmumIdPropertyName));
            }
        }

        public bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                _selected = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(SelectedPropertyName));
            }
        }

        public string NamaBiaya
        {
            get
            {
                return _namaBiaya;
            }
            set
            {
                _namaBiaya = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamaBiayaPropertyName));
            }
        }

        public double NilaiBiaya
        {
            get
            {
                return _nilaiBiaya;
            }
            set
            {
                _nilaiBiaya = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NilaiBiayaPropertyName));
            }
        }

        public bool? IsPaid
        {
            get
            {
                return _isPaid;
            }
            set
            {
                if (value != null) _isPaid = value;
                else _isPaid = false;

                InvokePropertyChanged(new PropertyChangedEventArgs(IsPaidPropertyName));
            }
        }
    }
}
