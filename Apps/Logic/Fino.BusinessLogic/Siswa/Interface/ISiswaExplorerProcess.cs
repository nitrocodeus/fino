﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;

namespace Fino.BusinessLogic
{
    public interface ISiswaExplorerProcess
    {
        ProcessResult GetAllSiswa();
        ProcessResult GetAllRefKelas();
    }
}
