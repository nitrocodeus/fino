﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;
using Fino.Model;

namespace Fino.Repository
{
    public interface IMutasiTabunganRepository
    {
        MutasiTabunganModel NewMutasiRekening(MutasiTabunganModel p_Model);
        MutasiTabunganModel GetRekeningByRekeningNo(string p_NoRek);
        List<LaporanSaldoModel> GetAllSaldoAsOn(DateTime p_Date);
        List<LaporanMutasiModel> GetAllMutasiAsOn(DateTime p_Date);
    }
}
