﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View.Siswa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IEditRefKelasView : IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;

        RefKelasModel SelectedRefKelas { get; }
        IRefKelasView RefKelasView { get; set; }
        object TingkatDataSource { get; set; }

        int KelasId { get; set; }
        string KelasCode { get; set; }
        string Nama { get; set; }
        int Tingkat { get; set; }

        void SetBinding();
        void ClearData();
    }
}
