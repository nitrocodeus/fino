﻿using System;
using System.Windows.Forms;
using Fino.Model;
using Fino.View;
using Fino.Lib.Win32;

namespace Fino.Cient
{
    public partial class DaftarSiswaBaruView : BaseForm, IDaftarSiswaBaruView
    {

        public event Action SaveButtonClicked;
        public event Action SaveNextButtonClicked;
        public event Action NewDataButtonClicked;
        public event Action PotonganButtonClicked;
        public event Action KateringButtonClicked;
        public event Action JemputanButtonClicked;
        public event Action CloseButtonClicked;
        public event Action PindahanCheckChanged;

        public DaftarSiswaBaruModel ViewModel { get; set; }
        public object KelasDataSource { get; set; }
        public object OptionDataSource { get; set; }

        public string NoInduk
        {
            get
            {
                return txtNoInduk.Text.Trim();
            }

            set
            {
                txtNoInduk.Text = value.Trim();
            }
        }
        public string NamaSiswa
        {
            get
            {
                return txtNamaSiswa.Text.Trim();
            }

            set
            {
                txtNamaSiswa.Text = value.Trim();
            }
        }

        public bool JenisKelamin
        {
            get
            {
                return rbLaki.Checked & !rbPerempuan.Checked;
            }

            set
            {
                rbLaki.Checked = value;
                rbPerempuan.Checked = !value;
            }
        }

        public int KelasId
        {
            get
            {
                return Int32.Parse(cboKelas.SelectedValue.ToString());
            }
            set
            {
                cboKelas.SelectedValue = value.ToString();
            }
        }


        public DateTime MulaiTanggal
        {
            get
            {
                return dtpMulaiTanggal.Value;
            }
            set
            {
                dtpMulaiTanggal.Value = value;
            }
        }

        int _TahunAjaranId;
        public int TahunAjaranId
        {
            get
            {
                return _TahunAjaranId;
            }
            set
            {
                _TahunAjaranId = value;
            }
        }

        public string TahunAjaranName
        {
            get
            {
                return txtTahunAjaran.Text.Trim();
            }
            set
            {
                txtTahunAjaran.Text = value;
            }
        }


        public bool IsPindahan
        {
            get
            {
                return chkPindahan.Checked;
            }
            set
            {
                chkPindahan.Checked = value;

            }
        }
        public DaftarSiswaBaruView()
        {
            InitializeComponent();


        }



        private void btnPotongan_Click(object sender, EventArgs e)
        {
            if (PotonganButtonClicked != null)
            {
                PotonganButtonClicked();
            }
        }

        private void btnJemputan_Click(object sender, EventArgs e)
        {
            if (JemputanButtonClicked != null)
            {
                JemputanButtonClicked();
            }
        }

        private void btnKatering_Click(object sender, EventArgs e)
        {
            if (KateringButtonClicked != null)
            {
                KateringButtonClicked();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
            {
                SaveButtonClicked();
            }
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveNextButtonClicked != null)
            {
                SaveNextButtonClicked();
            }
        }


        public void ClearView()
        {
            SetBinding();

        }

        public void SetOptionEnable(bool enabled)
        {
            cboOpsi.Enabled = enabled;
        }

        public void ShowErrorMessage(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowInformation(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void ChangeEditable(bool enabled)
        {
            foreach (var c in groupBox2.Controls)
            {
                var ctype = c.GetType();
                if (ctype.Equals(typeof(TextBox)))
                {
                    if (!((TextBox)c).Name.Equals("txtTahunAjaran"))
                    {
                        ((TextBox)c).ReadOnly = !enabled;
                    }
                }
                if (ctype.Equals(typeof(RadioButton)))
                {
                    ((RadioButton)c).Enabled = enabled;
                }
                if (ctype.Equals(typeof(ComboBox)))
                {
                    ((ComboBox)c).Enabled = enabled;
                }
                if (ctype.Equals(typeof(DateTimePicker)))
                {
                    ((DateTimePicker)c).Enabled = enabled;
                }
            }

            btnSaveNew.Enabled = enabled;
            btnSave.Enabled = enabled;
        }

        public void SetBinding()
        {
            try
            {
                txtNoInduk.DataBindings.Clear();
                txtNoInduk.DataBindings.Add("Text", ViewModel, DaftarSiswaBaruModel.CodePropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                txtNamaSiswa.DataBindings.Clear();
                txtNamaSiswa.DataBindings.Add("Text", ViewModel, DaftarSiswaBaruModel.NamaPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                cboKelas.DataSource = KelasDataSource;
                cboKelas.DisplayMember = "Value";
                cboKelas.ValueMember = "Id";
                cboKelas.DataBindings.Clear();
                cboKelas.DataBindings.Add("SelectedValue", ViewModel, DaftarSiswaBaruModel.KelasIdPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                rbLaki.DataBindings.Clear();
                rbLaki.DataBindings.Add("Checked", ViewModel, DaftarSiswaBaruModel.JKelaminPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                dtpMulaiTanggal.DataBindings.Clear();
                dtpMulaiTanggal.DataBindings.Add("Value", ViewModel, DaftarSiswaBaruModel.MulaiTanggalPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                TahunAjaranId = ViewModel.TahunAjaran_Id;

                txtTahunAjaran.DataBindings.Clear();
                txtTahunAjaran.DataBindings.Add("Text", ViewModel, DaftarSiswaBaruModel.TahunAjaranNamaPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                chkPindahan.DataBindings.Clear();
                chkPindahan.DataBindings.Add("Checked", ViewModel, DaftarUlangSiswaModel.IsPindahanPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                cboOpsi.DataSource = OptionDataSource;
                cboOpsi.DisplayMember = "Value";
                cboOpsi.ValueMember = "Id";
                cboOpsi.DataBindings.Clear();
                cboOpsi.DataBindings.Add("SelectedValue", ViewModel, DaftarUlangSiswaModel.OptionTingkat_IdPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message, "Error Binding");
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (NewDataButtonClicked != null)
            {
                NewDataButtonClicked();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        private void chkPindahan_CheckedChanged(object sender, EventArgs e)
        {
            if (PindahanCheckChanged != null)
            {
                PindahanCheckChanged();
            }
        }



    }
}
