﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public interface IPengelolaanNilaiBiayaProcess
    {
        IPotonganRepository PotonganRepo { get; set; }
        IBiayaNilaiOptionRepository BiayaNilaiRepo { get; set; }
        ProcessResult GetBiayaList();
        ProcessResult GetTingkatList();
        ProcessResult GetNilaiBiayaOption(int biaya_id, int option);
        ProcessResult UpdateNilaiBiayaOption(PengelolaanNilaiBiayaModel nilaiBiaya);
        ProcessResult DeleteNilaiBiayaOption(PengelolaanNilaiBiayaModel nilaiBiaya);
    }
}
