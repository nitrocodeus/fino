﻿namespace Fino.Cient
{
    partial class RefAccountView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefAccountView));
            this.gvKonfigurasiKodeAkun = new System.Windows.Forms.DataGridView();
            this.kodeAkunModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnTambah = new System.Windows.Forms.Button();
            this.btnTutup = new System.Windows.Forms.Button();
            this.btnHapus = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.accountidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvKonfigurasiKodeAkun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kodeAkunModelBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gvKonfigurasiKodeAkun
            // 
            this.gvKonfigurasiKodeAkun.AllowUserToAddRows = false;
            this.gvKonfigurasiKodeAkun.AllowUserToDeleteRows = false;
            this.gvKonfigurasiKodeAkun.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvKonfigurasiKodeAkun.AutoGenerateColumns = false;
            this.gvKonfigurasiKodeAkun.BackgroundColor = System.Drawing.Color.White;
            this.gvKonfigurasiKodeAkun.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvKonfigurasiKodeAkun.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.accountidDataGridViewTextBoxColumn,
            this.accountcodeDataGridViewTextBoxColumn,
            this.accountnameDataGridViewTextBoxColumn});
            this.gvKonfigurasiKodeAkun.DataSource = this.kodeAkunModelBindingSource;
            this.gvKonfigurasiKodeAkun.Location = new System.Drawing.Point(132, 11);
            this.gvKonfigurasiKodeAkun.Margin = new System.Windows.Forms.Padding(4);
            this.gvKonfigurasiKodeAkun.Name = "gvKonfigurasiKodeAkun";
            this.gvKonfigurasiKodeAkun.ReadOnly = true;
            this.gvKonfigurasiKodeAkun.RowHeadersVisible = false;
            this.gvKonfigurasiKodeAkun.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvKonfigurasiKodeAkun.Size = new System.Drawing.Size(494, 368);
            this.gvKonfigurasiKodeAkun.TabIndex = 5;
            // 
            // kodeAkunModelBindingSource
            // 
            this.kodeAkunModelBindingSource.DataSource = typeof(Fino.Model.RefAccountModel);
            // 
            // btnTambah
            // 
            this.btnTambah.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnTambah.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTambah.ForeColor = System.Drawing.Color.White;
            this.btnTambah.Location = new System.Drawing.Point(12, 14);
            this.btnTambah.Margin = new System.Windows.Forms.Padding(4);
            this.btnTambah.Name = "btnTambah";
            this.btnTambah.Size = new System.Drawing.Size(100, 28);
            this.btnTambah.TabIndex = 0;
            this.btnTambah.Text = "Tambah";
            this.btnTambah.UseVisualStyleBackColor = false;
            // 
            // btnTutup
            // 
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(12, 86);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(4);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(100, 28);
            this.btnTutup.TabIndex = 2;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            // 
            // btnHapus
            // 
            this.btnHapus.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnHapus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHapus.ForeColor = System.Drawing.Color.White;
            this.btnHapus.Location = new System.Drawing.Point(12, 50);
            this.btnHapus.Margin = new System.Windows.Forms.Padding(4);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(100, 28);
            this.btnHapus.TabIndex = 1;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnTambah);
            this.panel1.Controls.Add(this.btnTutup);
            this.panel1.Controls.Add(this.btnHapus);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(124, 390);
            this.panel1.TabIndex = 4;
            // 
            // accountidDataGridViewTextBoxColumn
            // 
            this.accountidDataGridViewTextBoxColumn.DataPropertyName = "Account_id";
            this.accountidDataGridViewTextBoxColumn.HeaderText = "Account_id";
            this.accountidDataGridViewTextBoxColumn.Name = "accountidDataGridViewTextBoxColumn";
            this.accountidDataGridViewTextBoxColumn.ReadOnly = true;
            this.accountidDataGridViewTextBoxColumn.Visible = false;
            // 
            // accountcodeDataGridViewTextBoxColumn
            // 
            this.accountcodeDataGridViewTextBoxColumn.DataPropertyName = "Account_code";
            this.accountcodeDataGridViewTextBoxColumn.HeaderText = "Kode";
            this.accountcodeDataGridViewTextBoxColumn.Name = "accountcodeDataGridViewTextBoxColumn";
            this.accountcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // accountnameDataGridViewTextBoxColumn
            // 
            this.accountnameDataGridViewTextBoxColumn.DataPropertyName = "Account_name";
            this.accountnameDataGridViewTextBoxColumn.HeaderText = "Nama";
            this.accountnameDataGridViewTextBoxColumn.Name = "accountnameDataGridViewTextBoxColumn";
            this.accountnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // RefAccountView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 390);
            this.Controls.Add(this.gvKonfigurasiKodeAkun);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RefAccountView";
            this.Text = "Kode Akun";
            ((System.ComponentModel.ISupportInitialize)(this.gvKonfigurasiKodeAkun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kodeAkunModelBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gvKonfigurasiKodeAkun;
        private System.Windows.Forms.BindingSource kodeAkunModelBindingSource;
        private System.Windows.Forms.Button btnTambah;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountnameDataGridViewTextBoxColumn;
    }
}