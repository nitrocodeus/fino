﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class TahunAjaranRepository : BaseRepository, ITahunAjaranRepository
    {
        public RefTahunAjaran GetValidTahunAjaran(int currentYear, int currentMonth)
        {
            RefTahunAjaran result = null;

            // Seed must be change to impelement semester
            var allTA = Context.RefTahunAjarans
                .Where(e => currentYear >= e.mulai_tahun
                    && currentYear <= e.hingga_tahun
                    && currentMonth >= e.mulai_bulan
                    && currentMonth <= e.hingga_bulan);

            foreach (var TA in allTA)
            {
                result = TA;
            }

            return result;
        }
    }
}
