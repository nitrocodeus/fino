﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IEditRefPayableView : IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;

        RefPayableModel SelectedRefPayable { get; }
        IRefPayableView RefPayableView { get; set; }

        int RefPayableId { get; set; }
        string Nama { get; set; }
        string Kode { get; set; }
        bool Aktif { get; set; }

        void SetBinding();
        void ClearData();        
    }
}
