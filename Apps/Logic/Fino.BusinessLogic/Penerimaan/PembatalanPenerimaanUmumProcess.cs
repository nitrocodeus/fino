﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class PembatalanPenerimaanUmumProcess : IPembatalanPenerimaanUmumProcess
    {
        public IPenerimaanUmumRepository PenerimaanUmumRepo { get; set; }
        public INotaPenerimaanUmumRepository NotaPenerimaanUmumRepo { get; set; }

        public ProcessResult GetPembatalanPenerimaanUmumList(DateTime p_FromDate, DateTime p_ToDate)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<PembatalanPenerimaanUmumModel> dataResult = PenerimaanUmumRepo.GetPembatalanPenerimaanUmumList(p_FromDate, p_ToDate);

                result.DataResult = dataResult.OrderBy(x => x.RefSumberPenerimaan_Name).ThenBy(x => x.DatePaid).ToList();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdatePenerimaanUmum(PenerimaanUmumModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                PenerimaanUmumRepo.UpdatePenerimaanUmum(entity);

                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult DeletePenerimaanUmum(PenerimaanUmumModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                PenerimaanUmumRepo.DeletePenerimaanUmum(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetPenerimaanUmum(int p_PenerimaanUmumId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = PenerimaanUmumRepo.GetPenerimaanUmum(p_PenerimaanUmumId);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult DeleteNotaPenerimaanUmum(int p_NotaPenerimaanUmumId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                NotaPenerimaanUmumRepo.DeleteNotaPenerimaanUmum(p_NotaPenerimaanUmumId);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
