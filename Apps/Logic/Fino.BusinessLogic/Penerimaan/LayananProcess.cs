﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public class LayananProcess : ILayananProcess
    {
        public ILayananRepository LayananRepo { get; set; }

        public ProcessResult GetAllLayanan()
        {            
            ProcessResult result = new ProcessResult();

            try
            {
                List<RefLayananModel> resultModel = GetLayanan(true);

                result.DataResult = resultModel;
                result.IsSucess = true;

            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllLayananAsValueList()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<RefLayananModel> resultModel = GetLayanan(true);
                List<ValueList> valueList = new List<ValueList>();

                foreach (RefLayananModel item in resultModel)
                {
                    ValueList vl = new ValueList();
                    vl.Id = item.Code;
                    vl.Value = item.Nama;
                    vl.Tag = item;

                    valueList.Add(vl);
                }

                result.DataResult = valueList;
                result.IsSucess = true;

            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        private List<RefLayananModel> GetLayanan(bool p_all)
        {
            List<RefLayananModel> resultModel = null;

            if (p_all)
            {
                List<RefLayanan> resultEntity = LayananRepo.GetAllLayanan();

                resultModel = resultEntity.Select(x => Mapper.Map<RefLayananModel>(x)).ToList();
            }

            return resultModel;
        }


        public ProcessResult SimpanDaftarLayanan(DaftarLayananModel p_model)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                // Check apakah sudah terdaftar
                bool checkResult = LayananRepo.CheckPendaftaranLayanan(p_model);

                if (!checkResult)
                {
                    // Simpan bila belum terdaftar
                    LayananRepo.SimpanDaftarLayanan(p_model);

                    result.IsSucess = true;
                }
                else
                {
                    result.IsSucess = false;
                    result.ProcessException = new Exception("Layanan sudah terdaftar");
                }

            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
