﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefTahunAjaranConfiguration
    {
        internal static void ConfigureRefTahunAjaran(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefTahunAjaran>().HasKey(e => e.tahunajaran_id);
            modelBuilder.Entity<RefTahunAjaran>().Property(e => e.tahunajaran_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
