﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core.Util
{
    public class EnumHelper
    {
        public object ConvertToValueValueDataSource<T>()
        {
            List<ValueList> result = new List<ValueList>();

            foreach (var enumValue in Enum.GetValues(typeof(T)))
            {
                result.Add(new Fino.Lib.Core.ValueList { Id = (int)enumValue, Value = (int)enumValue });
            }

            return result;
        }

        public object ConvertToValueStringDataSource<T>()
        {
            List<ValueList> result = new List<ValueList>();

            foreach (var enumValue in Enum.GetValues(typeof(T)))
            {
                result.Add(new Fino.Lib.Core.ValueList { Id = (int)enumValue, Value = enumValue.ToString() });
            }

            return result;
        }

        #region Singleton

        private static readonly Lazy<EnumHelper> lazy = new Lazy<EnumHelper>(() => new EnumHelper());

        public static EnumHelper Instance { get { return lazy.Value; } }

        private EnumHelper() { }

        #endregion Singleton
    }
}
