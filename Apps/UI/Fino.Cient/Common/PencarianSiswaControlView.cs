﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.Lib.Win32;
using Fino.Lib.Core;
using Fino.View;
using Fino.Model;

namespace Fino.Cient.Common
{
    public partial class PencarianSiswaControlView : UserControl, IPencarianSiswaView
    {
        BindingSource _bsSiswa;
        List<SiswaModel> _source;

        public event Action gvSiswaClicked;
        public event Action CariRaised;
        public event Action HideControl;

        public string StringSiswa { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<SiswaModel> gvSiswaSource 
        { 
            get
            {
                return _source;
            }
            set
            {
                _source = value;
                _bsSiswa.DataSource = _source;
                gvDataSiswa.DataSource = _bsSiswa;
            }
        }

        public SiswaModel SelectedSiswa 
        { 
            get
            {
                return _bsSiswa.Current as SiswaModel;
            }
        }        

        public PencarianSiswaControlView()
        {
            InitializeComponent();
            this.Font = new Font(Constants.FontName, Constants.FontSize);
            _bsSiswa = new BindingSource();
        }

        public void Close()
        {
            this.Hide();
        }

        private void gvDataSiswa_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        
        public void Cari()
        {
            if (CariRaised != null)
            {
                CariRaised();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            if (HideControl != null)
            {
                HideControl();
            }
        }

        public void RaiseHideControl()
        {
            if (HideControl != null)
            {
                HideControl();
            }
        }

        private void gvDataSiswa_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (gvSiswaClicked != null && e.RowIndex > -1)
            {
                gvSiswaClicked();
            }
        }

        public void ClearSelected()
        {
            if (_bsSiswa.Current != null)
            {
                _bsSiswa.RemoveCurrent();
            }
        }


        public DialogResult ShowDialog()
        {
            throw new NotImplementedException();
        }

        public DialogResult ShowDialog(IWin32Window owner)
        {
            throw new NotImplementedException();
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Form MdiParent
        {
            get;
            set;
        }
    }
}
