﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View.Akuntansi
{
    public interface IEditAccountClassSettingView: IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;

        AccountClassSettingModel SelectedAccountClassSetting { get; }
        IAccountClassSettingView AccountClassSettingView { get; set; }

        int AccClassId { get; set; }
        int AccountId { get; set; }
        int ParamId { get; set; }
        object AccountIdDataSource { get; set; }
        object ParamIdDataSource { get; set; }

        void SetBinding();
        void ClearData();
    }
}
