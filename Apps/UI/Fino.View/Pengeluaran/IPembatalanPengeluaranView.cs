﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IPembatalanPengeluaranView : IView
    {
        event Action BtnCariClicked;
        event Action BtnTutupClicked;
        event Action BtnBatalClicked;
        event Action BtnAllClicked;
        event Action BtnResetClicked;

        DateTime DateFrom { get; }
        DateTime DateTo { get; }

        void SetData(List<PembatalanPengeluaranModel> p_ListPengeluaran);
        void RefreshDataGrid();
        void SelectAllItem();
        void UnselectAllItem();
        List<PembatalanPengeluaranModel> GetSelectedPengeluaran();
        List<PembatalanPengeluaranModel> GetGVPengeluaranDataSource();
    }
}
