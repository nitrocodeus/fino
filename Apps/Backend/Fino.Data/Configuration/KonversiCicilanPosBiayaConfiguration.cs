﻿using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class KonversiCicilanPosBiayaConfiguration
    {
        internal static void ConfigureKonversiCicilanPosBiaya(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<KonversiCicilanPosBiaya>().HasKey(e => e.source_posbiaya_id);
            modelBuilder.Entity<KonversiCicilanPosBiaya>()
                .HasRequired(e => e.Konversi)
                .WithMany(e => e.KonversiCicilanPosBiayas)
                .HasForeignKey(e=>e.source_posbiaya_id);

            modelBuilder.Entity<KonversiCicilanPosBiaya>().HasKey(e => e.destination_posbiaya_id);
            modelBuilder.Entity<KonversiCicilanPosBiaya>()
                .HasRequired(e => e.DestinationPosBiaya)
                .WithOptional(e => e.KonversiCicilanBiaya);
        }
    }
}
