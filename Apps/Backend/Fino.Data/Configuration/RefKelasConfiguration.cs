﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefKelasConfiguration
    {
        internal static void ConfigureRefKelas(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefKelas>().HasKey(e => e.kelas_id);
            modelBuilder.Entity<RefKelas>().Property(e => e.kelas_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RefKelas>().Property(e => e.nama).HasMaxLength(15);
            modelBuilder.Entity<RefKelas>().Property(e => e.kelas_code).HasMaxLength(10);
        }
    }
}
