﻿using Fino.Model;

namespace Fino.Repository
{
    public interface IPosBiayaPaidRepository
    {
        void DeletePosBiayaPaid(PosBiayaPaidModel entity);
    }
}
