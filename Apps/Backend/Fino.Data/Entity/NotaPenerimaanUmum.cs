﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class NotaPenerimaanUmum
    {
        public int nota_penerimaan_Id { get; set; }
        public DateTime tanggal_cetak { get; set; }
        public int status { get; set; }
        public int SumberPenerimaanId { get; set; }
        public int user_id { get; set; }

        public virtual RefSumberPenerimaanUmum SumberPenerimaanUmum { get; set; }
        public virtual SysUser User { get; set; }
        public virtual List<PenerimaanUmum> PenerimaanUmums { get; set; }
    }
}
