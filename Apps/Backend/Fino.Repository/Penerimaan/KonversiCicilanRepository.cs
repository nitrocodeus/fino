﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;

namespace Fino.Repository
{
    public class KonversiCicilanRepository : BaseRepository, IKonversiCicilanRepository
    {
        public KonversiCicilanSiswaModel GetSiswaByCode(string pCode)
        {
            KonversiCicilanSiswaModel returnModel = null;

            RefSiswa siswaEntity = Context.RefSiswas.Where(e => e.siswa_code.Equals(pCode)).FirstOrDefault();

            if (siswaEntity != null)
            {
                returnModel = new KonversiCicilanSiswaModel
                {
                    Siswa_Code = siswaEntity.siswa_code,
                    Siswa_Id = siswaEntity.siswa_id,
                    JKelamin = siswaEntity.jkelamin,
                    Nama = siswaEntity.nama,
                    KonversiCicilanHeaders = new List<KonversiCicilanHeaderModel>(),
                    PosBiayas = new List<PosBiayaModel>()
                };

                // Get posbiaya
                var siswaPosBiayas = siswaEntity.PosBiayas.Where(e => e.Biaya.bisacicil
                    // && e.IsActive 
                    // && !e.IsPaid 
                    && !Context.KonversiCicilanPosBiayas.Any(k => k.destination_posbiaya_id.Equals(e.PosBiaya_Id)));
                foreach (var p in siswaPosBiayas)
                {
                    returnModel.PosBiayas.Add(new PosBiayaModel
                        {
                            IsPaid = p.IsPaid,
                            JTempo = p.JTempo,
                            LayananId = 0,
                            NamaBiaya = p.Deskripsi,
                            NilaiBiaya = p.Nilai,
                            Potongan = p.NilaiPotongan,
                            RefBiayaId = p.Biaya_Id,
                            Selected = false,
                            PosBiayaId = p.PosBiaya_Id
                        });
                }

                // Get konversi headers
                returnModel.KonversiCicilanHeaders = _GetJenisBiayaWithExistingPosBiaya(returnModel.Siswa_Id);

            }

            return returnModel;
        }

        private void GetConvertedPosBiaya(List<KonversiCicilanHeaderModel> p_HeaderList, int p_SiswaId)
        {
            var konversiCicilanOfSiswa = Context.KonversiCicilans.Where(e => e.ConvertedPosBiaya.Siswa_Id.Equals(p_SiswaId)
                                && e.IsActive);

            foreach (var c in konversiCicilanOfSiswa)
            {
                var header = new KonversiCicilanHeaderModel
                    {
                        DateCreated = c.DateCreated,
                        IsActive = c.IsActive,
                        MulaiTanggal = c.MulaiTanggal,
                        NilaiCicilan = c.NilaiCicilan,
                        PosBiaya_Id = c.PosBiaya_Id,
                        Status_Id = c.Status_Id,
                        Tenor = c.Tenor,
                        IsTenor = true,
                        TotalNilai = c.ConvertedPosBiaya.Nilai - c.ConvertedPosBiaya.NilaiPotongan,
                        IsSaved = true
                    };
                var details = new List<KonversiCicilanDetailModel>();
                foreach (var d in c.KonversiCicilanPosBiayas)
                {
                    details.Add(new KonversiCicilanDetailModel
                    {
                        JTempo = d.DestinationPosBiaya.JTempo,
                        NamaBiaya = d.DestinationPosBiaya.Deskripsi,
                        NilaiBiaya = d.DestinationPosBiaya.Nilai,
                        PosBiayaId = d.destination_posbiaya_id,
                        RefBiayaId = d.DestinationPosBiaya.Biaya_Id
                    });
                }
                header.Details = details;
                p_HeaderList.Add(header);
            }
        }

        private void GetPosBiayaForInstallment(List<KonversiCicilanHeaderModel> p_HeaderList, int p_SiswaId)
        {
            var posBiayas = Context.PosBiayas.Where(e => e.Siswa_Id.Equals(p_SiswaId)
                            && e.Status_Id.Equals((int)TransactionStatusEnum.OPEN)
                            && e.IsActive
                            && !e.IsPaid
                            && e.Biaya.bisacicil
                            && !Context.KonversiCicilans.Any(k => k.PosBiaya_Id.Equals(e.PosBiaya_Id)));
            foreach (var c in posBiayas)
            {
                var header = new KonversiCicilanHeaderModel
                {
                    DateCreated = DateTime.Now,
                    IsActive = c.IsActive,
                    MulaiTanggal = DateTime.Now.Date,
                    TotalNilai = c.Nilai - c.NilaiPotongan,
                    NilaiCicilan = 0,
                    PosBiaya_Id = c.PosBiaya_Id,
                    Status_Id = 0,
                    Tenor = 0,
                    IsTenor = true
                };
                p_HeaderList.Add(header);
            }
        }

        private List<KonversiCicilanHeaderModel> _GetJenisBiayaWithExistingPosBiaya(int p_SiswaId)
        {
            var headerList = new List<KonversiCicilanHeaderModel>();
            // Get converted cicilan
            GetConvertedPosBiaya(headerList, p_SiswaId);

            // not converted pos biaya
            // GetPosBiayaForInstallment(context, headerList, p_SiswaId);

            return headerList;
        }

        public List<KonversiCicilanHeaderModel> GetJenisBiayaWithExistingPosBiaya(int p_SiswaId)
        {
            var headerList = new List<KonversiCicilanHeaderModel>();

            using (Context)
            {
                headerList = _GetJenisBiayaWithExistingPosBiaya(p_SiswaId);
            }

            return headerList;
        }

        public List<KonversiCicilanHeaderModel> CreateInstallment(List<KonversiCicilanHeaderModel> p_Model)
        {
            foreach (var header in p_Model)
            {
                if (header.Details.Count > 0 && !header.IsSaved)
                {
                    var existingPosBiaya = Context.PosBiayas.Find(header.PosBiaya_Id);
                    existingPosBiaya.IsActive = false;
                    if (existingPosBiaya.Status_Id.Equals((int)TransactionStatusEnum.POSTED))
                    {
                        existingPosBiaya.Status_Id = (int)TransactionStatusEnum.CANCELED;
                    }

                    var newKonversi = new KonversiCicilan
                    {
                        DateCreated = DateTime.Now,
                        IsActive = true,
                        MulaiTanggal = header.MulaiTanggal,
                        NilaiCicilan = header.NilaiCicilan,
                        PosBiaya_Id = header.PosBiaya_Id,
                        Status_Id = 0,
                        Tenor = header.Tenor,
                        KonversiCicilanPosBiayas = new List<KonversiCicilanPosBiaya>()
                    };
                    foreach (var d in header.Details)
                    {
                        var newPosBiaya = new PosBiaya
                        {
                            Biaya_Id = existingPosBiaya.Biaya_Id,
                            Deskripsi = d.NamaBiaya,
                            IsActive = true,
                            IsPaid = false,
                            IsPersen = false,
                            JTempo = d.JTempo,
                            Nilai = d.NilaiBiaya,
                            NilaiPotongan = 0,
                            Siswa_Id = existingPosBiaya.Siswa_Id,
                            Status_Id = 0
                        };

                        var konversiPosBiaya = new KonversiCicilanPosBiaya
                        {
                            DestinationPosBiaya = newPosBiaya
                        };

                        newKonversi.KonversiCicilanPosBiayas.Add(konversiPosBiaya);
                    }
                    Context.KonversiCicilans.Add(newKonversi);


                    header.IsSaved = true;
                }
            }

            Context.SaveChanges();

            return p_Model;
        }
    }
}
