﻿namespace Fino.Cient
{
    partial class RefPayableView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefPayableView));
            this.btnTambah = new System.Windows.Forms.Button();
            this.btnHapus = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.gvRefPayable = new System.Windows.Forms.DataGridView();
            this.refpayableIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aktifDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refPayableModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grupBiayaModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.refPayableModelBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.pnlMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvRefPayable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refPayableModelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grupBiayaModelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refPayableModelBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTambah
            // 
            this.btnTambah.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnTambah.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTambah.ForeColor = System.Drawing.Color.White;
            this.btnTambah.Location = new System.Drawing.Point(16, 13);
            this.btnTambah.Margin = new System.Windows.Forms.Padding(4);
            this.btnTambah.Name = "btnTambah";
            this.btnTambah.Size = new System.Drawing.Size(118, 28);
            this.btnTambah.TabIndex = 0;
            this.btnTambah.Text = "Tambah";
            this.btnTambah.UseVisualStyleBackColor = false;
            this.btnTambah.Click += new System.EventHandler(this.btnTambah_Click);
            // 
            // btnHapus
            // 
            this.btnHapus.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnHapus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHapus.ForeColor = System.Drawing.Color.White;
            this.btnHapus.Location = new System.Drawing.Point(16, 212);
            this.btnHapus.Margin = new System.Windows.Forms.Padding(4);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(118, 28);
            this.btnHapus.TabIndex = 1;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.UseVisualStyleBackColor = false;
            this.btnHapus.Visible = false;
            this.btnHapus.Click += new System.EventHandler(this.btnHapus_Click);
            // 
            // btnClose
            // 
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Location = new System.Drawing.Point(16, 46);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(118, 28);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Tutup";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pnlMenu
            // 
            this.pnlMenu.Controls.Add(this.btnHapus);
            this.pnlMenu.Controls.Add(this.btnTambah);
            this.pnlMenu.Controls.Add(this.btnClose);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Margin = new System.Windows.Forms.Padding(4);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(150, 354);
            this.pnlMenu.TabIndex = 3;
            // 
            // gvRefPayable
            // 
            this.gvRefPayable.AllowUserToAddRows = false;
            this.gvRefPayable.AllowUserToDeleteRows = false;
            this.gvRefPayable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvRefPayable.AutoGenerateColumns = false;
            this.gvRefPayable.BackgroundColor = System.Drawing.Color.White;
            this.gvRefPayable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvRefPayable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.refpayableIdDataGridViewTextBoxColumn,
            this.namaDataGridViewTextBoxColumn,
            this.codeDataGridViewTextBoxColumn,
            this.aktifDataGridViewCheckBoxColumn,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvRefPayable.DataSource = this.refPayableModelBindingSource;
            this.gvRefPayable.Location = new System.Drawing.Point(159, 11);
            this.gvRefPayable.Margin = new System.Windows.Forms.Padding(4);
            this.gvRefPayable.Name = "gvRefPayable";
            this.gvRefPayable.ReadOnly = true;
            this.gvRefPayable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvRefPayable.Size = new System.Drawing.Size(445, 331);
            this.gvRefPayable.TabIndex = 4;
            // 
            // refpayableIdDataGridViewTextBoxColumn
            // 
            this.refpayableIdDataGridViewTextBoxColumn.DataPropertyName = "RefpayableId";
            this.refpayableIdDataGridViewTextBoxColumn.HeaderText = "RefpayableId";
            this.refpayableIdDataGridViewTextBoxColumn.Name = "refpayableIdDataGridViewTextBoxColumn";
            this.refpayableIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.refpayableIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // namaDataGridViewTextBoxColumn
            // 
            this.namaDataGridViewTextBoxColumn.DataPropertyName = "Nama";
            this.namaDataGridViewTextBoxColumn.HeaderText = "Nama";
            this.namaDataGridViewTextBoxColumn.Name = "namaDataGridViewTextBoxColumn";
            this.namaDataGridViewTextBoxColumn.ReadOnly = true;
            this.namaDataGridViewTextBoxColumn.Width = 175;
            // 
            // codeDataGridViewTextBoxColumn
            // 
            this.codeDataGridViewTextBoxColumn.DataPropertyName = "Code";
            this.codeDataGridViewTextBoxColumn.HeaderText = "Code";
            this.codeDataGridViewTextBoxColumn.Name = "codeDataGridViewTextBoxColumn";
            this.codeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // aktifDataGridViewCheckBoxColumn
            // 
            this.aktifDataGridViewCheckBoxColumn.DataPropertyName = "Aktif";
            this.aktifDataGridViewCheckBoxColumn.HeaderText = "Aktif";
            this.aktifDataGridViewCheckBoxColumn.Name = "aktifDataGridViewCheckBoxColumn";
            this.aktifDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.ReadOnly = true;
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // refPayableModelBindingSource
            // 
            this.refPayableModelBindingSource.DataSource = typeof(Fino.Model.RefPayableModel);
            // 
            // grupBiayaModelBindingSource
            // 
            this.grupBiayaModelBindingSource.DataSource = typeof(Fino.Model.GrupBiayaModel);
            // 
            // refPayableModelBindingSource1
            // 
            this.refPayableModelBindingSource1.DataSource = typeof(Fino.Model.RefPayableModel);
            // 
            // RefPayableView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 354);
            this.Controls.Add(this.gvRefPayable);
            this.Controls.Add(this.pnlMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RefPayableView";
            this.Text = "Jenis Pengeluaran";
            this.pnlMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvRefPayable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refPayableModelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grupBiayaModelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refPayableModelBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTambah;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.DataGridView gvRefPayable;
        private System.Windows.Forms.BindingSource grupBiayaModelBindingSource;
        private System.Windows.Forms.BindingSource refPayableModelBindingSource;
        private System.Windows.Forms.BindingSource refPayableModelBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn refpayableIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn aktifDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
    }
}