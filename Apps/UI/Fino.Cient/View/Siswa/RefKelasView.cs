﻿using Fino.Cient.Startup;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View.Siswa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class RefKelasView : BaseForm, IRefKelasView
    {
        #region Events

        public event Action GvRefKelasDoubleClick;
        public event Action BtnTambahClick;
        public event Action BtnHapusClick;
        public event Action BtnTutupClick;
        public event Action ReloadData;

        #endregion

        #region Public Properties

        public RefKelasModel ViewModel { get; set; }
        public object TingkatDataObject { get; set; }

        #endregion Public Properties

        #region Contructors

        public RefKelasView()
        {
            InitializeComponent();

            this.btnTambah.Click += btnTambah_Click;
            this.btnHapus.Click += btnHapus_Click;
            this.btnTutup.Click += btnTutup_Click;
            this.gvRefKelas.DoubleClick += gvRefKelas_DoubleClick;
            this.FormClosed += RefKelasView_FormClosed;
        }

        #endregion Constructors

        #region Control Events

        void RefKelasView_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ViewLocator.Instance.EditRefKelasView != null)
            {
                ViewLocator.Instance.EditRefKelasView.Close();
            }
        }

        void gvRefKelas_DoubleClick(object sender, EventArgs e)
        {
            if (this.GvRefKelasDoubleClick != null)
            {
                this.GvRefKelasDoubleClick();
            }
        }

        void btnTutup_Click(object sender, EventArgs e)
        {
            if (this.BtnTutupClick != null)
            {
                this.BtnTutupClick();
            }
        }

        void btnHapus_Click(object sender, EventArgs e)
        {
            if (this.BtnHapusClick != null)
            {
                this.BtnHapusClick();
            }
        }

        void btnTambah_Click(object sender, EventArgs e)
        {
            if (this.BtnTambahClick != null)
            {
                this.BtnTambahClick();
            }
        }

        #endregion Control Events

        #region IRefKelasView

        public void SetData(List<RefKelasModel> listRefKelas)
        {
            gvRefKelas.DataSource = listRefKelas;
        }

        public void RefreshDataGrid()
        {
            gvRefKelas.Refresh();
        }

        public void SetBinding()
        {
            cboTingkat.DataSource = TingkatDataObject;
            cboTingkat.DisplayMember = "Value";
            cboTingkat.ValueMember = "Id";
            cboTingkat.DataBindings.Clear();
            cboTingkat.DataBindings.Add("SelectedValue", ViewModel, RefKelasModel.TingkatPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public RefKelasModel GetSelectedRefKelas()
        {
            RefKelasModel result = new RefKelasModel();

            if (gvRefKelas.SelectedCells.Count > 0)
            {
                result = (RefKelasModel)gvRefKelas.Rows[gvRefKelas.SelectedCells[0].RowIndex].DataBoundItem;
            }

            return result;
        }

        public void ReloadDataEvent()
        {
            if (ReloadData != null)
            {
                ReloadData();
            }
        }

        public void ShowEditRefKelasView()
        {
            if (gvRefKelas.SelectedCells.Count > 0)
            {
                ViewLocator.Instance.EditRefKelasView.RefKelasView = this;
                ViewLocator.Instance.EditRefKelasView.ShowForm(GetSelectedRefKelas());
            }
        }

        public void ShowTambahRefKelasView()
        {
            ViewLocator.Instance.EditRefKelasView.RefKelasView = this;
            ViewLocator.Instance.EditRefKelasView.ShowForm(null);
        }

        public void ShowErrorMessage(string message, string caption)
        {
            WinApi.ShowErrorMessage(message, caption);
        }

        #endregion IRefKelasView
    }
}
