﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class PembatalanPengeluaranModel : BaseModel
    {
        public bool Selected { get; set; }
        public int Payable_Id { get; set; }
        public int RefPayable_Id { get; set; }
        public string RefPayable_Name { get; set; }
        public int RefVendor_Id { get; set; }
        public string RefVendor_Name { get; set; }
        public string Deskripsi { get; set; }
        public double Nilai { get; set; }
        public double Jumlah { get; set; }
        public string ReferenceNo { get; set; }
        public int Status_Id { get; set; }
        public DateTime? Status_Date { get; set; }
        public DateTime JTempo { get; set; }
        public DateTime? DatePaid { get; set; }
    }
}
