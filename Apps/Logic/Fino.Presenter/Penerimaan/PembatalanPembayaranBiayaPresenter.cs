﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class PembatalanPembayaranBiayaPresenter : BasePresenter
    {
        private IPembatalanPembayaranBiayaView _view;
        private IPembatalanPembayaranPelunasanProcess _process;

        public PembatalanPembayaranBiayaPresenter(IPembatalanPembayaranBiayaView p_View, IPembatalanPembayaranPelunasanProcess p_process)
            : base(p_View)
        {
            _view = p_View;
            _process = p_process;

            _view.BtnCariClicked += _view_BtnCariClicked;
            _view.BtnTutupClicked += _view_BtnTutupClicked;
            _view.BtnBatalClicked += _view_BtnBatalClicked;
            _view.BtnAllClicked += _view_BtnAllClicked;
            _view.BtnResetClicked += _view_BtnResetClicked;
        }

        private void LoadData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _process.GetPembatalanPenerimaanPelunasanList(_view.DateFrom, _view.DateTo)),
                AppResource.MSG_CARI_PEMBATALAN_PENERIMAAN_PELUNASAN);

            if (result.IsSucess)
            {
                List<PembatalanPenerimaanPelunasanModel> data = result.DataResult as List<PembatalanPenerimaanPelunasanModel>;

                _view.SetData(data);
                _view.RefreshDataGrid();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        void _view_BtnBatalClicked()
        {
            var selectedPosBiayaToCancel = _view.GetSelectedPosBiaya();

            if (selectedPosBiayaToCancel.Count > 0 && 
                MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_CANCEL_POSBIAYA, selectedPosBiayaToCancel.Count), "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    foreach (var selectedPosBiaya in selectedPosBiayaToCancel)
                    {
                        var posBiayaPaid = _process.GetPosBiayaPaid(selectedPosBiaya.PosBiaya_Id).DataResult as PosBiayaPaidModel;

                        if (posBiayaPaid != null)
                        {
                            var posBiaya = _process.GetPosBiaya(selectedPosBiaya.PosBiaya_Id).DataResult as PosBiayaModel;

                            if (posBiaya != null)
                            {
                                if (posBiayaPaid.Status == 0)
                                {
                                    posBiaya.IsPaid = false;
                                    posBiaya.DatePaid = null;
                                    _process.UpdatePosBiaya(posBiaya);

                                    // Delete PosBiayaPaid
                                    _process.DeletePosBiayaPaid(posBiayaPaid);
                                }
                                else if (posBiayaPaid.Status == 1)
                                {
                                    posBiaya.IsPaid = false;
                                    posBiaya.DatePaid = null;
                                    _process.UpdatePosBiaya(posBiaya);

                                    // Delete PosBiayaPaid
                                    _process.DeletePosBiayaPaid(posBiayaPaid);
                                    // Move to PosBiayaPaidCancel
                                    _process.AddPosBiayaPaidCancel(new PosBiayaPaidCancelModel
                                        {
                                            PosBiaya_Id = posBiayaPaid.PosBiayaId,
                                            datepaid = posBiayaPaid.DatePaid,
                                            paynilai = posBiayaPaid.NilaiBayar,
                                            status = posBiayaPaid.Status,
                                            nota_pembayaran_id = posBiayaPaid.NotaPembayaranId,
                                            DateCancel = DateTime.Now
                                        });
                                }

                                LoadData();
                            }
                        }
                    }
                }
                catch(Exception)
                {
                    MessageBox.Show(AppResource.MSG_GAGAL_PEMBATALAN, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        void _view_BtnResetClicked()
        {
            _view.UnselectAllItem();
        }

        void _view_BtnAllClicked()
        {
            _view.SelectAllItem();
        }

        void _view_BtnTutupClicked()
        {
            _view.Close();
        }

        void _view_BtnCariClicked()
        {
            LoadData();
        }
    }
}
