﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Cient.Configuration
{
    public class WindsorViewInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyNamed("Fino.Cient")
                  .Where(c => c.IsClass &&
                                (c.Name.Contains("View")))
                  .WithService.AllInterfaces()
                  .LifestyleTransient()
                );   
        }
    }
}
