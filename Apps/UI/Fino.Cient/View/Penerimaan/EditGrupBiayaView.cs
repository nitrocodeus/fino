﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditGrupBiayaView : BaseForm, IEditGrupBiayaView
    {
        public event Action BtnBatalClicked;
        public event Action BtnSimpanClicked;

        public EditGrupBiayaView()
        {
            InitializeComponent();
        }

        public GrupBiayaModel SelectedGrupBiaya { get; private set; }

        public IGrupBiayaView GrupBiayaView { get; set; }

        public string Nama
        {
            get
            {
                return txtNamaGrupBiaya.Text;
            }
            set
            {
                txtNamaGrupBiaya.Text = value;
            }
        }

        public string Kode
        {
            get
            {
                return txtKodeGrupBiaya.Text;
            }
            set
            {
                txtKodeGrupBiaya.Text = value;
            }
        }

        public void SetBinding()
        {
            txtNamaGrupBiaya.DataBindings.Add("Text", SelectedGrupBiaya, GrupBiayaModel.NamaPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtKodeGrupBiaya.DataBindings.Add("Text", SelectedGrupBiaya, GrupBiayaModel.CodePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void ClearData()
        {
            throw new NotImplementedException();
        }

        public void ShowForm(GrupBiayaModel p_GrupBiaya)
        {
            if (p_GrupBiaya != null)
            {
                SelectedGrupBiaya = p_GrupBiaya;
            }
            else
            {
                SelectedGrupBiaya = new GrupBiayaModel();
            }
            this.Show();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        public void RaiseBtnSimpanClicked()
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        public void RaiseBtnBatalClicked()
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }
    }
}
