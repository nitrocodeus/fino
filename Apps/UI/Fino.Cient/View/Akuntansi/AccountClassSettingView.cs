﻿using Fino.Cient.Startup;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class AccountClassSettingView : BaseForm, IAccountClassSettingView
    {
        private delegate void GetSelectedAccClassIdCallback(out int accClassId);

        #region Events

        public event Action GvAccountClassSettingDoubleClick;
        public event Action BtnTambahClick;
        public event Action BtnHapusClick;
        public event Action BtnTutupClick;
        public event Action ReloadData;

        #endregion

        #region Public Properties

        public AccountClassSettingModel ViewModel { get; set; }
        public object AccClassDataObject { get; set; }
        public int AccClassId { get; set; }

        #endregion Public Properties

        #region Constructors

        public AccountClassSettingView()
        {
            InitializeComponent();

            this.btnTambah.Click += btnTambah_Click;
            this.btnTutup.Click += btnTutup_Click;
            this.btnHapus.Click += btnHapus_Click;
            this.gvAccountClassSetting.DoubleClick += gvAccountClassSetting_DoubleClick;
            this.FormClosed += AccountClassSettingView_FormClosed;
        }

        void AccountClassSettingView_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ViewLocator.Instance.EditAccountClassSettingView != null)
            {
                ViewLocator.Instance.EditAccountClassSettingView.Close();
            }
        }

        #region Control Events

        void btnHapus_Click(object sender, EventArgs e)
        {
            if (BtnHapusClick != null)
            {
                BtnHapusClick();
            }
        }

        void gvAccountClassSetting_DoubleClick(object sender, EventArgs e)
        {
            if (GvAccountClassSettingDoubleClick != null)
            {
                GvAccountClassSettingDoubleClick();
            }
        }

        void btnTutup_Click(object sender, EventArgs e)
        {
            if (BtnTutupClick != null)
            {
                BtnTutupClick();
            }
        }

        void btnTambah_Click(object sender, EventArgs e)
        {
            if (BtnTambahClick != null)
            {
                BtnTambahClick();
            }
        }

        #endregion Control Events

        #endregion Constructors

        #region IAccountClassSettingView

        public void SetData(List<AccountClassSettingModel> listAccountClassSetting)
        {
            gvAccountClassSetting.DataSource = listAccountClassSetting;
        }

        public void RefreshDataGrid()
        {
            gvAccountClassSetting.Refresh();
        }

        public AccountClassSettingModel GetSelectedAccountClassSetting()
        {
            AccountClassSettingModel result = new AccountClassSettingModel();

            if (gvAccountClassSetting.SelectedCells.Count > 0)
            {
                result = (AccountClassSettingModel)gvAccountClassSetting.Rows[gvAccountClassSetting.SelectedCells[0].RowIndex].DataBoundItem;
            }

            return result;
        }

        public void ReloadDataEvent()
        {
            if (ReloadData != null)
            {
                ReloadData();
            }
        }

        public void ShowEditAccountClassSettingView()
        {
            if (gvAccountClassSetting.SelectedCells.Count > 0)
            {
                ViewLocator.Instance.EditAccountClassSettingView.AccountClassSettingView = this;
                ViewLocator.Instance.EditAccountClassSettingView.ShowForm(GetSelectedAccountClassSetting()); 
            }
        }

        public void ShowTambahAccountClassSettingView()
        {
            ViewLocator.Instance.EditAccountClassSettingView.AccountClassSettingView = this;
            ViewLocator.Instance.EditAccountClassSettingView.ShowForm(null);
        }

        public void ShowErrorMessage(string message, string caption)
        {
            WinApi.ShowErrorMessage(message, caption);
        }
        
        public void SetBinding()
        {
            cboAccountClassSetting.DataSource = AccClassDataObject;
            cboAccountClassSetting.DisplayMember = "Value";
            cboAccountClassSetting.ValueMember = "Id";
            cboAccountClassSetting.DataBindings.Clear();
            cboAccountClassSetting.DataBindings.Add("SelectedValue", ViewModel, AccountClassSettingModel.AccClassIdPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void SetDataGridViewParamHeaderText()
        {
            string paramHeaderText = "Param";
            AccountClassificationEnum accType = (AccountClassificationEnum)ViewModel.AccClassId;
            switch(accType)
            {
                case AccountClassificationEnum.PIUTANG:
                case AccountClassificationEnum.PENDAPATAN:
                    paramHeaderText = "Jenis Penerimaan";
                    break;
                case AccountClassificationEnum.BEBAN:
                    paramHeaderText = "Jenis Pengeluaran";
                    break;
            }

            gvAccountClassSetting.Columns["paramNameDataGridViewTextBoxColumn"].HeaderText = paramHeaderText;
        }

        #endregion IAccountClassSettingView
    }
}
