﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core
{
    public class Constants
    {
        private Dictionary<string, Image> _layananImage;

        public List<ValueList> Bulan { get; set; }
        public List<ValueList> BulanWithoutNone { get; set; }
        public List<string> MenuGroupHeader { get; set; }
        public List<ValueList> Repetisi { get; set; }

        public Dictionary<string, Image> LayananImage
        {
            get { return _layananImage; }
        }

        public const string FontName = "Verdana";
        public const float FontSize = 10;
        public const string SystemUsername = "system";

        public const int LayananImageWidth = 24;
        public const int LayananImageSpacing = 5;
        public static readonly string APPLICATION_DIRECTORY = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        public static readonly string MENU_IMAGE_DIRECTORY = APPLICATION_DIRECTORY + @"\Images\Menus";
        public const string MAIN_MENU_SISWA_GROUP_NAME = "Siswa";
        public const string MAIN_MENU_PENERIMAAN_GROUP_NAME = "Penerimaan";
        public const string MAIN_MENU_PENGELUARAN_GROUP_NAME = "Pengeluaran";
        public const string MAIN_MENU_AKUNTANSI_GROUP_NAME = "Akuntansi";
        public const string MAIN_MENU_TABUNGAN_GROUP_NAME = "Tabungan";
        public const string AUTHORIZATION_ROLE_ADMIN = "Admin";
        public const string AUTHORIZATION_ROLE_OPERATOR = "Operator";

        #region Singleton
        private static readonly Lazy<Constants> lazy = new Lazy<Constants>(() => new Constants());
    
        public static Constants Instance { get { return lazy.Value; } }

        private Constants() 
        {
            GenerateBulan();
            GenerateBulanWithoutNone();
            GenerateMenuGroupHeader();
            GenerateRepetisi();
            GenerateLayananImage();
        }

        private void GenerateRepetisi()
        {
            Repetisi = new List<ValueList>();

            Repetisi.Add(new ValueList() { Id = 0, Value = "NONE" });
            Repetisi.Add(new ValueList() { Id = 1, Value = "MINGGUAN" });
            Repetisi.Add(new ValueList() { Id = 2, Value = "BULANAN" });
            Repetisi.Add(new ValueList() { Id = 3, Value = "TAHUNAN" });
            Repetisi.Add(new ValueList() { Id = 4, Value = "TAHUNAJARAN" });
            Repetisi.Add(new ValueList() { Id = 5, Value = "SEMESTER" });
            Repetisi.Add(new ValueList() { Id = 6, Value = "CAWU" });
        }

        private void GenerateBulan()
        {
            Bulan = new List<ValueList>();

            Bulan.Add(new ValueList() { Id = 0, Value = "None" });
            Bulan.Add(new ValueList() { Id = 1, Value = "Januari" });
            Bulan.Add(new ValueList() { Id = 2, Value = "Februari" });
            Bulan.Add(new ValueList() { Id = 3, Value = "Maret" });
            Bulan.Add(new ValueList() { Id = 4, Value = "April" });
            Bulan.Add(new ValueList() { Id = 5, Value = "Mei" });
            Bulan.Add(new ValueList() { Id = 6, Value = "Juni" });
            Bulan.Add(new ValueList() { Id = 7, Value = "Juli" });
            Bulan.Add(new ValueList() { Id = 8, Value = "Agustus" });
            Bulan.Add(new ValueList() { Id = 9, Value = "September" });
            Bulan.Add(new ValueList() { Id = 10, Value = "Oktober" });
            Bulan.Add(new ValueList() { Id = 11, Value = "November" });
            Bulan.Add(new ValueList() { Id = 12, Value = "Desember" });            
        }

        private void GenerateBulanWithoutNone()
        {
            BulanWithoutNone = new List<ValueList>();

            BulanWithoutNone.Add(new ValueList() { Id = 1, Value = "Januari" });
            BulanWithoutNone.Add(new ValueList() { Id = 2, Value = "Februari" });
            BulanWithoutNone.Add(new ValueList() { Id = 3, Value = "Maret" });
            BulanWithoutNone.Add(new ValueList() { Id = 4, Value = "April" });
            BulanWithoutNone.Add(new ValueList() { Id = 5, Value = "Mei" });
            BulanWithoutNone.Add(new ValueList() { Id = 6, Value = "Juni" });
            BulanWithoutNone.Add(new ValueList() { Id = 7, Value = "Juli" });
            BulanWithoutNone.Add(new ValueList() { Id = 8, Value = "Agustus" });
            BulanWithoutNone.Add(new ValueList() { Id = 9, Value = "September" });
            BulanWithoutNone.Add(new ValueList() { Id = 10, Value = "Oktober" });
            BulanWithoutNone.Add(new ValueList() { Id = 11, Value = "November" });
            BulanWithoutNone.Add(new ValueList() { Id = 12, Value = "Desember" });
        }

        private void GenerateMenuGroupHeader()
        {
            MenuGroupHeader = new List<string>();

            MenuGroupHeader.Add(MAIN_MENU_SISWA_GROUP_NAME);
            MenuGroupHeader.Add(MAIN_MENU_PENERIMAAN_GROUP_NAME);
            MenuGroupHeader.Add(MAIN_MENU_PENGELUARAN_GROUP_NAME);
            MenuGroupHeader.Add(MAIN_MENU_AKUNTANSI_GROUP_NAME);
            MenuGroupHeader.Add(MAIN_MENU_TABUNGAN_GROUP_NAME);
        }

        private void GenerateLayananImage()
        {
            _layananImage = new Dictionary<string, Image>();

            _layananImage.Add("JPT", Image.FromFile(Path.Combine(Constants.MENU_IMAGE_DIRECTORY, MainMenuEnum.Jemputan.ToString() + ".png")));
            _layananImage.Add("KAT", Image.FromFile(Path.Combine(Constants.MENU_IMAGE_DIRECTORY, MainMenuEnum.Katering.ToString() + ".png")));
        }
        #endregion
    }

    public class ValueList
    {
        public object Id { get; set; }
        public object Value { get; set; }
        public object Tag { get; set; }
    }
}
