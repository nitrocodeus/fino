﻿namespace Fino.Cient
{
    partial class PembatalanPenerimaanUmumView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PembatalanPenerimaanUmumView));
            this.panel2 = new System.Windows.Forms.Panel();
            this.gvPenerimaanUmum = new System.Windows.Forms.DataGridView();
            this.penerimaanUmumBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnReset = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblDari = new System.Windows.Forms.Label();
            this.btnTutup = new System.Windows.Forms.Button();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.lblSampai = new System.Windows.Forms.Label();
            this.btnCari = new System.Windows.Forms.Button();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.btnBatal = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.selectedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.penerimaanUmumIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refSumberPenerimaanIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refSumberPenerimaanNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deskripsiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datePaidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nilaiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.notaPenerimaanUmumIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPenerimaanUmum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.penerimaanUmumBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gvPenerimaanUmum);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 136);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(963, 197);
            this.panel2.TabIndex = 19;
            // 
            // gvPenerimaanUmum
            // 
            this.gvPenerimaanUmum.AllowUserToAddRows = false;
            this.gvPenerimaanUmum.AllowUserToDeleteRows = false;
            this.gvPenerimaanUmum.AllowUserToResizeRows = false;
            this.gvPenerimaanUmum.AutoGenerateColumns = false;
            this.gvPenerimaanUmum.BackgroundColor = System.Drawing.Color.White;
            this.gvPenerimaanUmum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvPenerimaanUmum.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectedDataGridViewCheckBoxColumn,
            this.penerimaanUmumIdDataGridViewTextBoxColumn,
            this.refSumberPenerimaanIdDataGridViewTextBoxColumn,
            this.refSumberPenerimaanNameDataGridViewTextBoxColumn,
            this.deskripsiDataGridViewTextBoxColumn,
            this.datePaidDataGridViewTextBoxColumn,
            this.nilaiDataGridViewTextBoxColumn,
            this.notaPenerimaanUmumIdDataGridViewTextBoxColumn,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvPenerimaanUmum.DataSource = this.penerimaanUmumBindingSource;
            this.gvPenerimaanUmum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvPenerimaanUmum.Location = new System.Drawing.Point(0, 0);
            this.gvPenerimaanUmum.Name = "gvPenerimaanUmum";
            this.gvPenerimaanUmum.RowHeadersVisible = false;
            this.gvPenerimaanUmum.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvPenerimaanUmum.Size = new System.Drawing.Size(963, 197);
            this.gvPenerimaanUmum.TabIndex = 16;
            // 
            // penerimaanUmumBindingSource
            // 
            this.penerimaanUmumBindingSource.DataSource = typeof(Fino.Model.PembatalanPenerimaanUmumModel);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.ForeColor = System.Drawing.Color.Black;
            this.btnReset.Location = new System.Drawing.Point(149, 88);
            this.btnReset.Margin = new System.Windows.Forms.Padding(2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(110, 39);
            this.btnReset.TabIndex = 14;
            this.btnReset.Text = "Kosongkan";
            this.btnReset.UseVisualStyleBackColor = false;
            // 
            // btnAll
            // 
            this.btnAll.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAll.ForeColor = System.Drawing.Color.White;
            this.btnAll.Location = new System.Drawing.Point(11, 88);
            this.btnAll.Margin = new System.Windows.Forms.Padding(2);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(133, 39);
            this.btnAll.TabIndex = 15;
            this.btnAll.Text = "Pilih Semua";
            this.btnAll.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblDari);
            this.groupBox1.Controls.Add(this.btnTutup);
            this.groupBox1.Controls.Add(this.dtFrom);
            this.groupBox1.Controls.Add(this.lblSampai);
            this.groupBox1.Controls.Add(this.btnCari);
            this.groupBox1.Controls.Add(this.dtTo);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(931, 71);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informasi periode penerimaan umum";
            // 
            // lblDari
            // 
            this.lblDari.AutoSize = true;
            this.lblDari.Location = new System.Drawing.Point(7, 32);
            this.lblDari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDari.Name = "lblDari";
            this.lblDari.Size = new System.Drawing.Size(99, 17);
            this.lblDari.TabIndex = 11;
            this.lblDari.Text = "Dari tanggal:";
            // 
            // btnTutup
            // 
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(554, 31);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(4);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(100, 28);
            this.btnTutup.TabIndex = 1;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            // 
            // dtFrom
            // 
            this.dtFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(114, 31);
            this.dtFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(116, 24);
            this.dtFrom.TabIndex = 0;
            // 
            // lblSampai
            // 
            this.lblSampai.AutoSize = true;
            this.lblSampai.Location = new System.Drawing.Point(238, 32);
            this.lblSampai.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSampai.Name = "lblSampai";
            this.lblSampai.Size = new System.Drawing.Size(31, 17);
            this.lblSampai.TabIndex = 12;
            this.lblSampai.Text = "s/d";
            // 
            // btnCari
            // 
            this.btnCari.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnCari.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCari.ForeColor = System.Drawing.Color.White;
            this.btnCari.Location = new System.Drawing.Point(446, 31);
            this.btnCari.Margin = new System.Windows.Forms.Padding(4);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(100, 28);
            this.btnCari.TabIndex = 2;
            this.btnCari.Text = "Cari";
            this.btnCari.UseVisualStyleBackColor = false;
            // 
            // dtTo
            // 
            this.dtTo.CustomFormat = "dd-MMM-yyyy";
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(277, 31);
            this.dtTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(161, 24);
            this.dtTo.TabIndex = 1;
            // 
            // btnBatal
            // 
            this.btnBatal.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.ForeColor = System.Drawing.Color.White;
            this.btnBatal.Location = new System.Drawing.Point(265, 88);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 39);
            this.btnBatal.TabIndex = 0;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.btnAll);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btnBatal);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(963, 136);
            this.panel1.TabIndex = 18;
            // 
            // selectedDataGridViewCheckBoxColumn
            // 
            this.selectedDataGridViewCheckBoxColumn.DataPropertyName = "Selected";
            this.selectedDataGridViewCheckBoxColumn.Frozen = true;
            this.selectedDataGridViewCheckBoxColumn.HeaderText = "";
            this.selectedDataGridViewCheckBoxColumn.Name = "selectedDataGridViewCheckBoxColumn";
            this.selectedDataGridViewCheckBoxColumn.Width = 30;
            // 
            // penerimaanUmumIdDataGridViewTextBoxColumn
            // 
            this.penerimaanUmumIdDataGridViewTextBoxColumn.DataPropertyName = "PenerimaanUmum_Id";
            this.penerimaanUmumIdDataGridViewTextBoxColumn.HeaderText = "PenerimaanUmum_Id";
            this.penerimaanUmumIdDataGridViewTextBoxColumn.Name = "penerimaanUmumIdDataGridViewTextBoxColumn";
            this.penerimaanUmumIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.penerimaanUmumIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // refSumberPenerimaanIdDataGridViewTextBoxColumn
            // 
            this.refSumberPenerimaanIdDataGridViewTextBoxColumn.DataPropertyName = "RefSumberPenerimaan_Id";
            this.refSumberPenerimaanIdDataGridViewTextBoxColumn.HeaderText = "RefSumberPenerimaan_Id";
            this.refSumberPenerimaanIdDataGridViewTextBoxColumn.Name = "refSumberPenerimaanIdDataGridViewTextBoxColumn";
            this.refSumberPenerimaanIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.refSumberPenerimaanIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // refSumberPenerimaanNameDataGridViewTextBoxColumn
            // 
            this.refSumberPenerimaanNameDataGridViewTextBoxColumn.DataPropertyName = "RefSumberPenerimaan_Name";
            this.refSumberPenerimaanNameDataGridViewTextBoxColumn.HeaderText = "Sumber Penerimaan Umum";
            this.refSumberPenerimaanNameDataGridViewTextBoxColumn.Name = "refSumberPenerimaanNameDataGridViewTextBoxColumn";
            this.refSumberPenerimaanNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.refSumberPenerimaanNameDataGridViewTextBoxColumn.Width = 300;
            // 
            // deskripsiDataGridViewTextBoxColumn
            // 
            this.deskripsiDataGridViewTextBoxColumn.DataPropertyName = "Deskripsi";
            this.deskripsiDataGridViewTextBoxColumn.HeaderText = "Nama Transaksi";
            this.deskripsiDataGridViewTextBoxColumn.Name = "deskripsiDataGridViewTextBoxColumn";
            this.deskripsiDataGridViewTextBoxColumn.ReadOnly = true;
            this.deskripsiDataGridViewTextBoxColumn.Width = 200;
            // 
            // datePaidDataGridViewTextBoxColumn
            // 
            this.datePaidDataGridViewTextBoxColumn.DataPropertyName = "DatePaid";
            dataGridViewCellStyle1.Format = "dd MMM yyyy";
            this.datePaidDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.datePaidDataGridViewTextBoxColumn.HeaderText = "Tanggal";
            this.datePaidDataGridViewTextBoxColumn.Name = "datePaidDataGridViewTextBoxColumn";
            this.datePaidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nilaiDataGridViewTextBoxColumn
            // 
            this.nilaiDataGridViewTextBoxColumn.DataPropertyName = "Nilai";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##";
            this.nilaiDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.nilaiDataGridViewTextBoxColumn.HeaderText = "Nilai";
            this.nilaiDataGridViewTextBoxColumn.Name = "nilaiDataGridViewTextBoxColumn";
            this.nilaiDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // notaPenerimaanUmumIdDataGridViewTextBoxColumn
            // 
            this.notaPenerimaanUmumIdDataGridViewTextBoxColumn.DataPropertyName = "NotaPenerimaanUmumId";
            dataGridViewCellStyle3.Format = "#;#;-";
            this.notaPenerimaanUmumIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.notaPenerimaanUmumIdDataGridViewTextBoxColumn.HeaderText = "No Nota Penerimaan Umum";
            this.notaPenerimaanUmumIdDataGridViewTextBoxColumn.Name = "notaPenerimaanUmumIdDataGridViewTextBoxColumn";
            this.notaPenerimaanUmumIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.notaPenerimaanUmumIdDataGridViewTextBoxColumn.Width = 200;
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // PembatalanPenerimaanUmumView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 333);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(971, 360);
            this.Name = "PembatalanPenerimaanUmumView";
            this.Text = "Pembatalan Penerimaan Umum";
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPenerimaanUmum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.penerimaanUmumBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView gvPenerimaanUmum;
        private System.Windows.Forms.BindingSource penerimaanUmumBindingSource;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblDari;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.Label lblSampai;
        private System.Windows.Forms.Button btnCari;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn penerimaanUmumIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn refSumberPenerimaanIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn refSumberPenerimaanNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deskripsiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datePaidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nilaiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn notaPenerimaanUmumIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
    }
}