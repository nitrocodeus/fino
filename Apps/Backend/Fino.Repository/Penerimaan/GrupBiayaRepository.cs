﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class GrupBiayaRepository : BaseRepository, IGrupBiayaRepository
    {
        public List<RefBiaya> GetBiayaOfGrupCode(string pCode)
        {
            List<RefBiaya> biayaList = new List<RefBiaya>();
            var groupBiaya = Context.RefGrupBiayas.Where(e => e.code.Equals(pCode)).FirstOrDefault();
            if (groupBiaya != null)
            {
                foreach(var gBiaya in groupBiaya.GrupBiayas)
                {
                    biayaList.Add(gBiaya.Biaya);
                }
            }

            return biayaList;
        }
    }
}
