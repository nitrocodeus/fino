﻿using System;

namespace Fino.Lib.Core.Interface
{
    public interface ITahunAjaranModel
    {
        int HinggaBulan { get; set; }
        int HinggaTahun { get; set; }
        int MulaiBulan { get; set; }
        int MulaiTahun { get; set; }
        string Nama { get; set; }
        int SemCawu { get; set; }
        int Status { get; set; }
        int TahunAjaranId { get; set; }
    }
}
