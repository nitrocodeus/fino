﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class PembatalanPenerimaanUmumPresenter : BasePresenter
    {
        private IPembatalanPenerimaanUmumView _view;
        private IPembatalanPenerimaanUmumProcess _process;

        public PembatalanPenerimaanUmumPresenter(IPembatalanPenerimaanUmumView p_View, IPembatalanPenerimaanUmumProcess p_process)
            : base(p_View)
        {
            _view = p_View;
            _process = p_process;

            _view.BtnCariClicked += _view_BtnCariClicked;
            _view.BtnTutupClicked += _view_BtnTutupClicked;
            _view.BtnBatalClicked += _view_BtnBatalClicked;
            _view.BtnAllClicked += _view_BtnAllClicked;
            _view.BtnResetClicked += _view_BtnResetClicked;
        }

        private void LoadData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _process.GetPembatalanPenerimaanUmumList(_view.DateFrom, _view.DateTo)),
                AppResource.MSG_CARI_PEMBATALAN_PENERIMAAN_UMUM);

            if (result.IsSucess)
            {
                List<PembatalanPenerimaanUmumModel> data = result.DataResult as List<PembatalanPenerimaanUmumModel>;

                _view.SetData(data);
                _view.RefreshDataGrid();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        void _view_BtnBatalClicked()
        {
            var selectedPenerimaanUmumToCancel = _view.GetSelectedPenerimaanUmum();

            if (selectedPenerimaanUmumToCancel.Count > 0 &&
                MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_CANCEL_PENERIMAAN_UMUM, selectedPenerimaanUmumToCancel.Count), "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    foreach (var selectedPenerimaanUmum in selectedPenerimaanUmumToCancel)
                    {
                        var penerimaanUmum = _process.GetPenerimaanUmum(selectedPenerimaanUmum.PenerimaanUmum_Id).DataResult as PenerimaanUmumModel;

                        if (penerimaanUmum != null)
                        {
                            if (penerimaanUmum.Status_Id == (int)TransactionStatusEnum.OPEN)
                            {
                                // Delete PenerimaanUmum
                                _process.DeletePenerimaanUmum(penerimaanUmum);
                                _process.DeleteNotaPenerimaanUmum(selectedPenerimaanUmum.NotaPenerimaanUmumId);
                            }
                            else if (penerimaanUmum.Status_Id == (int)TransactionStatusEnum.POSTED)
                            {
                                penerimaanUmum.Status_Id = (int)TransactionStatusEnum.CANCELED;
                                penerimaanUmum.Status_Date = DateTime.Now;

                                _process.UpdatePenerimaanUmum(penerimaanUmum);
                            }

                            LoadData();
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(AppResource.MSG_GAGAL_PEMBATALAN, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        void _view_BtnResetClicked()
        {
            _view.UnselectAllItem();
        }

        void _view_BtnAllClicked()
        {
            _view.SelectAllItem();
        }

        void _view_BtnTutupClicked()
        {
            _view.Close();
        }

        void _view_BtnCariClicked()
        {
            LoadData();
        }
    }
}
