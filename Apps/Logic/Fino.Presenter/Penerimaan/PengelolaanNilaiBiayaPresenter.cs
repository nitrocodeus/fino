﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Presenter
{
    public class PengelolaanNilaiBiayaPresenter : BasePresenter
    {
        private readonly IPengelolaanNilaiBiayaProcess _pengelolaanNilaiBiayaProcess;
        private readonly IPengelolaanNilaiBiayaView _pengelolaanNilaiBiayaView;

        #region Contructors

        public PengelolaanNilaiBiayaPresenter(IPengelolaanNilaiBiayaProcess p_Process, IPengelolaanNilaiBiayaView p_View)
            : base(p_View)
        {
            try
            {
                this._pengelolaanNilaiBiayaProcess = p_Process;
                this._pengelolaanNilaiBiayaView = p_View;

                this._pengelolaanNilaiBiayaView.BiayaDataSource = GetBiayaDataSource();
                this._pengelolaanNilaiBiayaView.TingkatDataSource = GetTingkatDataSource();
                this._pengelolaanNilaiBiayaView.ShowNilaiBiayaDetail(false);

                this._pengelolaanNilaiBiayaView.SaveButtonClicked += _pengelolaanNilaiBiayaView_SaveButtonClicked;
                this._pengelolaanNilaiBiayaView.NewDataButtonClicked += _pengelolaanNilaiBiayaView_NewDataButtonClicked;
                this._pengelolaanNilaiBiayaView.DeleteButtonClicked += _pengelolaanNilaiBiayaView_DeleteButtonClicked;
                this._pengelolaanNilaiBiayaView.CloseButtonClicked += _pengelolaanNilaiBiayaView_CloseButtonClicked;
                this._pengelolaanNilaiBiayaView.NilaiBiayaSelectionChanged += _pengelolaanNilaiBiayaView_BiayaListSelectionChanged;
                this._pengelolaanNilaiBiayaView.TingkatSelectionChanged += _pengelolaanNilaiBiayaView_TingkatSelectionChanged;

                GetNilaiBiayaDetail(_pengelolaanNilaiBiayaView.SelectedBiayaId, _pengelolaanNilaiBiayaView.SelectedOption);
            }
            catch (Exception ex)
            {
                _pengelolaanNilaiBiayaView.ShowErrorMessage(ex.Message, AppResource.CAP_PROCESS_ERROR);
            }
        }

        #endregion Contructors

        #region Private Methods

        private void SaveData()
        {
            var result = _pengelolaanNilaiBiayaProcess.UpdateNilaiBiayaOption(_pengelolaanNilaiBiayaView.CurrentDetail);

            if (!result.IsSucess)
            {
                _pengelolaanNilaiBiayaView.ShowErrorMessage(result.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
            else
            {

            }
        }

        private object GetBiayaDataSource()
        {
            return _pengelolaanNilaiBiayaProcess.GetBiayaList().DataResult;
        }

        private object GetTingkatDataSource()
        {
            List<ValueList> tkList = new List<ValueList>();
            tkList.Add(new ValueList { Id = 0, Value = "Semua tingkat" });
            tkList.AddRange((List<ValueList>)_pengelolaanNilaiBiayaProcess.GetTingkatList().DataResult);
            return tkList;
        }

        private void GetNilaiBiayaDetail(int biaya_id, int option)
        {
            var result = _pengelolaanNilaiBiayaProcess.GetNilaiBiayaOption(biaya_id, option);
            var nilaiBiaya = result.DataResult as PengelolaanNilaiBiayaModel;

            _pengelolaanNilaiBiayaView.CurrentDetail = nilaiBiaya;
            _pengelolaanNilaiBiayaView.ShowNilaiBiayaDetail(nilaiBiaya != null);
            _pengelolaanNilaiBiayaView.SetBinding();
        }

        #endregion Private Methods

        #region Control Events

        private void _pengelolaanNilaiBiayaView_SaveButtonClicked()
        {
            SaveData();
        }

        private void _pengelolaanNilaiBiayaView_NewDataButtonClicked()
        {
            if (_pengelolaanNilaiBiayaView.CurrentDetail == null)
            {
                _pengelolaanNilaiBiayaView.CurrentDetail = new PengelolaanNilaiBiayaModel
                {
                    Biaya_Id = _pengelolaanNilaiBiayaView.SelectedBiayaId,
                    Option = _pengelolaanNilaiBiayaView.SelectedOption
                };
            }
            _pengelolaanNilaiBiayaView.ShowNilaiBiayaDetail(true);
        }

        private void _pengelolaanNilaiBiayaView_DeleteButtonClicked()
        {
            var result = _pengelolaanNilaiBiayaProcess.DeleteNilaiBiayaOption(_pengelolaanNilaiBiayaView.CurrentDetail);

            if (!result.IsSucess)
            {
                _pengelolaanNilaiBiayaView.ShowErrorMessage(result.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
            else
            {
                _pengelolaanNilaiBiayaView.ShowNilaiBiayaDetail(false);
            }
        }

        private void _pengelolaanNilaiBiayaView_CloseButtonClicked()
        {
            this._pengelolaanNilaiBiayaView.Close();
        }

        private void _pengelolaanNilaiBiayaView_BiayaListSelectionChanged()
        {
            GetNilaiBiayaDetail(_pengelolaanNilaiBiayaView.SelectedBiayaId, _pengelolaanNilaiBiayaView.SelectedOption);
        }

        private void _pengelolaanNilaiBiayaView_TingkatSelectionChanged()
        {
            GetNilaiBiayaDetail(_pengelolaanNilaiBiayaView.SelectedBiayaId, _pengelolaanNilaiBiayaView.SelectedOption);
        }

        #endregion Control Events
    }
}
