﻿using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public interface IKonfigurasiBiayaRepository : IRepository
    {
        List<RefBiaya> KonfigurasiBiayaData();
        void AddKonfigurasiBiayaData(RefBiaya p_RefBiaya);
        void DeleteKonfigurasiBiayaData(RefBiaya p_RefBiaya);
        void UpdateKonfigurasiBiayaData(RefBiaya p_RefBiaya);

        List<RefGrupBiaya> GrupBiayaData();
        void AddGrupBiayaData(RefGrupBiaya p_RefGrupBiaya);
        void DeleteGrupBiayaData(RefGrupBiaya p_RefGrupBiaya);
        void UpdateGrupBiayaData(RefGrupBiaya p_RefGrupBiaya);

        void SaveChanges();

        void UpdateMappingGrupBiayaData(List<GrupBiaya> grupBiayas, RefGrupBiaya p_refGrupBiaya);
        List<GrupBiaya> GetMappingGrupBiayaData(int p_GrupBiayaId);
    }
}
