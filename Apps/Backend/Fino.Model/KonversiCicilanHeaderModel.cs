﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Fino.Lib.Core;

namespace Fino.Model
{
    public class KonversiCicilanHeaderModel : BaseModel
    {
        public static readonly string PosBiayaIdPropertyName = "PosBiaya_Id";
        public static readonly string TenorPropertyName = "Tenor";
        public static readonly string MulaiTanggalPropertyName = "MulaiTanggal";
        public static readonly string NilaiCicilanPropertyName = "NilaiCicilan";
        public static readonly string IsTenorPropertyName = "IsTenor";
        public static readonly string NamaBiayaPropertyName = "NamaBiaya";
        public static readonly string TotalNilaiPropertyName = "TotalNilai";

        public int PosBiaya_Id { get; set; }
        public bool IsSaved { get; set; }

        private bool _IsTenor;
        [Required]
        public bool IsTenor 
        { 
            get
            {
                return _IsTenor;
            }
            set
            {
                _IsTenor = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(IsTenorPropertyName));
                ValidateProperty(value, IsTenorPropertyName);
            }
        }

        public double TotalNilai { get; set; }

        public string NamaBiaya { get; set; }
        
        int _Tenor;
        [Display(Name = "Tenor")]
        [Required]
        public int Tenor
        {
            get
            {
                return _Tenor;
            }
            set
            {
                _Tenor = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(TenorPropertyName));
                ValidateProperty(value, TenorPropertyName);
            }
        }

        DateTime _MulaiTanggal;
        [Display(Name = "Mulai Tanggal")]
        [Required]
        public DateTime MulaiTanggal
        {
            get
            {
                return _MulaiTanggal;
            }
            set
            {
                _MulaiTanggal = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(MulaiTanggalPropertyName));
                ValidateProperty(value, MulaiTanggalPropertyName);
            }
        }

        double _NilaiCicilan;
        [Display(Name = "Nilai Cicilan")]
        [Required]
        public double NilaiCicilan
        {
            get
            {
                return _NilaiCicilan;
            }
            set
            {
                _NilaiCicilan = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NilaiCicilanPropertyName));
                ValidateProperty(value, NilaiCicilanPropertyName);
            }
        }
        public DateTime DateCreated { get; set; }
        public bool IsActive { get; set; }
        public int Status_Id { get; set; }



        public List<KonversiCicilanDetailModel> Details { get; set; }
    }
}
