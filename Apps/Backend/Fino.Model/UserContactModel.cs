﻿using Fino.Lib.Core;

namespace Fino.Model
{
    public class UserContactModel : BaseModel
    {
        public int Userid { get; set; }
        public int Type { get; set; }
        public string Contact { get; set; }
    }
}
