﻿using AutoMapper;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class PembatalanPembayaranPelunasanProcess : IPembatalanPembayaranPelunasanProcess
    {
        public IPosBiayaRepository PosBiayaRepo { get; set; }
        public IPosBiayaPaidCancelRepository PosBiayaPaidCancelRepo { get; set; }
        public IPosBiayaPaidRepository PosBiayaPaidRepo { get; set; }

        public ProcessResult GetPosBiaya(int p_PosBiayaId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                PosBiayaModel dataResult = null;
                var posBiaya = PosBiayaRepo.GetPosBiaya(p_PosBiayaId);

                if (posBiaya != null)
                {
                    dataResult = Mapper.Map<PosBiayaModel>(posBiaya);
                }

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetPembatalanPenerimaanPelunasanList(DateTime p_FromDate, DateTime p_ToDate)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<PembatalanPenerimaanPelunasanModel> dataResult = PosBiayaRepo.GetPembatalanPenerimaanPembayaranList(p_FromDate, p_ToDate);

                result.DataResult = dataResult.OrderBy(x => x.Nama_Siswa).ThenBy(x => x.JTempo).ThenBy(x => x.DatePaid).ToList();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetPosBiayaPaid(int p_PosBiayaId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                PosBiayaPaidModel dataResult = null;
                var posBiaya = PosBiayaRepo.GetPosBiaya(p_PosBiayaId);

                if (posBiaya != null && posBiaya.Paid != null)
                {
                    dataResult = Mapper.Map<PosBiayaPaidModel>(posBiaya.Paid);
                }

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdatePosBiaya(PosBiayaModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                PosBiayaRepo.UpdatePosBiaya(entity);

                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult DeletePosBiayaPaid(PosBiayaPaidModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                PosBiayaPaidRepo.DeletePosBiayaPaid(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult AddPosBiayaPaidCancel(PosBiayaPaidCancelModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                PosBiayaPaidCancelRepo.AddPosBiayaPaidCancel(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
