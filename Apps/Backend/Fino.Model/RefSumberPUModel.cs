﻿using Fino.Lib.Core;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Fino.Model
{
    public class RefSumberPUModel : BaseModel
    {
        public static readonly string SPUNamaPropertyName = "Nama";
        public static readonly string SPUAlamatPropertyName = "Alamat";
        public static readonly string SPUKelasCodePropertyName = "KelasCode";
        public static readonly string SPUKelasIdPropertyName = "KelasId";

        public int Id { get; set; }

        private string _Nama;
        [Required]
        public string Nama
        {
            get
            {
                return _Nama;
            }
            set
            {
                _Nama = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(SPUNamaPropertyName));
            }
        }

        private string _Alamat;
        [Required]
        public string Alamat
        {
            get
            {
                return _Alamat;
            }
            set
            {
                _Alamat = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(SPUAlamatPropertyName));
            }
        }        

        private string _KelasCode;
        
        public string KelasCode
        {
            get
            {
                return _KelasCode;
            }
            set
            {
                _KelasCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(SPUKelasCodePropertyName));
            }
        }

        private int? _KelasId;

        public int? KelasId
        {
            get
            {
                return _KelasId;
            }
            set
            {
                _KelasId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(SPUKelasIdPropertyName));
            }
        }
    }
}