﻿
namespace Fino.Lib.Resource
{
    public enum RepetisiEnum
    {
        NONE = 0,
        WEEKLY = 1,
        MONTHLY = 2,
        YEARLY = 3
    }
}
