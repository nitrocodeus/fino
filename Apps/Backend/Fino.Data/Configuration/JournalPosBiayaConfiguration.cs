﻿using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class JournalPosBiayaConfiguration
    {
        internal static void ConfigureJournalPosBiaya(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JournalPosBiaya>()
                .HasRequired(e => e.PosOfBiaya)
                .WithMany(e => e.JournalPosBiayas)
                .HasForeignKey(e => e.PosBiaya_Id);

            modelBuilder.Entity<JournalPosBiaya>().HasKey(e => e.journal_id);
            modelBuilder.Entity<JournalPosBiaya>()
                .HasRequired(e => e.Journal)
                .WithOptional(e => e.PosBiaya);
        }
    }
}
