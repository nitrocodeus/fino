﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class RefSumberPenerimaanUmum
    {
        public int refsumberpenerimaan_id { get; set; }
        public string refsumberpenerimaan_name { get; set; }
        public string refsumberpenerimaan_alamat { get; set; }
        public int? refsumberpenerimaan_kelasid { get; set; }        

        public RefKelas Kelas { get; set; }
        public virtual ICollection<PenerimaanUmum> PenerimaanUmums { get; set; }
        public virtual ICollection<NotaPenerimaanUmum> NotaPenerimaanUmums { get; set; }
    }
}
