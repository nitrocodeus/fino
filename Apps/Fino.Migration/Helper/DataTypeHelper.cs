﻿using Fino.Migration.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Migration.Helper
{
    public class DataTypeHelper
    {
        #region Singleton
        private static readonly Lazy<DataTypeHelper> lazy = new Lazy<DataTypeHelper>(() => new DataTypeHelper());

        public static DataTypeHelper Instance { get { return lazy.Value; } }

        private DataTypeHelper() { }
        #endregion

        public object ConvertToDataType(string data, string dataType)
        {
            switch(dataType)
            {
                case DataTypeConstants.BOOL:
                    return Convert.ToBoolean(data);
                case DataTypeConstants.INTEGER:
                    return Convert.ToInt32(data);
                case DataTypeConstants.LONG:
                    return Convert.ToInt64(data);
                case DataTypeConstants.FLOAT:
                    return Convert.ToSingle(data);
                case DataTypeConstants.DOUBLE:
                    return Convert.ToDouble(data);
                case DataTypeConstants.DECIMAL:
                    return Convert.ToDecimal(data);
                case DataTypeConstants.DATETIME:
                    return Convert.ToDateTime(data);
                default:
                    return data;
            }
        }
    }
}
