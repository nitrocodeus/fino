﻿
namespace Fino.Datalib.Entity
{
    public class GLAccount
    {
        public int glaccount_id { get; set; }
        public int gl_id { get; set; }
        public int accountclassification_id { get; set; }
        public int account_id { get; set; }
        public int factor { get; set; }
        public double amount { get; set; }

        public virtual RefGL GLHeader { get; set; }
        public virtual JournalGLAccount JournalAccountGL { get; set; }
        public virtual RefAccount Account { get; set; }
    }
}
