﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefPayableConfiguration
    {
        internal static void ConfigureRefPayable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefPayable>().HasKey(e => e.refpayable_id);
            modelBuilder.Entity<RefPayable>().Property(e => e.refpayable_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<RefPayable>().Property(e => e.code).HasMaxLength(10);
            modelBuilder.Entity<RefPayable>().Property(e => e.name).HasMaxLength(70);
        }
    }
}
