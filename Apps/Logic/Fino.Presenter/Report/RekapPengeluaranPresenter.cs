﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class RekapPengeluaranPresenter : BasePresenter
    {
        IRepRekapPengeluaranView _view;
        IPayableProcess _process;

        public RekapPengeluaranPresenter(IRepRekapPengeluaranView p_view, IPayableProcess p_process)
            : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.BtnOkClicked += OkBtnClicked;
            _view.BtnTutupClicked += BtnTutupClicked;
        }

        public void BtnTutupClicked()
        {
            _view.Close();
        }

        public void OkBtnClicked()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _process.GetRekapitulasiPengeluaran(_view.FromDate, _view.ToDate)),
                AppResource.MSG_REPORT_REKAP_PENGELUARAN);

            if (result.IsSucess)
            {
                _view.ShowRPP(result.DataResult as List<RekapitulasiPengeluaranModel>);
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            } 
        }
    }
}
