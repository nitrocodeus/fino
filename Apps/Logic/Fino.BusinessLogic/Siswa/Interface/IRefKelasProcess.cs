﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public interface IRefKelasProcess
    {
        IRefKelasRepository RefKelasRepository { get; set; }

        ProcessResult GetAllRefKelas();
        ProcessResult GetAllRefKelasByTingkat(int tingkat);
        ProcessResult GetRefKelas(int kelasId);
        ProcessResult UpdateRefKelas(RefKelasModel entity);
        ProcessResult DeleteRefKelas(RefKelasModel entity);
        ProcessResult GetTingkatDataSource();
    }
}
