﻿using System.Collections.Generic;

namespace Fino.Datalib.Entity
{
    public class RefAccGroup
    {
        public int accgroup_id { get; set; }
        public string accgroup_name { get; set; }

        public virtual ICollection<GroupAccount> GroupOfAccounts { get; set; }
        public virtual ICollection<GLAccount> GLAccounts { get; set; } 
    }
}
