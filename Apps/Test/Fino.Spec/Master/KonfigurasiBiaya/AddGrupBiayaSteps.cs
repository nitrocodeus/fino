﻿using Fino.BusinessLogic;
using Fino.Cient;
using Fino.Model;
using Fino.Presenter;
using Fino.View;
using System;
using System.Linq;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Fino.Spec.Master.KonfigurasiBiaya
{
    [Binding]
    public class AddGrupBiayaSteps
    {
        IEditGrupBiayaView _editGrupBiayaView = new EditGrupBiayaView();
        IGrupBiayaView _grupBiayaView = new GrupBiayaView();
        IKonfigurasiBiayaProcess _process = new KonfigurasiBiayaProcess();
        EditGrupBiayaPresenter _presenter;
        GrupBiayaPresenter _grupBiayaPresenter;

        [Given(@"I have input the data in grup biaya form")]
        public void GivenIHaveInputTheDataInGrupBiayaForm()
        {
            _presenter = new EditGrupBiayaPresenter(_editGrupBiayaView, _process);
            _grupBiayaPresenter = new GrupBiayaPresenter(_grupBiayaView, _process);

            _editGrupBiayaView.GrupBiayaView = _grupBiayaView;
            _editGrupBiayaView.ShowForm(null);                        
        }
        
        [Given(@"all fields are valid")]
        public void GivenAllFieldsAreValid()
        {
            _editGrupBiayaView.Nama = "ValidName";
            _editGrupBiayaView.Kode = "ValidKode";
        }
        
        [When(@"I click save")]
        public void WhenIClickSave()
        {
            _editGrupBiayaView.RaiseBtnSimpanClicked();
        }
        
        [Then(@"it should save the grup biaya data to the database")]
        public void ThenItShouldSaveTheGrupBiayaDataToTheDatabase()
        {
            List<GrupBiayaModel> loadedData = _grupBiayaView.GetGVGrupBiayaDataSource();

            GrupBiayaModel grupBiaya = (from a in loadedData
                                        where a.Code.Equals(_editGrupBiayaView.Kode) && a.Nama.Equals(_editGrupBiayaView.Nama)
                                        select a).FirstOrDefault();

            Assert.IsNotNull(grupBiaya);            
        }
        
        [Then(@"reload the grup biaya datagrid")]
        public void ThenReloadTheGrupBiayaDatagrid()
        {
            List<GrupBiayaModel> loadedData = _grupBiayaView.GetGVGrupBiayaDataSource();

            Assert.IsNotNull(loadedData);
            Assert.IsTrue(loadedData.Count > 0);
        }
    }
}
