﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class LaporanPengeluaranModel
    {
        public DateTime Tanggal { get; set; }
        public string JenisPengeluaran { get; set; }
        public string Deskripsi { get; set; }
        public double Nilai { get; set; }
    }
}
