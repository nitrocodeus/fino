﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;

namespace Fino.BusinessLogic
{
    public class UserManagementProcess : IUserManagementProcess
    {        
        public IUserManagementRepository UserManagementRepo { get; set; }

        public Lib.Core.ProcessResult GetUserForManagement(int userid)
        {
            ProcessResult result = new ProcessResult();

            try
            {                
                result.DataResult = UserManagementRepo.GetUserForManagement(userid);
                result.IsSucess = true;
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public Lib.Core.ProcessResult GetAllUserForManagement()
        {
            ProcessResult result = new ProcessResult();

            try
            {                
                result.DataResult = UserManagementRepo.GetAllUserForManagement();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public Lib.Core.ProcessResult UpdateUser(UserManagementModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {                
                UserManagementRepo.UpdateUser(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public Lib.Core.ProcessResult DeleteUser(Model.UserManagementModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                UserManagementRepo.DeleteUser(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
