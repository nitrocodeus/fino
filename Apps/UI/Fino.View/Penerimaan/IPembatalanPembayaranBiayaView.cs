﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IPembatalanPembayaranBiayaView : IView
    {
        event Action BtnCariClicked;
        event Action BtnTutupClicked;
        event Action BtnBatalClicked;
        event Action BtnAllClicked;
        event Action BtnResetClicked;

        DateTime DateFrom { get; }
        DateTime DateTo { get; }

        void SetData(List<PembatalanPenerimaanPelunasanModel> p_ListPosBiaya);
        void RefreshDataGrid();
        void SelectAllItem();
        void UnselectAllItem();
        List<PembatalanPenerimaanPelunasanModel> GetSelectedPosBiaya();
        List<PembatalanPenerimaanPelunasanModel> GetGVPosBiayaDataSource();
    }
}
