﻿using System;
using System.Collections.Generic;

namespace Fino.Datalib.Entity
{
    public class JournalHeader
    {
        public int journal_id { get; set; }
        public int period_id { get; set; }
        public DateTime journal_date { get; set; }
        public string journal_name { get; set; }
        public int status { get; set; }
        public bool is_generated { get; set; }
        /// <summary>
        /// PIUTANG; HUTANG; INCOME
        /// </summary>
        public int AccountClassificationId { get; set; } // 
        
        /// <summary>
        /// Id of the postbiaya / postbiayapaid
        /// </summary>
        public int sourceid { get; set; } // 

        public virtual ICollection<JournalDetail> Details { get; set; }
        public virtual RefAccPeriod AccountPeriod { get; set; }
        public virtual JournalPosBiaya PosBiaya { get; set; }
    }
}
