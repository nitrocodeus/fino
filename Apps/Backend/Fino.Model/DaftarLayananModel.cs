using Fino.Lib.Core;
using Fino.Lib.Core.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class DaftarLayananModel : BaseModel
    {
        const string DAFTARLAYANAN_PILIHSISWA = "VLD_DAFTARLAYANAN_PILIHSISWA";
        const string DAFTARLAYANAN_PILIHLAYANAN = "VLD_DAFTARLAYANAN_PILIHLAYANAN";
        const string DAFTARLAYANAN_NILAIRANGE = "VLD_DAFTARLAYANAN_NILAIRANGE";
        const string DAFTARLAYANAN_TANGGALAKHIRLEBIHKECIL = "VLD_DAFTARLAYANAN_TANGGALAKHIRLEBIHKECIL";

        public static readonly string SiswaIdPropertyName = "SiswaId";
        public static readonly string LayananIdPropertyName = "LayananId";
        public static readonly string BiayaIdPropertyName = "BiayaId";
        public static readonly string KodeLayananPropertyName = "KodeLayanan";
        public static readonly string NilaiPropertyName = "Nilai";
        public static readonly string MulaiTanggalPropertyName = "MulaiTanggal";
        public static readonly string HinggaTanggalPropertyName = "HinggaTanggal";

        int _siswaId;
        int _layananId;
        int _biayaId;
        string _kodeLayanan;
        decimal _nilai;
        DateTime _mulaiTanggal;
        DateTime _hinggaTanggal;

        [DisplayName("Pilih Siswa")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = DAFTARLAYANAN_PILIHSISWA)]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = DAFTARLAYANAN_PILIHSISWA)]
        public int SiswaId
        {
            get { return _siswaId; }
            set { _siswaId = value; }
        }

        [DisplayName("Pilih Layanan")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = DAFTARLAYANAN_PILIHLAYANAN)]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = DAFTARLAYANAN_PILIHLAYANAN)]
        public int LayananId
        {
            get { return _layananId; }
            set 
            { 
                _layananId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(LayananIdPropertyName));
                ValidateProperty(value, LayananIdPropertyName);
            }
        }

        public string KodeLayanan
        {
            get { return _kodeLayanan; }
            set 
            { 
                _kodeLayanan = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(KodeLayananPropertyName));
                ValidateProperty(value, KodeLayananPropertyName);
            }
        }

        [DisplayName("Biaya")]
        public int BiayaId
        {
            get { return _biayaId; }
            set { _biayaId = value; }
        }

        [DisplayName("Nilai")]
        [Required]
        [Range(1, double.MaxValue, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = DAFTARLAYANAN_NILAIRANGE)]
        public decimal Nilai
        {
            get { return _nilai; }
            set 
            { 
                _nilai = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NilaiPropertyName));
                ValidateProperty(value, NilaiPropertyName);
            }
        }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime MulaiTanggal
        {
            get { return _mulaiTanggal; }
            set 
            { 
                _mulaiTanggal = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(MulaiTanggalPropertyName));
                ValidateProperty(value, MulaiTanggalPropertyName);
            }
        }

        [Required]
        [DataType(DataType.DateTime)]
        [DateTimeCompare("MulaiTanggal", ValueComparison.IsGreaterThan, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = DAFTARLAYANAN_TANGGALAKHIRLEBIHKECIL)]
        public DateTime HinggaTanggal
        {
            get { return _hinggaTanggal; }
            set 
            { 
                _hinggaTanggal = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(HinggaTanggalPropertyName));
                ValidateProperty(value, HinggaTanggalPropertyName);
            }
        }
    }
}
