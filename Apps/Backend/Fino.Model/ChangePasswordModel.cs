﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class ChangePasswordModel : BaseModel
    {
        const string USERNAME_NOTEMPTY = "VLD_USERNAME_NOTEMPTY";
        const string USERNAME_LENGTH = "VLD_USERNAME_LENGTH";
        const string OLDPASSWORD_NOTEMPTY = "VLD_OLDPASSWORD_NOTEMPTY";
        const string OLDPASSWORD_LENGTH = "VLD_OLDPASSWORD_LENGTH";
        const string NEWPASSWORD_NOTEMPTY = "VLD_NEWPASSWORD_NOTEMPTY";
        const string NEWPASSWORD_LENGTH = "VLD_NEWPASSWORD_LENGTH";
        const string REPEATNEWPASSWORD_NOTEMPTY = "VLD_REPEATNEWPASSWORD_NOTEMPTY";
        const string REPEATNEWPASSWORD_LENGTH = "VLD_REPEATNEWPASSWORD_LENGTH";

        public static readonly string UsernamePropertyName = "Username";
        public static readonly string OldPasswordPropertyName = "OldPassword";
        public static readonly string NewPasswordPropertyName = "NewPassword";
        public static readonly string RepeatNewPasswordPropertyName = "RepeatNewPassword";

        string _username;
        string _oldPassword;
        string _newPassword;
        string _repeatNewPassword;
        
        [Display(Name="Username")]
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(UsernamePropertyName));
                ValidateProperty(value, UsernamePropertyName);
            }
        }

        [Display(Name="Old Password")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = OLDPASSWORD_NOTEMPTY)]
        [StringLength(100, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = OLDPASSWORD_LENGTH)]
        public string OldPassword
        {
            get
            {
                return _oldPassword;
            }
            set
            {
                _oldPassword = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(OldPasswordPropertyName));
                ValidateProperty(value, OldPasswordPropertyName);
            }
        }

        [Display(Name="New Password")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NEWPASSWORD_NOTEMPTY)]
        [StringLength(100, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NEWPASSWORD_LENGTH)]
        public string NewPassword
        {
            get
            {
                return _newPassword;
            }
            set
            {
                _newPassword = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NewPasswordPropertyName));
                ValidateProperty(value, NewPasswordPropertyName);
            }
        }

        [Display(Name="Repeat New Password")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = REPEATNEWPASSWORD_NOTEMPTY)]
        [StringLength(100, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = REPEATNEWPASSWORD_LENGTH)]
        public string RepeatNewPassword
        {
            get
            {
                return _repeatNewPassword;
            }
            set
            {
                _repeatNewPassword = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(RepeatNewPasswordPropertyName));
                ValidateProperty(value, RepeatNewPasswordPropertyName);
            }
        }
    }
}
