﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class ReportPenerimaanDetailPresenter : BasePresenter
    {
        IReportPenerimaanView _view;
        IPosBiayaProcess _process;

        public ReportPenerimaanDetailPresenter(IReportPenerimaanView p_view, IPosBiayaProcess p_process)
            : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.BtnOkClicked += OkBtnClicked;
            _view.BtnTutupClicked += BtnTutupClicked;
            _view.Load += _view_Load;
            _view.CmbKelasSelected += _view_CmbKelasSelected;
        }

        void _view_CmbKelasSelected()
        {
            int kelasId = _view.SelectedKelas;

            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _process.GetSiswaUntukKelas(kelasId)),
                        "Memuat daftar siswa");

            if (result.IsSucess)
            {
                _view.Siswa = result.DataResult as List<ValueList>;
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            } 
        }

        void _view_Load(object sender, EventArgs e)
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _process.GetAllKelas()),
                        "Memuat daftar kelas");

            if (result.IsSucess)
            {
                _view.Kelas = result.DataResult as List<ValueList>;
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            } 
        }

        public void BtnTutupClicked()
        {
            _view.Close();
        }

        public void OkBtnClicked()
        {
            int kelas = _view.SelectedKelas;
            int siswa = _view.SelectedSiswa;

            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _process.GetLaporanPenerimaanDetail(_view.DateFrom, _view.DateTo, kelas, siswa)),
                AppResource.MSG_REPORT_PENERIMAAN);

            if (result.IsSucess)
            {
                _view.ShowRPP(result.DataResult as List<PenerimaanHistoryModel>);
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            } 
        }
    }
}
