﻿using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IKonfigurasiBiayaView : IView
    {        
        event Action BtnTambahRefBiayaClick;
        event Action BtnHapusRefBiayaClick;
        event Action BtnTutupClick;
        event Action BtnSimpanDataClick;
        event Action BtnHapusSemuaRefBiayaData;
        event Action BtnTambahSemuaRefBiayaData;
        //event Action ReloadData;

        GrupBiayaModel SelectedGrupBiaya { get; }
        IGrupBiayaView GrupBiayaView { get; set; }

        void SetRefBiayaMasterGrid(List<RefBiayaModel> p_ListPosBiaya);
        void SetRefBiayaSelectedGrid(List<RefBiayaModel> p_ListPosBiaya);
        void RefreshRefBiayaMasterDataGrid();
        void RefreshRefBiayaSelectedDataGrid();
        void ReloadDataEvent();
        List<RefBiayaModel> GetGVRefBiayaMasterDataSource();
        List<RefBiayaModel> GetGVRefBiayaSelectedDataSource();
        RefBiayaModel GetSelectedRefBiayaMaster();
        RefBiayaModel GetSelectedRefBiayaSelected();   
        void ShowForm(GrupBiayaModel p_SelectedGrupBiaya);

        void RaiseBtnTambahRefBiaya();
        void RaiseBtnTambahSemuaRefBiaya();
        void RaiseBtnHapusRefBiaya();
        void RaiseBtnHapusSemuaRefBiaya();
        void RaiseBtnSimpan();
    }
}
