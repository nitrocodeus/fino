﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core.Interface;

namespace Fino.Model
{
    public class TahunAjaranModel : ITahunAjaranModel
    {
        int _tahunAjaranId;
        string _nama;
        int _mulaiBulan;
        int _mulaiTahun;
        int _hinggaBulan;
        int _hinggaTahun;
        int _semCawu;
        int _status;

        public int TahunAjaranId
        {
            get { return _tahunAjaranId; }
            set { _tahunAjaranId = value; }
        }

        public string Nama
        {
            get { return _nama; }
            set { _nama = value; }
        }        

        public int MulaiBulan
        {
            get { return _mulaiBulan; }
            set { _mulaiBulan = value; }
        }        

        public int MulaiTahun
        {
            get { return _mulaiTahun; }
            set { _mulaiTahun = value; }
        }        

        public int HinggaBulan
        {
            get { return _hinggaBulan; }
            set { _hinggaBulan = value; }
        }        

        public int HinggaTahun
        {
            get { return _hinggaTahun; }
            set { _hinggaTahun = value; }
        }        

        public int SemCawu
        {
            get { return _semCawu; }
            set { _semCawu = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }
    }
}
