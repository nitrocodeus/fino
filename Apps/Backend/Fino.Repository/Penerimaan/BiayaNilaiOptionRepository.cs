﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Model;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class BiayaNilaiOptionRepository : BaseRepository, IBiayaNilaiOptionRepository
    {
        public void UpdateNilaiBiaya(BiayaNilaiOption nilaiBiaya)
        {
            var existingNilaiBiaya = GetExistingNilaiBiaya(new RefBiaya { biaya_id = nilaiBiaya.biaya_id }, nilaiBiaya.option);
            if (existingNilaiBiaya == null)
            {
                Context.BiayaNilaiOptions.Add(nilaiBiaya);
            }
            else
            {
                existingNilaiBiaya.nilai = nilaiBiaya.nilai;
            }

            Context.SaveChanges();
        }

        public BiayaNilaiOption GetExistingNilaiBiaya(Datalib.Entity.RefBiaya pBiaya, int pOption)
        {
            return Context.BiayaNilaiOptions.Where(e => e.biaya_id.Equals(pBiaya.biaya_id) && e.option.Equals(pOption)).FirstOrDefault();
        }

        public PengelolaanNilaiBiayaModel GetNilaiBiaya(Datalib.Entity.RefBiaya pBiaya, int pOption)
        {
            var nBiaya = GetExistingNilaiBiaya(pBiaya, pOption);
            if (nBiaya != null)
            {
                return new PengelolaanNilaiBiayaModel
                {
                    BiayaNilaiOption_Id = nBiaya.biayanilaioption_id,
                    Biaya_Id = nBiaya.biaya_id,
                    Option = nBiaya.option,
                    Nilai = nBiaya.nilai
                };
            }

            return null;
        }

        public int GetNilaiBiayaInt(Datalib.Entity.RefBiaya pBiaya, int pOption)
        {
            var nilaiBiaya = Context.BiayaNilaiOptions.Where(e => e.biaya_id.Equals(pBiaya.biaya_id)).ToList();
            if (nilaiBiaya.Count.Equals(1))
            {
                return nilaiBiaya.First().nilai;
            }
            else
            {
                var nBiaya = nilaiBiaya.Where(e => e.option.Equals(pOption)).FirstOrDefault();
                if (nBiaya != null)
                {
                    return nBiaya.nilai;
                }
            }
            return 0;
        }

        public void DeleteNilaiBiaya(BiayaNilaiOption nilaiBiaya)
        {
            var existingNilaiBiaya = GetExistingNilaiBiaya(new RefBiaya { biaya_id = nilaiBiaya.biaya_id }, nilaiBiaya.option);

            if (existingNilaiBiaya != null)
            {
                existingNilaiBiaya.nilai = 0;
                Context.SaveChanges();
            }
        }
    }
}
