﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Fino.BusinessLogic
{
    public class RefAccPeriodProcess : IRefAccPeriodProcess
    {
        public IRefAccPeriodRepository RefAccPeriodRepo { get; set; }

        private bool ValidateData(RefAccPeriodModel entity, bool addNew)
        {
            if (entity.Period_end < DateTime.Today)
            {
                throw new ApplicationException(AppResource.VLD_ACCPERIOD_PERIODEND);
            }
            
            if (addNew)
            {
                var allPeriod = GetAllRefAccPeriodModel().DataResult as List<RefAccPeriodModel>;
                var overlappedPeriod = allPeriod.Where(x => entity.Period_start < x.Period_end && entity.Period_end > x.Period_start).ToList();

                if (overlappedPeriod.Count > 0)
                {
                    throw new ApplicationException(AppResource.VLD_ACCPERIOD_OVERLAP);
                }
            }

            return true;
        }

        public ProcessResult UpdateRefAccPeriod(RefAccPeriodModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                bool addNew = RefAccPeriodRepo.GetRefAccPeriod(entity.Period_id) == null;

                ValidateData(entity, addNew);

                RefAccPeriodRepo.UpdateRefAccPeriod(entity);

                result.IsSucess = true;

                RefAccPeriodRepo.DisposeDBContext();
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdateRefAccPeriodStatus(int period_id, int status)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var existingRefAccPeriod = RefAccPeriodRepo.GetRefAccPeriod(period_id);

                var IsActivation = status.Equals((int)RefAccPeriodStatusEnum.Opened);
                if (existingRefAccPeriod != null)
                {
                    if (IsActivation)
                    {
                        RefAccPeriodRepo.UpdateStatusAccPeriodAndImportBalance(new RefAccPeriodModel
                        {
                            Period_id = period_id,
                            Period_name = existingRefAccPeriod.Period_name,
                            Period_start = existingRefAccPeriod.Period_start,
                            Period_end = existingRefAccPeriod.Period_end,
                            Status = status
                        });
                    }
                    else
                    {
                        RefAccPeriodRepo.UpdateRefAccPeriod(new RefAccPeriodModel
                            {
                                Period_id = period_id,
                                Period_name = existingRefAccPeriod.Period_name,
                                Period_start = existingRefAccPeriod.Period_start,
                                Period_end = existingRefAccPeriod.Period_end,
                                Status = status
                            });
                    }
                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetRefAccPeriodModel(int period_id)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = RefAccPeriodRepo.GetRefAccPeriod(period_id);
                result.IsSucess = true;
                RefAccPeriodRepo.DisposeDBContext();
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllRefAccPeriodModel()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = RefAccPeriodRepo.GetAllRefAccPeriod();
                result.IsSucess = true;
                RefAccPeriodRepo.DisposeDBContext();
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
