﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using Fino.Datalib.Dbmodel;

namespace Fino.BusinessLogic
{
    public class PosBiayaProcess : IPosBiayaProcess
    {
        public IPosBiayaRepository PosBiayaRepo { get; set; }

        public ProcessResult GetLaporanKasDetail(DateTime p_ToDate, int p_KelasId, int p_SiswaId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<LaporanKasDetailModel> dataResult = PosBiayaRepo.GetLaporanKasDetail(p_ToDate, p_KelasId, p_SiswaId);

                result.DataResult = dataResult;
                result.IsSucess = true;                
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetRekapitulasiPiutangDanPenerimaan(DateTime p_FromDate, DateTime p_ToDate)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<RekapitulasiPiutangDanPenerimaanModel> dataResult = PosBiayaRepo.GetRekapitulasiPiutangDanPenerimaan(p_FromDate, p_ToDate);

                result.DataResult = dataResult;
                result.IsSucess = true;                
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetPosBiaya(int p_SiswaId, bool p_ShowAllBiaya)
        {
            ProcessResult result = new ProcessResult();
            if (PosBiayaRepo == null) PosBiayaRepo = new PosBiayaRepository();

            try
            {
                List<PosBiaya> dataResult = null;
                List<PosBiayaModel> dataResultAll = null;                

                if (!p_ShowAllBiaya)
                {
                    dataResult = PosBiayaRepo.GetNotPaidCurrentPosBiayaSiswa(p_SiswaId);

                    if (dataResult != null)
                    {
                        List<PosBiayaModel> dataModel = dataResult.Select(x => Mapper.Map<PosBiayaModel>(x)).ToList();

                        result.DataResult = dataModel;
                        result.IsSucess = true;
                    }
                    else
                    {
                        throw new Exception(AppResource.MSG_GAGAL_MEMUAT_DATA);
                    }
                }
                else
                {
                    dataResultAll = PosBiayaRepo.GetNextNotPaidAllPosBiayaSiswa(p_SiswaId);

                    if (dataResultAll != null)
                    {
                         var dataResultAllTemp = dataResultAll.Where(x => 
                             (x.PosBiayaId == 0 && !x.NoPosBiaya.Value) || 
                             (x.PosBiayaId == 0 && x.LayananId > 0)).ToList();

                         foreach (PosBiayaModel item in dataResultAllTemp)
                         {
                             dataResultAll.Remove(item);
                         }

                        result.DataResult = dataResultAll;
                        result.IsSucess = true;
                    }
                    else
                    {
                        throw new Exception(AppResource.MSG_GAGAL_MEMUAT_DATA);
                    }
                }                
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdatePembayaranPosBiaya(List<PosBiayaModel> p_PosBiayaModel, int p_SiswaId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<PosBiayaPaid> dataResult = PosBiayaRepo.UpdatePembayaranPosBiaya(p_PosBiayaModel, p_SiswaId);

                List<PosBiayaPaidModel> pbpms = dataResult.Select(x => Mapper.Map<PosBiayaPaidModel>(x)).ToList();                

                result.IsSucess = true;
                result.DataResult = pbpms;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult SimpanCetakPembayaran(CetakPembayaranModel p_CetakPembayaranModel, 
            List<PosBiayaPaidModel> p_PosBiayaPaidModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                CetakPembayaranModel dataResult = PosBiayaRepo.SimpanCetakPembayaran(p_CetakPembayaranModel, p_PosBiayaPaidModel);                

                result.IsSucess = true;
                result.DataResult = dataResult;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllKelas()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<RefKelas> dataResult = PosBiayaRepo.GetAllKelas();

                List<ValueList> vlistResult = ConvertRefKelasToValueList(dataResult);

                result.IsSucess = true;
                result.DataResult = vlistResult;

                PosBiayaRepo.DisposeDBContext();
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetSiswaUntukKelas(int p_KelasId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<SiswaKelas> dataResult = PosBiayaRepo.GetSiswaByKelas(p_KelasId);

                List<ValueList> vlistResult = ConvertSiswaKelasToValueList(dataResult);

                result.IsSucess = true;
                result.DataResult = vlistResult;

                PosBiayaRepo.DisposeDBContext();
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        private List<ValueList> ConvertSiswaKelasToValueList(List<SiswaKelas> dataResult)
        {
            List<ValueList> result = new List<ValueList>();

            foreach (SiswaKelas item in dataResult)
            {
                ValueList temp = new ValueList();

                temp.Id = item.siswa_id;
                temp.Value = item.Siswa.nama;

                result.Add(temp);
            }
            
            return result;
        }

        private List<ValueList> ConvertRefKelasToValueList(List<RefKelas> dataResult)
        {
            List<ValueList> result = new List<ValueList>();

            foreach (RefKelas item in dataResult)
            {
                ValueList temp = new ValueList();

                temp.Id = item.kelas_id;
                temp.Value = item.nama;

                result.Add(temp);
            }

            return result;
        }

        public ProcessResult GetLaporanPenerimaanDetail(DateTime p_FromDate, DateTime p_ToDate, int p_KelasId, int p_SiswaId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<PenerimaanHistoryModel> dataResult = PosBiayaRepo.GetLaporanPenerimaanDetail(p_FromDate, p_ToDate, p_KelasId, p_SiswaId);

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
