﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Migration.Constants
{
    public static class XmlConstants
    {
        public const string PARENT_ELEMENT_NAME = "MigrationConfig";

        public const string CONNECTION_STRING_ELEMENT_NAME = "ConnectionString";
        public const string CONNECTION_STRING_DATABASE_ATTR_NAME = "Database";

        public const string MIGRATION_ELEMENT_NAME = "Migration";
        public const string MIGRATION_ELEMENT_TYPE_ATTR_NAME = "Type";
        public const string MIGRATION_ELEMENT_SOURCE_FILE_ATTR_NAME = "SourceFile";

        public const string MAPPINGS_ELEMENT_NAME = "Mappings";
        public const string MAPPINGS_ELEMENT_TABLE_ATTR_NAME = "Table";

        public const string MAPPING_ELEMENT_NAME = "Mapping";
        public const string MAPPING_ELEMENT_INDEX_ATTR_NAME = "Index";
        public const string MAPPING_ELEMENT_SOURCE_COLUMN_ATTR_NAME = "SourceColumn";
        public const string MAPPING_ELEMENT_TARGET_COLUMN_ATTR_NAME = "TargetColumn";
        public const string MAPPING_ELEMENT_DATA_TYPE_ATTR_NAME = "DataType";
        public const string MAPPING_ELEMENT_CONVERTER = "Converter";
        public const string MAPPING_ELEMENT_CONVERTER_SOURCE_VALUE_ATTR_NAME = "SourceValue";
        public const string MAPPING_ELEMENT_CONVERTER_TARGET_VALUE_ATTR_NAME = "TargetValue";
    }
}
