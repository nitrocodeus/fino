using Fino.Datalib.Dbmodel;
using Fino.Model;
using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class LayananRepository : BaseRepository, ILayananRepository
    {
        public List<RefLayanan> GetAllLayanan()
        {
            return Context.RefLayanans.ToList();
        }
        
        public bool CheckPendaftaranLayanan(DaftarLayananModel p_model)
        {
            bool result = false;

            AgtLayanan agtLayanan = (from a in Context.AgtLayanans
                                     where a.siswa_id == p_model.SiswaId && a.layanan_id == p_model.LayananId
                                     && p_model.MulaiTanggal < a.hingga_tanggal
                                     select a).FirstOrDefault();

            if (agtLayanan != null)
            {
                throw new Exception(string.Format("Siswa sudah terdaftar untuk periode {0} sampai {1}", 
                    agtLayanan.mulai_tanggal.ToString("MMMM/yyyy"),
                    agtLayanan.hingga_tanggal.ToString("MMMM/yyyy")));
            }

            return result;
        }

        public DaftarLayananModel SimpanDaftarLayanan(DaftarLayananModel p_model)
        {
            DaftarLayananModel result = p_model;

            IPosBiayaRepository posBiayaRepo = new PosBiayaRepository();

            RefBiaya biaya = Context.RefBiayas.Find(p_model.BiayaId);
            RefSiswa siswa = Context.RefSiswas.Find(p_model.SiswaId);
            RefLayanan layanan = Context.RefLayanans.Find(p_model.LayananId);

            AgtLayanan agtLayanan = new AgtLayanan();

            agtLayanan.layanan_id = p_model.LayananId;
            agtLayanan.siswa_id = p_model.SiswaId;
            agtLayanan.tgl_daftar = DateTime.Today;
            agtLayanan.mulai_tanggal = p_model.MulaiTanggal;
            agtLayanan.hingga_tanggal = p_model.HinggaTanggal;
            agtLayanan.nilai = (double)p_model.Nilai;
            agtLayanan.Layanan = layanan;
            agtLayanan.Siswa = siswa;

            posBiayaRepo.Context = Context;
            posBiayaRepo.GeneratePosBiayaLayanan(p_model.MulaiTanggal.Date, p_model.HinggaTanggal.Date, agtLayanan, biaya, siswa);

            Context.AgtLayanans.Add(agtLayanan);
            foreach (var p in siswa.PosBiayas)
            {
                if (p.Biaya_Id == 0)
                {
                    Context.PosBiayas.Add(p);
                }
            }

            Context.SaveChanges();

            return result;
        }
    }
}
