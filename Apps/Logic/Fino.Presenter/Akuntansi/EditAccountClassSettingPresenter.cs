﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.View.Akuntansi;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class EditAccountClassSettingPresenter : BasePresenter
    {
        IEditAccountClassSettingView _editAccountClassSettingView;
        IAccountClassSettingProcess _accountClassSettingProcess;

        public EditAccountClassSettingPresenter(IEditAccountClassSettingView p_View, IAccountClassSettingProcess p_Process)
            : base(p_View)
        {
            this._editAccountClassSettingView = p_View;
            this._accountClassSettingProcess = p_Process;

            this._editAccountClassSettingView.Load += _editAccountClassSettingView_Load;
            this._editAccountClassSettingView.BtnSimpanClicked += _editAccountClassSettingView_BtnSimpanClicked;
            this._editAccountClassSettingView.BtnBatalClicked += _editAccountClassSettingView_BtnBatalClicked;
        }

        #region Control Events

        void _editAccountClassSettingView_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        void _editAccountClassSettingView_BtnBatalClicked()
        {
            this._editAccountClassSettingView.Close();
        }

        void _editAccountClassSettingView_BtnSimpanClicked()
        {
            var context = new ValidationContext(this._editAccountClassSettingView.SelectedAccountClassSetting, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(this._editAccountClassSettingView.SelectedAccountClassSetting, context, results, true);

            if (isValid)
            {
                Save();

                this._editAccountClassSettingView.SelectedAccountClassSetting.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                this._editAccountClassSettingView.SelectedAccountClassSetting.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        #endregion Control Events

        #region Private Methods

        private void LoadData()
        {
            this._editAccountClassSettingView.AccountIdDataSource = GetAccoundIdDataSource();
            this._editAccountClassSettingView.ParamIdDataSource = GetParamIdDataSource();
            this._editAccountClassSettingView.SetBinding();

            var accClassSettingEnum = (AccountClassificationEnum)Enum.Parse(typeof(AccountClassificationEnum), this._editAccountClassSettingView.SelectedAccountClassSetting.AccClassId.ToString());

            this._editAccountClassSettingView.AccClassId = (int)accClassSettingEnum;

        }

        private object GetAccoundIdDataSource()
        {
            return this._accountClassSettingProcess.GetAccountData().DataResult;
        }

        private object GetParamIdDataSource()
        {
            var accClassId = (AccountClassificationEnum)Enum.Parse(typeof(AccountClassificationEnum), this._editAccountClassSettingView.SelectedAccountClassSetting.AccClassId.ToString());
            object result = null;

            switch(accClassId)
            {
                case AccountClassificationEnum.KAS:
                    result = new List<object>(new object[] { new { Id = 1, Value= "1" } });
                    break;
                case AccountClassificationEnum.PIUTANG:
                case AccountClassificationEnum.PENDAPATAN:
                    result = this._accountClassSettingProcess.GetParamFromRefBiaya().DataResult;
                    break;
                case AccountClassificationEnum.BEBAN:
                    result = this._accountClassSettingProcess.GetParamFromRefPayable().DataResult;
                    break;
            }

            return result;
        }

        private void Save()
        {
            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => this._accountClassSettingProcess.UpdateAccountClassSetting(this._editAccountClassSettingView.SelectedAccountClassSetting)),
                        AppResource.MSG_SAVING_GRUP_BIAYA);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                this._editAccountClassSettingView.AccountClassSettingView.ReloadDataEvent();
                this._editAccountClassSettingView.Close();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        #endregion Private Methods
    }
}
