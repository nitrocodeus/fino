﻿using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace Fino.Cient.Configuration
{
    public class WindsorRepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //AssemblyFilter filter = new AssemblyFilter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ""));

            //container.AddFacility<LoggingFacility>(f => f.LogUsing(LoggerImplementation.Log4net).WithAppConfig().
            //    ToLog(ConfigurationManager.AppSettings["LogLevel"].ToString()));
            //log4net.Config.XmlConfigurator.Configure();

            //container.Register(Classes.FromAssemblyNamed("Fino.Datalib")
            //      .Where(c => c.IsClass &&
            //                    (c.Name.Equals("FinoDBContext")))
            //      .WithService.DefaultInterfaces()
            //      .LifestyleTransient()
            //    );

            container.Register(Classes.FromAssemblyNamed("Fino.Repository")
                  .Where(c => c.IsClass &&
                                (c.Name.Contains("Repository")) && (!c.Name.Equals("BaseRepository")))
                  .WithService.AllInterfaces()
                  .LifestyleTransient()                  
                );            
        }
    }
}
