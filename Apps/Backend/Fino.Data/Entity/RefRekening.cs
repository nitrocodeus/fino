﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class RefRekening
    {
        public int rekening_id { get; set; }
        public string rekening_code { get; set; }
        public DateTime tgl_buka { get; set; }
        public int status { get; set; }

        public virtual ICollection<MutasiRekening> Mutasi { get; set; }
        public virtual SiswaRekening RekeningOfSiswa { get; set; }
    }
}
