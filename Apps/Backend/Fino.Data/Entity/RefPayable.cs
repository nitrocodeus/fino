﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class RefPayable
    {
        public int refpayable_id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public bool aktif { get; set; }

        public virtual ICollection<Payable> Payables { get; set; }
    }
}
