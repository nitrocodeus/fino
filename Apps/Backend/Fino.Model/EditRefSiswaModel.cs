﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class EditRefSiswaModel : BaseModel
    {
        const string CODE_NOTEMPTY = "VLD_REFSISWACODE_NOTEMPTY";
        const string NAME_NOTEMPTY = "VLD_REFSISWANAME_NOTEMPTY";

        public static readonly string KodeSiswaPropertyName = "KodeSiswa";
        public static readonly string NamaSiswaPropertyName = "NamaSiswa";

        public int SiswaId { get; set; }
        public bool JeniKelamin { get; set; }

        private string _KodeSiswa;
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = CODE_NOTEMPTY)]
        public string KodeSiswa
        {
            get
            {
                return _KodeSiswa;
            }
            set
            {
                _KodeSiswa = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(KodeSiswaPropertyName));
                ValidateProperty(value, KodeSiswaPropertyName);
            }
        }

        private string _NamaSiswa;
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = CODE_NOTEMPTY)]
        public string NamaSiswa
        {
            get
            {
                return _NamaSiswa;
            }
            set
            {
                _NamaSiswa = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamaSiswaPropertyName));
                ValidateProperty(value, NamaSiswaPropertyName);
            }
        }
    }
}
