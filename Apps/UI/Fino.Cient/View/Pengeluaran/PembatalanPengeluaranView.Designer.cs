﻿namespace Fino.Cient
{
    partial class PembatalanPengeluaranView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PembatalanPengeluaranView));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblDari = new System.Windows.Forms.Label();
            this.btnTutup = new System.Windows.Forms.Button();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.lblSampai = new System.Windows.Forms.Label();
            this.btnCari = new System.Windows.Forms.Button();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.btnBatal = new System.Windows.Forms.Button();
            this.pengeluaranBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvPengeluaran = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.selectedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.payableIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refPayableIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refPayableNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refVendorIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refVendorNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deskripsiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nilaiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jumlahDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referenceNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jTempoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datePaidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pengeluaranBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPengeluaran)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.btnAll);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btnBatal);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(963, 136);
            this.panel1.TabIndex = 20;
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.ForeColor = System.Drawing.Color.Black;
            this.btnReset.Location = new System.Drawing.Point(149, 88);
            this.btnReset.Margin = new System.Windows.Forms.Padding(2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(110, 39);
            this.btnReset.TabIndex = 14;
            this.btnReset.Text = "Kosongkan";
            this.btnReset.UseVisualStyleBackColor = false;
            // 
            // btnAll
            // 
            this.btnAll.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAll.ForeColor = System.Drawing.Color.White;
            this.btnAll.Location = new System.Drawing.Point(11, 88);
            this.btnAll.Margin = new System.Windows.Forms.Padding(2);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(133, 39);
            this.btnAll.TabIndex = 15;
            this.btnAll.Text = "Pilih Semua";
            this.btnAll.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblDari);
            this.groupBox1.Controls.Add(this.btnTutup);
            this.groupBox1.Controls.Add(this.dtFrom);
            this.groupBox1.Controls.Add(this.lblSampai);
            this.groupBox1.Controls.Add(this.btnCari);
            this.groupBox1.Controls.Add(this.dtTo);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(931, 71);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informasi periode pembayaran";
            // 
            // lblDari
            // 
            this.lblDari.AutoSize = true;
            this.lblDari.Location = new System.Drawing.Point(7, 32);
            this.lblDari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDari.Name = "lblDari";
            this.lblDari.Size = new System.Drawing.Size(99, 17);
            this.lblDari.TabIndex = 11;
            this.lblDari.Text = "Dari tanggal:";
            // 
            // btnTutup
            // 
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(554, 31);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(4);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(100, 28);
            this.btnTutup.TabIndex = 1;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            // 
            // dtFrom
            // 
            this.dtFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(114, 31);
            this.dtFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(116, 24);
            this.dtFrom.TabIndex = 0;
            // 
            // lblSampai
            // 
            this.lblSampai.AutoSize = true;
            this.lblSampai.Location = new System.Drawing.Point(238, 32);
            this.lblSampai.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSampai.Name = "lblSampai";
            this.lblSampai.Size = new System.Drawing.Size(31, 17);
            this.lblSampai.TabIndex = 12;
            this.lblSampai.Text = "s/d";
            // 
            // btnCari
            // 
            this.btnCari.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnCari.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCari.ForeColor = System.Drawing.Color.White;
            this.btnCari.Location = new System.Drawing.Point(446, 31);
            this.btnCari.Margin = new System.Windows.Forms.Padding(4);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(100, 28);
            this.btnCari.TabIndex = 2;
            this.btnCari.Text = "Cari";
            this.btnCari.UseVisualStyleBackColor = false;
            // 
            // dtTo
            // 
            this.dtTo.CustomFormat = "dd-MMM-yyyy";
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(277, 31);
            this.dtTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(161, 24);
            this.dtTo.TabIndex = 1;
            // 
            // btnBatal
            // 
            this.btnBatal.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.ForeColor = System.Drawing.Color.White;
            this.btnBatal.Location = new System.Drawing.Point(265, 88);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 39);
            this.btnBatal.TabIndex = 0;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = false;
            // 
            // pengeluaranBindingSource
            // 
            this.pengeluaranBindingSource.DataSource = typeof(Fino.Model.PembatalanPengeluaranModel);
            // 
            // gvPengeluaran
            // 
            this.gvPengeluaran.AllowUserToAddRows = false;
            this.gvPengeluaran.AllowUserToDeleteRows = false;
            this.gvPengeluaran.AllowUserToResizeRows = false;
            this.gvPengeluaran.AutoGenerateColumns = false;
            this.gvPengeluaran.BackgroundColor = System.Drawing.Color.White;
            this.gvPengeluaran.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvPengeluaran.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectedDataGridViewCheckBoxColumn,
            this.payableIdDataGridViewTextBoxColumn,
            this.refPayableIdDataGridViewTextBoxColumn,
            this.refPayableNameDataGridViewTextBoxColumn,
            this.refVendorIdDataGridViewTextBoxColumn,
            this.refVendorNameDataGridViewTextBoxColumn,
            this.deskripsiDataGridViewTextBoxColumn,
            this.nilaiDataGridViewTextBoxColumn,
            this.jumlahDataGridViewTextBoxColumn,
            this.referenceNoDataGridViewTextBoxColumn,
            this.statusIdDataGridViewTextBoxColumn,
            this.statusDateDataGridViewTextBoxColumn,
            this.jTempoDataGridViewTextBoxColumn,
            this.datePaidDataGridViewTextBoxColumn,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvPengeluaran.DataSource = this.pengeluaranBindingSource;
            this.gvPengeluaran.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvPengeluaran.Location = new System.Drawing.Point(0, 0);
            this.gvPengeluaran.Name = "gvPengeluaran";
            this.gvPengeluaran.RowHeadersVisible = false;
            this.gvPengeluaran.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvPengeluaran.Size = new System.Drawing.Size(963, 197);
            this.gvPengeluaran.TabIndex = 16;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gvPengeluaran);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 136);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(963, 197);
            this.panel2.TabIndex = 22;
            // 
            // selectedDataGridViewCheckBoxColumn
            // 
            this.selectedDataGridViewCheckBoxColumn.DataPropertyName = "Selected";
            this.selectedDataGridViewCheckBoxColumn.Frozen = true;
            this.selectedDataGridViewCheckBoxColumn.HeaderText = "";
            this.selectedDataGridViewCheckBoxColumn.Name = "selectedDataGridViewCheckBoxColumn";
            this.selectedDataGridViewCheckBoxColumn.Width = 30;
            // 
            // payableIdDataGridViewTextBoxColumn
            // 
            this.payableIdDataGridViewTextBoxColumn.DataPropertyName = "Payable_Id";
            this.payableIdDataGridViewTextBoxColumn.HeaderText = "Payable_Id";
            this.payableIdDataGridViewTextBoxColumn.Name = "payableIdDataGridViewTextBoxColumn";
            this.payableIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // refPayableIdDataGridViewTextBoxColumn
            // 
            this.refPayableIdDataGridViewTextBoxColumn.DataPropertyName = "RefPayable_Id";
            this.refPayableIdDataGridViewTextBoxColumn.HeaderText = "RefPayable_Id";
            this.refPayableIdDataGridViewTextBoxColumn.Name = "refPayableIdDataGridViewTextBoxColumn";
            this.refPayableIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // refPayableNameDataGridViewTextBoxColumn
            // 
            this.refPayableNameDataGridViewTextBoxColumn.DataPropertyName = "RefPayable_Name";
            this.refPayableNameDataGridViewTextBoxColumn.HeaderText = "Ref Payable";
            this.refPayableNameDataGridViewTextBoxColumn.Name = "refPayableNameDataGridViewTextBoxColumn";
            this.refPayableNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.refPayableNameDataGridViewTextBoxColumn.Visible = false;
            // 
            // refVendorIdDataGridViewTextBoxColumn
            // 
            this.refVendorIdDataGridViewTextBoxColumn.DataPropertyName = "RefVendor_Id";
            this.refVendorIdDataGridViewTextBoxColumn.HeaderText = "RefVendor_Id";
            this.refVendorIdDataGridViewTextBoxColumn.Name = "refVendorIdDataGridViewTextBoxColumn";
            this.refVendorIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // refVendorNameDataGridViewTextBoxColumn
            // 
            this.refVendorNameDataGridViewTextBoxColumn.DataPropertyName = "RefVendor_Name";
            this.refVendorNameDataGridViewTextBoxColumn.HeaderText = "Vendor";
            this.refVendorNameDataGridViewTextBoxColumn.Name = "refVendorNameDataGridViewTextBoxColumn";
            this.refVendorNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // deskripsiDataGridViewTextBoxColumn
            // 
            this.deskripsiDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.deskripsiDataGridViewTextBoxColumn.DataPropertyName = "Deskripsi";
            this.deskripsiDataGridViewTextBoxColumn.HeaderText = "Nama Transaksi";
            this.deskripsiDataGridViewTextBoxColumn.Name = "deskripsiDataGridViewTextBoxColumn";
            this.deskripsiDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nilaiDataGridViewTextBoxColumn
            // 
            this.nilaiDataGridViewTextBoxColumn.DataPropertyName = "Nilai";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "#,##";
            this.nilaiDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.nilaiDataGridViewTextBoxColumn.HeaderText = "Nilai";
            this.nilaiDataGridViewTextBoxColumn.Name = "nilaiDataGridViewTextBoxColumn";
            this.nilaiDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jumlahDataGridViewTextBoxColumn
            // 
            this.jumlahDataGridViewTextBoxColumn.DataPropertyName = "Jumlah";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##";
            this.jumlahDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.jumlahDataGridViewTextBoxColumn.HeaderText = "Jumlah";
            this.jumlahDataGridViewTextBoxColumn.Name = "jumlahDataGridViewTextBoxColumn";
            this.jumlahDataGridViewTextBoxColumn.ReadOnly = true;
            this.jumlahDataGridViewTextBoxColumn.Visible = false;
            // 
            // referenceNoDataGridViewTextBoxColumn
            // 
            this.referenceNoDataGridViewTextBoxColumn.DataPropertyName = "ReferenceNo";
            this.referenceNoDataGridViewTextBoxColumn.HeaderText = "No Reference";
            this.referenceNoDataGridViewTextBoxColumn.Name = "referenceNoDataGridViewTextBoxColumn";
            this.referenceNoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // statusIdDataGridViewTextBoxColumn
            // 
            this.statusIdDataGridViewTextBoxColumn.DataPropertyName = "Status_Id";
            this.statusIdDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusIdDataGridViewTextBoxColumn.Name = "statusIdDataGridViewTextBoxColumn";
            this.statusIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.statusIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // statusDateDataGridViewTextBoxColumn
            // 
            this.statusDateDataGridViewTextBoxColumn.DataPropertyName = "Status_Date";
            this.statusDateDataGridViewTextBoxColumn.HeaderText = "Status Date";
            this.statusDateDataGridViewTextBoxColumn.Name = "statusDateDataGridViewTextBoxColumn";
            this.statusDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.statusDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // jTempoDataGridViewTextBoxColumn
            // 
            this.jTempoDataGridViewTextBoxColumn.DataPropertyName = "JTempo";
            dataGridViewCellStyle3.Format = "dd/MM/yyyy";
            this.jTempoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.jTempoDataGridViewTextBoxColumn.HeaderText = "Tgl Jatuh Tempo";
            this.jTempoDataGridViewTextBoxColumn.Name = "jTempoDataGridViewTextBoxColumn";
            this.jTempoDataGridViewTextBoxColumn.ReadOnly = true;
            this.jTempoDataGridViewTextBoxColumn.Visible = false;
            // 
            // datePaidDataGridViewTextBoxColumn
            // 
            this.datePaidDataGridViewTextBoxColumn.DataPropertyName = "DatePaid";
            dataGridViewCellStyle4.Format = "dd/MM/yyyy";
            this.datePaidDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.datePaidDataGridViewTextBoxColumn.HeaderText = "Tgl Bayar";
            this.datePaidDataGridViewTextBoxColumn.Name = "datePaidDataGridViewTextBoxColumn";
            this.datePaidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // PembatalanPengeluaranView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 333);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(971, 360);
            this.Name = "PembatalanPengeluaranView";
            this.Text = "Pembatalan Pembayaran";
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pengeluaranBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPengeluaran)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblDari;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.Label lblSampai;
        private System.Windows.Forms.Button btnCari;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.BindingSource pengeluaranBindingSource;
        private System.Windows.Forms.DataGridView gvPengeluaran;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn payableIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn refPayableIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn refPayableNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn refVendorIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn refVendorNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deskripsiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nilaiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jumlahDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn referenceNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jTempoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datePaidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
    }
}