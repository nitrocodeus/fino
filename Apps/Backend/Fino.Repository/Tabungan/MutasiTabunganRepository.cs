﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;

namespace Fino.Repository
{
    public class MutasiTabunganRepository : BaseRepository, IMutasiTabunganRepository
    {
        private int Factor(int p_Jenis)
        {
            var f = 0;

            switch (p_Jenis)
            {
                case (int)JenisMutasiEnum.SETORAN:
                    f = 1;
                    break;

                case (int)JenisMutasiEnum.PENARIKAN:
                    f = -1;
                    break;

                default:
                    break;
            }
            return f;
        }

        public MutasiTabunganModel NewMutasiRekening(MutasiTabunganModel p_Model)
        {
            var pResult = p_Model;

            var mutasi = new MutasiRekening
            {
                factor = Factor(p_Model.JenisMutasi),
                is_awal = false,
                jenis_mutasi = p_Model.JenisMutasi,
                mutasi_nilai = p_Model.MutasiNilai,
                tgl_mutasi = DateTime.Now,
                rekening_id = p_Model.rekening_id
            };

            Context.MutasiRekenings.Add(mutasi);
            Context.SaveChanges();

            return pResult;
        }

        private double GetBalance(FinoDBContext context, int p_RekeningId)
        {
            var queryBuilder = new StringBuilder("SELECT SUM(Factor * Mutasi_nilai) as Balance ")
                                    .Append("FROM mutasirekenings ")
                                    .Append("WHERE rekening_id = @p0");
            var queryResult = context.Database.SqlQuery<double?>(queryBuilder.ToString(), p_RekeningId.ToString()).FirstOrDefault();
            if (queryResult != null)
            {
                return queryResult.Value;
            }
            else
            {
                return -1;
            }
        }

        public MutasiTabunganModel GetRekeningByRekeningNo(string p_NoRek)
        {
            MutasiTabunganModel pResult = null;

            using (Context)
            {
                var rek = Context.Rekenings.Where(e => e.rekening_code.Equals(p_NoRek)).FirstOrDefault();
                var refSiswa = (from sr in Context.SiswaRekenings
                               join siswa in Context.RefSiswas on sr.siswa_id equals siswa.siswa_id
                               where sr.rekening_id.Equals(rek.rekening_id)
                               select new
                               {
                                   siswa_code = siswa.siswa_code,
                                   siswa_name = siswa.nama,
                                   jk = siswa.jkelamin,
                                   siswa_id = siswa.siswa_id
                               }).FirstOrDefault();

                pResult = new MutasiTabunganModel
                {
                    jkelamin = refSiswa.jk,
                    MutasiNilai = 0,
                    rekening_code = rek.rekening_code,
                    rekening_id = rek.rekening_id,
                    Saldo = GetBalance(Context, rek.rekening_id),
                    siswa_code = refSiswa.siswa_code,
                    siswa_id = refSiswa.siswa_id,
                    siswa_nama = refSiswa.siswa_name,
                    status = 0,
                    JenisMutasi = 1
                };
            }

            return pResult;
        }


        public List<LaporanSaldoModel> GetAllSaldoAsOn(DateTime p_Date)
        {
            var endDate = new DateTime(p_Date.Year, p_Date.Month, p_Date.Day, 23, 59, 59);
            var allSaldo = new List<LaporanSaldoModel>();
            var sqlSb = new StringBuilder("SELECT ")
                        .Append("f.nama as Kelas ")
                        .Append(",b.rekening_code as NoRekening ")
                        .Append(",d.nama as Siswa ")
                        .Append(",sum(Case ")
                        .Append("      when factor = 1 then mutasi_nilai ")
                        .Append("      else 0 ")
                        .Append("     end) as Setoran ")
                        .Append(",sum(Case ")
                        .Append("      when factor = -1 then mutasi_nilai ")
                        .Append("      else 0")
                        .Append("     end) as Penarikan ")
                        .Append("from MutasiRekenings a inner join ")
                        .Append("RefRekenings b on a.rekening_id = b.rekening_id inner join ")
                        .Append("SiswaRekenings c on b.rekening_id = c.rekening_id inner join ")
                        .Append("RefSiswas d on c.siswa_id = d.siswa_id inner join ")
                        .Append("SiswaKelas e on d.siswa_id = e.siswa_id inner join ")
                        .Append("RefKelas f on e.kelas_id = f.kelas_id inner join ")
                        .Append("( select siswa_id, ")
                        .Append("         max(mulai_tanggal) mdtm ")
                        .Append("  from SiswaKelas ")
                        .Append("  group by siswa_id ) g on e.siswa_id = g.siswa_id and e.mulai_tanggal = g.mdtm ")
                        .Append("where a.tgl_mutasi <= @p0 ")
                        .Append("group by f.nama, b.rekening_code, d.nama ");

            allSaldo = Context.Database.SqlQuery<LaporanSaldoModel>(sqlSb.ToString(), endDate.ToString("yyyy/MM/dd HH:mm:ss")).ToList();
            return allSaldo;
        }

        public List<LaporanMutasiModel> GetAllMutasiAsOn(DateTime p_Date)
        {
            var endDate = new DateTime(p_Date.Year, p_Date.Month, p_Date.Day);
            var allSaldo = new List<LaporanMutasiModel>();
            var sqlSb = new StringBuilder("SELECT ")
                        .Append("k.nama as Kelas ")
                        .Append(",r.rekening_code as NoRekening ")
                        .Append(",s.siswa_code as NoInduk ")
                        .Append(",s.nama as Siswa ")
                        .Append(",coalesce(sa.saldo_awal,0) as SaldoAwal ")
                        .Append(",m.tgl_mutasi as TglMutasi ")
                        .Append(",factor * m.mutasi_nilai as MutasiNilai ")
                        .Append(",coalesce(sk.saldo_akhir,0) as SaldoAkhir ")
                        .Append("from MutasiRekenings m inner join ")
                        .Append("	 RefRekenings r on m.rekening_id = r.rekening_id inner join ")
                        .Append("	 SiswaRekenings sr on m.rekening_id = sr.rekening_id inner join ")
                        .Append("     RefSiswas s on sr.siswa_id = s.siswa_id inner join ")
                        .Append("	 SiswaKelas wk on s.siswa_id = wk.siswa_id inner join ")
                        .Append("	 RefKelas k on wk.kelas_id = k.kelas_id inner join ")
                        .Append("	 ( select siswa_id, ")
                        .Append("              max(mulai_tanggal) mdtm ")
                        .Append("       from SiswaKelas ")
                        .Append("       group by siswa_id ) g on s.siswa_id = g.siswa_id and wk.mulai_tanggal = g.mdtm left join ")
                        .Append("	 (select ")
                        .Append("	 	rekening_id ")
                        .Append("	   ,sum((mutasi_nilai * factor)) as saldo_awal ")
                        .Append("	  from MutasiRekenings ")
                        .Append("	  where tgl_mutasi < @p0 ")
                        .Append("	  Group by rekening_id) sa on m.rekening_id = sa.rekening_id left join ")
                        .Append("	  (select ")
                        .Append("	 	rekening_id ")
                        .Append("	   ,sum((mutasi_nilai * factor)) as saldo_akhir ")
                        .Append("	  from MutasiRekenings ")
                        .Append("	  where tgl_mutasi < @p1 ")
                        .Append("	  Group by rekening_id) sk on m.rekening_id = sk.rekening_id ")
                        .Append("where m.tgl_mutasi < @p2 ");

            var pAwal = new DateTime(endDate.Year, endDate.Month, 1);
            var pAkhir = pAwal.AddMonths(1);
            allSaldo = Context.Database.SqlQuery<LaporanMutasiModel>(sqlSb.ToString()
                            , pAwal.ToString("yyyy/MM/dd")
                            , pAkhir.ToString("yyyy/MM/dd")
                            , pAkhir.ToString("yyyy/MM/dd")).ToList();

            return allSaldo;
        }
    }
}
