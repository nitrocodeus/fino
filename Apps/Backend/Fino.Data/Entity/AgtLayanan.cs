﻿using System;

namespace Fino.Datalib.Entity
{
    public class AgtLayanan
    {
        public int agtlayanan_id {get;set;}
        public int siswa_id {get;set;}
        public int layanan_id { get; set; }
        public DateTime mulai_tanggal {get;set;}
        public DateTime hingga_tanggal {get;set;}
        public DateTime tgl_daftar {get;set;}
        public double nilai {get;set;}

        public virtual RefSiswa Siswa {get; set;}
        public virtual RefLayanan Layanan {get; set;}
    }
}
