﻿using Fino.Model;

namespace Fino.Repository
{
    public interface IPosBiayaPaidCancelRepository
    {
        void AddPosBiayaPaidCancel(PosBiayaPaidCancelModel entity);
    }
}
