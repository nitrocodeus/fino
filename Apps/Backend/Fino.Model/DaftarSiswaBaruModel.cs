﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Fino.Lib.Core;

namespace Fino.Model
{
    public class DaftarSiswaBaruModel: BaseModel
    {
        public static readonly string SiswaIdPropertyName = "Siswa_Id";
        public static readonly string NamaPropertyName = "Nama";
        public static readonly string CodePropertyName = "Code";
        public static readonly string JKelaminPropertyName = "JKelamin";
        public static readonly string KelasIdPropertyName = "Kelas_Id";
        public static readonly string TahunAjaranIdPropertyName = "TahunAjaran_Id";
        public static readonly string TahunAjaranNamaPropertyName = "TahunAjaran_Nama";
        public static readonly string MulaiTanggalPropertyName = "Mulai_Tanggal";
        public static readonly string OptionTingkat_IdPropertyName = "OptionTingkat_Id";
        public static readonly string IsPindahanPropertyName = "IsPindahan";

        
        int? _Siswa_Id;
        public int? Siswa_Id
        {
            get
            {
                return _Siswa_Id;
            }
            set{
                _Siswa_Id = value;
            }
        }

        string _Nama;
        [Display(Name = "Nama Siswa")]
        [Required]
        [StringLength(25)]
        public string Nama
        {
            get
            {
                return _Nama;
            }
            set
            {
                _Nama = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamaPropertyName));
                ValidateProperty(value, NamaPropertyName);
            }
        }
        string _Code;
        [Display(Name = "No Induk")]
        [Required]
        [StringLength(10)]
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                _Code = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(CodePropertyName));
                ValidateProperty(value, CodePropertyName);
            }
        }

        bool _JKelamin;
        [Display(Name = "Jenis Kelamin")]
        [Required]
        public bool JKelamin
        {
            get
            {
                return _JKelamin;
            }
            set
            {
                _JKelamin = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(JKelaminPropertyName));
                ValidateProperty(value, JKelaminPropertyName);
            }
        }

        int _Kelas_Id;
        [Display(Name = "Kelas")]
        [Required]
        public int Kelas_Id
        {
            get
            {
                return _Kelas_Id;
            }
            set
            {
                _Kelas_Id = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(KelasIdPropertyName));
                ValidateProperty(value, KelasIdPropertyName);
            }
        }

        int _TahunAjaran_Id;
        [Display(Name = "Tahun Ajaran Id")]
        [Required]
        public int TahunAjaran_Id
        {
            get
            {
                return _TahunAjaran_Id;
            }
            set
            {
                _TahunAjaran_Id = value;
            }
        }

        string _TahunAjaran_Nama;
        [Display(Name = "Tahun Ajaran")]
        public string TahunAjaran_Nama
        {
            get
            {
                return _TahunAjaran_Nama;
            }
            set
            {
                _TahunAjaran_Nama = value;
            }
        }

        DateTime _Mulai_Tanggal;
        [Required]
        public DateTime Mulai_Tanggal
        {
            get
            {
                return _Mulai_Tanggal;
            }
            set
            {
                _Mulai_Tanggal = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(MulaiTanggalPropertyName));
                ValidateProperty(value, MulaiTanggalPropertyName);
            }
        }


        int _OptionTingkat_Id;
        [Display(Name = "Setara Kelas")]
        [Required]
        public int OptionTingkat_Id
        {
            get
            {
                return _OptionTingkat_Id;
            }
            set
            {
                _OptionTingkat_Id = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(OptionTingkat_IdPropertyName));
                ValidateProperty(value, OptionTingkat_IdPropertyName);
            }
        }

        bool _IsPindahan;
        [Display(Name = "Siswa Pindahan")]
        [Required]
        public bool IsPindahan
        {
            get
            {
                return _IsPindahan;
            }
            set
            {
                _IsPindahan = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(IsPindahanPropertyName));
                ValidateProperty(value, IsPindahanPropertyName);
            }
        }
    }
}
