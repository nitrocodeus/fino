﻿using System;
using System.ComponentModel;
using Fino.Lib.Core;

namespace Fino.Model
{
    public class PosBiayaModel : BaseModel
    {
        public static readonly string PosBiayaIdPropertyName = "PosBiayaId";
        public static readonly string SelectedPropertyName = "Selected";
        public static readonly string NamaBiayaPropertyName = "NamaBiaya";
        public static readonly string JTempoPropertyName = "JTempo";
        public static readonly string NilaiBiayaPropertyName = "NilaiBiaya";
        public static readonly string PotonganPropertyName = "Potongan";
        public static readonly string IsPaidPropertyName = "IsPaid";
        
        private int _posBiayaId;
        private bool _selected;
        private string _namaBiaya;
        private DateTime _jtempo;
        private double _nilaiBiaya;
        private double _potongan;
        private bool? _isPaid;
        private int _layananId;
        private bool? _noPosBiaya;
        private int _refBiayaId;

        public DateTime? DatePaid { get; set; }

        public int RefBiayaId
        {
            get { return _refBiayaId; }
            set { _refBiayaId = value; }
        }
        public bool? NoPosBiaya
        {
            get { return _noPosBiaya; }
            set { _noPosBiaya = value; }
        }

        public int LayananId
        {
            get { return _layananId; }
            set { _layananId = value; }
        }

        public int PosBiayaId
        {
            get
            {
                return _posBiayaId;
            }
            set
            {
                _posBiayaId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PosBiayaIdPropertyName));
            }
        }

        public bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                _selected = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(SelectedPropertyName));
            }
        }

        public string NamaBiaya
        {
            get
            {
                return _namaBiaya;
            }
            set
            {
                _namaBiaya = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamaBiayaPropertyName));
            }
        }

        public DateTime JTempo
        {
            get
            {
                return _jtempo;
            }
            set
            {
                _jtempo = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(JTempoPropertyName));
            }
        }

        public double NilaiBiaya
        {
            get
            {
                return _nilaiBiaya;
            }
            set
            {
                _nilaiBiaya = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NilaiBiayaPropertyName));
            }
        }

        public double Potongan
        {
            get
            {
                return _potongan;
            }
            set
            {
                _potongan = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PotonganPropertyName));
            }
        }

        public bool? IsPaid
        {
            get
            {
                return _isPaid;
            }
            set
            {
                if (value != null) _isPaid = value;
                else _isPaid = false;

                InvokePropertyChanged(new PropertyChangedEventArgs(IsPaidPropertyName));
            }
        }

        public double JumlahBiaya
        {
            get
            {
                return NilaiBiaya - Potongan;
            }
        }
    }
}
