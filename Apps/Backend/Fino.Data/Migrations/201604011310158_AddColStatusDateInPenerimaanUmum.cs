namespace Fino.Datalib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColStatusDateInPenerimaanUmum : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PenerimaanUmums", "Status_Date", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PenerimaanUmums", "Status_Date");
        }
    }
}
