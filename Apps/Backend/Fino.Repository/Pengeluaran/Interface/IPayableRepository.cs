﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Model;

namespace Fino.Repository
{
    public interface IPayableRepository
    {
        List<RekapitulasiPengeluaranModel> GetRekapitulasiPengeluaran(DateTime p_FromDate, DateTime p_ToDate);
        List<PembatalanPengeluaranModel> GetPembatalanPengeluaranList(DateTime p_FromDate, DateTime p_ToDate);
        PayableModel GetPayable(int p_PayableId);
        void UpdatePayable(PayableModel entity);
        void DeletePayable(PayableModel entity);
    }
}
