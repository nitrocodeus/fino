﻿using System;
using System.Collections.Generic;
using Fino.Datalib.Entity;
using Fino.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Fino.Model;
using Fino.BusinessLogic;
using Fino.Lib.Core;

namespace Test.Fino.Backend
{
    [TestClass]
    public class AccountPeriod_TestUnit
    {

        [TestMethod]
        public void OpenNextPeriod_Fail_PreviousActive()
        {
            IRefAccPeriodProcess proc = new RefAccPeriodProcess();
            var result = proc.GetAllRefAccPeriodModel();
            if (result.IsSucess && ((List<RefAccPeriodModel>) result.DataResult).Count > 0)
            {
                var periodList = (List<RefAccPeriodModel>)result.DataResult;
                var nextPeriod = periodList.Where(e=>e.Period_end >= DateTime.Now).OrderBy(o => o.Period_end).ToList()[1];
                var updResult = proc.UpdateRefAccPeriodStatus(nextPeriod.Period_id, (int)RefAccPeriodStatusEnum.Opened);
                Assert.IsFalse(updResult.IsSucess);
            }
            else
            {
                Assert.Fail();
            }
        }
    }
}
