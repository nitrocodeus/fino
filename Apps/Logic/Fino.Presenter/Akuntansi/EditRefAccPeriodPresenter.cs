﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class EditRefAccPeriodPresenter : BasePresenter
    {
        IRefAccPeriodProcess _editRefAccPeriodProcess;
        IEditRefAccPeriodView _editRefAccPeriodView;

        public EditRefAccPeriodPresenter(IRefAccPeriodProcess p_Process, IEditRefAccPeriodView p_View) : base(p_View)
        {
            this._editRefAccPeriodProcess = p_Process;
            this._editRefAccPeriodView = p_View;

            this._editRefAccPeriodView.Load += _editRefAccPeriodView_Load;
            this._editRefAccPeriodView.SaveButtonClicked += _editRefAccPeriodView_SaveButtonClicked;
            this._editRefAccPeriodView.CloseButtonClicked += _editRefAccPeriodView_CloseButtonClicked;
        }

        #region Control Events

        private void _editRefAccPeriodView_Load(object sender, EventArgs e)
        {
            if (_editRefAccPeriodView.SelectedRefAccPeriod != null)
            {
                _editRefAccPeriodView.PeriodName = _editRefAccPeriodView.SelectedRefAccPeriod.Period_name;

                if (_editRefAccPeriodView.SelectedRefAccPeriod.Period_start != DateTime.MinValue)
                {
                    _editRefAccPeriodView.PeriodStart = _editRefAccPeriodView.SelectedRefAccPeriod.Period_start;
                }
                else
                {
                    _editRefAccPeriodView.SelectedRefAccPeriod.Period_start = DateTime.Today;
                }

                if (_editRefAccPeriodView.SelectedRefAccPeriod.Period_end != DateTime.MinValue)
                {
                    _editRefAccPeriodView.PeriodEnd = _editRefAccPeriodView.SelectedRefAccPeriod.Period_end;
                }
                else
                {
                    _editRefAccPeriodView.SelectedRefAccPeriod.Period_end = DateTime.Today;
                }
            }

            _editRefAccPeriodView.SelectedRefAccPeriod.ErrorValidation += SelectedRefAccPeriod_ErrorValidation;
            _editRefAccPeriodView.SetBinding();
        }

        private void _editRefAccPeriodView_SaveButtonClicked()
        {
            var context = new ValidationContext(_editRefAccPeriodView.SelectedRefAccPeriod, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(_editRefAccPeriodView.SelectedRefAccPeriod, context, results, true);

            if (isValid)
            {
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => _editRefAccPeriodProcess.UpdateRefAccPeriod(_editRefAccPeriodView.SelectedRefAccPeriod)),
                                AppResource.MSG_SAVING_REF_BIAYA);

                if (result.IsSucess)
                {
                    WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                    _editRefAccPeriodView.RefAccPeriodView.ReloadDataEvent();
                    _editRefAccPeriodView.Close();
                }
                else
                {
                    WinApi.ShowErrorMessage(result.ProcessException.Message, AppResource.ERROR_TITLE);
                }
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        private void _editRefAccPeriodView_CloseButtonClicked()
        {
            _editRefAccPeriodView.Close();
        }

        private void SelectedRefAccPeriod_ErrorValidation(object sender, CustomEventArgs<string> e)
        {
            WinApi.ShowErrorMessage(e.Data, AppResource.CAP_VALIDASI_ERROR);
        }

        #endregion Control Events
    }
}
