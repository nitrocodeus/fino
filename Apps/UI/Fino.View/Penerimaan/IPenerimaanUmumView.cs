﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IPenerimaanUmumView : IView
    {
        event Action SaveButtonClicked;
        event Action SaveNextButtonClicked;
        event Action NewDataButtonClicked;
        event Action CloseButtonClicked;
        event Action AddNewSPUButtonClicked;
        event Action ReloadDataEvent;

        string RefSPU { get; set; }
        string RefBiaya { get; set; }
        string Deskripsi { get; set; }
        double Nilai { get; set; }        

        PenerimaanUmumModel ViewModel { get; set; }
        object RefSPUDataSource { get; set; }
        object RefBiayaDataSource { get; set; }

        void SetBinding();
        void ClearView();
        void ChangeEditable(bool enabled);
        void ShowErrorMessage(string message, string caption);
        void ShowInformation(string message, string caption);
        void ShowCreateNewRefSPUView();
        void ReloadData();
        void CetakNota(CetakPenerimaanUmumModel cetakNotaModel);
    }
}
