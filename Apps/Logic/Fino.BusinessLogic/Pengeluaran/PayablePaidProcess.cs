﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class PayablePaidProcess : IPayablePaidProcess
    {
        public IPayablePaidRepository PayablePaidRepo { get; set; }
        public IRefVendorRepository RefVendorRepository { get; set; }
        public IRefPayableRepository RefPayableRepository { get; set; }

        public ProcessResult GetAllRefVendor()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = RefVendorRepository.GetAllVendor().Select(x => new { Id = x.VendorId, Value = x.VendorName }).ToList();
                result.IsSucess = true;
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllRefPayable()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = RefPayableRepository.RefPayableData().Select(x => new { Id = x.refpayable_id, Value = x.name }).ToList();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdatePayablePaid(PayablePaidModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                PayablePaidRepo.UpdatePayablePaid(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult CreateNewRefVendor(RefVendorModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefVendorRepository.UpdateVendor(entity);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
