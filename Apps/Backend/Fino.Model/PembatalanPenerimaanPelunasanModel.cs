﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class PembatalanPenerimaanPelunasanModel : BaseModel
    {
        public bool Selected { get; set; }
        public int PosBiaya_Id { get; set; }
        public int Siswa_Id { get; set; }
        public string Nama_Siswa { get; set; }
        public string Deskripsi { get; set; }
        public double Nilai { get; set; }
        public double Potongan { get; set; }
        public double Jumlah { get; set; }
        public int NotaPembayaranId { get; set; }
        public DateTime JTempo { get; set; }
        public DateTime DatePaid { get; set; }
    }
}
