﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class RefGrupBiaya
    {
        public int Grupbiaya_id { get; set; }
        public string nama { get; set; }
        public string code { get; set; }
        public bool active { get; set; }

        public virtual ICollection<GrupBiaya> GrupBiayas { get; set; }
    }
}
