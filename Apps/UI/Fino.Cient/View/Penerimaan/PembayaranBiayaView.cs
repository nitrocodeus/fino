﻿using Fino.BusinessLogic;
using Fino.Cient.Common;
using Fino.Lib.Core.Settings;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.Presenter;
using Fino.View;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class PembayaranBiayaView : BaseForm, IPembayaranBiayaView
    {
        public event Action btnSimpanClicked;
        public event Action btnTutupClicked;
        public event Action AddingNewPosBiaya;
        public event Action SelectingSiswaDone;
        public event Action txtSiswaKeyPressed;
        public event Action btnTambahBiayaClicked;        

        public int SiswaId
        {
            get
            {
                if (pencarianSiswaControlView1.SelectedSiswa != null)
                {
                    return pencarianSiswaControlView1.SelectedSiswa.Id;
                }

                return 0;
            }
        }

        public string NamaSiswa
        {
            get
            {
                return txtNamaSiswa.Text;
            }
            set
            {
                txtNamaSiswa.Text = value;
            }
        }

        public string NoInduk
        {
            get
            {
                return txtNoInduk.Text;
            }
            set
            {
                txtNoInduk.Text = value;
            }
        }

        public int IdKelas
        {
            get
            {
                if (pencarianSiswaControlView1.SelectedSiswa != null)
                {
                    return pencarianSiswaControlView1.SelectedSiswa.IdKelas;
                }

                return 0;
            }
        }

        public string Kelas
        {
            get
            {
                if (pencarianSiswaControlView1.SelectedSiswa != null)
                {
                    return pencarianSiswaControlView1.SelectedSiswa.Kelas;
                }

                return null;
            }
        }

        public int IdTahunAjaran
        {
            get
            {
                if (pencarianSiswaControlView1.SelectedSiswa != null)
                {
                    return pencarianSiswaControlView1.SelectedSiswa.IdTahunAjaran;
                }

                return 0;
            }
        }

        public string TahunAjaran
        {
            get
            {
                if (pencarianSiswaControlView1.SelectedSiswa != null)
                {
                    return pencarianSiswaControlView1.SelectedSiswa.TahunAjaran;
                }

                return null;
            }
        }

        public string TextSearchSiswa
        {
            get
            {
                return txtSiswa.Text;
            }
        }

        public IPencarianSiswaView PencarianSiswaView
        {
            get { return pencarianSiswaControlView1; }
        }

        public IPosBiayaView CurrentPosBiayaView
        {
            get { return ctPosBiaya; }
        }

        public IPosBiayaView AddPosBiayaView
        {
            get { return ctAddPosBiaya; }
        }

        public PembayaranBiayaView()
        {
            InitializeComponent();
            pencarianSiswaControlView1.HideControl += pencarianSiswaControlView1_HideControl;
            ctAddPosBiaya.HideControl += ctAddPosBiaya_HideControl;
            ctAddPosBiaya.btnTambahkanClicked += ctAddPosBiaya_btnTambahkanClicked;

            InitialisePosBiaya();

            InitialiseAddPosBiaya();

            InitialiseSelectSiswa();
        }

        public void AddPosBiayaMode(bool p_Mode)
        {
            txtSiswa.Enabled = !p_Mode;
            btnSimpan.Enabled = !p_Mode;
            btnTambahBiaya.Enabled = !p_Mode;
        }

        private void ctAddPosBiaya_btnTambahkanClicked()
        {
            if (AddingNewPosBiaya != null)
            {
                AddingNewPosBiaya();
            }
        }

        private void ctAddPosBiaya_HideControl()
        {
            AddPosBiayaMode(false);
        }

        private void pencarianSiswaControlView1_HideControl()
        {
            if (SelectingSiswaDone != null && pencarianSiswaControlView1.SelectedSiswa != null)
            {
                SelectingSiswaDone();
            }
        }

        private void txtSiswa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && txtSiswaKeyPressed != null)
            {
                txtSiswaKeyPressed();
            }
        }

        private void btnTambahBiaya_Click(object sender, EventArgs e)
        {
            if (btnTambahBiayaClicked != null && pencarianSiswaControlView1.SelectedSiswa != null)
            {
                btnTambahBiayaClicked();
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            if (btnTutupClicked != null)
            {
                btnTutupClicked();
            }
        }

        #region Initialise other view
        private void InitialiseSelectSiswa()
        {
            SiswaProcess processSiswa = new SiswaProcess();
            PencarianSiswaPresenter presenterSiswa = new PencarianSiswaPresenter(pencarianSiswaControlView1, processSiswa);
        }

        private void InitialiseAddPosBiaya()
        {
            ctAddPosBiaya.ShowSelectedColumn(true);
            ctAddPosBiaya.ShowToolbar(true);
            ctAddPosBiaya.ShowAllBiaya = true;
            ctAddPosBiaya.ByPassGridClick = true;

            PosBiayaProcess process = new PosBiayaProcess();
            PosBiayaPresenter presenter = new PosBiayaPresenter(ctPosBiaya, process);

        }

        private void InitialisePosBiaya()
        {
            ctPosBiaya.ShowSelectedColumn(true);
            ctPosBiaya.ShowToolbar(false);
            ctPosBiaya.ShowAllBiaya = false;
            ctPosBiaya.ByPassGridClick = false;

            PosBiayaProcess processAdd = new PosBiayaProcess();
            PosBiayaPresenter presenterAdd = new PosBiayaPresenter(ctAddPosBiaya, processAdd);
        }
        #endregion

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (btnSimpanClicked != null)
            {
                btnSimpanClicked();
            }
        }

        public void CetakNota(CetakPembayaranModel p_Model)
        {
            LocalReport report = new LocalReport();
            report.ReportEmbeddedResource = @"Fino.Cient.View.Report.NotaPembayaran.rdlc";

            ReportParameter[] parameters = new ReportParameter[2];
            parameters[0] = new ReportParameter("Petugas", AppVariable.Instance.GetLoggedOnUserFullName());
            parameters[1] = new ReportParameter("TahunAjaran", AppVariable.Instance.GetTahunAjaranAktif());

            BindingSource bsNotaPembayaran = new BindingSource();
            BindingSource bsBiayaPembayaran = new BindingSource();
            BindingSource bsUser = new BindingSource();

            bsNotaPembayaran.DataSource = p_Model;
            bsBiayaPembayaran.DataSource = p_Model.DaftarPembayaran;
            bsUser.DataSource = p_Model.User;

            report.SetParameters(parameters);
            report.DataSources.Add(
               new ReportDataSource("NotaPembayaran", bsNotaPembayaran));
            report.DataSources.Add(
                new ReportDataSource("BiayaPembayaran", bsBiayaPembayaran));     
            
            ReportPrintingHelper.Instance.Export(report);
            ReportPrintingHelper.Instance.Print();
        }

        public string TotalBiaya
        {
            get
            {
                return txtTotal.Text;
            }
            set
            {
                txtTotal.Text = value;
            }
        }
    }
}
