﻿using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IPayablePaidProcess
    {
        ProcessResult GetAllRefVendor();
        ProcessResult GetAllRefPayable();
        ProcessResult UpdatePayablePaid(PayablePaidModel entity);
        ProcessResult CreateNewRefVendor(RefVendorModel entity);
    }
}
