﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public interface IPotonganRepository : IRepository
    {
        List<Potongan> GetPotonganBySiswaId(int p_SiswaId);
        List<RefBiaya> GetRefBiaya();
        List<Potongan> UpdatePotongan(List<Potongan> p_Potongan);
    }
}
