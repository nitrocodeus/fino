﻿using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class PayablePaidRepository : BaseRepository, IPayablePaidRepository
    {
        private PayablePaid GetExistingPayablePaid(int payablePaidId)
        {
            return this.Context.PayablePaids.Where(x => x.payable_id == payablePaidId).FirstOrDefault();
        }

        public void UpdatePayablePaid(PayablePaidModel entity)
        {
            var existingPayablePaid = GetExistingPayablePaid(entity.PayablePaidId);

            if (existingPayablePaid == null)
            {
                int status = (int)TransactionStatusEnum.OPEN;

                Payable payable = new Payable
                {
                    refpayable_id = entity.RefPayableId,
                    refvendor_id = entity.RefVendorId,
                    deskripsi = entity.Deskripsi,
                    jtempo = entity.JatuhTempo,
                    nilai = entity.Nilai,
                    ispaid = true,
                    isactive = true,
                    datepaid = DateTime.Today,
                    created = DateTime.Now,
                    status_id = status
                };

                PayablePaid payablePaid = new PayablePaid
                    {
                        datepaid = DateTime.Today,
                        paynilai = entity.Nilai,
                        referenceno = entity.ReferenceNo,
                        status = status,
                        payable = payable
                    };

                this.Context.PayablePaids.Add(payablePaid);
            }

            this.Context.SaveChanges();
        }
    }
}
