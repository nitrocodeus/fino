﻿using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using Fino.View.Akuntansi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditAccountClassSettingView : BaseForm, IEditAccountClassSettingView
    {
        #region Events

        public event Action BtnBatalClicked;
        public event Action BtnSimpanClicked;

        #endregion Events

        #region Public Properties

        public AccountClassSettingModel SelectedAccountClassSetting { get; private set; }
        public IAccountClassSettingView AccountClassSettingView { get; set; }
        public object AccountIdDataSource { get; set; }
        public object ParamIdDataSource { get; set; }
        
        public int AccClassId
        {
            get
            {
                return Int32.Parse(txtAccSettingId.Text);
            }
            set
            {
                txtAccSettingId.Text = value.ToString();
                var accClassSettingEnum = (AccountClassificationEnum)Enum.Parse(typeof(AccountClassificationEnum), value.ToString());
                txtAccClassSettingName.Text = accClassSettingEnum.ToString();
            }
        }

        public int AccountId
        {
            get
            {
                return (int)cboAccountId.SelectedValue;
            }
            set
            {
                cboAccountId.SelectedValue = value;
            }
        }

        public int ParamId
        {
            get
            {
                return (int)cboParamId.SelectedValue;
            }
            set
            {
                cboParamId.SelectedValue = value;
            }
        }

        #endregion Public Properties

        #region Constructors

        public EditAccountClassSettingView()
        {
            InitializeComponent();

            this.btnSimpan.Click += btnSimpan_Click;
            this.btnBatal.Click += btnBatal_Click;
        }

        void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        void btnSimpan_Click(object sender, EventArgs e)
        {
            if(BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        #endregion Contructors

        #region Public Methods

        public void ShowForm(AccountClassSettingModel accountClassSetting)
        {
            if (accountClassSetting != null)
            {
                SelectedAccountClassSetting = accountClassSetting;
                this.Text = "Edit Akun";
            }
            else
            {
                SelectedAccountClassSetting = new AccountClassSettingModel { AccClassId = AccountClassSettingView.ViewModel.AccClassId };
                this.Text = "Tambah Akun";
            }

            switch(SelectedAccountClassSetting.AccClassId)
            {
                case (int)AccountClassificationEnum.PENDAPATAN:
                case (int)AccountClassificationEnum.PIUTANG:
                    lblParam.Text = "Jenis Penerimaan:";
                    //RepositionLabelParam();
                    break;
                case (int)AccountClassificationEnum.BEBAN:
                    lblParam.Text = "Jenis Pengeluaran:";
                    //RepositionLabelParam();
                    break;
            }

            this.Show();
        }

        #endregion Public Methods

        #region Private Methods

        private void RepositionLabelParam()
        {
            lblParam.Left = lblAccountName.Left + lblAccountName.Width - lblParam.Width;
        }

        #endregion Private Methods

        #region IEditAccountClassSettingView

        public void SetBinding()
        {
            txtAccSettingId.DataBindings.Add("Text", SelectedAccountClassSetting, AccountClassSettingModel.AccClassIdPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            
            cboAccountId.DataSource = AccountIdDataSource;
            cboAccountId.DisplayMember = "Value";
            cboAccountId.ValueMember = "Id";
            cboAccountId.DataBindings.Clear();
            cboAccountId.DataBindings.Add("SelectedValue", SelectedAccountClassSetting, AccountClassSettingModel.AccountIdPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);

            cboParamId.DataSource = ParamIdDataSource;
            cboParamId.DisplayMember = "Value";
            cboParamId.ValueMember = "Id";
            cboParamId.DataBindings.Clear();
            cboParamId.DataBindings.Add("SelectedValue", SelectedAccountClassSetting, AccountClassSettingModel.ParamIdPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);

            if (ParamIdDataSource == null)
                cboParamId.Enabled = false;
        }

        public void ClearData()
        {
            
        }

        #endregion IEditAccountClassSettingView
    }
}
