﻿using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Presenter
{
    public class MainFormPresenter : BasePresenter
    {
        private readonly IMainFormView _MainFormView;

        public MainFormPresenter(IMainFormView p_MainFormView) : base(p_MainFormView)
        {
            _MainFormView = p_MainFormView;

            _MainFormView.Load += OnMainFormLoad;
            //_MainFormView.KonfigurasiBiayaClicked +=_MainFormView_KonfigurasiBiayaClicked;
            //_MainFormView.PendaftaranSiswaBaruClicked +=_MainFormView_PendaftaranSiswaBaruClicked;
            // _MainFormView.ShowGrupBiayaClicked += ShowGrupBiayaClicked;
            //_MainFormView.ReportRPPClicked += ReportRPPClicked;
            //_MainFormView.SiswaExplorerClicked += _MainFormView_SiswaExplorerClicked;
            _MainFormView.UbahKataSandiClicked += _MainFormView_UbahKataSandiClicked;
            _MainFormView.PengaturanPenggunaClicked += _MainFormView_PengaturanPenggunaClicked;
        }

        public void _MainFormView_PengaturanPenggunaClicked()
        {
            _MainFormView.ShowPengaturanPenggunaView();
        }

        public void _MainFormView_UbahKataSandiClicked()
        {
            _MainFormView.ShowUbahKataSandiView();
        }



        public void OnMainFormLoad(object sender, EventArgs e)
        {
            _MainFormView.InitializeMenuListView();
        }

        public void OnManageSiswaMenuClicked()
        {
            _MainFormView.ShowManageSiswaView();
        }

        //public void PendaftaranSiswaBaruClicked()
        //{
        //    _MainFormView.ShowDaftarSiswaBaruView();
        //}
    }
}
