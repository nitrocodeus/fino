﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core.Util
{
    public static class NumberTextExtensionMethod
    {
        public static string ToText(this int num)
        {
            var numberText = new NumberText();
            return numberText.ToText(num);
        }
    }

    public class NumberText
    {

        private Dictionary<int, string> textStrings = new Dictionary<int, string>();
        private Dictionary<int, string> scales = new Dictionary<int, string>();
        private StringBuilder builder;

        public NumberText()
        {
            Initialize();
        }

        public string ToText(int num)
        {
            builder = new StringBuilder();

            if (num == 0)
            {
                builder.Append(textStrings[num]);
                return builder.ToString();
            }

            num = scales.Aggregate(num, (current, scale) => Append(current, scale.Key));
            AppendLessThanOneThousand(num);

            return builder.ToString().Trim();
        }

        private int Append(int num, int scale)
        {
            if (num > scale - 1)
            {
                var baseScale = ((int)(num / scale));
                AppendLessThanOneThousand(baseScale);
                builder.AppendFormat("{0} ", scales[scale]);
                num = num - (baseScale * scale);
            }
            return num;
        }

        private int AppendLessThanOneThousand(int num)
        {
            num = AppendHundreds(num);
            num = AppendTens(num);
            AppendUnits(num);
            return num;
        }

        private void AppendUnits(int num)
        {
            if (num > 0)
            {
                builder.AppendFormat("{0} ", textStrings[num]);
            }
        }

        private int AppendTens(int num)
        {
            if (num > 19)
            {
                var tens = ((int)(num / 10)) * 10;
                builder.AppendFormat("{0} ", textStrings[tens]);
                num = num - tens;
            }
            return num;
        }

        private int AppendHundreds(int num)
        {
            if (num == 100)
            {
                var hundreds = ((int)(num / 100));
                builder.AppendFormat("{0} ", textStrings[hundreds]);
                num = num - (hundreds * 100);
            }
            else if (num > 100)
            {
                var hundreds = ((int)(num / 100));
                builder.AppendFormat("{0} ratus ", textStrings[hundreds]);
                num = num - (hundreds * 100);
            }
            return num;
        }

        private void Initialize()
        {
            textStrings.Add(0, "nol");
            textStrings.Add(1, "satu");
            textStrings.Add(2, "dua");
            textStrings.Add(3, "tiga");
            textStrings.Add(4, "empat");
            textStrings.Add(5, "lima");
            textStrings.Add(6, "enam");
            textStrings.Add(7, "tujuh");
            textStrings.Add(8, "delapan");
            textStrings.Add(9, "sembilan");
            textStrings.Add(10, "sepuluh");
            textStrings.Add(11, "sebelas");
            textStrings.Add(12, "dua belas");
            textStrings.Add(13, "tiga belas");
            textStrings.Add(14, "empat belas");
            textStrings.Add(15, "lima belas");
            textStrings.Add(16, "enam belas");
            textStrings.Add(17, "tujuh belas");
            textStrings.Add(18, "delapan belas");
            textStrings.Add(19, "sembilan belas");
            textStrings.Add(20, "dua puluh");
            textStrings.Add(30, "tiga puluh");
            textStrings.Add(40, "empat puluh");
            textStrings.Add(50, "lima puluh");
            textStrings.Add(60, "enam puluh");
            textStrings.Add(70, "tujuh puluh");
            textStrings.Add(80, "delapan puluh");
            textStrings.Add(90, "sembilan puluh");
            textStrings.Add(100, "seratus");

            scales.Add(1000000000, "milyar");
            scales.Add(1000000, "juta");
            scales.Add(1000, "ribu");
        }
    }
}
