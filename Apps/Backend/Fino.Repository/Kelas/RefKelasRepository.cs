﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class RefKelasRepository : BaseRepository, IRefKelasRepository
    {
        public List<RefKelas> SelectAllKelas()
        {
            List<RefKelas> result = null;
            result = Context.RefKelases.ToList();

            return result;
        }

        public RefKelas GetRefKelas(int kelasId)
        {
            return this.Context.RefKelases.Where(x => x.kelas_id == kelasId).FirstOrDefault();
        }

        public void UpdateRefKelas(RefKelas entity)
        {
            var existingRefKelas = GetRefKelas(entity.kelas_id);

            if (existingRefKelas == null)
            {
                this.Context.RefKelases.Add(new RefKelas
                {
                    kelas_code = entity.kelas_code,
                    nama = entity.nama,
                    tingkat = entity.tingkat
                });
            }
            else
            {
                existingRefKelas.kelas_code = entity.kelas_code;
                existingRefKelas.nama = entity.nama;
                existingRefKelas.tingkat = entity.tingkat;
            }

            this.Context.SaveChanges();
        }

        public void DeleteRefKelas(RefKelas entity)
        {
            var existingRefKelas = GetRefKelas(entity.kelas_id);

            if (existingRefKelas != null)
            {
                this.Context.RefKelases.Remove(existingRefKelas);
                this.Context.SaveChanges();
            }
        }
    }
}
