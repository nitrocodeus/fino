﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class SiswaKelas
    {
        public int siswa_kelas_id {get;set;}
        public int siswa_id {get;set;}
        public int tahun_ajaran_id  {get;set;}
        public bool is_siswa_baru {get;set;}
        public DateTime mulai_tanggal {get;set;}
        public DateTime tgl_daftar {get;set;}
        public int kelas_id { get; set; }
        public int option_tingkat { get; set; }

        public virtual RefKelas Kelas { get; set; }
        public virtual RefSiswa Siswa { get; set; }
        public virtual RefTahunAjaran TahunAjaran { get; set; }

    }
}
