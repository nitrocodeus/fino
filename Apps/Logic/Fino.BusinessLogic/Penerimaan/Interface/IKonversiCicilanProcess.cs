﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public interface IKonversiCicilanProcess
    {
        IPosBiayaRepository PosBiayaRepo { get; set; }
        IKonversiCicilanRepository KonversiCicilanRepo { get; set; }
        ProcessResult GetJenisBiayaWithExistingPosBiaya(int p_SiswaId);
        ProcessResult CreateInstallment(List<KonversiCicilanHeaderModel> p_Model);
        ProcessResult GenerateInstallamentSimulation(KonversiCicilanHeaderModel p_Model);
        ProcessResult GetSiswaByNoInduk(string pNoInduk);
    }
}
