﻿using Fino.Migration.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Migration.Interface
{
    public interface IMigrationEngine
    {
        object MigrationTypeDataSource { get; }

        void InitializeEngine(string migrationConfigFileName);
        void ExecuteMigration(MigrationModel migrationModel);
    }
}
