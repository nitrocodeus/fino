﻿using Fino.Datalib.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class NotaPenerimaanUmumConfiguration
    {
        internal static void ConfiguraNotaPenerimaanUmum(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NotaPenerimaanUmum>().HasKey(e => e.nota_penerimaan_Id);
            modelBuilder.Entity<NotaPenerimaanUmum>().Property(e => e.nota_penerimaan_Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<NotaPenerimaanUmum>()
                    .HasRequired(e => e.SumberPenerimaanUmum)
                    .WithMany(e => e.NotaPenerimaanUmums)
                    .HasForeignKey(e => e.SumberPenerimaanId);

            modelBuilder.Entity<NotaPenerimaanUmum>()
                    .HasRequired(e => e.User)
                    .WithMany(e => e.NotaPenerimaanUmums)
                    .HasForeignKey(e => e.user_id);
        }
    }
}
