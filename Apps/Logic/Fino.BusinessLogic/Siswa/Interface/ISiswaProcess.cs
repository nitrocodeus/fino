﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface ISiswaProcess
    {
        ProcessResult GetSiswa(string p_stringSiswa);
    }
}
