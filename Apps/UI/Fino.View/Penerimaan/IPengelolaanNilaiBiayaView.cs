﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IPengelolaanNilaiBiayaView : IView
    {
        event Action SaveButtonClicked;
        event Action NewDataButtonClicked;
        event Action DeleteButtonClicked;
        event Action CloseButtonClicked;
        event Action NilaiBiayaSelectionChanged;
        event Action TingkatSelectionChanged;

        int BiayaId { get; }
        int Nilai { get; }
        int Option { get; }
        int SelectedBiayaId { get; }
        int SelectedOption { get; }
        object BiayaDataSource { get; set; }
        object TingkatDataSource { get; set; }

        PengelolaanNilaiBiayaModel CurrentDetail { get; set; }

        void SetBinding();
        void ClearView();
        void ShowErrorMessage(string message, string caption);
        void ShowInformation(string message, string caption);
        void ShowNilaiBiayaDetail(bool p_Show);
    }
}
